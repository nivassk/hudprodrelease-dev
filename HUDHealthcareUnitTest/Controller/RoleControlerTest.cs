﻿using BusinessService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using HUDHealthcarePortal.Controllers;
using HUDHealthcarePortal.Core;
using Core;

namespace HUDHealthcareUnitTest.Controller
{
    [TestClass]
    public class RoleControlerTest
    {
        [TestMethod]
        public void Should_Get_All_Lenders()
        {
            var repo = new MockRepository(MockBehavior.Default);
            var accountMgr = repo.Create<IAccountManager>();
                
            accountMgr.Setup(p => p.GetInstitutionsByRoles(new string[] { "Lender" }))
                .Returns(new List<KeyValuePair<int, string>>()
                    {
                        new KeyValuePair<int,string>(1, "Bank of America"),
                        new KeyValuePair<int,string>(2, "Sun Trust Bank"),
                        new KeyValuePair<int,string>(3, "Wells Fargo")
                    });

            var roleController = new RoleController(accountMgr.Object, new Mock<IUploadDataManager>().Object);
            var result = roleController.GetLenders().Data as List<KeyValuePair<int,string>>;
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual("Sun Trust Bank", result[1].Value);
        }

        [TestMethod]
        public void Should_Get_Internal_Roles()
        {
            var roleController = new RoleController(new Mock<IAccountManager>().Object, new Mock<IUploadDataManager>().Object);
            var internalRoles = roleController.GetRolesByUserType((int)HUDRoleSource.InternalUser)
                .Data as List<EnumType>;
            Assert.AreEqual(5, internalRoles.Count);
            Assert.IsTrue(internalRoles.Contains(new EnumType("1", "HUD Admin")));
            Assert.IsTrue(internalRoles.Contains(new EnumType("2", "HUD Director")));
            Assert.IsTrue(internalRoles.Contains(new EnumType("3", "Account Executive")));
            Assert.IsTrue(internalRoles.Contains(new EnumType("4", "Workflow Manager")));
            Assert.IsTrue(internalRoles.Contains(new EnumType("5", "Attorney")));
        }
    }
}
