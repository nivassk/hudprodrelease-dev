﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using EntityObject.Interfaces;
using HUDHealthcarePortal.Core;

namespace EntityObject.Entities.HCP_intermediate
{
    public partial class HCP_intermediate : DbContext
    {
        private readonly int ANONYMOUSE_USER_ID = -1;
        public override int SaveChanges()
        {
            // date time all use Utc time
            var utcNowAuditDate = DateTime.UtcNow;
            var changeSet = ChangeTracker.Entries<IAuditable>();
            if (changeSet != null)
            foreach (DbEntityEntry<IAuditable> dbEntityEntry in changeSet)
            {

                switch (dbEntityEntry.State)
                {
                    case System.Data.Entity.EntityState.Added:
                    case System.Data.Entity.EntityState.Modified:
                        dbEntityEntry.Entity.ModifiedOn = utcNowAuditDate;
                        dbEntityEntry.Entity.ModifiedBy = UserPrincipal.Current.UserData != null ? UserPrincipal.Current.UserId : ANONYMOUSE_USER_ID;
                    break;
                }
            }

            return base.SaveChanges();
        }
    }
}

namespace EntityObject.Entities.HCP_live
{
    public partial class HCP_live : DbContext
    {
        private readonly int ANONYMOUSE_USER_ID = -1;
        public override int SaveChanges()
        {
            // date time all use Utc time
            var utcNowAuditDate = DateTime.UtcNow;
            var changeSet = ChangeTracker.Entries<IAuditable>();
            if (changeSet != null)
                foreach (DbEntityEntry<IAuditable> dbEntityEntry in changeSet)
                {

                    switch (dbEntityEntry.State)
                    {
                        case EntityState.Added:
                        case EntityState.Modified:
                            dbEntityEntry.Entity.ModifiedOn = utcNowAuditDate;
                            dbEntityEntry.Entity.ModifiedBy = UserPrincipal.Current.UserData != null ? UserPrincipal.Current.UserId : ANONYMOUSE_USER_ID;
                            break;
                    }
                }

            return base.SaveChanges();
        }
    }
}