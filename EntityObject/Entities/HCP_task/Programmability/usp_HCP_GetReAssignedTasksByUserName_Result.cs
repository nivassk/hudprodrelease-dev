﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
   public partial class usp_HCP_GetReAssignedTasksByUserName_Result
    {
        public int TaskId { get; set; }
        public System.Guid TaskInstanceId { get; set; }
        public int SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public System.DateTime StartTime { get; set; }
        public string Notes { get; set; }
        public int TaskStepId { get; set; }
        public Nullable<int> DataKey1 { get; set; }
        public Nullable<int> DataKey2 { get; set; }
        public string DataStore1 { get; set; }
        public string DataStore2 { get; set; }
        public string TaskOpenStatus { get; set; }
        public int TaskReAssignId { get; set; }
        public bool IsReAssigned { get; set;}
        public string ReAssignedTo { get; set;}
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set;}
        public string FromAE { get; set; }
        public string ReAssignedByName { get; set; }
    }
}
