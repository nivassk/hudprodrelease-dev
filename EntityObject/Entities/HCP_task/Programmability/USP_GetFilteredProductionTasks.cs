﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
   public partial class USP_GetFilteredProductionTasks
    {
       public Guid TaskInstanceId { get; set; }
       public int TaskId { get; set; }
       public string TaskName { get; set; }  
       public int SequenceId { get; set; }
       public int PageTypeId { get; set; }
       public DateTime StartTime { get; set; }
       public string AssignedBy { get; set; }
       public string AssignedTo { get; set; }
       public DateTime ModofiedOn { get; set; }
       public string Lender { get; set; }
       public string Status { get; set; }
       public string Type { get; set; }
       public int Duration { get; set; }
       public string LoanType { get; set; }
       public decimal LoanAmountInt { get; set; }
    }
}
