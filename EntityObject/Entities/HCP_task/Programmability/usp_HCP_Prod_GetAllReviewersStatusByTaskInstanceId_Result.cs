﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
    public class usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId_Result
    {
       public string  userName { get; set; }
       public string userRole { get; set; }
       public string AssignedBy { get; set; }  
       public DateTime CreatedOn { get; set; }
       public string status { get; set; }
       public DateTime? CompletedOn { get; set; }
       public string comments { get; set; }
       public bool IsRAI { get; set; }
     
    }
}
