﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
   public partial class USP_HCP_Prod_GetApplicationAndClosingType
    {
        public Guid TaskInstanceId { get; set; }
        public int PageTypeId { get; set; }
        public string PageTypeDescription { get; set; }
        public string FhaNumber { get; set; }
        public int TaskStepId { get; set; }
    }
}
