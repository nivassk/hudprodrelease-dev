﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
    public class usp_HCP_Prod_GetOpaHistory_FolderList_Result
    {
        public int FolderKey { get; set; }
        public string FolderName { get; set; }
        public string SubfolderSequence { get; set; }
        public int? FolderSortingNumber { get; set; }
    }
}
