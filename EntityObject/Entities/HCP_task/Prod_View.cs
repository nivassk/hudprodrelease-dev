﻿
namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


   [Table("Prod_View")]
   public partial class Prod_View
    {
 
       [DatabaseGenerated(DatabaseGeneratedOption.None)]
       [Key]
       public int ViewId { get; set; }
       public string ViewName { get; set; }
       public int ViewTypeId { get; set; }
       public DateTime? Modifiedon { get; set; }
    }
}
