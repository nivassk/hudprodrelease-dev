namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_EmailType
    {
        [Key]
        public byte EmailTypeId { get; set; }

        [Required]
        [StringLength(10)]
        public string EmailTypeCd { get; set; }

        [Required]
        [StringLength(50)]
        public string EmailTypeDescription { get; set; }

        public bool? Deleted_ind { get; set; }
    }
}
