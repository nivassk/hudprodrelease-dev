﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_AdditionalFinancingResources")]
    public class Prod_AdditionalFinancingResources
    {
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
    }
}
