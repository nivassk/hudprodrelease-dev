namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Address")]
    public partial class Address
    {
        public Address()
        {
            HCP_Authentication = new HashSet<HCP_Authentication>();
            HUD_Project_Manager = new HashSet<HUD_Project_Manager>();
            HUD_WorkLoad_Manager = new HashSet<HUD_WorkLoad_Manager>();
        }

        public int AddressID { get; set; }

        [StringLength(80)]
        public string AddressLine1 { get; set; }

        [StringLength(80)]
        public string AddressLine2 { get; set; }

        [StringLength(56)]
        public string City { get; set; }

        [StringLength(2)]
        public string StateCode { get; set; }

        [StringLength(20)]
        public string ZIP { get; set; }

        [StringLength(10)]
        public string ZIP4_Code { get; set; }

        [StringLength(25)]
        public string PhonePrimary { get; set; }

        [StringLength(25)]
        public string PhoneAlternate { get; set; }

        [StringLength(25)]
        public string Fax { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(128)]
        public string Title { get; set; }

        [StringLength(128)]
        public string Organization { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual State State { get; set; }

        public virtual ICollection<HCP_Authentication> HCP_Authentication { get; set; }

        public virtual ICollection<HUD_Project_Manager> HUD_Project_Manager { get; set; }

        public virtual ICollection<HUD_WorkLoad_Manager> HUD_WorkLoad_Manager { get; set; }
    }
}
