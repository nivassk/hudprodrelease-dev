﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live.Programmability
{
   public class usp_HCP_Prod_GetSubFolder
    {
        #region Production Application
        public int? FolderKey { get; set; }
        public string FolderName { get; set; }
        public int? ParentKey { get; set; }
        public int? ViewTypeId { get; set; }
        public int? FolderSortingNumber { get; set; }
        public string SubfolderSequence { get; set; }
        public string FhaNo { get; set; }
        public string ProjectNo { get; set; }
        #endregion
    }
}
