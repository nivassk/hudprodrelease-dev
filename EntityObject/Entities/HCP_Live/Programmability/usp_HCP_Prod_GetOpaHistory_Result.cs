﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live.Programmability
{
    public class usp_HCP_Prod_GetOpaHistory_Result : usp_HCP_GetOpaHistory_Result
    {
        #region Production Application
        public string id { get; set; }
        public string folderNodeName { get; set; }
        public int level { get; set; }
        public string parent { get; set; }
        public bool isLeaf { get; set; }
        public bool expanded { get; set; }
        public bool loaded { get; set; }
        public int FolderKey { get; set; }
        public int Status { get; set; }
        public string SubfolderSequence { get; set; }
        public int CreatedBy { get; set; }
        public int PageTypeId { get; set; }
        #endregion
    }
}
