﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{

    [Table("Prod_LoanCommittee")]
    public class Prod_LoanCommittee
    {
        [Key]
        public Guid LoanCommitteeId { get; set; }
        public int SequenceId { get; set; }
        public string FHANumber { get; set; }
        public Guid ApplicationTaskinstanceId { get; set; }
        public int LenderId { get; set; }
        public int? ProjectScheduledForLC { get; set; }
        public DateTime? LoanCommitteDate { get; set; }
        public DateTime? DecisionDate { get; set; }
        public int? LCRecommendation { get; set; }
        public int? A7Presentation { get; set; }
        public int? TwoStageSubmittal { get; set; }
        public string LCComments { get; set; }
        public string ProjectName { get; set; }
        public int? ProjectType { get; set; }
        public string NonProfit { get; set; }
        public decimal? LoanAmount { get; set; }
        public string Underwriter { get; set; }
        public string Wlm { get; set; }
        public string Ogc { get; set; }
        public string Appraiser { get; set; }
        public string Lender { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool Deleted_Ind { get; set; }
        public int? LCDecision { get; set; }
        public string ChangeToLoanCommitteeDate { get; set; }

    }
}
