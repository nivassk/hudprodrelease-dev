﻿

namespace EntityObject.Entities.HCP_live
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PageType_DisclaimerText")]
    public partial class PageType_DisclaimerText
    {
        [Key]
        public int DisclaimerTextId { get; set; }

        [Key]
        public int PageTypeId { get; set; }

    }
}
