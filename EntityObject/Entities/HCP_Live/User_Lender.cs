namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User_Lender
    {
        [Key]
        public int User_Lender_ID { get; set; }

        public int User_ID { get; set; }

        public int? Lender_ID { get; set; }

        public int? ServicerID { get; set; }

        public DateTime? DateInserted { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        [StringLength(15)]
        public string FHANumber { get; set; }

        public virtual HCP_Authentication HCP_Authentication { get; set; }

        public virtual LenderInfo LenderInfo { get; set; }

        public virtual ServicerInfo ServicerInfo { get; set; }
        public bool? Deleted_Ind { get; set; }
    }
}
