﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class NonCritical_ProjectInfo
    {
        public int PropertyID { get; set; }

        [Required]
        [StringLength(15)]
        public string FHANumber { get; set; }
        public int? LenderID { get; set; }
        public DateTime? ClosingDate { get; set; }
        public DateTime? EndingDate { get; set; }
        public decimal? InitialNCREBalance { get; set; }
        public decimal?  CurrentBalance { get; set; }
        public bool? IsAmountConfirmed { get; set; }
        public string AmountConfirmationStatus { get; set; }
        public DateTime? ExtensionApprovalDate { get; set; }
        public decimal? SuggestedAmount { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}