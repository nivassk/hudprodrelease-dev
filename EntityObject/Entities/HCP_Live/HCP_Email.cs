namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_Email
    {
        [Key]
        public int EmailId { get; set; }

        [Required]
        public string EmailTo { get; set; }

        [Required]
        [StringLength(128)]
        public string EmailFrom { get; set; }

        public string EmailCC { get; set; }

        public string EmailBCC { get; set; }

        [Required]
        [StringLength(256)]
        public string Subject { get; set; }

        public string ContentText { get; set; }

        public string ContentHtml { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public bool? IsSent { get; set; }

        public byte MailTypeId { get; set; }
    }
}
