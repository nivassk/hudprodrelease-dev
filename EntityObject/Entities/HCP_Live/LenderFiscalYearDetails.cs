﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LenderFiscalYearDetails
    {
        [Key]
        public int Id { get; set; }
        public string FHANumber { get; set; }
        public int LenderId { get; set; }
        public int FiscalYearEndMonth { get; set; }
        public int Q1 { get; set; }
        public int Q2 { get; set; }
        public int Q3 { get; set; }
        public int Q4 { get; set; }
    }
}
