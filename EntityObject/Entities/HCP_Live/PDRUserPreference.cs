﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityObject.Entities.HCP_live
{
    [Table("PDRUserPreference")]
  public partial  class PDRUserPreference
    {
      
        [Key]
        public int PDRUserPreferenceID { get; set; }

        public int UserID { get; set; }
      
        public int PDRColumnID { get; set; }

        public bool Visibility { get; set; }

        public virtual HCP_Authentication User { get; set; }

        public virtual PDRColumn PdrColumn { get; set; }
    }
}
