namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Lender_DataUpload_Live
    {
        [Key]
        public int LDP_ID { get; set; }

        [StringLength(100)]
        public string ProjectName { get; set; }

        [StringLength(100)]
        public string ServiceName { get; set; }

        [Required]
        [StringLength(15)]
        public string FHANumber { get; set; }

        public int LenderID { get; set; }

        public int? Servicer_ID { get; set; }

        [StringLength(25)]
        public string OperatorOwner { get; set; }

        public DateTime PeriodEnding { get; set; }

        public int? MonthsInPeriod { get; set; }

        [StringLength(25)]
        public string FinancialStatementType { get; set; }

        public int? UnitsInFacility { get; set; }

        public int PropertyID { get; set; }

        public decimal? OperatingCash { get; set; }

        public decimal? Investments { get; set; }

        public decimal? ReserveForReplacementEscrowBalance { get; set; }

        public decimal? AccountsReceivable { get; set; }

        public decimal? CurrentAssets { get; set; }

        public decimal? CurrentLiabilities { get; set; }

        public decimal? TotalRevenues { get; set; }

        public decimal? RentLeaseExpense { get; set; }

        public decimal? DepreciationExpense { get; set; }

        public decimal? AmortizationExpense { get; set; }

        public decimal? TotalExpenses { get; set; }

        public decimal? NetIncome { get; set; }

        public decimal? ReserveForReplacementDeposit { get; set; }

        public decimal? FHAInsuredPrincipalPayment { get; set; }

        public decimal? FHAInsuredInterestPayment { get; set; }

        public decimal? MortgageInsurancePremium { get; set; }

        public decimal? FHAInsuredPrincipalInterestPayment  { get; set; }

        public decimal? ActualNumberOfResidentDays { get; set; }

        public decimal? DebtCoverageRatio2 { get; set; }

        public decimal? AverageDailyRateRatio { get; set; }

        public decimal? NOIRatio { get; set; }

        public DateTime DateInserted { get; set; }

        public int? HUD_Project_Manager_ID { get; set; }

        public int LDI_ID { get; set; }

        public decimal? ReserveForReplacementBalancePerUnit { get; set; }

        public decimal? WorkingCapital { get; set; }

        public decimal? DebtCoverageRatio { get; set; }

        public decimal? DaysCashOnHand { get; set; }

        public decimal? DaysInAcctReceivable { get; set; }

        public decimal? AvgPaymentPeriod { get; set; }

        public decimal? WorkingCapitalScore { get; set; }

        public decimal? DebtCoverageRatioScore { get; set; }

        public decimal? DaysCashOnHandScore { get; set; }

        public decimal? DaysInAcctReceivableScore { get; set; }

        public decimal? AvgPaymentPeriodScore { get; set; }

        public int? FHAQuarter { get; set; }

        public decimal? ScoreTotal { get; set; }

        public int? ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public int UserID { get; set; }

        public bool? Deleted_Ind { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual FHAInfo_iREMS FHAInfo_iREMS { get; set; }

        public virtual HCP_Authentication HCP_Authentication { get; set; }

        public virtual LenderInfo LenderInfo { get; set; }

        public virtual LenderInfo LenderInfo1 { get; set; }

        public virtual ServicerInfo ServicerInfo { get; set; }
    }
}
