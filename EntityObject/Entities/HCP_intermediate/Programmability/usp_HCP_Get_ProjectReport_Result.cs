﻿using System;


namespace EntityObject.Entities.HCP_intermediate
{
    public class usp_HCP_Get_ProjectInfoReport_Result
    {
        public string ProjectName { get; set; }
        public string ServiceName { get; set; }
        public string FHANumber { get; set; }
    }
}
