using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EntityObject.Models.Mapping
{
    public class Lender_DataUpload_IntermediateMap : EntityTypeConfiguration<Lender_DataUpload_Intermediate>
    {
        public Lender_DataUpload_IntermediateMap()
        {
            // Primary Key
            this.HasKey(t => t.LDI_ID);

            // Properties
            this.Property(t => t.ProjectName)
                .HasMaxLength(100);

            this.Property(t => t.ServiceName)
                .HasMaxLength(100);

            this.Property(t => t.FHANumber)
                .HasMaxLength(15);

            this.Property(t => t.OperatorOwner)
                .HasMaxLength(25);

            this.Property(t => t.PeriodEnding)
                .HasMaxLength(25);

            this.Property(t => t.MonthsInPeriod)
                .HasMaxLength(5);

            this.Property(t => t.FinancialStatementType)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Lender_DataUpload_Intermediate");
            this.Property(t => t.ProjectName).HasColumnName("ProjectName");
            this.Property(t => t.ServiceName).HasColumnName("ServiceName");
            this.Property(t => t.FHANumber).HasColumnName("FHANumber");
            this.Property(t => t.LenderID).HasColumnName("LenderID");
            this.Property(t => t.OperatorOwner).HasColumnName("OperatorOwner");
            this.Property(t => t.PeriodEnding).HasColumnName("PeriodEnding");
            this.Property(t => t.MonthsInPeriod).HasColumnName("MonthsInPeriod");
            this.Property(t => t.FinancialStatementType).HasColumnName("FinancialStatementType");
            this.Property(t => t.UnitsInFacility).HasColumnName("UnitsInFacility");
            this.Property(t => t.OperatingCash).HasColumnName("OperatingCash");
            this.Property(t => t.Investments).HasColumnName("Investments");
            this.Property(t => t.ReserveForReplacementEscrowBalance).HasColumnName("ReserveForReplacementEscrowBalance");
            this.Property(t => t.AccountsReceivable).HasColumnName("AccountsReceivable");
            this.Property(t => t.CurrentAssets).HasColumnName("CurrentAssets");
            this.Property(t => t.CurrentLiabilities).HasColumnName("CurrentLiabilities");
            this.Property(t => t.TotalRevenues).HasColumnName("TotalRevenues");
            this.Property(t => t.RentLeaseExpense).HasColumnName("RentLeaseExpense");
            this.Property(t => t.DepreciationExpense).HasColumnName("DepreciationExpense");
            this.Property(t => t.AmortizationExpense).HasColumnName("AmortizationExpense");
            this.Property(t => t.TotalExpenses).HasColumnName("TotalExpenses");
            this.Property(t => t.NetIncome).HasColumnName("NetIncome");
            this.Property(t => t.ReserveForReplacementDeposit).HasColumnName("ReserveForReplacementDeposit");
            this.Property(t => t.FHAInsuredPrincipalPayment).HasColumnName("FHAInsuredPrincipalPayment");
            this.Property(t => t.FHAInsuredInterestPayment).HasColumnName("FHAInsuredInterestPayment");
            this.Property(t => t.MortgageInsurancePremium).HasColumnName("MortgageInsurancePremium");
            this.Property(t => t.PropertyID).HasColumnName("PropertyID");
            this.Property(t => t.LDI_ID).HasColumnName("LDI_ID");
            this.Property(t => t.ReserveForReplacementBalancePerUnit).HasColumnName("ReserveForReplacementBalancePerUnit");
            this.Property(t => t.WorkingCapital).HasColumnName("WorkingCapital");
            this.Property(t => t.DebtCoverageRatio).HasColumnName("DebtCoverageRatio");
            this.Property(t => t.DaysCashOnHand).HasColumnName("DaysCashOnHand");
            this.Property(t => t.DaysInAcctReceivable).HasColumnName("DaysInAcctReceivable");
            this.Property(t => t.AvgPaymentPeriod).HasColumnName("AvgPaymentPeriod");
            this.Property(t => t.WorkingCapitalScore).HasColumnName("WorkingCapitalScore");
            this.Property(t => t.DebtCoverageRatioScore).HasColumnName("DebtCoverageRatioScore");
            this.Property(t => t.DaysCashOnHandScore).HasColumnName("DaysCashOnHandScore");
            this.Property(t => t.DaysInAcctReceivableScore).HasColumnName("DaysInAcctReceivableScore");
            this.Property(t => t.AvgPaymentPeriodScore).HasColumnName("AvgPaymentPeriodScore");
            this.Property(t => t.HasCalculated).HasColumnName("HasCalculated");
            this.Property(t => t.ScoreTotal).HasColumnName("ScoreTotal");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.OnBehalfOfBy).HasColumnName("OnBehalfOfBy");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.DataInserted).HasColumnName("DataInserted");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.IsUploadNotComplete).HasColumnName("IsUploadNotComplete");
            this.Property(t => t.IsLate).HasColumnName("IsLate");
        }
    }
}
