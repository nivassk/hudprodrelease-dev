﻿CREATE FUNCTION [dbo].[fn_HCP_GetLenderRole]
(
	@InstanceID uniqueidentifier
)
RETURNS nVARCHAR(100)
AS
BEGIN
	DECLARE @Role nVARCHAR(50)
Return (Select top 1  r.rolename from   [$(DatabaseName)].dbo.Task task 
				   inner join (SELECT MIN(SequenceId) AS maxSeqId, TaskInstanceId
								FROM [$(DatabaseName)].[dbo].[Task] where task.TaskInstanceId=@InstanceID
								GROUP BY TaskInstanceId
							) TMAX ON task.TaskInstanceId = TMAX.TaskInstanceId AND task.SequenceId = TMAX.maxSeqId
							
							inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = task.AssignedBy
	left join [$(LiveDB)].dbo.webpages_UsersInRoles l on l.UserId = a.userid 
	left join [$(LiveDB)].dbo.webpages_Roles r on r.roleid = l.roleid)

END

