﻿
CREATE TABLE [dbo].[TaskStep](
	[TaskStepId] [int] IDENTITY(1,1) NOT NULL,
	[TaskType] [nvarchar](60) NULL,
	[TaskStepNm] [nvarchar](60) NULL,
	[TaskDesc] [nvarchar](50) NOT NULL,
	[TaskDuration] [time](7) NULL,
 CONSTRAINT [PK_TaskStep] PRIMARY KEY CLUSTERED 
(
	[TaskStepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

