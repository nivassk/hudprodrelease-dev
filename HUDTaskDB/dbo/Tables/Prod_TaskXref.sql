﻿
CREATE TABLE [dbo].[Prod_TaskXref](
	[TaskXrefid] [uniqueidentifier] NOT NULL,
	[TaskId] [int] NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[AssignedBy] [int] NULL,
	[AssignedTo] [int] NULL,
	[Comments] [varchar](max) NULL,
	[IsReviewer] [bit] NOT NULL CONSTRAINT [DF_Prod_TaskXref_IsReviewer]  DEFAULT ((0)),
	[ViewId] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Status] [int] NULL,
	[CompletedOn] [datetime] NULL,
	[IsReassigned] [bit] NULL,
 [AssignedDate] DATETIME NULL, 
    CONSTRAINT [PK_Prod_TaskXref] PRIMARY KEY CLUSTERED 
(
	[TaskXrefid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_TaskXref]  WITH CHECK ADD  CONSTRAINT [FK_Prod_TaskXref_Prod_View] FOREIGN KEY([ViewId])
REFERENCES [dbo].[Prod_View] ([ViewId])
GO

ALTER TABLE [dbo].[Prod_TaskXref] CHECK CONSTRAINT [FK_Prod_TaskXref_Prod_View]
GO

