﻿
CREATE TABLE [dbo].[Prod_FolderStructure](
	[FolderKey] [int] NOT NULL,
	[FolderName] [nvarchar](max) NOT NULL,
	[ParentKey] [int] NULL,
	[level] [int] NULL,
	[ViewTypeId] [int] NULL,
	[FolderSortingNumber] [int] NULL,
	[SubfolderSequence] [nchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]