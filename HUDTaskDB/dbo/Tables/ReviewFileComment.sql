﻿
CREATE TABLE [dbo].[ReviewFileComment](
	[ReviewFileCommentId] [uniqueidentifier] NOT NULL,
	[FileTaskId] [uniqueidentifier] NOT NULL,
	[Comment] [nchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ReviewerProdViewId] [int] NULL,
 CONSTRAINT [PK_ReviewFileComment] PRIMARY KEY CLUSTERED 
(
	[ReviewFileCommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

