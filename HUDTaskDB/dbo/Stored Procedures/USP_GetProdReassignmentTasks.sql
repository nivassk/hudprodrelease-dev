﻿
CREATE  PROCEDURE [dbo].[USP_GetProdReassignmentTasks](
@AppType nvarchar(50),
@ProgramType nvarchar(50),
@Userlist nvarchar(50),
@startTime datetime,
@endTime datetime


)
AS

    BEGIN
       declare @userId int
       if (@startTime is not null and @endTime is null)   set @endTime =   getutcdate()
		
		
	 if (@startTime is null and @endTime is not null)   set @startTime = dateadd(year, -1, getutcdate())
 
	   SELECT a.* FROM (
       --FHA# REQUEST Insert
              SELECT DATEDIFF( day ,fhaRequest.Requestsubmitdate,GETUTCDATE()) as DaysActive, 4 as pagetypeid, fhaRequest.Requestsubmitdate as StartDate,task.starttime as EndDate,task.TaskStepId as Processedstat,'InsertFha' as viewname,fhaRequest.fhanumber as fhanumber,null as Form,fhaRequest.Propertyid,fhaRequest.ProjectTypeId,PA.ProjectTypeName as LoanType, null as Reassignedto,null as Reassignedby,null as ReassignedDate,fhaRequest.LoanAmount , fhaRequest.ProjectName+'('+ pageType.PageTypeDescription+')' as TaskName,fhaRequest.ProjectName,task.TaskInstanceId as ParentChildInstanceId,
                      lender.Lender_Name as LenderName,fhaRequest.FHANumberRequestId as FhaRequestInstanceId,task.PageTypeId as FhaRequestType,
                      users.FirstName+' '+users.LastName as assignedTo,[dbo].[fn_HCP_GetUserIDbyUserName]( CASE WHEN task.sequenceid =  0 THEN task.assignedto
   WHEN task.sequenceid =  1 THEN task.assignedby

  END) AS assigneduserid,[dbo].[fn_HCP_GetUserIDbyUserName]( CASE WHEN task.sequenceid =  0 THEN task.assignedto
   WHEN task.sequenceid =  1 THEN task.assignedby

  END)  as ProcessedByID, [dbo].[fn_HCP_GetUserNamebyUserID] ([dbo].[fn_HCP_GetUserIDbyUserName]( CASE WHEN task.sequenceid =  0 THEN task.assignedto
   WHEN task.sequenceid =  1 THEN task.assignedby

  END))  as ProcessedBy,
                           task.TaskInstanceId as groupid,[dbo].[fn_HCP_GetLenderRole](task.TaskInstanceId) AS Lenderrole,
                      pageType.PageTypeDescription as productiontype,
                      fhaRequest.ModifiedOn as LastUpdated,
                      fhaRequest.Comments as Comments, DATEDIFF(day,
                           fhaRequest.ModifiedOn,GETDATE()) AS Duration ,
                           taskStep.TaskStepNm as Status,taskStep.TaskStepId as StatusId,'FHA# REQUEST' as ProductionTaskType,
                           fhaRequest.CreatedBy,null as RAI
              FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
                   join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
                   join [$(DatabaseName)].dbo.Task task on fhaRequest.TaskinstanceId=task.TaskInstanceId
				   inner join (SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
								FROM [$(DatabaseName)].[dbo].[Task]
								GROUP BY TaskInstanceId
							) TMAX ON task.TaskInstanceId = TMAX.TaskInstanceId AND task.SequenceId = TMAX.maxSeqId
                   join [$(LiveDB)].dbo.HCP_Authentication users on users.UserName=task.AssignedTo
                   join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType.PageTypeId=task.PageTypeId
                     join [$(DatabaseName)].dbo.TaskStep taskStep on task.TaskStepId = taskStep.TaskStepId
					  left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA on 
					  PA.ProjectTypeId=fhaRequest.ProjectTypeId
          WHERE (task.TaskStepId=17 or  task.TaskStepId=18 or task.TaskStepId=19) 
		  --and (task.AssignedTo=@userName or task.AssignedBy =@username)

            UNION

       --FHA# REQUEST Credit Review and Portifolio
          SELECT DATEDIFF( day ,txref.modifiedon,GETUTCDATE()) as DaysActive,4 as pagetypeid,txref.modifiedon as StartDate,txref.CompletedOn as EndDate, txref.status as Processedstat,productiontype.viewname,fhaRequest.fhanumber as fhanumber,null as Form,fhaRequest.Propertyid,fhaRequest.ProjectTypeId,PA.ProjectTypeName as LoanType,null as Reassignedto,null as Reassignedby,null as ReassignedDate,fhaRequest.LoanAmount ,  fhaRequest.ProjectName+' ('+productiontype.ViewName +')'as TaskName,fhaRequest.ProjectName,txref.TaskXrefid as ParentChildInstanceId,
                  lender.Lender_Name as lenderName,fhaRequest.FHANumberRequestId as FhaRequestInstanceId,txref.ViewId as FhaRequestType,
                  users.FirstName+' '+users.LastName as AssignedTo ,[dbo].[fn_HCP_GetUserIDbyUserName]( users.username) AS assigneduserid,txref.assignedto as ProcessedByID, [dbo].[fn_HCP_GetUserNamebyUserID] ([dbo].[fn_HCP_GetUserIDbyUserName]( users.username))  as ProcessedBy,
                        txref.TaskInstanceId as groupid,[dbo].[fn_HCP_GetLenderRole]( txref.TaskInstanceId) AS Lenderrole,
                        productiontype.ViewName as productiontype,
                        fhaRequest.ModifiedOn as LastUpdated,
                  fhaRequest.Comments as Comments,DATEDIFF(day,
                        fhaRequest.ModifiedOn,GETDATE()) AS Duration,taskStep.TaskStepNm as Status,taskStep.TaskStepId as StatusId,'FHA# REQUEST' as ProductionTaskType,
                        fhaRequest.CreatedBy, null as RAI
         FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
              join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
              join [$(DatabaseName)].dbo.Prod_TaskXref txref on fhaRequest.TaskinstanceId=txref.TaskInstanceId
              join [$(LiveDB)].dbo.HCP_Authentication users on users.UserID=txref.AssignedTo
              join [$(DatabaseName)].dbo.Prod_View productiontype on productiontype.ViewId=txref.ViewId 
                 join [$(DatabaseName)].dbo.TaskStep taskStep on txref.status = taskStep.TaskStepId
				   left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA on 
					  PA.ProjectTypeId=fhaRequest.ProjectTypeId
         WHERE (txref.Status=18 or txref.Status=19)
		 -- and txref.AssignedTo=@userId

         Union
   SELECT * FROM (   select DATEDIFF( day ,txref.modifiedon,GETUTCDATE()) as DaysActive,FP.pagetypeid as pagetypeid, txref.modifiedon as StartDate,txref.CompletedOn as EndDate, txref.status as Processedstat ,productiontype.viewname, opa.fhaNumber,null as Form,fhaRequest.Propertyid, PA.ProjectTypeId,PA.ProjectTypeName as LoanType,null as ReassignedDate,fhaRequest.LoanAmount ,null as Reassignedto,null as Reassignedby,'(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName,fhaRequest.ProjectName,txref.TaskXrefid as ParentChildInstanceId,
                  lender.Lender_Name as lenderName,fhaRequest.FHANumberRequestId as FhaRequestInstanceId,txref.ViewId as FhaRequestType,
                 FP.AssignedTo as AssignedTo ,[dbo].[fn_HCP_GetUserIDbyUserName]( FP.AssignedTo) AS assigneduserid,txref.assignedto as ProcessedByID, [dbo].[fn_HCP_GetUserNamebyUserID] (txref.assignedto)  as ProcessedBy,
                        txref.taskinstanceid as groupid,[dbo].[fn_HCP_GetLenderRole]( txref.TaskInstanceId) AS Lenderrole,
                        productiontype.ViewName as productiontype,
                        FP.starttime as LastUpdated,
                  opa.aeComments as Comments,DATEDIFF(day,
                        txref.ModifiedOn,GETDATE()) AS Duration,taskStep.TaskStepNm as Status,fp.TaskStepId as StatusId,pageType.PageTypeDescription as ProductionTaskType,
                       txref.assignedto as  CreatedBy,null as RAI
         FROM [$(LiveDB)].dbo.opaform opa
          join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
on opa.fhaNumber=fhaRequest.fhanumber
             left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
               join [$(DatabaseName)].dbo.Prod_TaskXref txref on opa.TaskinstanceId=txref.TaskInstanceId
			   left join  (select ct.* from [$(DatabaseName)].[dbo].[Task] ct
                     inner join 
                     (select [TaskInstanceId], max([SequenceId]) as SequenceId 
                      from [$(DatabaseName)].[dbo].[Task] mct  group by [TaskInstanceId] )mct on mct.[TaskInstanceId] = ct.[TaskInstanceId] and mct.SequenceId = ct.SequenceId ) fp on
                     FP.TaskinstanceId=txref.TaskInstanceId
					 left join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType.PageTypeId=FP.PageTypeId
                 left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA

on PA. ProjecttypeId=opa.Projectactiontypeid
              left join [$(LiveDB)].dbo.HCP_Authentication users on users.UserID=txref.AssignedTo
              
              left join [$(DatabaseName)].dbo.Prod_View productiontype on productiontype.ViewId=txref.ViewId 
                 left join [$(DatabaseName)].dbo.TaskStep taskStep on txref.status = taskStep.TaskStepId 
				 --and txref.AssignedTo=@userId

union
select DATEDIFF( day ,xref.modifiedon,GETUTCDATE()) as DaysActive, FP.PageTypeId,xref.modifiedon as StartDate,FP.starttime as EndDate, xref.status as Processedstat, productiontype.viewname, opa.fhaNumber,null as Form,fhaRequest.Propertyid, PA.ProjectTypeId,PA.ProjectTypeName as LoanType,null as Reassignedto,null as Reassignedby,null as ReassignedDate,fhaRequest.LoanAmount ,CASE WHEN fp.TaskStepId =  20 THEN 'FC Request ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
   WHEN fp.TaskStepId =  21 THEN 'FC Response ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
  ELSE 'RAI ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
  END
  as TaskName,fhaRequest.ProjectName,pct.childtaskinstanceid as parenttaskinstanceid,lender.Lender_Name as lenderName,opa.projectactionformid as FhaRequestInstanceId,xref.ViewId as FhaRequestType,  fp.assignedto as AssignedTo ,[dbo].[fn_HCP_GetUserIDbyUserName]( fp.assignedto) AS assigneduserid,xref.assignedto as ProcessedByID,[dbo].[fn_HCP_GetUserNamebyUserID] (xref.assignedto)  as ProcessedBy, xref.taskinstanceid as groupid, [dbo].[fn_HCP_GetLenderRole]( xref.TaskInstanceId) AS Lenderrole, productiontype.ViewName as productiontype,fp.starttime as LastUpdated,null as Comments,DATEDIFF(day,
                        fp.starttime,GETDATE()) AS Duration,'RAI' as status,
CASE WHEN fp.TaskStepId =  20 THEN fp.TaskStepId
   WHEN fp.TaskStepId =  21 THEN fp.TaskStepId 
  ELSE 16
  END
  as StatusId,pageType.pagetypedescription as ProductionTaskType, xref.assignedto as  CreatedBy,'YES' as RAI from parentchildtask pct
join Prod_TaskXref xref on pct.parenttaskinstanceid=xref.taskxrefid
LEFT JOIN Task tk on xref.TaskInstanceId=tk.taskinstanceid 

left join  (select ct.* from [$(DatabaseName)].[dbo].[Task] ct
                     inner join 
                     (select [TaskInstanceId], max([SequenceId]) as SequenceId 
                      from [$(DatabaseName)].[dbo].[Task] mct  group by [TaskInstanceId] )mct on mct.[TaskInstanceId] = ct.[TaskInstanceId] and mct.SequenceId = ct.SequenceId ) fp on
                       fp.TaskInstanceId= pct.childtaskinstanceid
					    left join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType.PageTypeId=FP.PageTypeId
       LEFT join  [$(LiveDB)].dbo.opaform opa on opa.fhanumber=fp.fhanumber
       join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
on opa.fhaNumber=fhaRequest.fhanumber
             left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
             
   left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA

on PA. ProjecttypeId=opa.Projectactiontypeid
              left join [$(LiveDB)].dbo.HCP_Authentication users on users.UserID=xref.AssignedTo
              left join [$(DatabaseName)].dbo.Prod_View productiontype on productiontype.ViewId=xref.ViewId 
                 left join [$(DatabaseName)].dbo.TaskStep taskSteps on xref.status = taskSteps.TaskStepId 
				 --WHERE xref.Status=18
                 
                 )as t) a

				 where  
				( a.status='InProcess' or a.Status='InQueue') and a.RAI is Null and 

				  (@AppType is null or a.pagetypeid in( SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@AppType)))
				and
(@ProgramType is null or a.projecttypeid in( SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@ProgramType))) and
(@Userlist is null or a.processedbyid in( SELECT * FROM [$(LiveDB)].dbo.fn_StringSplitterToInt(@Userlist))) 
--and   (@startTime is null and @endTime is null  or StartDate between @startTime and @endTime )
 and 	 CONVERT (VARCHAR ( 10), cast (a.startdate as date), 101 ) between convert( datetime , @startTime , 101 ) and convert( datetime , @endTime , 101 )

				
--and  a.status<>'RAI' and   a.viewname='UnderWriter' or a.viewname='InsertFha' 
--and  a.viewname='UnderWriter' or a.viewname='InsertFha' 
--and a.RAI is null
       
				 -- where t.CreatedBy=@userId 
              -- Select * from Task
			--select * from Prod_TaskXref

			--select * from [$(LiveDB)].dbo.opaform
			--select * from [$(LiveDB)].dbo.Prod_FHANumberRequest

                 
                   order by a.pagetypeid
				

     


END

