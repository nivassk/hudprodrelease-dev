﻿
CREATE  PROCEDURE [dbo].[USP_GetOPAFilesByTaskId](
@TaskInstanceId as VARCHAR(50),
@FileType as VARCHAR(20)
)
AS
BEGIN

declare @PagetypeId int
select @PagetypeId = (select top 1 PageTypeId from [$(DatabaseName)].[dbo].[Task] where TaskInstanceId=@TaskInstanceId)

if(@PagetypeId=3)

begin
       SELECT distinct auth.FirstName + ', ' + auth.LastName as UploadedBy,
       tf.TaskFileId as TaskId, tf.FileId as FileId,
       tf.FileType,
       tf.uploadtime as UploadTime,
       tf.filename as FileName,
       null as FileData,
       tf.FileSize ,
       wpr.RoleName as RoleName
  FROM [$(DatabaseName)].[dbo].[TaskFile] tf
         join [$(LiveDB)].[dbo].[webpages_UsersInRoles] roles on tf.CreatedBy = roles.UserId
         join [$(LiveDB)].[dbo].[webpages_Roles] wpr on roles.RoleId = wpr.RoleId
         join [$(LiveDB)].[dbo].HCP_Authentication auth on tf.CreatedBy = auth.UserID
       where tf.TaskInstanceId= @TaskInstanceId  and tf.FileType = @FileType
      -- Duplicate records are comming for dual role users
	   and wpr.RoleName in ('AccountExecutive')
	   --and wpr.RoleName in ('ProductionWlm','ProductionUser')
      -- and wpr.RoleName = 'AccountExecutive' or wpr.RoleName = 'ProductionUser'   and tf.FileType = @FileType
        end

		else

		begin

		  SELECT distinct auth.FirstName + ', ' + auth.LastName as UploadedBy,
       tf.TaskFileId as TaskId, tf.FileId as FileId,
       tf.FileType,
       tf.uploadtime as UploadTime,
       tf.filename as FileName,
       null as FileData,
       tf.FileSize ,
       wpr.RoleName as RoleName
  FROM [$(DatabaseName)].[dbo].[TaskFile] tf
         join [$(LiveDB)].[dbo].[webpages_UsersInRoles] roles on tf.CreatedBy = roles.UserId
         join [$(LiveDB)].[dbo].[webpages_Roles] wpr on roles.RoleId = wpr.RoleId
         join [$(LiveDB)].[dbo].HCP_Authentication auth on tf.CreatedBy = auth.UserID
       where tf.TaskInstanceId= @TaskInstanceId  and tf.FileType = @FileType
      -- Duplicate records are comming for dual role users
	   --and wpr.RoleName in ('ProductionWlm','ProductionUser')	

	   -- This condition is for lender uploade firm commitment files not able to see with old condition one that is the reson I added lender roles 
	   and (wpr.RoleName in ('ProductionWlm','ProductionUser') or  wpr.RoleName in ('LenderAccountManager','LenderAccountRepresentative','BackupAccountManager'))	
      -- and wpr.RoleName = 'AccountExecutive' or wpr.RoleName = 'ProductionUser'   and tf.FileType = @FileType
		
		end
       
END


