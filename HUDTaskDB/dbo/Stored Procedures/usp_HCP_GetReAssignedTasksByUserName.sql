﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetReAssignedTasksByUserName]
(
@UserName NVARCHAR(150)
)
AS
DECLARE @HudToken VARCHAR(8)
DECLARE @IsHudUser BIT
SET @HudToken = '@hud.gov'

--IF LOWER(RIGHT(@UserName, 8)) = @HudToken
 	SET @IsHudUser = 1
--ELSE
	--SET @IsHudUser = 0

SELECT T.[TaskId]
      ,T.[TaskInstanceId]
      ,T.[SequenceId]
      ,T.[AssignedBy]
      ,T.[AssignedTo]
      ,T.[DueDate]
      ,T.[StartTime]
      ,T.[Notes]
      ,T.[TaskStepId]
      ,T.[DataKey1]
      ,T.[DataKey2]
      ,T.[DataStore1]
      ,T.[DataStore2]
	  ,T.[TaskOpenStatus]
	  ,TAssign.CreatedBy  
	  ,TAssign.CreatedOn  
	  ,TAssign.ReAssignedTo 
	  ,TAssign.TaskReAssignId
	  ,TAssign.FromAE
	  ,auth.FirstName + ' ' + auth.LastName as ReAssignedByName     
FROM 
--select * from 
[dbo].[TaskReAssignments] TAssign
INNER JOIN 
[$(DatabaseName)].[dbo].[Task] T
on TAssign.TaskInstanceId = t.TaskInstanceId
INNER JOIN 
(
SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
FROM [$(DatabaseName)].[dbo].[Task]
GROUP BY TaskInstanceId
) TMAX
ON T.TaskInstanceId = TMAX.TaskInstanceId AND T.SequenceId = TMAX.maxSeqId
INNER JOIN 
(
SELECT 
   CASE 
      WHEN @IsHudUser=1 THEN MAX(TaskId) 
      ELSE MIN(TaskId) 
   END AS MaxMinTaskId, 
   TaskInstanceId
FROM [$(DatabaseName)].[dbo].[Task]
GROUP BY TaskInstanceId, SequenceId
) TSeq
ON T.TaskInstanceId = TSeq.TaskInstanceId AND T.TaskId = TSeq.MaxMinTaskId
INNER JOIN [$(LiveDB)].dbo.HCP_Authentication auth
	ON TAssign.CreatedBy = auth.UserID  
where TAssign.ReAssignedTo  = @UserName and IsReassigned = 1
and (TAssign.Deleted_Ind is null or TAssign.Deleted_Ind = 0)




