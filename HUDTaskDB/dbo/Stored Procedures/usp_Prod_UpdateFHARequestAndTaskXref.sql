﻿
-- =============================================
-- Author:		Rashmi Ganapathy
-- Create date: Feb 10,2017
-- Description:	update Status in Xref and FHARequest table on complete of each child task
-- =============================================
Create PROCEDURE [dbo].[usp_Prod_UpdateFHARequestAndTaskXref] 
	@TaskInstanceId uniqueidentifier,
	@ViewId int,
	@Comments nvarchar(500),
	@FhaNumber nvarchar(50),
	@PortfolioName nvarchar(500),
	@PortfolioNumber int
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	BEGIN TRANSACTION;
	BEGIN TRY
		
		if(@ViewId = 4)
			begin
			update Task set TaskStepId = 19 where TaskInstanceId = @TaskInstanceId 
			update [$(LiveDB)].dbo.Prod_FHANumberRequest set IsFhaInsertComplete = 1,InsertFHAComments = @Comments, FHANumber = @FhaNumber
			where TaskinstanceId = @TaskInstanceId
			end		
		else if(@ViewId = 9)
			begin
			update Prod_TaskXref set Status = 19,CompletedOn = GETDATE() where TaskInstanceId = @TaskInstanceId and ViewId = @ViewId
			update [$(LiveDB)].dbo.Prod_FHANumberRequest set IsPortfolioComplete = 1, PortfolioComments = @Comments,
			Portfolio_Name = @PortfolioName, Portfolio_Number = @PortfolioNumber where TaskinstanceId = @TaskInstanceId
			end		
		else if(@ViewId = 10)
			begin
			update Prod_TaskXref set Status = 19,CompletedOn = GETDATE()  where TaskInstanceId = @TaskInstanceId and ViewId = @ViewId
			update [$(LiveDB)].dbo.Prod_FHANumberRequest set IsCreditReviewAttachComplete = 1, CreditreviewComments = @Comments
			,CreditReviewDate = getdate() 
			where TaskinstanceId = @TaskInstanceId
			end
	COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
	print 'error ocurred'
	ROLLBACK TRANSACTION;
	END CATCH

END

