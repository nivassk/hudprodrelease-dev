﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_SendExecutedClosingReminderEmail]
As
Begin
	
	Declare @RowsToProcess  int
	Declare @CurrentRow     int
	Declare @TaskInstanceId uniqueidentifier
	Declare @FHANumber varchar(9)
	Declare @DateFirstEmailSent datetime
	Declare @ReturnIsWeekend int
	Declare @DateToSendEmail datetime
	Declare @profile_name varchar(max)
	Declare @from_address varchar(max)
	Declare @recipients varchar(max)
	Declare @copy_recipients varchar(max)
    Declare @subject varchar(max)
	Declare @body varchar(max)
	Declare @body_format varchar(10)
	Declare @importance varchar(10)
	Declare @LenderSubmitterEmail varchar(max)
	Declare @AEEmail varchar(max)
	Declare @CloserEmail varchar(max)
	Declare @ReminderEmailsCount int
	Declare @ProjectName varchar(max)
	Declare @AEName varchar(max)
	Declare @IsNextDay1Holiday int
	Declare @IsNextDay2Holiday int
	Declare @NextDay1 datetime
	Declare @NextDay2 datetime
	Declare @DayOfFirstEmailSent varchar(100)
	Declare @DateLastEmailSent datetime
	Declare @DayofLastEmailSent varchar(100)

	Set @profile_name = 'CloudMail' 
    Set @from_address = 'hhcpsupport@c3-systems.com'
    Set @recipients = 'rganapathy@c3-systems.com'
    Set @copy_recipients = 'rganapathy@c3-systems.com' 
	Set @body_format = 'HTML' 
	Set @importance = 'High'
	    
	Declare @FHACompleted290 Table (RowID int not null primary key identity(1,1), FHANumber varchar(15), TaskInstanceId uniqueidentifier, DateFirstEmailSent datetime, DateLastEmailSent datetime)  
	Insert into @FHACompleted290 select tk.FhaNumber as FHANumber, ft.TaskInstanceID as TaskInstanceId, tk.StartTime as DateFirstEmailSent, 
								 ft.DateFollowingReminderSent as DateLastEmailSent 
								 from Prod_Form290Task ft join Task tk on ft.TaskInstanceID = tk.TaskInstanceId where SequenceId = 1

	Set @RowsToProcess=@@ROWCOUNT

	Set @CurrentRow=0
	While @CurrentRow<@RowsToProcess
		Begin
			Set @CurrentRow=@CurrentRow+1

			Select @TaskInstanceId=TaskInstanceId from @FHACompleted290 where RowID=@CurrentRow

			Select @FHANumber=FHANumber from @FHACompleted290 where RowID=@CurrentRow

			Select @DateFirstEmailSent=DateFirstEmailSent from @FHACompleted290 where RowID=@CurrentRow

			Select @DateLastEmailSent=DateLastEmailSent from @FHACompleted290 where RowID=@CurrentRow

			Select @LenderSubmitterEmail = AssignedBy from Task where FhaNumber = @FHANumber and PageTypeId = 10 and SequenceId = 0

			Set @AEEmail = (select Email from [$(LiveDB)].dbo.Prod_SharepointScreen sp
									join [$(LiveDB)].dbo.Prod_SharePointAccountExecutives ae
									on sp.ClosingAEId = ae.SharePointAEId where FHANumber = @FHANumber and ClosingAEId !=0)

			Set @CloserEmail = (select AssignedTo from Task where FhaNumber = @FHANumber and PageTypeId = 10 and SequenceId = 0)

			Set @recipients = @LenderSubmitterEmail + ',' + @AEEmail + ',' + @CloserEmail

			Select @ProjectName = ProjectName from [$(LiveDB)].dbo.Prod_FHANumberRequest where FHANumber = @FHANumber

			Set @AEName = (select SharePointAEName from [$(LiveDB)].dbo.Prod_SharepointScreen sp
									join [$(LiveDB)].dbo.Prod_SharePointAccountExecutives ae
									on sp.ClosingAEId = ae.SharePointAEId where FHANumber = @FHANumber and ClosingAEId !=0)

			Set @subject = 'Executed Documents Submission Reminder- '+@ProjectName+', '+@FHANumber

			Set @body = '<p>This is a friendly reminder to submit the Executed Documents for this project as soon as possible. 
				You will continue to receive this reminder daily, until the Executed Documents have been uploaded to the Portal.</p><br/>
				<p>If you should have any questions or issues regarding the project, please direct them to '+@AEName+', 
				the assigned Account Executive for this project.</p>'

			
			
			If((Select top 1 TaskInstanceId from Task where FhaNumber = @FHANumber and PageTypeId = 17) is null)
				Begin
					If((select DateFirstReminderSent from Prod_Form290Task where TaskInstanceID = @TaskInstanceId) is null)
						Begin
							--'Send first reminder in 2 business days'
							Set @DayOfFirstEmailSent = DateName(dw,@DateFirstEmailSent)

							If ((@DayOfFirstEmailSent = 'Monday') or (@DayOfFirstEmailSent = 'Tuesday') or (@DayOfFirstEmailSent = 'Wednesday'))
								Begin
									Set @NextDay1 = DATEADD(DAY,1,@DateFirstEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1

									Set @NextDay2 = DATEADD(DAY,2,@DateFirstEmailSent)
									Exec @IsNextDay2Holiday = fn_IsFederalHoliday @NextDay2

									If(@IsNextDay1Holiday = 1 or @IsNextDay2Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,4,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else
										Begin
											Set @DateToSendEmail = DATEADD(day,3,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
								End							
							Else If (DateName(dw,@DateFirstEmailSent) = 'Thursday')
								Begin
									Set @NextDay1 = DATEADD(DAY,1,@DateFirstEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1

									Set @NextDay2 = DATEADD(DAY,4,@DateFirstEmailSent)
									Exec @IsNextDay2Holiday = fn_IsFederalHoliday @NextDay2

									If(@IsNextDay1Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,4,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else If(@IsNextDay2Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,6,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else 
										Begin
											Set @DateToSendEmail = DATEADD(day,5,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End

								End
							Else If (DateName(dw,@DateFirstEmailSent) = 'Friday')
								Begin
									Set @NextDay1 = DATEADD(DAY,3,@DateFirstEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1

									Set @NextDay2 = DATEADD(DAY,4,@DateFirstEmailSent)
									Exec @IsNextDay2Holiday = fn_IsFederalHoliday @NextDay2

									If(@IsNextDay1Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,6,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else If(@IsNextDay2Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,6,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else 
										Begin
											Set @DateToSendEmail = DATEADD(day,5,@DateFirstEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
								End
							
							Set @ReminderEmailsCount = (Select ReminderEmailsCount from Prod_Form290Task where TaskInstanceID = @TaskInstanceId)
							Update Prod_Form290Task set DateFirstReminderSent = GETDATE(), ReminderEmailsCount = @ReminderEmailsCount+1
							where TaskInstanceID = @TaskInstanceId
						End
					Else
						Begin
							--'Send following reminder every business day'
							Set @DayOfLastEmailSent = DateName(dw,@DateLastEmailSent)

							If ((@DayOfLastEmailSent = 'Monday') or (@DayOfLastEmailSent = 'Tuesday') or (@DayOfLastEmailSent = 'Wednesday'))
								Begin
									Set @NextDay1 = DATEADD(DAY,1,@DayOfLastEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1									

									If(@IsNextDay1Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,2,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else
										Begin
											Set @DateToSendEmail = DATEADD(day,1,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
								End							
							Else If (DateName(dw,@DayOfLastEmailSent) = 'Thursday')
								Begin
									Set @NextDay1 = DATEADD(DAY,1,@DayOfLastEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1

									If(@IsNextDay1Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,4,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
									Else 
										Begin
											Set @DateToSendEmail = DATEADD(day,1,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End

								End
							Else If (DateName(dw,@DayOfLastEmailSent) = 'Friday')
								Begin
									Set @NextDay1 = DATEADD(DAY,3,@DayOfLastEmailSent)
									Exec @IsNextDay1Holiday = fn_IsFederalHoliday @NextDay1

									If(@IsNextDay1Holiday = 1)
										Begin
											Set @DateToSendEmail = DATEADD(day,4,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End									
									Else 
										Begin
											Set @DateToSendEmail = DATEADD(day,3,@DayOfLastEmailSent)
											If(Convert(date,GetDate()) = Convert(date,@DateToSendEmail))																		
												Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
																			 @recipients = @recipients, @copy_recipients = @copy_recipients,
																			 @subject = @subject,@body = @body,@body_format = @body_format,
																			 @importance = @importance
										End
								End

							Set @ReminderEmailsCount = (Select ReminderEmailsCount from Prod_Form290Task where TaskInstanceID = @TaskInstanceId)
							Update Prod_Form290Task set DateFollowingReminderSent = GETDATE(), ReminderEmailsCount = @ReminderEmailsCount+1
							where TaskInstanceID = @TaskInstanceId
						End
				End
			
		End


End


