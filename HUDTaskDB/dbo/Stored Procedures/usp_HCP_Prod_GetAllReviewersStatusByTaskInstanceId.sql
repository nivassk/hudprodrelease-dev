﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId]
(
@TaskInstanceId uniqueidentifier
)
AS

BEGIN



select   a.FirstName +' '+ a.LastName as userName,
v.ViewName as userRole,
aBy.FirstName +' '+ aBy.LastName as AssignedBy,
xref.ModifiedOn as CreatedOn ,
case when s.TaskDesc = ' Project Action Request Approved' then 'Review Complete' else s.TaskDesc end as [status],
xref.CompletedOn as CompletedOn,
xref.Comments as comments,
CAST(
				CASE 
					WHEN rai.ReviewerUserId is null
						THEN 0 
					ELSE 1
				END as bit) as IsRAI


from [$(DatabaseName)].[dbo].[Prod_TaskXref] xref
inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserID = xref.AssignedTo
inner join [$(LiveDB)].dbo.HCP_Authentication aBy on aBy.UserID = xref.AssignedBy 
inner join TaskStep s on s.TaskStepId = xref.[Status]
inner join [dbo].[Prod_View] v on v.ViewId = xref.ViewId
left join (select top (1) tf.TaskInstanceId,rfs.ReviewerUserId from TaskFile tf 
			inner join  [dbo].[ReviewFileStatus] rfs on tf.TaskFileId = rfs.TaskFileId 
			where rfs.[Status] = 4 and tf.TaskInstanceId = @TaskInstanceId) rai on rai.TaskInstanceId = xref.TaskInstanceId and rai.ReviewerUserId = xref.AssignedTo

where xref.[TaskInstanceId]= @TaskInstanceId


 

end

