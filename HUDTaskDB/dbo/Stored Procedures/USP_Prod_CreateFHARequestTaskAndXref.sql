﻿
-- =============================================
-- Author:		Muni MAHIMALURU
-- Create date: Jan 22, 2017
-- Description:	FHAInsert/Portfolio/CreditReview/Parent task Insert on FHARequest Submit
-- =============================================
CREATE PROCEDURE [dbo].[USP_Prod_CreateFHARequestTaskAndXref] 
	-- Add the parameters for the stored procedure here
	@Assignedby varchar(200),
	@TaskinstanceId uniqueidentifier = null,
	@UserId int = null,
	@UserRole varchar(100) = null,
	@IsPortfolioRequired bit = null,
	@IsCreditReviewRequired bit = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION;
BEGIN TRY

--declare @Assignedby varchar(100) 
--set @Assignedby = 'servicerlam@yahoo.com'
--declare		@TaskinstanceId uniqueidentifier  
--set @TaskinstanceId = null

--declare	@UserId int	
--set @UserId = 4449
--declare	@UserRole varchar(100)	
--set @UserRole = NULL
--declare		@IsPortfolioRequired  bit
--set @IsPortfolioRequired = '1'

--	declare 	@IsCreditReviewRequired bit
--	set @IsCreditReviewRequired = '1'


   IF (@TaskinstanceId is null)
    Begin
	 set @TaskinstanceId = NEWID();
	 End

   --   declare @Assignedby varchar(200)
	  --set @Assignedby = 'servicerlam@yahoo.com'
   --   declare @TaskinstanceId uniqueidentifier 
	  --

    INSERT INTO [$(DatabaseName)].dbo.TASK(
	TaskInstanceId,
	SequenceId,
	AssignedBy,
	AssignedTo,
--	DueDate,
	StartTime,
--	Notes,
	TaskStepId,
--	DataKey1,
--	DataKey2,
--	DataStore1,
--	DataStore2,
--	TaskOpenStatus,
 	IsReassigned,
	PageTypeId,
	FhaNumber
	)
	VALUES
	(
	 @TaskinstanceId,
	 0,
	 @Assignedby,
	 'Queue',
	 GETUTCDATE(),
		  17,
		  null,
	  4,
	  null 
	);

	--declare @Taskinstanceid uniqueidentifier
	--set @TaskInstanceid = 'B9DF2B14-00BE-477A-85EF-56249EE767CC'

	
	--DECLARE @UserId int
	--set @UserId = 1
	----set @Assignedby = 'servicer'
	DECLARE @TaskXrefid uniqueidentifier = null
	SET @TaskXrefid = NEWID();
	DECLARE @TaskId int
	SELECT  @TaskId = Taskid FROM [$(DatabaseName)].dbo.TASK WHERE TaskInstanceId = @TaskinstanceId
	--print @TaskId + '-'

	

	IF(@IsPortfolioRequired = 1)
	BEGIN 
	

	SET @TaskXrefid = NEWID();

	-- portfolio insert
		INSERT INTO [$(DatabaseName)].[dbo].[Prod_TaskXref](
	TaskXrefid,
	TaskId,
	TaskInstanceId,
	AssignedBy,
	AssignedTo,
	Comments, 
	ViewId,
	ModifiedOn,
	ModifiedBy,
	[Status]
	)
	VALUES(
	@TaskXrefid,
	@TaskId,
	@TaskInstanceId,
	@UserId,
	null,
	null,	 
	9,
	GETUTCDATE(),
	@userid,
	17);

	END
	IF(@IsCreditReviewRequired = 1)
	BEGIN
	SET @TaskXrefid = NEWID();
	--FHA credi review Insert
	INSERT INTO [$(DatabaseName)].[dbo].[Prod_TaskXref](
	TaskXrefid,
	TaskId,
	TaskInstanceId,
	AssignedBy,
	AssignedTo,
	Comments, 
	ViewId,
	ModifiedOn,
	ModifiedBy,
	[Status]
	)
	VALUES(
	@TaskXrefid,
	@TaskId,
	@TaskInstanceId,
	@UserId,
	null,
	null, 
	10,
	GETUTCDATE(),
	@userid,
	17)

	END

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
print ERROR_MESSAGE ()
ROLLBACK TRANSACTION;
END CATCH

END

