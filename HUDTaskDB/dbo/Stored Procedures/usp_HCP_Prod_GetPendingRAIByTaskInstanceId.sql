﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetPendingRAIByTaskInstanceId]
(
@TaskInstanceId uniqueidentifier
)
AS

BEGIN

select aBy.UserID as userId,
              aBy.FirstName +' '+aBy.LastName as userName,
              pv.ViewId as viewId,
          pv.ViewName as userRole,
          fs.FolderName as folderName,
          tf.[FileName] as fileName,
          rfs.ModifiedOn as CreatedOn,
          rfc.Comment as comments,
          tf.TaskFileId as taskFileId,
          tf.FileId as fileId ,
          COALESCE(tfchild.[FileId], tf.[FileId]) as [downloadFileId] ,
          CAST(
                           CASE 
                                  WHEN fileStatus.TaskFileId is null 
                                         THEN 0 
                                  ELSE 1
                           END as bit) as isInOpenChildTask,
                           rfs.ReviewerProdViewId as viewId
          from [dbo].[ReviewFileStatus] rfs
inner join [dbo].[TaskFile] tf on tf.TaskFileId = rfs.TaskFileId
inner join [dbo].[TaskFile_FolderMapping] tfm on tfm.TaskFileId = rfs.TaskFileId
inner join [dbo].[Prod_FolderStructure] fs on fs.FolderKey = tfm.FolderKey
inner join [$(LiveDB)].dbo.HCP_Authentication aBy on aBy.UserID = rfs.ReviewerUserId
inner join [dbo].[Prod_View] pv on pv.ViewId = rfs.ReviewerProdViewId
inner join [dbo].[ReviewFileComment] rfc on rfc.FileTaskId = rfs.TaskFileId and rfc.CreatedOn = rfs.ModifiedOn and rfc.CreatedBy= rfs.ReviewerUserId
--join to get latest version of uploaded file
left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
              from [$(DatabaseName)].dbo.TaskFile tf1
              inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
                                                       from [$(DatabaseName)].dbo.TaskFile tfc 
                                                       inner join [$(DatabaseName)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
                                                       inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
                                                                           from [$(DatabaseName)].dbo.TaskFileHistory tfpf
                                                                           group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
                                                                                                                                                  and basetable.MaxDate = tfh.CreatedOn ) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
            ) tfchild on tf.TaskFileId = tfchild.ParentTaskFileId
--table join to get file's review status
left join ( select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(DatabaseName)].[dbo].[ReviewFileStatus] rfs1 
                     --select rfs1.[TaskFileId],rfs1.[ModifiedOn],[ReviewerUserId],[ReviewerProdViewId],[Status] from [$(DatabaseName)].[dbo].[ReviewFileStatus] rfs1 
                     --inner join 
                     --(select TaskFileId, max(ModifiedOn) as ModifiedOn 
                     --from [$(DatabaseName)].[dbo].[ReviewFileStatus]

                     --group by TaskFileId) rfs2 on rfs2.TaskFileId = rfs1.TaskFileId and rfs2.ModifiedOn = rfs1.ModifiedOn
                     
)fileStatus  on fileStatus.TaskFileId = tf.TaskFileId and fileStatus.[Status] = 4
where tf.TaskInstanceId = @TaskInstanceId and rfs.[Status] = 6

end

