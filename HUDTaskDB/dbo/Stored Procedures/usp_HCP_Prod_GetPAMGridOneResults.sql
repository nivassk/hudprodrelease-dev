﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetPAMGridOneResults]
(
	@TaskInstanceId uniqueidentifier
)
as

Declare @StartTime datetime
set @StartTime = (select top 1 StartTime from [$(LiveDB)].dbo.Prod_FHANumberRequest fr 
join task tk on fr.FHANumber = tk.FhaNumber
where fr.TaskinstanceId = @TaskInstanceId and tk.PageTypeId = 10 and tk.SequenceId = 1)

Declare @CurrentTaskInstanceId uniqueidentifier
set @CurrentTaskInstanceId = (select top 1 tk.TaskInstanceId from Task tk
								join [$(LiveDB)].dbo.Prod_FHANumberRequest fr 
								on tk.FhaNumber = fr.FHANumber
								where fr.TaskinstanceId = @TaskInstanceId
								order by PageTypeId desc )
select fr.ProjectName as ProjectName,
fr.FHANumber as FhaNumber,
pt.ProjectTypeName as LoanType,
fr.CreatedOn as ProjectStartDate,

(select DATEDIFF(dd,
(select top 1 StartTime from task  
where TaskInstanceId = @TaskInstanceId
order by SequenceId),
(case when @StartTime is null then GETDATE() else
@StartTime end)))as TotalDays,


case when ((select tk2.TaskId from HCP_Task_prod.dbo.task tk1
join task tk2 on tk1.FhaNumber = tk2.FhaNumber
where tk1.TaskInstanceId =  @TaskInstanceId and tk2.PageTypeId > 10 and tk2.SequenceId = 1 and tk2.TaskStepId = 15) is null)
then 'Open' else 'Complete' end as Status,
fr.LoanAmount as LoanAmount,
@CurrentTaskInstanceId as CurrentTaskInstanceId

from [$(LiveDB)].dbo.Prod_FHANumberRequest fr
join [$(LiveDB)].dbo.Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId
left join [$(LiveDB)].dbo.Prod_NextStage n on fr.TaskinstanceId = n.CompletedPgTaskInstanceId 
where fr.TaskinstanceId = @TaskInstanceId

