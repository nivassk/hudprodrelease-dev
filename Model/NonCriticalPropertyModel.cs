﻿using System;
using System.Collections.Generic;
using Model;

namespace HUDHealthcarePortal.Model
{
    public class NonCriticalPropertyModel : PropertyInfoModel
    {
        public string ClosingDate { get; set; }
        public string EndingDate { get; set; }
        public string ExtensionApprovalDate { get; set;}
        public bool IsAnyPendingExtensionRequestExist { get; set; }
        public decimal? NonCriticalAccountBalance { get; set; }
        public decimal? NCRECurrentBalance { get; set; }
        public int DrawNumber { get; set; }
        public bool IsNCRRequestPending { get; set; }
        public bool IsNCREValidForFHANumber { get; set; }
        public bool ShowTransactionLink { get; set; }
        public IList<TransactionModel> Transactions {get;set;}
        public bool IsFinalRequest { get; set; }
        public Guid FinalRquestId { get; set;}
        public bool IsTransactionLedger { get; set; }
    }
}