﻿using System.ComponentModel;
using Core;
using HUDHealthcarePortal.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;

namespace HUDHealthcarePortal.Model
{
    //PRC Story 575 Add class comments
    public class UpdateRoleModel 
    {
        public HUDRoleSource RoleSourceType { get; set; }
        public IList<EnumType> RolesToUpdate { get; set; }
    }

    public class UserModel
    {
        public PaginateSortModel<UserViewModel> USerReportGridModel { get; set; }
        public IList<UserViewModel> UserReportGridlList { get; set; }
    }

    public class UserViewModel //: IValidatableObject
    {
        public IList<SelectListItem> AllLenders { get; set; }
        public IList<SelectListItem> AllServicers { get; set; }
        public IList<SelectListItem> AllWorkloadManagers { get; set; }
        public IList<SelectListItem> AllAccountExecutives { get; set; }
        public IList<SelectListItem> AllRoles { get; set; }
        public IList<SelectListItem> AllTitles { get; set; }
        public OPAViewModel OPAViewModel { get; set; }
        [Display(Name = "State")]
        public IList<SelectListItem> AllStates { get; set; }
        public IList<SelectListItem> AllTimezones { get; set; }
        public IList<SelectListItem> AllRoleSources { get; set; }
        public UpdateRoleModel RoleUpdateModel { get; set; }
        public AddressModel AddressModel { get; set; }
        [Display(Name = "Institution")]
        public int? SelectedLenderId { get; set; }
        [Display(Name = "Institution")]
        //umesh
        public int? TitleId { get; set; }
        public string IsProdUser { get; set; }
        public int? SelectedServicerId { get; set; }
        [Display(Name = "Workload Manager")]
        public int? SelectedWorkloadManagerId { get; set; }
        [Display(Name = "Account Executive")]
        public int? SelectedAccountExecutiveId { get; set; }
        //[Required]
        [Display(Name = "Role")]
        public string SelectedRoles { get; set; }       // comma delimited roles
        [Required(ErrorMessage = "Time Zone is required")]
        [Display(Name = "Time Zone")]
        public string SelectedTimezone { get; set; }
        public string SelectedTimezoneId { get; set; }

        //[Required]
        [Display(Name = "User Type")]
        public string SelectedRoleSource { get; set; }

        public int? AddressID
        {
            get { return AddressModel.AddressID; }
            set { AddressModel.AddressID = value; }
        }

        public bool? IsAccountLocked { get; set; }

        public bool? IsRegisterComplete { get; set; }

        public bool? Deleted_Ind { get; set; }

        public string PasswordHist { get; set; }

        //PRC Story 575. It should be IsEditUserAllowed. The verb should be first which defines action.
        public bool IsAllowUserEdit { get; set; }

        public DateTime? Login_Time { get; set; }
        public DateTime? Logout_Time { get; set; }
        public string History { get; set; }
        public string RoleName { get; set; }

        public UserViewModel()
        {
            AllLenders = new List<SelectListItem>();
            AllServicers = new List<SelectListItem>();
            //AllRoles = new List<SelectListItem>();
            AllStates = new List<SelectListItem>();
            AllRoleSources = new List<SelectListItem>();
            AddressModel = new AddressModel();
            LendersAssignedtoServicer = new List<int>();
            AllWorkloadManagers = new List<SelectListItem>();
            AllAccountExecutives = new List<SelectListItem>();
            IsAllowUserEdit = true;
            AllAccountExecutives = new List<SelectListItem>();
            AllTitles = new List<SelectListItem>();

        }

        [Required(ErrorMessage = "User Name is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        public int UserID { get; set; }

        // if user is lender, her owner lender company
        public int LenderID { get; set; }
        public string LenderName { get; set; }

        // if user is servicer, her owner servicer company
        public int ServierID { get; set; }
        public string ServicerName { get; set; }

        public List<int> LendersAssignedtoServicer { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Middle Name/Initial")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [Display(Name = "Title")]
        public string Title
        {
            get { return AddressModel.Title; }
            set { AddressModel.Title = value; }
        }

        [Required(ErrorMessage = "Organization is required")]
        [Display(Name = "Organization")]
        public string Organization
        {
            get { return AddressModel.Organization; }
            set { AddressModel.Organization = value; }
        }

        [Required(ErrorMessage = "Phone Number is required")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber
        {
            get { return AddressModel.PhonePrimary; }
            set { AddressModel.PhonePrimary = value; }
        }


        [Required(ErrorMessage = "Street Address is required")]
        [Display(Name = "Street Address")]
        public string AddressLine1
        {
            get { return AddressModel.AddressLine1; }
            set { AddressModel.AddressLine1 = value; }
        }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string City
        {
            get { return AddressModel.City; }
            set { AddressModel.City = value; }
        }

        [Required(ErrorMessage = "State is required")]
        [Display(Name = "State")]
        public string SelectedStateCd
        {
            get { return AddressModel == null ? "" : AddressModel.StateCode; }
            set { AddressModel.StateCode = value; }
        }

        [Required(ErrorMessage = "Zip Code is required")]
        [Display(Name = "Zip Code")]
        [DataType(DataType.PostalCode)]
        public string ZipCode
        {
            get { return AddressModel.ZIP; }
            set { AddressModel.ZIP = value; }
        }

        [Required(ErrorMessage = "Email Address is required")]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress
        {
            get { return AddressModel.Email; }
            set { AddressModel.Email = value; }
        }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email Address")]
        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "Email Address and Confirm Email Address don't match.")]
        public string ConfirmEmailAddress
        {
            get { return AddressModel.ConfirmEmail; }
            set { AddressModel.ConfirmEmail = value; }
        }
     
        [Display(Name = "Task Name")]
        public string TaskName { get; set; }

     
        [Display(Name = "FHA Number")]
        public string FHANumber { get; set; }

        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[\[\]\^\$\.\|\?\*\+\(\)\\~`\!@#%&\-_+={}'""<>:;,]).{8,}$", ErrorMessage = "Password should be at least 8 characters and contain at least one upper case letter, one lower case letter, one number, one special character.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
