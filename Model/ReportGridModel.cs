﻿using System;
using System.ComponentModel.DataAnnotations;
using HUDHealthcarePortal.Core;
using System.Web.Mvc;

namespace Model
{
    public class ReportGridModel : ReportHeaderModel
    {
        public string ProjectName { get; set; }

        public string FhaNumber { get; set; }
        //Management_High Level items
        public int? isWLM { get; set; }
      
        public string  WLMName { get; set; }
        public string Email { get; set; }

        public int? ProjectCountWLM { get; set; }
        public int? ErrorCountWLM { get; set; }
        public int? ProjectCountAE { get; set; }
        public int? ErrorCountAE { get; set; }

        public decimal? DebtCoverageRatio { get; set; }

        public decimal? WorkingCapital { get; set; }

        public decimal? DaysCashOnHand { get; set; }

        public decimal? DaysInAcctReceivable { get; set; }

        public decimal? AvgPaymentPeriod { get; set; }

        public string PortfolioName { get; set; }

        public string PortfolioNumber { get; set; }

        public string MissingQuarter { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime QuarterEndDate { get; set; }
        public DateTime DataInserted { get; set; }

        public string FormatttedQuarterEndDate
        {
            get { return QuarterEndDate.ToString("MM/dd/yyyy"); }
        } 

        public int? NumberOfProjects { get; set; }

        public int ExpectedNumber { get; set; }

        public int ReceivedNumber { get; set; }

        public int MissingNumber { get; set; }

        //Project Detail Informations

      
        public string ServiceName { get; set; }
        public string HUD_WorkLoad_Manager_Name { get; set; }
        public string HUD_Project_Manager_Name { get; set; }
        public Nullable<int> UnitsInFacility { get; set; }
        public string CreditReview { get; set; }
        public string PeriodEnding { get; set; }
        public int MonthsInPeriod { get; set; }
        public Nullable<decimal> NOICumulative { get; set; }
        public string QuaterlyNoi { get; set; }
        public string NOIQuaterbyQuater { get; set; }
        public string NOIPercentageChange { get; set; }
        public Nullable<decimal> CumulativeRevenue { get; set; }
        public Nullable<decimal> CumulativeExpenses { get; set; }
        public string QuaterlyOperatingRevenue { get; set; }
        public Nullable<decimal> QuarterlyOperatingExpense { get; set; }
        public string RevenueQuarterbyQuarter { get; set; }
        public string RevenuePercentageChange { get; set; }
        public string DSCRDifferencePerQuarter { get; set; }
        public string DSCRPercentageChange { get; set; }
        public Nullable<int> CumulativeResidentDays { get; set; }
        public Nullable<int> QuarterlyResidentDays { get; set; }
        public string ADRperQuarter { get; set; }
        public string ADRDifferencePerQuarter { get; set; }
        public string ADRPercentageChange { get; set; }
        public string QuarterlyDSCR { get; set; }

        //Error Fileds
        public Nullable<decimal> DebtCoverageRatio2 { get; set; }
        public Nullable<decimal> AverageDailyRateRatio { get; set; }
        public Nullable<int> IsProjectError { get; set; }
        public Nullable<int> HasError { get; set; }
        public Nullable<int> IsQtrlyNOIPTError { get; set; }
        public Nullable<int> IsQtrlyDSCRPTError { get; set; }
        public Nullable<int> IsQtrlyORevError { get; set; }
        public Nullable<int> IsQtrlyNOIError { get; set; }
        public Nullable<int> IsQtrlyDSCRError { get; set; }
        public Nullable<int> IsADRError { get; set; }

        public Nullable<int> RowWiseError { get; set; }
        public Nullable<int> PropertyID { get; set; }

        public string LenderName { get; set; }

        public Nullable<decimal> UnPaidBalance { get; set; }
        public Nullable<decimal> fhainsuredprincipalInterestpayment { get; set; }
        public string RowWiseErrorchar { get; set; }
        public Nullable<int> Ismissing { get; set; }
        public Nullable<decimal> QuarterlyMIP { get; set; }
        public Nullable<decimal> QuarterlyFHAPI { get; set; }
        public Nullable<decimal> MIPCumulative { get; set; }
    }
}
