﻿

using System;

namespace Model
{
    public class ProjectInfoModel
    {
        public int ProjectInfoID { get; set; }
        public int PropertyID { get; set; }
        public string FHANumber { get; set; }
        public string ProjectName { get; set; }
        public Nullable<int> Property_AddressID { get; set; }
        public Nullable<int> LenderID { get; set; }
        public Nullable<int> ServicerID { get; set; }
        public Nullable<int> ManagementID { get; set; }
        public Nullable<int> HUD_WorkLoad_Manager_ID { get; set; }
        public Nullable<int> HUD_Project_Manager_ID { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public Nullable<int> Primary_Loan_Code { get; set; }
        public Nullable<System.DateTime> Initial_Endorsement_Date { get; set; }
        public Nullable<System.DateTime> Final_Endorsement_Date { get; set; }
        public string Field_Office_Status_Name { get; set; }
        public string Soa_Code { get; set; }
        public string Soa_Numeric_Name { get; set; }
        public string Soa_Description_Text { get; set; }
        public string Troubled_Code { get; set; }
        public string Troubled_status_update_date { get; set; }
        public string Reac_Last_Inspection_Score { get; set; }
        public Nullable<System.DateTime> Reac_Last_Inspection_Date { get; set; }
        public string Mddr_In_Default_Status_Name { get; set; }
        public string Is_Active_Dec_Case_Ind { get; set; }
        public Nullable<decimal> Original_Loan_Amount { get; set; }
        public Nullable<decimal> Original_Interest_Rate { get; set; }
        public Nullable<decimal> Amortized_Unpaid_Principal_Bal { get; set; }
        public string Is_Nursing_Home_Ind { get; set; }
        public string Is_Board_And_Care_Ind { get; set; }
        public string Is_Assisted_Living_Ind { get; set; }
        public Nullable<bool> Deleted_Ind { get; set; }
        public Nullable<int> Lender_AddressID { get; set; }
        public string Lender_Mortgagee_Name { get; set; }
        public Nullable<int> Servicer_AddressID { get; set; }
        public string Servicing_Mortgagee_Name { get; set; }
        public Nullable<int> Owner_AddressID { get; set; }
        public string Owner_Company_Type { get; set; }
        public string Owner_Legal_Structure_Name { get; set; }
        public string Owner_Organization_Name { get; set; }
        public Nullable<int> Owner_Contact_AddressID { get; set; }
        public string Owner_Contact_Indv_Fullname { get; set; }
        public string Owner_Contact_Indv_Title { get; set; }
        public Nullable<int> Mgmt_Agent_AddressID { get; set; }
        public string Mgmt_Agent_Company_Type { get; set; }
        public string Mgmt_Agent_org_name { get; set; }
        public Nullable<int> Mgmt_Contact_AddressID { get; set; }
        public string Mgmt_Contact_FullName { get; set; }
        public string Mgmt_Contact_Indv_Title { get; set; }
        public string Portfolio_Number { get; set; }
        public string Portfolio_Name { get; set; }
    }
}
