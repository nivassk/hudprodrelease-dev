﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;


namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class FormUploadModel : ExcelUploadView_Model
    {
        [XmlIgnore]
        public IList<SelectListItem> AllProjectNames { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AllServicerNames { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AllOperatorOwners { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AllMonthsInPeriod { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AllFinancialStatementType { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AvailableOperators { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AvailableLenderUsers { get; set; }
//        [XmlIgnore]
//		public IList<SelectListItem> AvailableFHANumbers { get; set; }
        [XmlIgnore]
        public int SelectedProjectId { get; set; }
        [XmlIgnore]
        public int SelectedServicerId { get; set; }
        [XmlIgnore]
        public int? SelectedOperatorId { get; set; }
        [XmlIgnore]
        public int? SelectedLenderUserId { get; set; }
        [XmlIgnore]
        public string SelectedLenderName { get; set; }
        [XmlIgnore]
        public bool IsFhaValid { get; set; }
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }

        [XmlIgnore]
        public bool IsFromTask { get; set; }

        public bool? IsLate { get; set; }

        public bool IsSubmitMode { get; set; }
        public int? SubmitByUserId { get; set; }
        public int? SubmitToUserId { get; set; }
        public string Notes { get; set; }
        public string NewNotes { get; set; }

        // task related fields
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }

        public bool IsReadyToSubmitUpload { get; set; }
        public bool IsUploadTermConfirmed { get; set; }
        public bool IsConcurrentUserUpdateFound { get; set; }

        public string TaskFileName { get; set; }

        [Display(Name = "Attach File")]
        public HttpPostedFileBase UploadFile { get; set; }

        public string FullyQualifiedFileName { get; set; }
        public string BaseFileName { get; set; }

        public FormUploadModel()
        {
            AllProjectNames = new List<SelectListItem>();
            AllServicerNames = new List<SelectListItem>();
            AllOperatorOwners = new List<SelectListItem>();            
            AllMonthsInPeriod = new List<SelectListItem>();
            AllFinancialStatementType = new List<SelectListItem>();
            AvailableOperators = new List<SelectListItem>();
            AvailableLenderUsers = new List<SelectListItem>();
			//AvailableFHANumbers = new List<SelectListItem>();
            AvailableFHANumbersList = new List<string>();
            IsSubmitMode = true;
            IsReadyToSubmitUpload = false;
            IsUploadTermConfirmed = false;
        }        
    }
}
