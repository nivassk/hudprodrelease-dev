﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class FormTypeModel
    {
        public int PageTypeId { get; set; }
        public string PageTypeDescription { get; set; }
    }
}
