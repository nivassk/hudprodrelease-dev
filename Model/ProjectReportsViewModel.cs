﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Model;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class ProjectReportsViewModel : ReportGridModel
    {
        public string WorkloadManager { get; set; }
        public int? WorkloadManagerId { get; set; }
        public string AccountExecutive { get; set; }
        public int? AccountExecutiveId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public string PeriodEndingDate { get; set; }
       // new public  string MonthsInPeriod { get; set; }
        public bool IsHighRisk { get; set; }
        public decimal? TotalUnpaidPrincipalBalance { get; set; }

        public ProjectReportsViewModel()
        {

        }
    }
}
