﻿using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    public class ProjectReportModel : ReportModel
    {
        public ProjectReportModel(ReportType reportType) : base(reportType)
        {
            ReportType = reportType;
            RatioExceptionsCriteriaCheckBoxList = new List<ColumnCheckBoxModel>();
            ProjectVisibilitySelectList = new List<SelectListItem>();
        }

        public ReportType ReportType { get; set; }
        public ReportLevel ReportLevel { get; set; }
        public int SelectedProjectVisibilityId { get; set; }
        public List<SelectListItem> ProjectVisibilitySelectList { get; set; }
        public IList<ProjectReportsViewModel> ProjectReportGridlList { get; set; }
        public List<ColumnCheckBoxModel> RatioExceptionsCriteriaCheckBoxList { get; set; }

        public string MinUploadDate { get; set; }
        public string MaxUploadDate { get; set; }

        public bool IsDebtCoverageRatioException { get; set; }
        public bool IsDaysCashOnHandException { get; set; }
        public bool IsAvgPaymentPeriodException { get; set; }
        public bool IsWorkingCapitalScoreException { get; set; }
        public bool IsDaysInAcctReceivableException { get; set; }
    }
}