﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class ActivityTypeModel
    {
        public int ActivityTypeId { get; set; }
        public string ActivityName { get; set; }
    }
}
