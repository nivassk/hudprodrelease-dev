﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class PAMReportV3ViewModel
    {
        public string ProjectName { get; set; }
        public string FhaNumber { get; set; }
        public string LoanType { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public int TotalDays { get; set; }
        public string Status { get; set; }
        public decimal LoanAmount { get; set; }
        public Guid CurrentTaskInstanceId { get; set; }
        public string ProjectStage { get; set; }
        public string ProjectStatus { get; set; }
        public string AppraiserStatus { get; set; }
        public string EnvironmentalistStatus { get; set; }
        public string TitleSurveyStatus { get; set; }

		public string ReviewerAppraiserStatus { get; set; }
		public string ReviewerEnvironmentalistStatus { get; set; }
		public string ReviewerTitleSurveyStatus { get; set; }
		public bool IsContractorAssigned { get; set; }
        public bool IsUnderWriterAssigned { get; set; }

		public string UnderWriterAssigned { get; set; }
		public string CreditReviewStatus { get; set; }
        public string PortfolioStatus { get; set; }
		public int? PageTypeId { get; set; }
		public int OrderByPageTypeId { get; set; }
		public int? AmendmentCount { get; set; }
		public int FHARequestStatus { get; set; }

		public int Amendment { get; set; }

        public int TaskId { get; set; }

    }
}
