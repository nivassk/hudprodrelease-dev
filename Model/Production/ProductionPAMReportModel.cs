﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;
using System.Linq;
using System.Collections;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
    public class ProductionPAMReportModel : ReportModel
    {
        public ProductionPAMReportModel(ReportType reportType)
            : base(reportType)
        {
            ReportType = reportType;
        }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public bool IsDateDefault { get; set; }
        public ReportType ReportType { get; set; }
        public ReportLevel ReportLevel { get; set; }
        public MultiSelectList Status { get; set; }
        public MultiSelectList ApplicationOrTask { get; set; }
        public IList<PAMReportProductionViewModel> PAMReportGridProductionlList { get; set; }
        public MultiSelectList ProgramTypes { get; set; }
        public MultiSelectList PamStatusList { get; set; }
        public MultiSelectList  PageTypes{ get; set; }
        public MultiSelectList ProductionUsers { get; set; }
        public MultiSelectList ProductionPageTypes { get; set; }
        public MultiSelectList ProductionProjecTypeList { get; set; }
        public string ApplicationTaskLevel { get; set; }


        //Lender PAM Report
        public IList<string> SelectedStatusList { get; set; }
        public IList<string> SelectedProgramTypesList { get; set; }
        public IList<string> SelectedPageTypesList { get; set; }
       
        public MultiSelectList LAMListItems { get; set; }
        public MultiSelectList BAMListItems { get; set; }
        public MultiSelectList LARListItems { get; set; }
        public MultiSelectList RoleListItems { get; set; }
        public string LenderName { get; set; }
        public string LenderUserFullName { get; set; }
        /// <summary>
        /// selected serach criteria LAM names list
        /// </summary>
        public IList<string> SelectedLAMNameList { get; set; }
        /// <summary>
        /// selected serach criteria BAM names list
        /// </summary>
        public IList<string> SelectedBAMNameList { get; set; }
        /// <summary>
        /// selected serach criteria LAR names list
        /// </summary>
        public IList<string> SelectedLARNameList { get; set; }
        public IList<string> SelectedRolesList { get; set; }

        public string ReportViewName { get; set; }
    }
}
