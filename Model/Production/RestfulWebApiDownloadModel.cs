﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public class RestfulWebApiDownloadModel
    {
        public string filename { get; set; }
        public string folderName { get; set; }
        public string documentType { get; set; }
        
    }
}
