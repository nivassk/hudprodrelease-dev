﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class Prod_ViewModel
    {
        public int ViewId { get; set; }
        public string ViewName { get; set; }
        public int ViewTypeId { get; set; }
        public DateTime? Modifiedon { get; set; }
    }
}
