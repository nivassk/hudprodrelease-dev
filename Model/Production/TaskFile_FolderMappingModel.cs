﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class TaskFile_FolderMappingModel
    {
        public int fileFolderId { get; set; }
        public Guid TaskFileId { get; set; }
        public int FolderKey { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
