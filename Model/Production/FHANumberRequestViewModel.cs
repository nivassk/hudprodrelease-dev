﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
    public class FHANumberRequestViewModel
    {
        public Guid FHANumberRequestId { get; set; }
        public string CurrentFHANumber { get; set; }
        public string FHANumber { get; set; }
        public string ConfirmFHANumber { get; set; }
        public string ProjectName { get; set; }
        public int? PropertyId { get; set; }
        public int ProjectTypeId { get; set; }
        public int? CurrentLoanTypeId { get; set; }
        public int LoanTypeId { get; set; }
        public int? ActivityTypeId { get; set; }
        public int BorrowerTypeId { get; set; }
        public bool? IsAddressCorrect { get; set; }
        public int PropertyAddressId { get; set; }
        public AddressModel PropertyAddressModel { get; set; }
        public int CongressionalDtId { get; set; }
        public int LenderId { get; set; }
        public string LenderName { get; set; }
        public string LenderContactName { get; set; }
        public int LenderContactAddressId { get; set; }
        public AddressModel LenderContactAddressModel { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal ConfirmLoanAmount { get; set; }
        public int SkilledNursingBeds { get; set; }
        public int SkilledNursingUnits { get; set; }
        public int AssistedLivingBeds { get; set; }
        public int AssistedLivingUnits { get; set; }
        public int BoardCareBeds { get; set; }
        public int BoardCareUnits { get; set; }
        public int MemoryCareBeds { get; set; }
        public int MemoryCareUnits { get; set; }
        public int IndependentBeds { get; set; }
        public int IndependentUnits { get; set; }
        public int OtherBeds { get; set; }
        public int OtherUnits { get; set; }
        public bool? IsMidLargePortfolio { get; set; }
        public bool? IsMasterLeaseProposed { get; set; }
        public string Portfolio_Name { get; set; }
        public int? Portfolio_Number { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ConstructionStageTypeId { get; set; }
        public int? CMSStarRating { get; set; }
        public decimal ProposedInterestRate { get; set; }
        public int IsNewOrExistOrNAPortfolio { get; set; }
        public bool IsPortfolioRequired { get; set; }
        public bool IsCreditReviewRequired { get; set; }
        public bool IsFhaInsertComplete { get; set; }
        public bool IsPortfolioComplete { get; set; }
        public bool IsCreditReviewAttachComplete { get; set; }
        public string InsertFHAComments { get; set; }
        public string PortfolioComments { get; set; }
        public string CreditreviewComments { get; set; }
        public DateTime? CreditReviewDate { get; set; }
        public bool IsReadyForApplication { get; set; }
        public bool RequestDeny { get; set; }
        public Guid TaskinstanceId { get; set; }
        public string LenderPhone { get; set; }
        public bool IsSmallPortfolio { get; set; }
        public IList<SelectListItem> AllProjectTypes { get; set; }
        public IList<SelectListItem> AllActivityTypes { get; set; }
        public IList<SelectListItem> AllMortgageInsuranceTypes { get; set; }
        public IList<SelectListItem> AllBorrowerTypes { get; set; }
        public IList<SelectListItem> AllCmsStarRatings { get; set; }
        public IList<SelectListItem> AllStateCodes { get; set; }
        public IList<SelectListItem> AllStagesForConstruction { get; set; }
        public IList<SelectListItem> AllPortfolioStatus { get; set; }
        public IList<SelectListItem> AllCurrentLoanTypes { get; set; } 
        
        public bool IsLIHTC { get; set; }
        public bool IsHome { get; set; }
        public bool IsTaxExempted { get; set; }
        public bool IsCDBG { get; set; }
        public bool IsOther { get; set; }
        public bool IsDisclaimerAccepted { get; set; }
        public string DisclaimerText { get; set; }
        public string SubmitType { get; set; }
        public int RequestStatus { get; set; }
        public string CreditLetterFileName { get; set; }
        public HttpPostedFileBase CreditLetterFile { get; set; }
        public int FhaRequestType { get; set; }
        public IList<SelectedAdditionalFinancingModel> SelectedAdditionalFinancingList { get; set; }

        public string SelectedProjectTypeName { get; set; }
        public string SelectedBorrowerTypeName { get; set; }
        public string SelectedActivityTypeName { get; set; }
        public string SelectedMortgageInsuranceTypeName { get; set; }
        public string SelectedCmsStarRating { get; set; }
        public string SelectedStateName { get; set; }
        public string SelectedConstructionStageName { get; set;}
        public string SelectedPortfolioStatusName { get; set; }
        public string SelectedCurrentLoanTypeName { get; set; }

        public string PropertyStreetAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyZip { get; set; }
        public string LenderContactEmail { get; set; }
        public string LenderContactPhone { get; set; }

        public int FileId { get; set; }
        public bool IsMyTask { get; set; }
        public bool IsGroupTask { get; set; }
        public bool IsPAMReport { get; set; }
        public bool IsLenderPAMReport { get; set; }
        public bool IsQueue { get; set; }
        public bool TransAccessStatus { get; set; }
        public DateTime? RequestSubmitDate { get; set; }
        public string DenyComments { get; set; }
        public string CancelComments { get; set; }
        public bool IsPortfolioCancelled { get; set; }
        public bool IsCreditCancelled { get; set; }
        public string PortfolioCancelComments { get; set; }
        public string CreditCancelComments { get; set; }
        public IList<String> AvailableFHANumbersList { get; set; }
        public string LenderUserName { get; set; }
        public int LenderUserId { get; set; }
        public FHANumberRequestViewModel()
        {
            SelectedAdditionalFinancingList = new List<SelectedAdditionalFinancingModel>();
            AllProjectTypes = new List<SelectListItem>();
            AllActivityTypes = new List<SelectListItem>();
            AllMortgageInsuranceTypes = new List<SelectListItem>();
            AllBorrowerTypes = new List<SelectListItem>();
            AllCmsStarRatings = new List<SelectListItem>();
            AllStateCodes = new List<SelectListItem>();
            AllStagesForConstruction = new List<SelectListItem>();
            AllPortfolioStatus = new List<SelectListItem>();
            PropertyAddressModel = new AddressModel();
            LenderContactAddressModel = new AddressModel();
            AllCurrentLoanTypes = new List<SelectListItem>();
            AvailableFHANumbersList = new List<string>();
           
        }
       
    }
}
