﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Model.Production
{
    public class Prod_LoanCommitteeViewModel
    {
        public Guid LoanCommitteeId { get; set; }
        public int SequenceId { get; set; }
        public string FHANumber { get; set; }
        public int LenderId { get; set; }
        public int? ProjectScheduledForLC { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LoanCommitteDate { get; set; }
        public int? LCRecommendation { get; set; }
        public int? A7Presentation { get; set; }
        public int? TwoStageSubmittal { get; set; }
        public DateTime? LCDateIssued { get; set; }
        public DateTime? DecisionDate { get; set; }
        public string LCComments { get; set; }
        public string DisplayLCComments { get; set; }
        public string ProjectName { get; set; }
        public int? ProjectType { get; set; }
        public string ProjectTypeName { get; set; }
        public string NonProfit { get; set; }
        public decimal LoanAmount { get; set; }
        public string Underwriter { get; set; }
        public string WLM { get; set; }
        public string Ogc { get; set; }
        public string Appraiser { get; set; }
        public string Lender { get; set; }
        public int? LCDecision { get; set; }
        public Guid ApplicationTaskinstanceId { get; set; }
        public IList<SelectListItem> ProjectScheduledLC { get; set; }
        public IList<SelectListItem> RecommendationLC { get; set; }
        public IList<SelectListItem> A7PresentationLC { get; set; }
        public IList<SelectListItem> TwoStageSubmittalLC { get; set; }
        public IList<SelectListItem> LCDecisionList { get; set; }        
        public DateTime? LoanCommitteDateSubmitedselected { get; set; }
        public IList<SelectListItem> LoanCommitteDateSubmitedList { get; set; }
        public string ChangeToLoanCommitteeDate { get; set; }
        public IList<SelectListItem> ChangeToLoanCommitteeDateList { get; set; }
        public LoanCommitteeFiltersModel LoanCommitteeFiltersModel { get; set; }       

    }
}
