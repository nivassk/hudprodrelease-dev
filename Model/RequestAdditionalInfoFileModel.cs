﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class RequestAdditionalInfoFileModel
    {
        public Guid RequestAdditionalFileId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public Guid TaskFileId { get; set; }
        
    }
}
