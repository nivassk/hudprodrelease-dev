﻿namespace HUDHealthcarePortal.Model
{

    public class ReportsModel
    {
        public int PropertyID { get; set; }
        public string ProjectName { get; set; }

        public string FHANumber { get; set; }
        public string FHADescription { get; set; }

        public int LenderID { get; set; }
        public string LenderName { get; set; }

        public string Soa_Description_Text { get; set; }
        public int PropertyID_Count { get; set; }

    }
}
