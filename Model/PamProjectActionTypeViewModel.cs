﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class PamProjectActionTypeViewModel
    {
        public int PamProjectActionId { get; set; }
        public string Name { get; set; }
    }
}
