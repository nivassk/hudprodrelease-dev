﻿using HUDHealthcarePortal.Core;

namespace HUDHealthcarePortal.Model
{
    public class MenuRestrictionsViewmodel
    {
        public int RestrictionId { get; set; }
        public string MenuName { get; set; }
        public int LenderId { get; set; }
    }
}
