﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Model
{
    public class ReportHeaderModel
    {
        public ReportHeaderModel()
        {
            HudAccountExecutive = new HUDManagerModel { AddressModel = new AddressModel() };
            HudWorkloadManager = new HUDManagerModel { AddressModel = new AddressModel() };
        }

        public HUDManagerModel HudAccountExecutive { get; set; }

        public HUDManagerModel HudWorkloadManager { get; set; }

        public string LenderName { get; set; }
        public int? TotalProperties { get; set; }
        public int? TotalPropertieswithException { get; set; }
        public int? TotalNumberOfUnits { get; set; }
        public string HUD_WorkLoad_Manager_Name { get; set; }
        public string  HUD_Project_Manager_Name { get; set; }

        public int? LenderId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public decimal? UnpaidPrincipalBalance { get; set; }

        public int? NumberOfProjects { get; set; }
        public string CreatedBy { get; set; }

        public int HudProjectManagerId
        {
            get { return HudAccountExecutive.Manager_ID; }
            set { HudAccountExecutive.Manager_ID = value; }
        }

        public string HudProjectManagerName
        {
            get { return HudAccountExecutive.Manager_Name; }
            set { HudAccountExecutive.Manager_Name = value; }
        }

        public string HudProjectManagerEmailAddress
        {
            get { return HudAccountExecutive.AddressModel.Email; }
            set { HudAccountExecutive.AddressModel.Email = value; }
        }

        public int HudWorkloadManagerId
        {
            get { return HudWorkloadManager.Manager_ID; }
            set { HudWorkloadManager.Manager_ID = value; }
        }

        public string HudWorkloadManagerName
        {
            get { return HudWorkloadManager.Manager_Name; }
            set { HudWorkloadManager.Manager_Name = value; }
        }

        public string HudWorkloadManagerEmailAddress
        {
            get { return HudWorkloadManager.AddressModel.Email; }
            set { HudWorkloadManager.AddressModel.Email = value; }
        }

        public ReportType ReportType { get; set; }

        public ReportYear ReportYear { get; set; }

        //Fileds for Capturing types of Error Counts
        public int? TotalNoiError { get; set; }
        public int? TotalDSCRError { get; set; }
        public int? TotalADRError { get; set; }
        public int? TotalOpRevError { get; set; }
        public int? TotalColError { get; set; }

        //Production code
          public string ProdWLMName { get; set; }
          public string ProdUserName { get; set; }
          public string ViewName { get; set; }

    }
}
