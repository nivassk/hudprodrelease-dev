﻿CREATE FUNCTION  [dbo].[fn_StringSplitter]
(
			@pParamsString	VARCHAR(MAX)
)
Returns @Tbl_Params Table  (Param VARCHAR(50))  AS 

BEGIN 
 -- Append comma
 SET @pParamsString =  @pParamsString + ',' 
 -- Indexes to keep the position of searching
 DECLARE @Pos1 INT
 DECLARE @Pos2 INT
 
 -- Start from first character 
 SET @Pos1=1
 SET @Pos2=1

 WHILE @Pos1<LEN(@pParamsString)
 BEGIN
  SET @Pos1 = CHARINDEX(',',@pParamsString,@Pos1)
  INSERT @Tbl_Params SELECT  Cast(SUBSTRING(@pParamsString,@Pos2,@Pos1-@Pos2) AS VARCHAR(50))
  -- Go to next non comma character
  SET @Pos2=@Pos1+1
  -- Search from the next charcater
  SET @Pos1 = @Pos1+1
 END 
 RETURN
END

