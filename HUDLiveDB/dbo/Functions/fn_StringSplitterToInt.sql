﻿
CREATE FUNCTION  [dbo].[fn_StringSplitterToInt]
(
			@pParamsString	VARCHAR(MAX)
)
Returns @Tbl_Params Table  (id INT)  AS 

BEGIN 
	declare @v_XMLString xml
	if nullif(@pParamsString, '') is not null
	begin
		set @v_XMLString = cast(replace('<Record><T>' + replace(@pParamsString, ',', '</T><T>') + '</T></Record>', '<T></T>', '') as xml)

		insert into @Tbl_Params (id)
		select tbl.col.value('.','int') as id
		from   @v_XMLString.nodes('//T') tbl(col) 
	end
 RETURN
END

