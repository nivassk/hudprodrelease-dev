﻿
CREATE FUNCTION [dbo].[fn_HCP_GetQuarter]
(
	@MonthInPeriod INT
)
RETURNS VARCHAR(2)
AS
BEGIN
	DECLARE @QuarterPart VARCHAR(2)
	SET @QuarterPart =
      CASE @MonthInPeriod
         WHEN 3 THEN 'Q1'
         WHEN 6 THEN 'Q2'
         WHEN 9 THEN 'Q3'
         WHEN 12 THEN 'Q4'
         ELSE '??'
      END   

    RETURN @QuarterPart
END
