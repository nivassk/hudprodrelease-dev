﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_taskidForExecutedClosing]
(
@PagetypeId int,
@FHANumber varchar(10) = null,
@TaskInstanceId uniqueidentifier = null
)
AS

BEGIN

	select taskid from [HCP_Task_Prod].[dbo].[Task] where TaskInstanceId=@TaskInstanceId and PageTypeId=@PagetypeId and FhaNumber=@FHANumber;	  		
end