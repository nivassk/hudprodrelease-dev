﻿CREATE PROCEDURE [dbo].[usp_HCP_GetAeEmailByFhaNumber]
(
@fhaNumber varchar(MAX)
)
AS

select ad.Email
	   
from ProjectInfo p 
join HUD_Project_Manager pm on p.HUD_Project_Manager_ID = pm.HUD_Project_Manager_ID 
join Address ad on pm.AddressID = ad.AddressID and p.FHANumber = @fhaNumber
order by p.ModifiedOn desc

