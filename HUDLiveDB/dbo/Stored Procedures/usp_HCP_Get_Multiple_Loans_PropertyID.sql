﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Multiple_Loans_PropertyID]
AS

WITH MultipleLoansPropertyID (PropertyID,FHANumber,ProjectName,LenderID,Soa_Description_Text, PropertyID_Count)
AS
(
SELECT PI.PropertyID,PI.FHANumber,PI.ProjectName,PI.LenderID,PI.Soa_Description_Text, (SELECT Count(*) FROM [dbo].[ProjectInfo] PI1 WHERE PI1.propertyid = PI.propertyid) AS PropertyID_Count FROM [dbo].[ProjectInfo] PI
)

SELECT MLP.PropertyID, MLP.ProjectName, MLP.FHANumber,FHAi.FHADescription,MLP.LenderID,LI.Lender_Name,MLP.Soa_Description_Text, MLP.PropertyID_Count FROM MultipleLoansPropertyID MLP
INNER JOIN [$(DatabaseName)].dbo.FHAInfo_iREMS FHAi ON FHAi.FHANumber = MLP.FHANumber
INNER JOIN [$(DatabaseName)].dbo.LenderInfo LI ON LI.LenderID = MLP.LenderID
WHERE PropertyID_Count > 1
ORDER BY MLP.ProjectName



--WITH Multiple_Loans_PropertyID (PropertyID,FHANumber,ProjectName,LenderID,Soa_Description_Text, PropertyID_Count)
--AS
--(
--SELECT PI.PropertyID,PI.FHANumber,PI.ProjectName,PI.LenderID,PI.Soa_Description_Text, (SELECT Count(*) FROM [dbo].[ProjectInfo] PI1 WHERE PI1.propertyid = PI.propertyid) AS PropertyID_Count FROM [dbo].[ProjectInfo] PI
--)

--SELECT PropertyID,FHANumber,ProjectName,LenderID,Soa_Description_Text, PropertyID_Count FROM Multiple_Loans_PropertyID
--WHERE PropertyID_Count > 1

