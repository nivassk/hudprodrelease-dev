﻿-- =============================================
-- Author:		Rashmi Ganapathy
-- Create date: 08/07/2018
-- Description:	Adhoc Data Dump
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetAllProdData]

AS
/*
Date		Changed by		Reason  
====		===========     ======  
12/4/2018   rganapathy		Populate ServicerSubmissionDate for some of the FHA#s
12/6/2018	rganapathy		Corrected Portfolio, Credit Review information, Invalid dates 1/1/1900
						


*/
BEGIN
	
	if OBJECT_ID('TEMPDB..#ProductionData') is not null
		drop table #ProductionData

	CREATE TABLE #ProductionData
	(
		FHANumberRequestId uniqueidentifier null,
		FHATaskInstanceId uniqueidentifier null,
		FHANumber nvarchar(20) null,
		ProjectName nvarchar(max) null,
		PropertyId int,
		ProjectType nvarchar(500) null,
		ConstructionStageType nvarchar(500) null,
		CurrentLoanType nvarchar(500) null,
		ActivityType nvarchar(500) null,
		MortgageInsuranceType nvarchar(500) null,
		BorrowerType nvarchar(500) null,
		IsAddressCorrect nvarchar(20) null,
		ProjectStreetaddress nvarchar(max) null,
		ProjectCity nvarchar(500) null,
		ProjectState nvarchar(20) null,
		ProjectZipcode nvarchar(20) null,
		CongressionalDistrict int, 
		LenderId int,
		LenderName nvarchar(max) null,
		LenderContactName nvarchar(max) null,
		LenderContactEmail nvarchar(max) null,
		LenderContactPhonenumber nvarchar(20) null,
		ProposedLoanAmount decimal(19,2) null,
		ProposedInterestRate decimal(19,2) null,
		SkilledNursingBeds int,
		SkilledNursingUnits int,
		AssistedLivingBeds int,
		AssistedLivingUnits int,
		BoardCareBeds int,
		BoardCareUnits int,
		MemoryCareBeds int,
		MemoryCareUnits int,
		IndependentBeds int,
		IndependentUnits int,
		OtherBeds int,
		OtherUnits int,
		CMSStarRating int,
		MasterLeaseProposed nvarchar(20) null,
		PortfolioStatus nvarchar(20) null,
		MidLargePortfolio nvarchar(20) null,
		SmallPortfolio nvarchar(20) null,
		Portfolio_Name nvarchar(max) null,
		Portfolio_Number int,
		Comments nvarchar(max) null,
		InsertFHAAssignedTo nvarchar(max) null,
		InsertFHAAssignedDate datetime null,
		InsertFHACompletedDate datetime null,
		InsertFHAComments nvarchar(max) null,
		InsertFHAIsReassigned nvarchar(20) null,
		InsertFHAReassignedFrom nvarchar(500) null,
		InsertFHAReassignedBy nvarchar(500) null,
		InsertFHAReassignedDate datetime null,
		PortfolioAssignedTo nvarchar(max) null,
		PortfolioAssignedDate datetime null,
		PortfolioCompletedDate datetime null,
		PortfolioComments nvarchar(max) null,
		PortfolioIsReassigned nvarchar(20) null,
		PortfolioReassignedFrom nvarchar(500) null,
		PortfolioReassignedBy nvarchar(500) null,
		PortfolioReassignedDate datetime null,
		CreditReviewAssignedTo nvarchar(max) null,
		CreditReviewAssignedDate datetime null,
		CreditReviewCompletedDate datetime null,
		CreditreviewComments nvarchar(max) null,
		CreditReviewDate datetime null,
		CreditReviewIsReassigned nvarchar(20) null,
		CreditReviewReassignedFrom nvarchar(500) null,
		CreditReviewReassignedBy nvarchar(500) null,
		CreditReviewReassignedDate datetime null,
		RequestStatus nvarchar(500) null,
		CurrentFHANumber nvarchar(20) null,
		RequestSubmitDate datetime null,
		DenyComments nvarchar(max) null,
		CancelComments nvarchar(max) null,
		One80WarningSent nvarchar(20) null,
		Two70WarningSent nvarchar(20) null,
		IsReadyforApplication nvarchar(20) null,
		LIHTC nvarchar(20) null,
		TaxExemptBonds nvarchar(20) null,
		HOME nvarchar(20) null,
		CDBG nvarchar(20) null,
		Other nvarchar(20) null,
		FHARequestCompletionDate datetime null,
		FHARequestNotificationType nvarchar(20) null,
		AppProductionType nvarchar(500) null,
		AppDateofLenderSubmission datetime null,
		AppLenderComments nvarchar(max) null,
		AppTaskInstanceId uniqueidentifier null,
		AppLongTermHold nvarchar(20) null,
		AppDateOfHold datetime null,
		AppReasonForHold nvarchar(max) null,
		AppLoanCommitteApproved nvarchar(20) null,
		AppLoanCommitteApprovedDate datetime null,
		AppFirmCommitmentIssued nvarchar(20) null,
		AppFirmCommitmentIssuedDate datetime null,
		AppEarlyCommencementRequested nvarchar(20) null,
		AppEarlyCommencementRequestedDate datetime null,
		AppEarlyCommencementIssuedDate datetime null,
		AppLDLIssued nvarchar(20) null,
		AppLDLIssuedDate datetime null,
		AppLDLCompleteResponseReceived datetime null,
		AppMiscellaneousComments nvarchar(max) null,
		AppOtherQueueWorkComments nvarchar(max) null,
		AppAccountExecutiveMiscInfo nvarchar(500) null,
		AppIremsCompleted nvarchar(20) null,
		AppAppsCompleted nvarchar(20) null,
		AppIremsPrintoutSaved nvarchar(20) null,
		AppMirandaCompletedCMSListCheck nvarchar(20) null,
		AppContractorContractPrice decimal(19,2) null,
		AppContractorSecondaryAmtPaid decimal(19,2) null,
		AppContractorAmountPaid decimal(19,2) null,
		AppBackupOGCComment nvarchar(max) null,
		AppOGCComment nvarchar(max) null,
		AppEnvironmentalComment nvarchar(max) null,
		AppSurveyComment nvarchar(max) null,
		AppContractorComment nvarchar(max) null,
		AppAppraisalComment nvarchar(max) null,
		AppBackupOgcAddress nvarchar(max) null,
		AppOgcAddress nvarchar(max) null,
		AppDateFirmReviewBegan datetime null,
		AppDateEnteredQueue datetime null,
		AppProductionWLMName nvarchar(500) null,
		AppAssignedUWDate datetime null,
		AppAssignedUWName nvarchar(500) null,
		AppUWComments nvarchar(max) null,
		AppUWStatus nvarchar(20) null,
		AppUWCompletedOn datetime null,
		AppUWIsReassigned nvarchar(20) null,
		AppUWReassignedFrom nvarchar(500) null,
		AppUWReassignedBy nvarchar(500) null,
		AppUWReassignedDate datetime null,
		AppAssignedEnvName nvarchar(500) null,
		AppEnvAssignedDate datetime null,
		AppEnvComments nvarchar(max) null,
		AppEnvStatus nvarchar(20) null,
		AppEnvCompletedOn datetime null,
		AppEnvIsReassigned nvarchar(20) null,
		AppEnvReassignedFrom nvarchar(500) null,
		AppEnvReassignedBy nvarchar(500) null,
		AppEnvReassignedDate datetime null,
		AppAssignedSurveyName nvarchar(500) null,
		AppSurveyAssignedDate datetime null,
		AppSurveyComments nvarchar(max) null,
		AppSurveyStatus nvarchar(20) null,
		AppSurveyCompletedOn datetime null,
		AppSurveyIsReassigned nvarchar(20) null,
		AppSurveyReassignedFrom nvarchar(500) null,
		AppSurveyReassignedBy nvarchar(500) null,
		AppSurveyReassignedDate datetime null,
		AppAssignedAppraiserName nvarchar(500) null,
		AppAppraiserAssignedDate datetime null,
		AppAppraiserComments nvarchar(max) null,
		AppAppraiserStatus nvarchar(20) null,
		AppAppraiserCompletedOn datetime null,
		AppAppraiserIsReassigned nvarchar(20) null,
		AppAppraiserReassignedFrom nvarchar(500) null,
		AppAppraiserReassignedBy nvarchar(500) null,
		AppAppraiserReassignedDate datetime null,
		AppAssignedOGCName nvarchar(500) null,
		AppOGCAssignedDate datetime null,
		AppOGCComments nvarchar(max) null,
		AppOGCStatus nvarchar(20) null,
		AppOGCCompletedOn datetime null,
		AppOGCIsReassigned nvarchar(20) null,
		AppOGCReassignedFrom nvarchar(500) null,
		AppOGCReassignedBy nvarchar(500) null,
		AppOGCReassignedDate datetime null,
		AppAssignedBackupOGCName nvarchar(500) null,
		AppBackupOGCAssignedDate datetime null,
		AppBackupOGCComments nvarchar(max) null,
		AppBackupOGCStatus nvarchar(20) null,
		AppBackupOGCCompletedOn datetime null,
		AppBackupOGCIsReassigned nvarchar(20) null,
		AppBackupOGCReassignedFrom nvarchar(500) null,
		AppBackupOGCReassignedBy nvarchar(500) null,
		AppBackupOGCReassignedDate datetime null,
		AppAssignedContractUWName nvarchar(500) null,
		AppContractUWAssignedDate datetime null,
		AppContractUWComments nvarchar(max) null,
		AppContractUWStatus nvarchar(20) null,
		AppContractUWCompletedOn datetime null,
		AppContractUWIsReassigned nvarchar(20) null,
		AppContractUWReassignedFrom nvarchar(500) null,
		AppContractUWReassignedBy nvarchar(500) null,
		AppContractUWReassignedDate datetime null,
		AppAssignedDECName nvarchar(500) null,
		AppDECAssignedDate datetime null,
		AppDECComments nvarchar(max) null,
		AppDECStatus nvarchar(20) null,
		AppDECCompletedOn datetime null,
		AppDECIsReassigned nvarchar(20) null,
		AppDECReassignedFrom nvarchar(500) null,
		AppDECReassignedBy nvarchar(500) null,
		AppDECReassignedDate datetime null,
		AppFSProductionType nvarchar(500) null,
		AppFSDateofLenderSubmission datetime null,
		AppFSLenderComments nvarchar(max) null,
		AppFSTaskInstanceId uniqueidentifier null,
		AppFSLongTermHold nvarchar(20) null,
		AppFSDateOfHold datetime null,
		AppFSReasonForHold nvarchar(max) null,
		AppFSLoanCommitteApproved nvarchar(20) null,
		AppFSLoanCommitteApprovedDate datetime null,
		AppFSFirmCommitmentIssued nvarchar(20) null,
		AppFSFirmCommitmentIssuedDate datetime null,
		AppFSEarlyCommencementRequested nvarchar(20) null,
		AppFSEarlyCommencementRequestedDate datetime null,
		AppFSEarlyCommencementIssuedDate datetime null,
		AppFSLDLIssued nvarchar(20) null,
		AppFSLDLIssuedDate datetime null,
		AppFSLDLCompleteResponseReceived datetime null,
		AppFSMiscellaneousComments nvarchar(max) null,
		AppFSOtherQueueWorkComments nvarchar(max) null,
		AppFSAccountExecutiveMiscInfo nvarchar(500) null,
		AppFSIremsCompleted nvarchar(20) null,
		AppFSAppsCompleted nvarchar(20) null,
		AppFSIremsPrintoutSaved nvarchar(20) null,
		AppFSMirandaCompletedCMSListCheck nvarchar(20) null,
		AppFSContractorContractPrice decimal(19,2) null,
		AppFSContractorSecondaryAmtPaid decimal(19,2) null,
		AppFSContractorAmountPaid decimal(19,2) null,
		AppFSBackupOGCComment nvarchar(max) null,
		AppFSOGCComment nvarchar(max) null,
		AppFSEnvironmentalComment nvarchar(max) null,
		AppFSSurveyComment nvarchar(max) null,
		AppFSContractorComment nvarchar(max) null,
		AppFSAppraisalComment nvarchar(max) null,
		AppFSBackupOgcAddress nvarchar(max) null,
		AppFSOgcAddress nvarchar(max) null,
		AppFSDateFirmReviewBegan datetime null,
		AppFSDateEnteredQueue datetime null,
		AppFSProductionWLMName nvarchar(500) null,
		AppFSAssignedUWDate datetime null,
		AppFSAssignedUWName nvarchar(500) null,
		AppFSUWComments nvarchar(max) null,
		AppFSUWStatus nvarchar(20) null,
		AppFSUWCompletedOn datetime null,
		AppFSUWIsReassigned nvarchar(20) null,
		AppFSUWReassignedFrom nvarchar(500) null,
		AppFSUWReassignedBy nvarchar(500) null,
		AppFSUWReassignedDate datetime null,
		AppFSAssignedEnvName nvarchar(500) null,
		AppFSEnvAssignedDate datetime null,
		AppFSEnvComments nvarchar(max) null,
		AppFSEnvStatus nvarchar(20) null,
		AppFSEnvCompletedOn datetime null,
		AppFSEnvIsReassigned nvarchar(20) null,
		AppFSEnvReassignedFrom nvarchar(500) null,
		AppFSEnvReassignedBy nvarchar(500) null,
		AppFSEnvReassignedDate datetime null,
		AppFSAssignedSurveyName nvarchar(500) null,
		AppFSSurveyAssignedDate datetime null,
		AppFSSurveyComments nvarchar(max) null,
		AppFSSurveyStatus nvarchar(20) null,
		AppFSSurveyCompletedOn datetime null,
		AppFSSurveyIsReassigned nvarchar(20) null,
		AppFSSurveyReassignedFrom nvarchar(500) null,
		AppFSSurveyReassignedBy nvarchar(500) null,
		AppFSSurveyReassignedDate datetime null,
		AppFSAssignedAppraiserName nvarchar(500) null,
		AppFSAppraiserAssignedDate datetime null,
		AppFSAppraiserComments nvarchar(max) null,
		AppFSAppraiserStatus nvarchar(20) null,
		AppFSAppraiserCompletedOn datetime null,
		AppFSAppraiserIsReassigned nvarchar(20) null,
		AppFSAppraiserReassignedFrom nvarchar(500) null,
		AppFSAppraiserReassignedBy nvarchar(500) null,
		AppFSAppraiserReassignedDate datetime null,
		AppFSAssignedOGCName nvarchar(500) null,
		AppFSOGCAssignedDate datetime null,
		AppFSOGCComments nvarchar(max) null,
		AppFSOGCStatus nvarchar(20) null,
		AppFSOGCCompletedOn datetime null,
		AppFSOGCIsReassigned nvarchar(20) null,
		AppFSOGCReassignedFrom nvarchar(500) null,
		AppFSOGCReassignedBy nvarchar(500) null,
		AppFSOGCReassignedDate datetime null,
		AppFSAssignedBackupOGCName nvarchar(500) null,
		AppFSBackupOGCAssignedDate datetime null,
		AppFSBackupOGCComments nvarchar(max) null,
		AppFSBackupOGCStatus nvarchar(20) null,
		AppFSBackupOGCCompletedOn datetime null,
		AppFSBackupOGCIsReassigned nvarchar(20) null,
		AppFSBackupOGCReassignedFrom nvarchar(500) null,
		AppFSBackupOGCReassignedBy nvarchar(500) null,
		AppFSBackupOGCReassignedDate datetime null,
		AppFSAssignedContractUWName nvarchar(500) null,
		AppFSContractUWAssignedDate datetime null,
		AppFSContractUWComments nvarchar(max) null,
		AppFSContractUWStatus nvarchar(20) null,
		AppFSContractUWCompletedOn datetime null,
		AppFSContractUWIsReassigned nvarchar(20) null,
		AppFSContractUWReassignedFrom nvarchar(500) null,
		AppFSContractUWReassignedBy nvarchar(500) null,
		AppFSContractUWReassignedDate datetime null,
		AppFSAssignedDECName nvarchar(500) null,
		AppFSDECAssignedDate datetime null,
		AppFSDECComments nvarchar(max) null,
		AppFSDECStatus nvarchar(20) null,
		AppFSDECCompletedOn datetime null,
		AppFSDECIsReassigned nvarchar(20) null,
		AppFSDECReassignedFrom nvarchar(500) null,
		AppFSDECReassignedBy nvarchar(500) null,
		AppFSDECReassignedDate datetime null,
		DCProductionType nvarchar(500) null,
		DCDateofLenderSubmission datetime null,
		DCLenderComments nvarchar(max) null,
		DCTaskInstanceId uniqueidentifier null,
		DCClosingComment nvarchar(max) null,
		DCContractCloserAssigned  nvarchar(20) null,
		DCContractorContactName nvarchar(500) null,
		DCClosingProgramSpecialistId nvarchar(500) null,
		DCCostCertReceived nvarchar(20) null,
		DCCostCertRecievedDate datetime null,
		DCCostCertCompletedMIMIssued nvarchar(20) null,
		DCCostCertIssueDate datetime null,
		DCTwo290Completed nvarchar(20) null,
		DCTwo90CompletedDate datetime null,
		DCActiveNCRE nvarchar(20) null,
		DCNCREDueDate datetime null,
		DCNCREInitalBalance decimal(19,2) null,
		DCClosingPackageRecievedDate datetime null,
		DCProgramSpecialist nvarchar(500) null,
		DCAccountExecutiveClosing nvarchar(500) null,
		DCCloser nvarchar(500) null,
		DCInitialClosingDate datetime null,
		DCAssignedCloserName nvarchar(500) null,
		DCCloserAssignedDate datetime null,
		DCCloserComments nvarchar(max) null,
		DCCloserStatus nvarchar(20) null,
		DCCloserCompletedOn datetime null,
		DCCloserIsReassigned nvarchar(20) null,
		DCCloserReassignedFrom nvarchar(500) null,
		DCCloserReassignedBy nvarchar(500) null,
		DCCloserReassignedDate datetime null,
		DCAssignedEnvName nvarchar(500) null,
		DCEnvAssignedDate datetime null,
		DCEnvComments nvarchar(max) null,
		DCEnvStatus nvarchar(20) null,
		DCEnvCompletedOn datetime null,
		DCEnvIsReassigned nvarchar(20) null,
		DCEnvReassignedFrom nvarchar(500) null,
		DCEnvReassignedBy nvarchar(500) null,
		DCEnvReassignedDate datetime null,
		DCAssignedSurveyName nvarchar(500) null,
		DCSurveyAssignedDate datetime null,
		DCSurveyComments nvarchar(max) null, 
		DCSurveyStatus nvarchar(20) null,
		DCSurveyCompletedOn datetime null,
		DCSurveyIsReassigned nvarchar(20) null,
		DCSurveyReassignedFrom nvarchar(500) null,
		DCSurveyReassignedBy nvarchar(500) null,
		DCSurveyReassignedDate datetime null,
		DCAssignedAppraiserName nvarchar(500) null,
		DCAppraiserAssignedDate datetime null,
		DCAppraiserComments nvarchar(max) null,
		DCAppraiserStatus nvarchar(20) null,
		DCAppraiserCompletedOn datetime null,
		DCAppraiserIsReassigned nvarchar(20) null,
		DCAppraiserReassignedFrom nvarchar(500) null,
		DCAppraiserReassignedBy nvarchar(500) null,
		DCAppraiserReassignedDate datetime null,
		DCAssignedOGCName nvarchar(500) null,
		DCOGCAssignedDate datetime null,
		DCOGCComments nvarchar(max) null,
		DCOGCStatus nvarchar(20) null,
		DCOGCCompletedOn datetime null,
		DCOGCIsReassigned nvarchar(20) null,
		DCOGCReassignedFrom nvarchar(500) null,
		DCOGCReassignedBy nvarchar(500) null,
		DCOGCReassignedDate datetime null,
		DCAssignedBackupOGCName nvarchar(500) null,
		DCBackupOGCAssignedDate datetime null,
		DCBackupOGCComments nvarchar(max) null,
		DCBackupOGCStatus nvarchar(20) null,
		DCBackupOGCCompletedOn datetime null,
		DCBackupOGCIsReassigned nvarchar(20) null,
		DCBackupOGCReassignedFrom nvarchar(500) null,
		DCBackupOGCReassignedBy nvarchar(500) null,
		DCBackupOGCReassignedDate datetime null,
		DCAssignedContractUWName nvarchar(500) null,
		DCContractUWAssignedDate datetime null,
		DCContractUWComments nvarchar(max) null,
		DCContractUWStatus nvarchar(20) null,
		DCContractUWCompletedOn datetime null,
		DCContractCloserIsReassigned nvarchar(20) null,
		DCContractCloserReassignedFrom nvarchar(500) null,
		DCContractCloserReassignedBy nvarchar(500) null,
		DCContractCloserReassignedDate datetime null,
		DCAssignedDECName nvarchar(500) null,
		DCDECAssignedDate datetime null,
		DCDECComments nvarchar(max) null,
		DCDECStatus nvarchar(20) null,
		DCDECCompletedOn datetime null,
		DCDECIsReassigned nvarchar(20) null,
		DCDECReassignedFrom nvarchar(500) null,
		DCDECReassignedBy nvarchar(500) null,
		DCDECReassignedDate datetime null,
		ECProductionType nvarchar(500) null,
		ECDateofLenderSubmission datetime null,
		ECLenderComments nvarchar(max) null,
		ECTaskInstanceId uniqueidentifier null,
		ECClosingComment nvarchar(max) null,
		ECContractCloserAssigned  nvarchar(20) null,
		ECContractorContactName nvarchar(500) null,
		ECClosingProgramSpecialistId nvarchar(500) null,
		ECCostCertReceived nvarchar(20) null,
		ECCostCertRecievedDate datetime null,
		ECCostCertCompletedMIMIssued nvarchar(20) null,
		ECCostCertIssueDate datetime null,
		ECTwo290Completed nvarchar(20) null,
		ECTwo90CompletedDate datetime null,
		ECActiveNCRE nvarchar(20) null,
		ECNCREDueDate datetime null,
		ECNCREInitalBalance decimal(19,2) null,
		ECClosingPackageRecievedDate datetime null,
		ECProgramSpecialist nvarchar(500) null,
		ECAccountExecutiveClosing nvarchar(500) null,
		ECCloser nvarchar(500) null,
		ECInitialClosingDate datetime null,
		ECAssignedCloserName nvarchar(500) null,
		ECCloserAssignedDate datetime null,
		ECCloserComments nvarchar(max) null,
		ECCloserStatus nvarchar(20) null,
		ECCloserCompletedOn datetime null,
		ECCloserIsReassigned nvarchar(20) null,
		ECCloserReassignedFrom nvarchar(500) null,
		ECCloserReassignedBy nvarchar(500) null,
		ECCloserReassignedDate datetime null,
		ECAssignedEnvName nvarchar(500) null,
		ECEnvAssignedDate datetime null,
		ECEnvComments nvarchar(max) null,
		ECEnvStatus nvarchar(20) null,
		ECEnvCompletedOn datetime null,
		ECEnvIsReassigned nvarchar(20) null,
		ECEnvReassignedFrom nvarchar(500) null,
		ECEnvReassignedBy nvarchar(500) null,
		ECEnvReassignedDate datetime null,
		ECAssignedSurveyName nvarchar(500) null,
		ECSurveyAssignedDate datetime null,
		ECSurveyComments nvarchar(max) null, 
		ECSurveyStatus nvarchar(20) null,
		ECSurveyCompletedOn datetime null,
		ECSurveyIsReassigned nvarchar(20) null,
		ECSurveyReassignedFrom nvarchar(500) null,
		ECSurveyReassignedBy nvarchar(500) null,
		ECSurveyReassignedDate datetime null,
		ECAssignedAppraiserName nvarchar(500) null,
		ECAppraiserAssignedDate datetime null,
		ECAppraiserComments nvarchar(max) null,
		ECAppraiserStatus nvarchar(20) null,
		ECAppraiserCompletedOn datetime null,
		ECAppraiserIsReassigned nvarchar(20) null,
		ECAppraiserReassignedFrom nvarchar(500) null,
		ECAppraiserReassignedBy nvarchar(500) null,
		ECAppraiserReassignedDate datetime null,
		ECAssignedOGCName nvarchar(500) null,
		ECOGCAssignedDate datetime null,
		ECOGCComments nvarchar(max) null,
		ECOGCStatus nvarchar(20) null,
		ECOGCCompletedOn datetime null,
		ECOGCIsReassigned nvarchar(20) null,
		ECOGCReassignedFrom nvarchar(500) null,
		ECOGCReassignedBy nvarchar(500) null,
		ECOGCReassignedDate datetime null,
		ECAssignedBackupOGCName nvarchar(500) null,
		ECBackupOGCAssignedDate datetime null,
		ECBackupOGCComments nvarchar(max) null,
		ECBackupOGCStatus nvarchar(20) null,
		ECBackupOGCCompletedOn datetime null,
		ECBackupOGCIsReassigned nvarchar(20) null,
		ECBackupOGCReassignedFrom nvarchar(500) null,
		ECBackupOGCReassignedBy nvarchar(500) null,
		ECBackupOGCReassignedDate datetime null,
		ECAssignedContractUWName nvarchar(500) null,
		ECContractUWAssignedDate datetime null,
		ECContractUWComments nvarchar(max) null,
		ECContractUWStatus nvarchar(20) null,
		ECContractUWCompletedOn datetime null,
		ECContractCloserIsReassigned nvarchar(20) null,
		ECContractCloserReassignedFrom nvarchar(500) null,
		ECContractCloserReassignedBy nvarchar(500) null,
		ECContractCloserReassignedDate datetime null,
		ECAssignedDECName nvarchar(500) null,
		ECDECAssignedDate datetime null,
		ECDECComments nvarchar(max) null,
		ECDECStatus nvarchar(20) null,
		ECDECCompletedOn datetime null,
		ECDECIsReassigned nvarchar(20) null,
		ECDECReassignedFrom nvarchar(500) null,
		ECDECReassignedBy nvarchar(500) null,
		ECDECReassignedDate datetime null		

	);



	INSERT INTO #ProductionData (FHANumberRequestId,FHATaskInstanceId,FHANumber,ProjectName,PropertyId,ProjectType,ConstructionStageType,CurrentLoanType,ActivityType,MortgageInsuranceType,
								 BorrowerType,IsAddressCorrect,ProjectStreetaddress,ProjectCity,ProjectState,ProjectZipcode,CongressionalDistrict,LenderId,
								 LenderName,LenderContactName,LenderContactEmail,LenderContactPhonenumber,ProposedLoanAmount,ProposedInterestRate,SkilledNursingBeds,
								 SkilledNursingUnits,AssistedLivingBeds,AssistedLivingUnits,BoardCareBeds,BoardCareUnits,MemoryCareBeds,MemoryCareUnits,IndependentBeds,
								 IndependentUnits,OtherBeds,OtherUnits,CMSStarRating,MasterLeaseProposed,PortfolioStatus,MidLargePortfolio,SmallPortfolio,Portfolio_Name,
								 Portfolio_Number,Comments,InsertFHAAssignedDate,InsertFHAComments,PortfolioComments,CreditreviewComments,CreditReviewDate,RequestStatus,CurrentFHANumber,
								 RequestSubmitDate,DenyComments,CancelComments,One80WarningSent,Two70WarningSent,IsReadyforApplication,FHARequestCompletionDate,FHARequestNotificationType)
		select  fr.FHANumberRequestId, fr.taskinstanceid as FHATaskInstanceId, fr.FHANumber, fr.ProjectName, fr.PropertyId, pt.ProjectTypeName as ProjectType, 
		(case when fr.ConstructionStageTypeId = 1 then 'Single Stage' when fr.ConstructionStageTypeId = 2 then 'Two Stage' else 'N/A' end) as ConstructionStageType,
		(case when fr.CurrentLoanTypeId = 0 then 'N/A' end) as CurrentLoanType,
		ac.ActivityName as ActivityType, lt.LoanTypeName as MortgageInsuranceType, bt.BorrowerTypeName as BorrowerType, 
		(case when fr.IsAddressCorrect is null then '' when fr.IsAddressCorrect = 1 then 'Yes' else 'No' end) as IsAddressCorrect,
		ad.AddressLine1 as ProjectStreetaddress, ad.City as ProjectCity, ad.StateCode as ProjectState, ad.ZIP as ProjectZipcode, 
		fr.CongressionalDtId as CongressionalDistrict, fr.LenderId, li.Lender_Name as LenderName, fr.LenderContactName,
		fr.LenderContactEmail, fr.LenderContactPhone as LenderContactPhonenumber, fr.LoanAmount as ProposedLoanAmount, 
		fr.ProposedInterestRate as ProposedInterestRate, fr.SkilledNursingBeds, fr.SkilledNursingUnits, fr.AssistedLivingBeds,
		fr.AssistedLivingUnits, fr.BoardCareBeds, fr.BoardCareUnits, fr.MemoryCareBeds, fr.MemoryCareUnits, fr.IndependentBeds,
		fr.IndependentUnits, fr.OtherBeds,fr.OtherUnits, 
		(case when fr.CMSStarRating = 0 then '' else fr.CMSStarRating end) as CMSStarRating,
		(case when fr.IsMasterLeaseProposed = 1 then 'Yes' when fr.IsMasterLeaseProposed = 0 then 'No' else '' end) as MasterLeaseProposed,
		(case when fr.IsNewOrExistOrNAPortfolio = 1 then 'New' when fr.IsNewOrExistOrNAPortfolio = 2 then 'Existing' else 'N/A' end) as PortfolioStatus,
		(case when fr.IsMidLargePortfolio = 1 then 'Yes' else '' end) as MidLargePortfolio,
		(case when fr.IsMidLargePortfolio = 0 then 'Yes' else '' end) as SmallPortfolio,
		(case when fr.Portfolio_Name is null then '' else fr.Portfolio_Name end) as Portfolio_Name,
		(case when fr.Portfolio_Number != 0 then fr.Portfolio_Number else '' end) as Portfolio_Number,
		(case when fr.Comments is null then '' else fr.Comments end) as Comments,
		fr.AssignedDate as InsertFHAAssignedDate,
		(case when fr.InsertFHAComments is null then '' else fr.InsertFHAComments end) as InsertFHAComments,
		(case when fr.PortfolioComments is null then '' else fr.PortfolioComments end) as PortfolioComments,
		(case when fr.CreditreviewComments is null then '' else fr.CreditreviewComments end) as CreditreviewComments,
		ISNULL(fr.CreditReviewDate,null) as CreditReviewDate,
		(case when fr.RequestStatus = 6 then 'Draft' when fr.RequestStatus = 1 then 'Submit' when fr.RequestStatus = 2 then 'Approved' end) RequestStatus,
		(case when fr.CurrentFHANumber is null then '' else fr.CurrentFHANumber end) as CurrentFHANumber,
		(case when fr.RequestSubmitDate is null then '' else fr.RequestSubmitDate end) as RequestSubmitDate,
		(case when fr.DenyComments is null then '' else fr.DenyComments end) as DenyComments,
		(case when fr.CancelComments is null then '' else fr.CancelComments end) as CancelComments,
		(case when fr.One80Warning is null then 'No' else 'Yes' end) as One80Warning,
		(case when fr.Two70Warning is null then 'No' else 'Yes' end) as Two70Warning,
		(case when fr.IsReadyForApplication = 0 then 'No' else 'Yes' end) as IsReadyForApplication,
		(case when fr.IsReadyForApplication = 1 then fr.ModifiedOn else null end) as FHARequestCompletionDate,
		(case when fr.Two70Warning = 1 then '270' when fr.One80Warning = 1 then '180' else null end) as FHARequestNotificationType
			from [Prod_FHANumberRequest] fr
			left join [Prod_ActivityType] ac on fr.activityTypeid = ac.activityTypeId
			join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId
			join Prod_LoanType lt on fr.LoanTypeId = lt.LoanTypeId
			join Prod_BorrowerType bt on fr.BorrowerTypeId = bt.BorrowerTypeId
			join Address ad on fr.PropertyAddressId = ad.AddressID
			join LenderInfo li on fr.LenderId = li.LenderID

		DECLARE @RowsToProcess  int  
		DECLARE @CurrentRow     int  
		Declare @FHANumberRequestId uniqueidentifier  
		Declare @FHANumber varchar(9)
		DECLARE @FHARequest TABLE (RowID int not null primary key identity(1,1), FHANumberRequestId uniqueidentifier, FHANumber nvarchar(20) )    
		INSERT into @FHARequest (FHANumberRequestId, FHANumber) 
		SELECT FHANumberRequestId, FHANumber FROM dbo.Prod_FHANumberRequest 
		SET @RowsToProcess=@@ROWCOUNT  
		
		SET @CurrentRow=0  
		WHILE @CurrentRow<@RowsToProcess  
		BEGIN  
			SET @CurrentRow=@CurrentRow+1  
			
			SELECT @FHANumberRequestId=FHANumberRequestId FROM @FHARequest WHERE RowID=@CurrentRow 
			
			
			SELECT @FHANumber=FHANumber FROM @FHARequest WHERE RowID=@CurrentRow 
			
			update #ProductionData set LIHTC = 'Yes' from Prod_SelectedAdditionalFinancingResources fr 
													 join #ProductionData pd on fr.FHANumberRequestId = pd.FHANumberRequestId
													 where ResourceId = 1 and fr.FHANumberRequestId = @FHANumberRequestId
			update #ProductionData set TaxExemptBonds = 'Yes' from Prod_SelectedAdditionalFinancingResources fr
															  join #ProductionData pd on fr.FHANumberRequestId = pd.FHANumberRequestId
															  where ResourceId = 2 and fr.FHANumberRequestId = @FHANumberRequestId
			update #ProductionData set HOME = 'Yes' from Prod_SelectedAdditionalFinancingResources fr
													join #ProductionData pd on fr.FHANumberRequestId = pd.FHANumberRequestId
													where ResourceId = 3 and fr.FHANumberRequestId = @FHANumberRequestId
			update #ProductionData set CDBG = 'Yes' from Prod_SelectedAdditionalFinancingResources fr
													join #ProductionData pd on fr.FHANumberRequestId = pd.FHANumberRequestId
													where ResourceId = 4 and fr.FHANumberRequestId = @FHANumberRequestId
			update #ProductionData set Other = 'Yes' from Prod_SelectedAdditionalFinancingResources fr
													 join #ProductionData pd on fr.FHANumberRequestId = pd.FHANumberRequestId
													 where ResourceId = 5 and fr.FHANumberRequestId = @FHANumberRequestId
			
			update #ProductionData set InsertFHAAssignedTo = au.FirstName+' '+au.LastName  from [$(TaskDB)].dbo.Task tk
																	join HCP_Authentication au on tk.AssignedTo = au.UserName
																	join #ProductionData pd on pd.FHATaskInstanceId = tk.TaskInstanceId
																	where PageTypeId = 4 and SequenceId = 0 
			update #ProductionData set InsertFHACompletedDate = tk.StartTime  from [$(TaskDB)].dbo.Task tk
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and SequenceId = 1 
			update #ProductionData set InsertFHAIsReassigned = (case when tk.IsReassigned = 1 then 'Yes' else '' end),
								       InsertFHAReassignedFrom = au1.FirstName+' '+au1.LastName,
									   InsertFHAReassignedBy = au3.FirstName+' '+au3.LastName,
									   InsertFHAReassignedDate = pt.CreatedOn	
																  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on tk.TaskInstanceId = pt.TaskInstanceId
																	join HCP_Authentication au1 on pt.FromAE = au1.UserName
																	join HCP_Authentication au2 on pt.ReAssignedTo = au2.UserName
																	join HCP_Authentication au3 on pt.CreatedBy = au3.UserID
																	join #ProductionData pd on tk.FhaNumber = @FHANumber
																	where PageTypeId = 4 and IsReassigned = 1
			update #ProductionData set PortfolioAssignedTo = au.FirstName+' '+au.LastName  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join HCP_Authentication au on tx.AssignedTo = au.UserID
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 9 
			update #ProductionData set PortfolioAssignedDate = tx.AssignedDate  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 9 
			update #ProductionData set PortfolioCompletedDate = tx.CompletedOn  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 9
			update #ProductionData set PortfolioIsReassigned = (case when tx.IsReassigned = 1 then 'Yes' else '' end),
								       PortfolioReassignedFrom = au1.FirstName+' '+au1.LastName,
									   PortfolioReassignedBy = au3.FirstName+' '+au3.LastName,
									   PortfolioReassignedDate = pt.CreatedOn	
																  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref tx on tk.taskinstanceid = tx.Taskinstanceid
																	join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on tx.TaskInstanceId = pt.TaskInstanceId
																	join HCP_Authentication au1 on pt.FromAE = au1.UserName
																	join HCP_Authentication au2 on pt.ReAssignedTo = au2.UserName
																	join HCP_Authentication au3 on pt.CreatedBy = au3.UserID
																	join #ProductionData pd on tk.FhaNumber = @FHANumber
																	where PageTypeId = 4 and tx.ViewId = 9 and tx.IsReassigned = 1
			update #ProductionData set CreditReviewAssignedTo = au.FirstName+' '+au.LastName  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join HCP_Authentication au on tx.AssignedTo = au.UserID
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 10 
			update #ProductionData set CreditReviewAssignedDate = tx.AssignedDate  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 10
			update #ProductionData set CreditReviewCompletedDate = tx.CompletedOn  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref  tx on tk.TaskInstanceId = tx.TaskInstanceId
																	join #ProductionData pd on pd.FHANumber = tk.FhaNumber
																	where PageTypeId = 4 and pd.FhaNumber = @FHANumber and ViewId = 10
			update #ProductionData set CreditReviewIsReassigned = (case when tx.IsReassigned = 1 then 'Yes' else '' end),
								       CreditReviewReassignedFrom = au1.FirstName+' '+au1.LastName,
									   CreditReviewReassignedBy = au3.FirstName+' '+au3.LastName,
									   CreditReviewReassignedDate = pt.CreatedOn	
																  from [$(TaskDB)].dbo.Task tk
																	join [$(TaskDB)].dbo.Prod_TaskXref tx on tk.taskinstanceid = tx.Taskinstanceid
																	join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on tx.TaskInstanceId = pt.TaskInstanceId
																	join HCP_Authentication au1 on pt.FromAE = au1.UserName
																	join HCP_Authentication au2 on pt.ReAssignedTo = au2.UserName
																	join HCP_Authentication au3 on pt.CreatedBy = au3.UserID
																	join #ProductionData pd on tk.FhaNumber = @FHANumber
																	where PageTypeId = 4 and tx.ViewId = 10 and tx.IsReassigned = 1

			-----***** Application /Construction Single stage application/Construction initial stage application Data*****------
			update #ProductionData set 				 
				AppProductionType = (case when a.PageTypeId = 5 then 'Application Request' 
										  when a.PageTypeId = 6 then 'Construction Single Stage Application Request'
										  when a.PageTypeId = 7 then 'Construction Two Stage Initial Application Request' else '' end),
				AppDateofLenderSubmission = (case when a.ServicerSubmissionDate is null then '' else a.ServicerSubmissionDate end),
				AppLenderComments = (case when a.ServicerComments is null then '' else a.ServicerComments end),
				AppTaskInstanceId = a.opt,
				AppLongTermHold = (case when sp.IsLongTermHold is null then '' when sp.IsLongTermHold = 0 then 'No' else 'Yes' end),
				AppDateOfHold = sp.DateOfHold,--(case when sp.DateOfHold is null then '' else sp.DateOfHold end),
				AppReasonForHold = (case when sp.ReasonForHold is null then '' else sp.ReasonForHold end),
				AppLoanCommitteApproved = (case when sp.IsLoanCommitteApproved is null then '' when sp.IsLoanCommitteApproved = 0 then 'No' else 'Yes' end),
				AppLoanCommitteApprovedDate = sp.LoanCommitteApprovedDate,--(case when sp.LoanCommitteApprovedDate is null then '' else sp.LoanCommitteApprovedDate end),
				AppFirmCommitmentIssued = (case when sp.IsFirmCommitmentIssued is null then '' when sp.IsFirmCommitmentIssued = 0 then 'No' else 'Yes' end),
				AppFirmCommitmentIssuedDate = sp.FirmCommitmentIssuedDate,--(case when sp.FirmCommitmentIssuedDate is null then '' else sp.FirmCommitmentIssuedDate end),
				AppEarlyCommencementRequested = (case when sp.IsEarlyCommencementRequested is null then '' when sp.IsEarlyCommencementRequested = 0 then 'No' else 'Yes' end),
				AppEarlyCommencementRequestedDate = sp.EarlyCommencementRequestedDate,--(case when sp.EarlyCommencementRequestedDate is null then '' else sp.EarlyCommencementRequestedDate end),
				AppEarlyCommencementIssuedDate = sp.EarlyCommencementIssuedDate,--(case when sp.EarlyCommencementIssuedDate is null then '' else sp.EarlyCommencementIssuedDate  end),
				AppLDLIssued = (case when sp.IsLDLIssued is null then '' when sp.IsLDLIssued = 0 then 'No' else 'Yes' end),
				AppLDLIssuedDate = sp.LDLIssuedDate,--(case when sp.LDLIssuedDate is null then '' else sp.LDLIssuedDate end),
				AppLDLCompleteResponseReceived = sp.LDLCompleteResponseReceived,--(case when sp.LDLCompleteResponseReceived is null then '' else sp.LDLCompleteResponseReceived  end),
				AppMiscellaneousComments = (case when sp.MiscellaneousComments is null then '' else sp.MiscellaneousComments end),
				AppOtherQueueWorkComments = (case when sp.OtherQueueWorkComments is null then '' else sp.OtherQueueWorkComments end),
				AppAccountExecutiveMiscInfo = (case when sp.SelectedAEId is null then '' else sae.SharePointAEName end),
				AppIremsCompleted = (case when sp.IsIremsCompleted is null then '' when sp.IsIremsCompleted = 0 then 'No' else 'Yes' end),
				AppAppsCompleted = (case when sp.IsAppsCompleted is null then '' when sp.IsAppsCompleted = 0 then 'No' else 'Yes' end),
				AppIremsPrintoutSaved = (case when sp.IsIremsPrintoutSaved is null then '' when sp.IsIremsPrintoutSaved = 0 then 'No' else 'Yes' end),
				AppMirandaCompletedCMSListCheck = (case when sp.IsMirandaCompletedCMSListCheck is null then '' when sp.IsMirandaCompletedCMSListCheck = 0 then 'No' else 'Yes' end),
				AppContractorContractPrice = (case when sp.ContractorContractPrice is null then 0 else sp.ContractorContractPrice end),
				AppContractorSecondaryAmtPaid = (case when sp.ContractorSecondaryAmtPaid is null then 0 else sp.ContractorSecondaryAmtPaid end),
				AppContractorAmountPaid = (case when sp.ContractorAmountPaid is null then 0 else sp.ContractorAmountPaid end),
				AppOGCComment = (case when sp.OGCComment is null then '' else sp.OGCComment end),
				AppEnvironmentalComment = (case when sp.EnvironmentalComment is null then '' else sp.EnvironmentalComment end),
				AppSurveyComment = (case when sp.SurveyComment is null then '' else sp.SurveyComment end),
				AppContractorComment = (case when sp.ContractorComment is null then '' else sp.ContractorComment end),
				AppAppraisalComment = (case when sp.AppraisalComment is null then '' else sp.AppraisalComment end),
				AppBackupOgcAddress = (case when sp.BackupOgcAddressId is null or sp.BackupOgcAddressId = 0 then '' end),
				AppOgcAddress = (case when sp.OgcAddressId is null or sp.OgcAddressId = 0 then '' end)
				from  
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as opt,tk.PageTypeId,max(tk.SequenceId) seqid,
				op.ServicerSubmissionDate,op.ServicerComments from OPAForm op  
				 join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId where  tk.PageTypeId in (5,6,7) 
				 group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber,op.ServicerSubmissionDate,op.ServicerComments) a
				left join Prod_SharepointScreen sp on a.opt = sp.TaskinstanceId
				left join Prod_SharePointAccountExecutives sae on sae.SharePointAEId = sp.SelectedAEId
				join #ProductionData pd on a.FHANumber = pd.FHANumber
				where a.FhaNumber = @FHANumber

			update #ProductionData set 	
				 AppAssignedUWName = underwriter,
				 AppUWComments = comments1,
				 AppUWStatus = (case when status1 = 15 then 'Complete' when status1 = 18 then 'Inprocess' else '' end),
				 AppUWCompletedOn = completedon1,
				 AppAssignedUWDate = assignedto1,
				 AppUWIsReassigned = isreasgnd1,
				 AppUWReassignedFrom = fromae1,
				 AppUWReassignedBy = byae1,
				 AppUWReassignedDate = reasgnDt1,
				 AppAssignedEnvName = env,
				 AppEnvComments=comments3,
				 AppEnvStatus=(case when status3 = 15 then 'Complete' when status3 = 18 then 'Inprocess' else '' end),
				 AppEnvCompletedOn=completedon3,
				 AppEnvAssignedDate = assignedto3,
				 AppEnvIsReassigned  = isreasgnd3,
				 AppEnvReassignedFrom  = fromae3,
				 AppEnvReassignedBy  = byae3,
				 AppEnvReassignedDate  = reasgnDt3,
				 AppAssignedSurveyName=survey,
				 AppSurveyComments=comments2,
				 AppSurveyStatus=(case when status2 = 15 then 'Complete' when status2 = 18 then 'Inprocess' else '' end),
				 AppSurveyCompletedOn=completedon2,
				 AppSurveyAssignedDate=assignedto2,
				 AppSurveyIsReassigned  = isreasgnd2,
				 AppSurveyReassignedFrom  = fromae2,
				 AppSurveyReassignedBy  = byae2,
				 AppSurveyReassignedDate  = reasgnDt2,
				 AppAssignedAppraiserName=appraiser,
				 AppAppraiserComments=comments12,
				 AppAppraiserStatus=(case when status12 = 15 then 'Complete' when status12 = 18 then 'Inprocess' else '' end),
				 AppAppraiserCompletedOn=completedon12,
				 AppAppraiserAssignedDate=assignedto12,
				 AppAppraiserIsReassigned  = isreasgnd12,
				 AppAppraiserReassignedFrom  = fromae12,
				 AppAppraiserReassignedBy  = byae12,
				 AppAppraiserReassignedDate  = reasgnDt12,
				 AppAssignedOGCName=attorney,
				 AppOGCComments=comments4,
				 AppOGCStatus=(case when status4 = 15 then 'Complete' when status4 = 18 then 'Inprocess' else '' end),
				 AppOGCCompletedOn=completedon4,
				 AppOGCAssignedDate=assignedto4,
				 AppOGCIsReassigned  = isreasgnd4,
				 AppOGCReassignedFrom  = fromae4,
				 AppOGCReassignedBy  = byae4,
				 AppOGCReassignedDate  = reasgnDt4,
				 AppAssignedBackupOGCName=bkupogc,
				 AppBackupOGCComments=comments11,
				 AppBackupOGCStatus=(case when status11 = 15 then 'Complete' when status11 = 18 then 'Inprocess' else '' end),
				 AppBackupOGCCompletedOn=completedon11,
				 AppBackupOGCAssignedDate=assignedto11,
				 AppBackupOGCIsReassigned  = isreasgnd11,
				 AppBackupOGCReassignedFrom  = fromae11,
				 AppBackupOGCReassignedBy  = byae11,
				 AppBackupOGCReassignedDate  = reasgnDt11,
				 AppAssignedContractUWName=cuw,
				 AppContractUWComments=comments6,
				 AppContractUWStatus=(case when status6 = 15 then 'Complete' when status6 = 18 then 'Inprocess' else '' end),
				 AppContractUWCompletedOn=completedon6,
				 AppContractUWAssignedDate=assignedto6,
				 AppContractUWIsReassigned  = isreasgnd6,
				 AppContractUWReassignedFrom  = fromae6,
				 AppContractUWReassignedBy  = byae6,
				 AppContractUWReassignedDate  = reasgnDt6,
				 AppAssignedDECName=decname,
				 AppDECComments=comments5,
				 AppDECStatus=(case when status5 = 15 then 'Complete' when status5 = 18 then 'Inprocess' else '' end),
				 AppDECCompletedOn=completedon5,
				 AppDECAssignedDate=assignedto5,
				 AppDECIsReassigned  = isreasgnd5,
				 AppDECReassignedFrom  = fromae5,
				 AppDECReassignedBy  = byae5,
				 AppDECReassignedDate  = reasgnDt5

					 from 
					(select * from
					(select tx.taskinstanceid as taskinstanceid1,au.FirstName+' '+au.LastName as underwriter, tx.viewid as viewid1, status as status1, 
						comments as comments1, CompletedOn as completedon1, AssignedDate as assignedto1,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd1,
						au2.FirstName+' '+au2.LastName as fromae1,
						au3.FirstName+' '+au3.LastName as byae1,pt.CreatedOn as reasgnDt1
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 1) t1
					left join 
					(select tx.taskinstanceid as taskinstanceid2, au.FirstName+' '+au.LastName as survey, tx.viewid as viewid2, status as status2, 
						comments as comments2, CompletedOn as completedon2, AssignedDate as assignedto2,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd2,
						au2.FirstName+' '+au2.LastName as fromae2,
						au3.FirstName+' '+au3.LastName as byae2,pt.CreatedOn as reasgnDt2
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 2) t2 on t1.taskinstanceid1 = t2.taskinstanceid2
					left join 
					(select tx.taskinstanceid as taskinstanceid3,au.FirstName+' '+au.LastName as env, tx.viewid as viewid3, status as status3, 
						comments as comments3, CompletedOn as completedon3, AssignedDate as assignedto3,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd3,
						au2.FirstName+' '+au2.LastName as fromae3,
						au3.FirstName+' '+au3.LastName as byae3,pt.CreatedOn as reasgnDt3
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 3) t3 on t1.TaskInstanceId1 = t3.TaskInstanceId3
					left join 
					(select tx.taskinstanceid as taskinstanceid4,au.FirstName+' '+au.LastName as attorney, tx.viewid as viewid4, status as status4, 
						comments as comments4, CompletedOn as completedon4, AssignedDate as assignedto4,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd4,
						au2.FirstName+' '+au2.LastName as fromae4,
						au3.FirstName+' '+au3.LastName as byae4,pt.CreatedOn as reasgnDt4
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 4) t4 on t1.TaskInstanceId1 = t4.TaskInstanceId4
					left join 
					(select tx.taskinstanceid as taskinstanceid11,au.FirstName+' '+au.LastName as bkupogc, tx.viewid as viewid11, status as status11, 
						comments as comments11, CompletedOn as completedon11, AssignedDate as assignedto11,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd11,
						au2.FirstName+' '+au2.LastName as fromae11,
						au3.FirstName+' '+au3.LastName as byae11,pt.CreatedOn as reasgnDt11
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 11) t11 on t1.TaskInstanceId1 = t11.TaskInstanceId11
					left join 
					(select tx.taskinstanceid as taskinstanceid6,au.FirstName+' '+au.LastName as cuw, tx.viewid as viewid6, status as status6, 
						comments as comments6, CompletedOn as completedon6, AssignedDate as assignedto6,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd6,
						au2.FirstName+' '+au2.LastName as fromae6,
						au3.FirstName+' '+au3.LastName as byae6,pt.CreatedOn as reasgnDt6
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 6) t6 on t1.TaskInstanceId1 = t6.TaskInstanceId6
					left join 
					(select tx.taskinstanceid as taskinstanceid5,au.FirstName+' '+au.LastName as decname, tx.viewid as viewid5, status as status5, 
						comments as comments5, CompletedOn as completedon5, AssignedDate as assignedto5,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd5,
						au2.FirstName+' '+au2.LastName as fromae5,
						au3.FirstName+' '+au3.LastName as byae5,pt.CreatedOn as reasgnDt5
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 5) t5 on t1.TaskInstanceId1 = t5.TaskInstanceId5
					left join 
					(select tx.taskinstanceid as taskinstanceid12,au.FirstName+' '+au.LastName as appraiser, tx.viewid as viewid12, status as status12, 
					comments as comments12, CompletedOn as completedon12,AssignedDate as assignedto12,
					case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd12,
					au2.FirstName+' '+au2.LastName as fromae12,
					au3.FirstName+' '+au3.LastName as byae12,pt.CreatedOn as reasgnDt12
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 12) t12 on t1.TaskInstanceId1 = t12.TaskInstanceId12
					join
					(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as TaskInstanceId,tk.PageTypeId,max(tk.SequenceId) seqid
					from OPAForm op join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId 
					 where  tk.PageTypeId in (5,6,7) 
					 group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber) tk on t1.TaskInstanceId1 = tk.TaskInstanceId) result

					join #ProductionData pd on result.FHANumber = pd.FHANumber
					where result.fhanumber = @FHANumber		
					
					
			update #ProductionData set AppDateFirmReviewBegan = pd.AppAssignedUWDate from #ProductionData pd where pd.FHANumber = @FHANumber
			update #ProductionData set AppDateEnteredQueue = AppDateofLenderSubmission where FHANumber = @FHANumber
								
								
			-----***** Construction Two stage application data *****------
			update #ProductionData set 				 
				AppFSProductionType = (case when a.PageTypeId = 8 then 'Construction Two Stage Final Application Request' else ''end),
				AppFSDateofLenderSubmission = (case when a.ServicerSubmissionDate is null then '' else a.ServicerSubmissionDate end),
				AppFSLenderComments = (case when a.ServicerComments is null then '' else a.ServicerComments end),
				AppFSTaskInstanceId = a.opt,
				AppFSLongTermHold = (case when sp.IsLongTermHold is null then '' when sp.IsLongTermHold = 0 then 'No' else 'Yes' end),
				AppFSDateOfHold = sp.DateOfHold,--(case when sp.DateOfHold is null then '' else sp.DateOfHold end),
				AppFSReasonForHold = (case when sp.ReasonForHold is null then '' else sp.ReasonForHold end),
				AppFSLoanCommitteApproved = (case when sp.IsLoanCommitteApproved is null then '' when sp.IsLoanCommitteApproved = 0 then 'No' else 'Yes' end),
				AppFSLoanCommitteApprovedDate = sp.LoanCommitteApprovedDate,--(case when sp.LoanCommitteApprovedDate is null then '' else sp.LoanCommitteApprovedDate end),
				AppFSFirmCommitmentIssued = (case when sp.IsFirmCommitmentIssued is null then '' when sp.IsFirmCommitmentIssued = 0 then 'No' else 'Yes' end),
				AppFSFirmCommitmentIssuedDate = sp.FirmCommitmentIssuedDate,--(case when sp.FirmCommitmentIssuedDate is null then '' else sp.FirmCommitmentIssuedDate end),
				AppFSEarlyCommencementRequested = (case when sp.IsEarlyCommencementRequested is null then '' when sp.IsEarlyCommencementRequested = 0 then 'No' else 'Yes' end),
				AppFSEarlyCommencementRequestedDate = sp.EarlyCommencementRequestedDate,--(case when sp.EarlyCommencementRequestedDate is null then '' else sp.EarlyCommencementRequestedDate end),
				AppFSEarlyCommencementIssuedDate = sp.EarlyCommencementIssuedDate,--(case when sp.EarlyCommencementIssuedDate is null then '' else sp.EarlyCommencementIssuedDate  end),
				AppFSLDLIssued = (case when sp.IsLDLIssued is null then '' when sp.IsLDLIssued = 0 then 'No' else 'Yes' end),
				AppFSLDLIssuedDate = sp.LDLIssuedDate,--(case when sp.LDLIssuedDate is null then '' else sp.LDLIssuedDate end),
				AppFSLDLCompleteResponseReceived = sp.LDLCompleteResponseReceived,--(case when sp.LDLCompleteResponseReceived is null then '' else sp.LDLCompleteResponseReceived  end),
				AppFSMiscellaneousComments = (case when sp.MiscellaneousComments is null then '' else sp.MiscellaneousComments end),
				AppFSOtherQueueWorkComments = (case when sp.OtherQueueWorkComments is null then '' else sp.OtherQueueWorkComments end),
				AppFSAccountExecutiveMiscInfo = (case when sp.SelectedAEId is null then '' else sae.SharePointAEName end),
				AppFSIremsCompleted = (case when sp.IsIremsCompleted is null then '' when sp.IsIremsCompleted = 0 then 'No' else 'Yes' end),
				AppFSAppsCompleted = (case when sp.IsAppsCompleted is null then '' when sp.IsAppsCompleted = 0 then 'No' else 'Yes' end),
				AppFSIremsPrintoutSaved = (case when sp.IsIremsPrintoutSaved is null then '' when sp.IsIremsPrintoutSaved = 0 then 'No' else 'Yes' end),
				AppFSMirandaCompletedCMSListCheck = (case when sp.IsMirandaCompletedCMSListCheck is null then '' when sp.IsMirandaCompletedCMSListCheck = 0 then 'No' else 'Yes' end),
				AppFSContractorContractPrice = (case when sp.ContractorContractPrice is null then 0 else sp.ContractorContractPrice end),
				AppFSContractorSecondaryAmtPaid = (case when sp.ContractorSecondaryAmtPaid is null then 0 else sp.ContractorSecondaryAmtPaid end),
				AppFSContractorAmountPaid = (case when sp.ContractorAmountPaid is null then 0 else sp.ContractorAmountPaid end),
				AppFSOGCComment = (case when sp.OGCComment is null then '' else sp.OGCComment end),
				AppFSEnvironmentalComment = (case when sp.EnvironmentalComment is null then '' else sp.EnvironmentalComment end),
				AppFSSurveyComment = (case when sp.SurveyComment is null then '' else sp.SurveyComment end),
				AppFSContractorComment = (case when sp.ContractorComment is null then '' else sp.ContractorComment end),
				AppFSAppraisalComment = (case when sp.AppraisalComment is null then '' else sp.AppraisalComment end),
				AppFSBackupOgcAddress = (case when sp.BackupOgcAddressId is null or sp.BackupOgcAddressId = 0 then '' end),
				AppFSOgcAddress = (case when sp.OgcAddressId is null or sp.OgcAddressId = 0 then '' end)
				from  
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as opt,tk.PageTypeId,max(tk.SequenceId) seqid,
				op.ServicerSubmissionDate,op.ServicerComments from OPAForm op  
				 join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId where  tk.PageTypeId = 8 
				 group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber,op.ServicerSubmissionDate,op.ServicerComments) a
				left join Prod_SharepointScreen sp on a.opt = sp.TaskinstanceId
				left join Prod_SharePointAccountExecutives sae on sae.SharePointAEId = sp.SelectedAEId
				join #ProductionData pd on a.FHANumber = pd.FHANumber
				where a.FhaNumber = @FHANumber

			update #ProductionData set 	
				 AppFSAssignedUWName = underwriter,
				 AppFSUWComments = comments1,
				 AppFSUWStatus = (case when status1 = 15 then 'Complete' when status1 = 18 then 'Inprocess' else '' end),
				 AppFSUWCompletedOn = completedon1,
				 AppFSAssignedUWDate = assignedto1,
				 AppFSUWIsReassigned = isreasgnd1,
				 AppFSUWReassignedFrom = fromae1,
				 AppFSUWReassignedBy = byae1,
				 AppFSUWReassignedDate = reasgnDt1,
				 AppFSAssignedEnvName = env,
				 AppFSEnvComments=comments3,
				 AppFSEnvStatus=(case when status3 = 15 then 'Complete' when status3 = 18 then 'Inprocess' else '' end),
				 AppFSEnvCompletedOn=completedon3,
				 AppFSEnvAssignedDate = assignedto3,
				 AppFSEnvIsReassigned  = isreasgnd3,
				 AppFSEnvReassignedFrom  = fromae3,
				 AppFSEnvReassignedBy  = byae3,
				 AppFSEnvReassignedDate  = reasgnDt3,
				 AppFSAssignedSurveyName=survey,
				 AppFSSurveyComments=comments2,
				 AppFSSurveyStatus=(case when status2 = 15 then 'Complete' when status2 = 18 then 'Inprocess' else '' end),
				 AppFSSurveyCompletedOn=completedon2,
				 AppFSSurveyAssignedDate=assignedto2,
				 AppFSSurveyIsReassigned  = isreasgnd2,
				 AppFSSurveyReassignedFrom  = fromae2,
				 AppFSSurveyReassignedBy  = byae2,
				 AppFSSurveyReassignedDate  = reasgnDt2,
				 AppFSAssignedAppraiserName=appraiser,
				 AppFSAppraiserComments=comments12,
				 AppFSAppraiserStatus=(case when status12 = 15 then 'Complete' when status12 = 18 then 'Inprocess' else '' end),
				 AppFSAppraiserCompletedOn=completedon12,
				 AppFSAppraiserAssignedDate=assignedto12,
				 AppFSAppraiserIsReassigned  = isreasgnd12,
				 AppFSAppraiserReassignedFrom  = fromae12,
				 AppFSAppraiserReassignedBy  = byae12,
				 AppFSAppraiserReassignedDate  = reasgnDt12,
				 AppFSAssignedOGCName=attorney,
				 AppFSOGCComments=comments4,
				 AppFSOGCStatus=(case when status4 = 15 then 'Complete' when status4 = 18 then 'Inprocess' else '' end),
				 AppFSOGCCompletedOn=completedon4,
				 AppFSOGCAssignedDate=assignedto4,
				 AppFSOGCIsReassigned  = isreasgnd4,
				 AppFSOGCReassignedFrom  = fromae4,
				 AppFSOGCReassignedBy  = byae4,
				 AppFSOGCReassignedDate  = reasgnDt4,
				 AppFSAssignedBackupOGCName=bkupogc,
				 AppFSBackupOGCComments=comments11,
				 AppFSBackupOGCStatus=(case when status11 = 15 then 'Complete' when status11 = 18 then 'Inprocess' else '' end),
				 AppFSBackupOGCCompletedOn=completedon11,
				 AppFSBackupOGCAssignedDate=assignedto11,
				 AppFSBackupOGCIsReassigned  = isreasgnd11,
				 AppFSBackupOGCReassignedFrom  = fromae11,
				 AppFSBackupOGCReassignedBy  = byae11,
				 AppFSBackupOGCReassignedDate  = reasgnDt11,
				 AppFSAssignedContractUWName=cuw,
				 AppFSContractUWComments=comments6,
				 AppFSContractUWStatus=(case when status6 = 15 then 'Complete' when status6 = 18 then 'Inprocess' else '' end),
				 AppFSContractUWCompletedOn=completedon6,
				 AppFSContractUWAssignedDate=assignedto6,
				 AppFSContractUWIsReassigned  = isreasgnd6,
				 AppFSContractUWReassignedFrom  = fromae6,
				 AppFSContractUWReassignedBy  = byae6,
				 AppFSContractUWReassignedDate  = reasgnDt6,
				 AppFSAssignedDECName=decname,
				 AppFSDECComments=comments5,
				 AppFSDECStatus=(case when status5 = 15 then 'Complete' when status5 = 18 then 'Inprocess' else '' end),
				 AppFSDECCompletedOn=completedon5,
				 AppFSDECAssignedDate=assignedto5,
				 AppFSDECIsReassigned  = isreasgnd5,
				 AppFSDECReassignedFrom  = fromae5,
				 AppFSDECReassignedBy  = byae5,
				 AppFSDECReassignedDate  = reasgnDt5

					 from 
					(select * from
					(select tx.taskinstanceid as taskinstanceid1,au.FirstName+' '+au.LastName as underwriter, tx.viewid as viewid1, status as status1, 
						comments as comments1, CompletedOn as completedon1, AssignedDate as assignedto1,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd1,
						au2.FirstName+' '+au2.LastName as fromae1,
						au3.FirstName+' '+au3.LastName as byae1,pt.CreatedOn as reasgnDt1
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 1) t1
					left join 
					(select tx.taskinstanceid as taskinstanceid2, au.FirstName+' '+au.LastName as survey, tx.viewid as viewid2, status as status2, 
						comments as comments2, CompletedOn as completedon2, AssignedDate as assignedto2,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd2,
						au2.FirstName+' '+au2.LastName as fromae2,
						au3.FirstName+' '+au3.LastName as byae2,pt.CreatedOn as reasgnDt2
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 2) t2 on t1.taskinstanceid1 = t2.taskinstanceid2
					left join 
					(select tx.taskinstanceid as taskinstanceid3,au.FirstName+' '+au.LastName as env, tx.viewid as viewid3, status as status3, 
						comments as comments3, CompletedOn as completedon3, AssignedDate as assignedto3,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd3,
						au2.FirstName+' '+au2.LastName as fromae3,
						au3.FirstName+' '+au3.LastName as byae3,pt.CreatedOn as reasgnDt3
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 3) t3 on t1.TaskInstanceId1 = t3.TaskInstanceId3
					left join 
					(select tx.taskinstanceid as taskinstanceid4,au.FirstName+' '+au.LastName as attorney, tx.viewid as viewid4, status as status4, 
						comments as comments4, CompletedOn as completedon4, AssignedDate as assignedto4,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd4,
						au2.FirstName+' '+au2.LastName as fromae4,
						au3.FirstName+' '+au3.LastName as byae4,pt.CreatedOn as reasgnDt4
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 4) t4 on t1.TaskInstanceId1 = t4.TaskInstanceId4
					left join 
					(select tx.taskinstanceid as taskinstanceid11,au.FirstName+' '+au.LastName as bkupogc, tx.viewid as viewid11, status as status11, 
						comments as comments11, CompletedOn as completedon11, AssignedDate as assignedto11,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd11,
						au2.FirstName+' '+au2.LastName as fromae11,
						au3.FirstName+' '+au3.LastName as byae11,pt.CreatedOn as reasgnDt11
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 11) t11 on t1.TaskInstanceId1 = t11.TaskInstanceId11
					left join 
					(select tx.taskinstanceid as taskinstanceid6,au.FirstName+' '+au.LastName as cuw, tx.viewid as viewid6, status as status6, 
						comments as comments6, CompletedOn as completedon6, AssignedDate as assignedto6,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd6,
						au2.FirstName+' '+au2.LastName as fromae6,
						au3.FirstName+' '+au3.LastName as byae6,pt.CreatedOn as reasgnDt6
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 6) t6 on t1.TaskInstanceId1 = t6.TaskInstanceId6
					left join 
					(select tx.taskinstanceid as taskinstanceid5,au.FirstName+' '+au.LastName as decname, tx.viewid as viewid5, status as status5, 
						comments as comments5, CompletedOn as completedon5, AssignedDate as assignedto5,
						case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd5,
						au2.FirstName+' '+au2.LastName as fromae5,
						au3.FirstName+' '+au3.LastName as byae5,pt.CreatedOn as reasgnDt5
						 from [$(TaskDB)].dbo.Prod_TaskXref tx
						join HCP_Authentication au on au.UserID = tx.AssignedTo 
						left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
						left join HCP_Authentication au2 on au2.Username = pt.FromAE 
						left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 5) t5 on t1.TaskInstanceId1 = t5.TaskInstanceId5
					left join 
					(select tx.taskinstanceid as taskinstanceid12,au.FirstName+' '+au.LastName as appraiser, tx.viewid as viewid12, status as status12, 
					comments as comments12, CompletedOn as completedon12,AssignedDate as assignedto12,
					case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd12,
					au2.FirstName+' '+au2.LastName as fromae12,
					au3.FirstName+' '+au3.LastName as byae12,pt.CreatedOn as reasgnDt12
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 12) t12 on t1.TaskInstanceId1 = t12.TaskInstanceId12
					join
					(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as TaskInstanceId,tk.PageTypeId,max(tk.SequenceId) seqid
					from OPAForm op join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId 
					 where  tk.PageTypeId = 8 
					 group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber) tk on t1.TaskInstanceId1 = tk.TaskInstanceId) result

					join #ProductionData pd on result.FHANumber = pd.FHANumber
					where result.fhanumber = @FHANumber		
					
					
			update #ProductionData set AppFSDateFirmReviewBegan = pd.AppFSAssignedUWDate from #ProductionData pd where pd.FHANumber = @FHANumber
			update #ProductionData set AppFSDateEnteredQueue = AppFSDateofLenderSubmission where FHANumber = @FHANumber
			
													
			-----*****Non-Construction Draft Closing/ Construction Initial closing Data *****-----
			update #ProductionData set 				 
				DCProductionType = (case when a.PageTypeId = 10 then 'Non-Construction Draft Closing Request' 
										 when a.PageTypeId = 11 then 'Construction Initial Closing Request' else '' end),
				DCDateofLenderSubmission = (case when a.ServicerSubmissionDate is null then '' else a.ServicerSubmissionDate end),
				DCLenderComments = (case when a.ServicerComments is null then '' else a.ServicerComments end),
				DCTaskInstanceId = a.opt,
				DCClosingComment = (case when sp.ClosingComment is null then '' else sp.ClosingComment end),
				DCContractCloserAssigned = (case when sp.IsCloserContractor = 1 then 'Yes' else 'No' end),
				DCContractorContactName = sp.ContractorContactName,
				DCCostCertReceived = (case when sp.IsCostCertRecieved = 1 then 'Yes' else 'No' end),
				DCCostCertRecievedDate = sp.CostCertRecievedDate,
				DCCostCertCompletedMIMIssued = (case when sp.IsCostCertCompletedAndIssued = 1 then 'Yes' else 'No' end),
				DCCostCertIssueDate = sp.CostCertIssueDate,
				DCTwo290Completed = (case when sp.Is290Completed = 1 then 'Yes' else 'No' end),
				DCTwo90CompletedDate = sp.Two90CompletedDate,
				DCActiveNCRE = (case when sp.IsActiveNCRE = 1 then 'Yes' else 'No' end),
				DCNCREDueDate = sp.NCREDueDate,
				DCNCREInitalBalance = sp.NCREInitalBalance,
				DCProgramSpecialist = (case when sp.ClosingProgramSpecialistId is null then '' else sae.SharePointAEName end),
				DCAccountExecutiveClosing = (case when sp.ClosingAEId is null then '' else spg.SharePointAEName end),
				DCInitialClosingDate = sp.InitialClosingDate
		
				from  
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as opt,tk.PageTypeId,max(tk.SequenceId) seqid,
				op.ServicerSubmissionDate,op.ServicerComments from OPAForm op  
					join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId where  tk.PageTypeId in (10,11) 
					group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber,op.ServicerSubmissionDate,op.ServicerComments) a
				left join DBO.Prod_SharepointScreen sp on a.opt = sp.TaskinstanceId
				left join Prod_SharePointAccountExecutives spg on spg.SharePointAEId = sp.ClosingProgramSpecialistId
				left join Prod_SharePointAccountExecutives sae on sae.SharePointAEId = sp.ClosingAEId
				left join Prod_SharePointAccountExecutives scl on scl.SharePointAEId = sp.ClosingCloserId
				join #ProductionData pd on a.FHANumber = pd.FHANumber
				where a.FhaNumber = @FHANumber

			update #ProductionData set 	
				DCAssignedCloserName = underwriter,
				DCCloserComments = comments1,
				DCCloserStatus = (case when status1 = 15 then 'Complete' when status1 = 18 then 'Inprocess' else '' end),
				DCCloserCompletedOn = completedon1,
				DCCloserAssignedDate=assignedto1,
				DCCloserIsReassigned  = isreasgnd1,
				DCCloserReassignedFrom  = fromae1,
				DCCloserReassignedBy  = byae1,
				DCCloserReassignedDate  = reasgnDt1,
				DCAssignedEnvName = env,
				DCEnvComments=comments3,
				DCEnvStatus=(case when status3 = 15 then 'Complete' when status3 = 18 then 'Inprocess' else '' end),
				DCEnvCompletedOn=completedon3,
				DCEnvAssignedDate=assignedto3,
				DCEnvIsReassigned  = isreasgnd3,
				DCEnvReassignedFrom  = fromae3,
				DCEnvReassignedBy  = byae3,
				DCEnvReassignedDate  = reasgnDt3,
				DCAssignedSurveyName=survey,
				DCSurveyComments=comments2,
				DCSurveyStatus=(case when status2 = 15 then 'Complete' when status2 = 18 then 'Inprocess' else '' end),
				DCSurveyCompletedOn=completedon2,
				DCSurveyAssignedDate=assignedto2,
				DCSurveyIsReassigned  = isreasgnd2,
				DCSurveyReassignedFrom  = fromae2,
				DCSurveyReassignedBy  = byae2,
				DCSurveyReassignedDate  = reasgnDt2,
				DCAssignedAppraiserName=appraiser,
				DCAppraiserComments=comments12,
				DCAppraiserStatus=(case when status12 = 15 then 'Complete' when status12 = 18 then 'Inprocess' else '' end),
				DCAppraiserCompletedOn=completedon12,
				DCAppraiserAssignedDate=assignedto12,
				DCAppraiserIsReassigned  = isreasgnd12,
				DCAppraiserReassignedFrom  = fromae12,
				DCAppraiserReassignedBy  = byae12,
				DCAppraiserReassignedDate  = reasgnDt12,
				DCAssignedOGCName=attorney,
				DCOGCComments=comments4,
				DCOGCStatus=(case when status4 = 15 then 'Complete' when status4 = 18 then 'Inprocess' else '' end),
				DCOGCCompletedOn=completedon4,
				DCOGCAssignedDate=assignedto4,
				DCOGCIsReassigned  = isreasgnd4,
				DCOGCReassignedFrom  = fromae4,
				DCOGCReassignedBy  = byae4,
				DCOGCReassignedDate  = reasgnDt4,
				DCAssignedBackupOGCName=bkupogc,
				DCBackupOGCComments=comments11,
				DCBackupOGCStatus=(case when status11 = 15 then 'Complete' when status11 = 18 then 'Inprocess' else '' end),
				DCBackupOGCCompletedOn=completedon11,
				DCBackupOGCAssignedDate=assignedto11,
				DCBackupOGCIsReassigned  = isreasgnd11,
				DCBackupOGCReassignedFrom  = fromae11,
				DCBackupOGCReassignedBy  = byae11,
				DCBackupOGCReassignedDate  = reasgnDt11,
				DCAssignedContractUWName=cuw,
				DCContractUWComments=comments6,
				DCContractUWStatus=(case when status6 = 15 then 'Complete' when status6 = 18 then 'Inprocess' else '' end),
				DCContractUWCompletedOn=completedon6,
				DCContractUWAssignedDate=assignedto6,
				DCContractCloserIsReassigned  = isreasgnd6,
				DCContractCloserReassignedFrom  = fromae6,
				DCContractCloserReassignedBy  = byae6,
				DCContractCloserReassignedDate  = reasgnDt6,
				DCAssignedDECName=decname,
				DCDECComments=comments5,
				DCDECStatus=(case when status5 = 15 then 'Complete' when status5 = 18 then 'Inprocess' else '' end),
				DCDECCompletedOn=completedon5,
				DCDECAssignedDate=assignedto5,
				DCDECIsReassigned  = isreasgnd5,
				DCDECReassignedFrom  = fromae5,
				DCDECReassignedBy  = byae5,
				DCDECReassignedDate  = reasgnDt5

				from 
				(select * from
				(select tx.taskinstanceid as taskinstanceid1,au.FirstName+' '+au.LastName as underwriter, tx.viewid as viewid1, status as status1, 
				comments as comments1, CompletedOn as completedon1,AssignedDate as assignedto1,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd1,
					au2.FirstName+' '+au2.LastName as fromae1,
					au3.FirstName+' '+au3.LastName as byae1,pt.CreatedOn as reasgnDt1
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 1) t1
				left join 
				(select tx.taskinstanceid as taskinstanceid2, au.FirstName+' '+au.LastName as survey, tx.viewid as viewid2, status as status2, 
				comments as comments2, CompletedOn as completedon2,AssignedDate as assignedto2,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd2,
					au2.FirstName+' '+au2.LastName as fromae2,
					au3.FirstName+' '+au3.LastName as byae2,pt.CreatedOn as reasgnDt2
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy
				where tx.ViewId = 2) t2 on t1.taskinstanceid1 = t2.taskinstanceid2
				left join 
				(select tx.taskinstanceid as taskinstanceid3,au.FirstName+' '+au.LastName as env, tx.viewid as viewid3, status as status3, 
				comments as comments3, CompletedOn as completedon3,AssignedDate as assignedto3,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd3,
					au2.FirstName+' '+au2.LastName as fromae3,
					au3.FirstName+' '+au3.LastName as byae3,pt.CreatedOn as reasgnDt3
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 3) t3 on t1.TaskInstanceId1 = t3.TaskInstanceId3
				left join 
				(select tx.taskinstanceid as taskinstanceid4,au.FirstName+' '+au.LastName as attorney, tx.viewid as viewid4, status as status4, 
				comments as comments4, CompletedOn as completedon4,AssignedDate as assignedto4, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd4,
					au2.FirstName+' '+au2.LastName as fromae4,
					au3.FirstName+' '+au3.LastName as byae4,pt.CreatedOn as reasgnDt4
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 4) t4 on t1.TaskInstanceId1 = t4.TaskInstanceId4
				left join 
				(select tx.taskinstanceid as taskinstanceid11,au.FirstName+' '+au.LastName as bkupogc, tx.viewid as viewid11, status as status11, 
				comments as comments11, CompletedOn as completedon11,AssignedDate as assignedto11, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd11,
					au2.FirstName+' '+au2.LastName as fromae11,
					au3.FirstName+' '+au3.LastName as byae11,pt.CreatedOn as reasgnDt11
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 11) t11 on t1.TaskInstanceId1 = t11.TaskInstanceId11
				left join 
				(select tx.taskinstanceid as taskinstanceid6,au.FirstName+' '+au.LastName as cuw, tx.viewid as viewid6, status as status6, 
				comments as comments6, CompletedOn as completedon6,AssignedDate as assignedto6, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd6,
					au2.FirstName+' '+au2.LastName as fromae6,
					au3.FirstName+' '+au3.LastName as byae6,pt.CreatedOn as reasgnDt6
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 6) t6 on t1.TaskInstanceId1 = t6.TaskInstanceId6
				left join 
				(select tx.taskinstanceid as taskinstanceid5,au.FirstName+' '+au.LastName as decname, tx.viewid as viewid5, status as status5, 
				comments as comments5, CompletedOn as completedon5,AssignedDate as assignedto5, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd5,
					au2.FirstName+' '+au2.LastName as fromae5,
					au3.FirstName+' '+au3.LastName as byae5,pt.CreatedOn as reasgnDt5
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 5) t5 on t1.TaskInstanceId1 = t5.TaskInstanceId5
				left join  
				(select tx.taskinstanceid as taskinstanceid12,au.FirstName+' '+au.LastName as appraiser, tx.viewid as viewid12, status as status12, 
				comments as comments12, CompletedOn as completedon12,AssignedDate as assignedto12, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd12,
					au2.FirstName+' '+au2.LastName as fromae12,
					au3.FirstName+' '+au3.LastName as byae12,pt.CreatedOn as reasgnDt12
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 12) t12 on t1.TaskInstanceId1 = t12.TaskInstanceId12
				join
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as TaskInstanceId,tk.PageTypeId,max(tk.SequenceId) seqid
				from OPAForm op join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId 
					where  tk.PageTypeId in (10,11)
					group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber) tk on t1.TaskInstanceId1 = tk.TaskInstanceId) result

				join #ProductionData pd on result.FHANumber = pd.FHANumber
				where result.fhanumber = @FHANumber

			update #ProductionData set DCClosingPackageRecievedDate = DCDateofLenderSubmission from #ProductionData where Fhanumber = @FHANumber
			update #ProductionData set DCCloser = DCAssignedCloserName from #ProductionData where Fhanumber = @FHANumber

			-----***** Non-Construction Executed Closing *****-----
			update #ProductionData set 				 
				ECProductionType = (case when a.PageTypeId = 17 then 'Non-Construction Executed Closing Request'
										 when a.PageTypeId = 12 then 'Construction Final Closing Request' else ''end),
				ECDateofLenderSubmission = (case when a.ServicerSubmissionDate is null then '' else a.ServicerSubmissionDate end),
				ECLenderComments = (case when a.ServicerComments is null then '' else a.ServicerComments end),
				ECTaskInstanceId = a.opt,
				ECClosingComment = (case when sp.ClosingComment is null then '' else sp.ClosingComment end),
				ECContractCloserAssigned = (case when sp.IsCloserContractor = 1 then 'Yes' else 'No' end),
				ECContractorContactName = sp.ContractorContactName,
				ECCostCertReceived = (case when sp.IsCostCertRecieved = 1 then 'Yes' else 'No' end),
				ECCostCertRecievedDate = sp.CostCertRecievedDate,
				ECCostCertCompletedMIMIssued = (case when sp.IsCostCertCompletedAndIssued = 1 then 'Yes' else 'No' end),
				ECCostCertIssueDate = sp.CostCertIssueDate,
				ECTwo290Completed = (case when sp.Is290Completed = 1 then 'Yes' else 'No' end),
				ECTwo90CompletedDate = sp.Two90CompletedDate,
				ECActiveNCRE = (case when sp.IsActiveNCRE = 1 then 'Yes' else 'No' end),
				ECNCREDueDate = sp.NCREDueDate,
				ECNCREInitalBalance = sp.NCREInitalBalance,
				ECClosingPackageRecievedDate = sp.ClosingPackageRecievedDate,
				ECProgramSpecialist = (case when sp.ClosingProgramSpecialistId is null then '' else sae.SharePointAEName end),
				ECAccountExecutiveClosing = (case when sp.ClosingAEId is null then '' else spg.SharePointAEName end),
				ECCloser = (case when sp.ClosingCloserId is null then '' else scl.SharePointAEName end),
				ECInitialClosingDate = sp.InitialClosingDate
		
				from  
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as opt,tk.PageTypeId,max(tk.SequenceId) seqid,
				op.ServicerSubmissionDate,op.ServicerComments from OPAForm op  
					join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId where  tk.PageTypeId in (17,12) 
					group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber,op.ServicerSubmissionDate,op.ServicerComments) a
				left join DBO.Prod_SharepointScreen sp on a.opt = sp.TaskinstanceId
				left join Prod_SharePointAccountExecutives spg on spg.SharePointAEId = sp.ClosingProgramSpecialistId
				left join Prod_SharePointAccountExecutives sae on sae.SharePointAEId = sp.ClosingAEId
				left join Prod_SharePointAccountExecutives scl on scl.SharePointAEId = sp.ClosingCloserId
				join #ProductionData pd on a.FHANumber = pd.FHANumber
				where a.FhaNumber = @FHANumber

			update #ProductionData set 	
				ECAssignedCloserName = underwriter,
				ECCloserComments = comments1,
				ECCloserStatus = (case when status1 = 15 then 'Complete' when status1 = 18 then 'Inprocess' else '' end),
				ECCloserCompletedOn = completedon1,
				ECCloserAssignedDate=assignedto1,
				ECCloserIsReassigned  = isreasgnd1,
				ECCloserReassignedFrom  = fromae1,
				ECCloserReassignedBy  = byae1,
				ECCloserReassignedDate  = reasgnDt1,
				ECAssignedEnvName = env,
				ECEnvComments=comments3,
				ECEnvStatus=(case when status3 = 15 then 'Complete' when status3 = 18 then 'Inprocess' else '' end),
				ECEnvCompletedOn=completedon3,
				ECEnvAssignedDate=assignedto3,
				ECEnvIsReassigned  = isreasgnd3,
				ECEnvReassignedFrom  = fromae3,
				ECEnvReassignedBy  = byae3,
				ECEnvReassignedDate  = reasgnDt3,
				ECAssignedSurveyName=survey,
				ECSurveyComments=comments2,
				ECSurveyStatus=(case when status2 = 15 then 'Complete' when status2 = 18 then 'Inprocess' else '' end),
				ECSurveyCompletedOn=completedon2,
				ECSurveyAssignedDate=assignedto2,
				ECSurveyIsReassigned  = isreasgnd2,
				ECSurveyReassignedFrom  = fromae2,
				ECSurveyReassignedBy  = byae2,
				ECSurveyReassignedDate  = reasgnDt2,
				ECAssignedAppraiserName=appraiser,
				ECAppraiserComments=comments12,
				ECAppraiserStatus=(case when status12 = 15 then 'Complete' when status12 = 18 then 'Inprocess' else '' end),
				ECAppraiserCompletedOn=completedon12,
				ECAppraiserAssignedDate=assignedto12,
				ECAppraiserIsReassigned  = isreasgnd12,
				ECAppraiserReassignedFrom  = fromae12,
				ECAppraiserReassignedBy  = byae12,
				ECAppraiserReassignedDate  = reasgnDt12,
				ECAssignedOGCName=attorney,
				ECOGCComments=comments4,
				ECOGCStatus=(case when status4 = 15 then 'Complete' when status4 = 18 then 'Inprocess' else '' end),
				ECOGCCompletedOn=completedon4,
				ECOGCAssignedDate=assignedto4,
				ECOGCIsReassigned  = isreasgnd4,
				ECOGCReassignedFrom  = fromae4,
				ECOGCReassignedBy  = byae4,
				ECOGCReassignedDate  = reasgnDt4,
				ECAssignedBackupOGCName=bkupogc,
				ECBackupOGCComments=comments11,
				ECBackupOGCStatus=(case when status11 = 15 then 'Complete' when status11 = 18 then 'Inprocess' else '' end),
				ECBackupOGCCompletedOn=completedon11,
				ECBackupOGCAssignedDate=assignedto11,
				ECBackupOGCIsReassigned  = isreasgnd11,
				ECBackupOGCReassignedFrom  = fromae11,
				ECBackupOGCReassignedBy  = byae11,
				ECBackupOGCReassignedDate  = reasgnDt11,
				ECAssignedContractUWName=cuw,
				ECContractUWComments=comments6,
				ECContractUWStatus=(case when status6 = 15 then 'Complete' when status6 = 18 then 'Inprocess' else '' end),
				ECContractUWCompletedOn=completedon6,
				ECContractUWAssignedDate=assignedto6,
				ECContractCloserIsReassigned  = isreasgnd6,
				ECContractCloserReassignedFrom  = fromae6,
				ECContractCloserReassignedBy  = byae6,
				ECContractCloserReassignedDate  = reasgnDt6,
				ECAssignedDECName=decname,
				ECDECComments=comments5,
				ECDECStatus=(case when status5 = 15 then 'Complete' when status5 = 18 then 'Inprocess' else '' end),
				ECDECCompletedOn=completedon5,
				ECDECAssignedDate=assignedto5,
				ECDECIsReassigned  = isreasgnd5,
				ECDECReassignedFrom  = fromae5,
				ECDECReassignedBy  = byae5,
				ECDECReassignedDate  = reasgnDt5

				from 
				(select * from
				(select tx.taskinstanceid as taskinstanceid1,au.FirstName+' '+au.LastName as underwriter, tx.viewid as viewid1, status as status1, 
				comments as comments1, CompletedOn as completedon1,AssignedDate as assignedto1,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd1,
					au2.FirstName+' '+au2.LastName as fromae1,
					au3.FirstName+' '+au3.LastName as byae1,pt.CreatedOn as reasgnDt1
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 1) t1
				left join 
				(select tx.taskinstanceid as taskinstanceid2, au.FirstName+' '+au.LastName as survey, tx.viewid as viewid2, status as status2, 
				comments as comments2, CompletedOn as completedon2,AssignedDate as assignedto2,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd2,
					au2.FirstName+' '+au2.LastName as fromae2,
					au3.FirstName+' '+au3.LastName as byae2,pt.CreatedOn as reasgnDt2
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy
				where tx.ViewId = 2) t2 on t1.taskinstanceid1 = t2.taskinstanceid2
				left join 
				(select tx.taskinstanceid as taskinstanceid3,au.FirstName+' '+au.LastName as env, tx.viewid as viewid3, status as status3, 
				comments as comments3, CompletedOn as completedon3,AssignedDate as assignedto3,
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd3,
					au2.FirstName+' '+au2.LastName as fromae3,
					au3.FirstName+' '+au3.LastName as byae3,pt.CreatedOn as reasgnDt3
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 3) t3 on t1.TaskInstanceId1 = t3.TaskInstanceId3
				left join 
				(select tx.taskinstanceid as taskinstanceid4,au.FirstName+' '+au.LastName as attorney, tx.viewid as viewid4, status as status4, 
				comments as comments4, CompletedOn as completedon4,AssignedDate as assignedto4, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd4,
					au2.FirstName+' '+au2.LastName as fromae4,
					au3.FirstName+' '+au3.LastName as byae4,pt.CreatedOn as reasgnDt4
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 4) t4 on t1.TaskInstanceId1 = t4.TaskInstanceId4
				left join 
				(select tx.taskinstanceid as taskinstanceid11,au.FirstName+' '+au.LastName as bkupogc, tx.viewid as viewid11, status as status11, 
				comments as comments11, CompletedOn as completedon11,AssignedDate as assignedto11, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd11,
					au2.FirstName+' '+au2.LastName as fromae11,
					au3.FirstName+' '+au3.LastName as byae11,pt.CreatedOn as reasgnDt11
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 11) t11 on t1.TaskInstanceId1 = t11.TaskInstanceId11
				left join 
				(select tx.taskinstanceid as taskinstanceid6,au.FirstName+' '+au.LastName as cuw, tx.viewid as viewid6, status as status6, 
				comments as comments6, CompletedOn as completedon6,AssignedDate as assignedto6, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd6,
					au2.FirstName+' '+au2.LastName as fromae6,
					au3.FirstName+' '+au3.LastName as byae6,pt.CreatedOn as reasgnDt6
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 6) t6 on t1.TaskInstanceId1 = t6.TaskInstanceId6
				left join 
				(select tx.taskinstanceid as taskinstanceid5,au.FirstName+' '+au.LastName as decname, tx.viewid as viewid5, status as status5, 
				comments as comments5, CompletedOn as completedon5,AssignedDate as assignedto5, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd5,
					au2.FirstName+' '+au2.LastName as fromae5,
					au3.FirstName+' '+au3.LastName as byae5,pt.CreatedOn as reasgnDt5
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 5) t5 on t1.TaskInstanceId1 = t5.TaskInstanceId5
				left join  
				(select tx.taskinstanceid as taskinstanceid12,au.FirstName+' '+au.LastName as appraiser, tx.viewid as viewid12, status as status12, 
				comments as comments12, CompletedOn as completedon12,AssignedDate as assignedto12, 
				case when pt.Taskinstanceid is not null then 'Yes' end as isreasgnd12,
					au2.FirstName+' '+au2.LastName as fromae12,
					au3.FirstName+' '+au3.LastName as byae12,pt.CreatedOn as reasgnDt12
					 from [$(TaskDB)].dbo.Prod_TaskXref tx
					join HCP_Authentication au on au.UserID = tx.AssignedTo 
					left join [$(TaskDB)].dbo.Prod_TaskReAssignments pt on pt.Taskinstanceid = tx.TaskXrefid 
					left join HCP_Authentication au2 on au2.Username = pt.FromAE 
					left join HCP_Authentication au3 on au3.UserID = pt.CreatedBy where tx.ViewId = 12) t12 on t1.TaskInstanceId1 = t12.TaskInstanceId12
				join
				(select op.fhanumber, op.RequestStatus,op.TaskInstanceId as TaskInstanceId,tk.PageTypeId,max(tk.SequenceId) seqid
				from OPAForm op join [$(TaskDB)].dbo.Task tk on op.TaskInstanceId = tk.TaskInstanceId 
					where  tk.PageTypeId in (17,12)
					group by op.TaskInstanceId,tk.PageTypeId, op.RequestStatus,op.fhanumber) tk on t1.TaskInstanceId1 = tk.TaskInstanceId) result

				join #ProductionData pd on result.FHANumber = pd.FHANumber
				where result.fhanumber = @FHANumber

			update #ProductionData set ECClosingPackageRecievedDate = ECDateofLenderSubmission from #ProductionData where Fhanumber = @FHANumber
			update #ProductionData set ECCloser = ECAssignedCloserName from #ProductionData where Fhanumber = @FHANumber

			



		END


			select * from #ProductionData


END