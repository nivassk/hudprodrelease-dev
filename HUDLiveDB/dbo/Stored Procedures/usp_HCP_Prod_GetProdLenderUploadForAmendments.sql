﻿
Create PROCEDURE [dbo].[usp_HCP_Prod_GetProdLenderUploadForAmendments]
(
@TaskInstanceId uniqueidentifier,
@UserId int,
@PropertyId varchar(10) = null,
@FHANumber varchar(10) = null
)
AS

BEGIN


				Select convert(nvarchar(max),fs.[FolderKey] )as id,
			fs.[FolderName] as folderNodeName,
			fs.[level] as [level],
			fs.[FolderSortingNumber] as sortorder, 
			fs.[SubfolderSequence] as SubfolderSequence, 
			convert(nvarchar(max),fs.[ParentKey] )as parent,
			cast(0 as bit) as isLeaf,
			cast(1 as bit) as expanded,
			cast(1 as bit) as loaded,
			null as fileId,
			null as fileName,
			null as fileSize,
			null as uploadDate,
			null as DocTypeID,
			0 as CreatedBy,
			0 as PageTypeId	
			
			from [$(TaskDB)].dbo.Prod_FolderStructure fs  where fs.[FolderKey] not in (1,2) 
			and fs.ViewTypeId =4
			--from [$(TaskDB)].dbo.Prod_FolderStructure fs  where fs.[FolderKey] not in (1,2) and fs.ViewTypeId!=2 and fs.ViewTypeId!=3 
			
			union
			Select convert(nvarchar(max),sfs.[FolderKey] )as id,
			sfs.[FolderName] as folderNodeName,
			cast(0 as bit) as [level],
			sfs.[FolderSortingNumber] as sortorder, 
			sfs.[SubfolderSequence] as SubfolderSequence,
			null as parent, 
			cast(0 as bit) as isLeaf,
			cast(1 as bit) as expanded,
			cast(1 as bit) as loaded,
			null as fileId,
			null as fileName,
			null as fileSize,
			null as uploadDate,
			null as DocTypeID,
			0 as CreatedBy,
			ISNULL(sfs.PageTypeId, 0 ) as PageTypeId
			from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
			join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.FolderKey = sfs.ParentKey
			 where sfs.ParentKey = fs.FolderKey and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber
			
			union

			Select convert(nvarchar(max),tf.[FolderKey] ) +'_' +convert(nvarchar(max), tfile.[FileId] )as id,
			 tfile.[Filename] as folderNodeName,
			cast(1 as bit) as [level],
			fs.[FolderSortingNumber] as sortorder, 
			fs.[SubfolderSequence] as SubfolderSequence,
			convert(nvarchar(max),tf.[FolderKey] )as parent,
			cast(1 as bit) as isLeaf,
			cast(1 as bit) as expanded,
			cast(1 as bit) as loaded,
			tfile.[fileId] as fileId,
			tfile.[fileName] as fileName,
			tfile.[fileSize] as fileSize,
			tfile.[uploadTime] as uploadDate,
			tfile.[DocTypeID] as DocTypeID,
			-- validating added created by
			tfile.CreatedBy as CreatedBy,
			0 as PageTypeId
			from [$(TaskDB)].dbo.Prod_FolderStructure fs 
			inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
			inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
			where tfile.TaskInstanceId = @TaskInstanceId
			
			union

			Select convert(nvarchar(max),tf.[FolderKey] ) +'_' +convert(nvarchar(max), tfile.[FileId] )as id,
			 tfile.[Filename] as folderNodeName,
			cast(1 as bit) as [level],
			sfs.[FolderSortingNumber] as sortorder, 
			sfs.[SubfolderSequence] as SubfolderSequence,
			convert(nvarchar(max),tf.[FolderKey] )as parent,
			cast(1 as bit) as isLeaf,
			cast(1 as bit) as expanded,
			cast(1 as bit) as loaded,
			tfile.[fileId] as fileId,
			tfile.[fileName] as fileName,
			tfile.[fileSize] as fileSize,
			tfile.[uploadTime] as uploadDate,
			tfile.[DocTypeID] as DocTypeID,
			tfile.CreatedBy as CreatedBy,
			ISNULL(sfs.PageTypeId, 0 ) as PageTypeId
			from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
			inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
			inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId				
			where tfile.TaskInstanceId = @TaskInstanceId 
			
			and tfile.CreatedBy=@UserId order by fs.[FolderSortingNumber], fs.[SubfolderSequence]  
			  

		
end
