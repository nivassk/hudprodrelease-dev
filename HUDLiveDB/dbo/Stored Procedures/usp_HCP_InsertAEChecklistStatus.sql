﻿
-- =============================================
-- Author:		Muni
-- Create date: 01/12/2016
-- Description:	inserting all checklist items for AE
-- =============================================
 
CREATE PROCEDURE [dbo].[usp_HCP_InsertAEChecklistStatus] (
	-- Add the parameters for the stored procedure here
	@ProjectActionFormId  uniqueidentifier,
	@ProjectActionTypeId int,
	@UserId int
	)
AS
BEGIN
 
	SET NOCOUNT ON;

	----testing
--	set @ProjectActionFormId = 'AA062F0E-12A3-4325-B4E8-11209515403A'
	--set @ProjectActionTypeId = 30
	---testing
    -- Insert statements for procedure here
	-- select * from projectactionforms
	if NOT EXISTS(SELECT 1 from dbo.ProjectActionAEFormStatus WHERE ProjectActionFormId = @ProjectActionFormId)
	BEGIN
	   BEGIN TRY
		BEGIN TRANSACTION
		  INSERT INTO dbo.ProjectActionAEFormStatus(ProjectActionFormId,
													CheckListId,
													RequestStatus,
													ModifiedOn,
													ModifiedBy)
													select 
													@ProjectActionFormId, 
													CheckListId,
													null,
													getdate(),
													@UserId
													FROM [dbo].[CheckListQuestions] 
													WHERE ProjectActionId = @ProjectActionTypeId and IsAttachmentRequired = 1
          COMMIT TRANSACTION
		 END TRY
		 BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				DECLARE @Err nvarchar(1000)
				SET @Err = ERROR_MESSAGE()
					RAISERROR (@Err,16,1)
		 END CATCH
	END

END


GO

