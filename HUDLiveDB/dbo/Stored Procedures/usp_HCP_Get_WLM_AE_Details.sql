﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_WLM_AE_Details]
(
@Username  NVARCHAR(100)
)

AS
SET FMTONLY OFF
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #ManagerDetails
	(
		HudWorkLoadManagerID INT NULL,
		HudWorkLoadManagerName NVARCHAR(100) NULL,
		HudWorkLoadManagerEmailAddress NVARCHAR(100) NULL,	
		HudProjectManagerID INT NULL,
		HudProjectManagerName NVARCHAR(100) NULL,
		HudProjectManagerEmailAddress NVARCHAR(100) NULL
	);

	INSERT INTO #ManagerDetails

	SELECT DISTINCT WLM.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name,AD2.Email,
	AE.HUD_Project_Manager_ID,AE.HUD_Project_Manager_Name,AD1.Email
	FROM ProjectInfo PF
	INNER JOIN HUD_Project_Manager AE
	ON PF.HUD_Project_Manager_ID = AE.HUD_Project_Manager_ID
	INNER JOIN HUD_WorkLoad_Manager WLM 
	ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
	INNER JOIN Address AD1
	ON AE.AddressID = AD1.AddressID
	INNER JOIN Address AD2
	ON WLM.AddressID = AD2.AddressID
	where (AD1.Email = @Username OR AD2.Email = @Username)

	SELECT * FROM #ManagerDetails
END
SET FMTONLY ON

GO

