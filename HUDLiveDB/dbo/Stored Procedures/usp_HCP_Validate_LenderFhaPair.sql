﻿
CREATE PROCEDURE [dbo].[usp_HCP_Validate_LenderFhaPair]
(
@LenderFhaPairs XML
)
AS

;WITH ValPairs AS
(
    SELECT
        LenderID = Pair.value('@LenderId', 'int'),
        FhaNumber = Pair.value('@FhaNumber', 'varchar(100)')
    FROM @LenderFhaPairs.nodes('/LenderFhaCol/LenderFhaPairs/LenderFhaPairModel') AS Tbl(Pair)
)

SELECT 
    a.FhaNumber 
FROM ValPairs a
LEFT JOIN Lender_FHANumber b
ON a.LenderID=b.LenderID and a.FhaNumber=b.FHANumber and b.FHA_EndDate is null
WHERE b.LenderID IS NULL
GO

