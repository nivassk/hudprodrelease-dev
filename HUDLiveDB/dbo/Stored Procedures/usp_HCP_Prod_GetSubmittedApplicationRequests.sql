﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetSubmittedApplicationRequests]

AS


		  select '(' +opa.fhaNumber +')'+ PA.ProjectTypename as ProjectName,null as LenderContactName,
	           lender.Lender_Name as LenderName,null as FhaRequestInstanceId,opa.TaskInstanceId,
	          users.FirstName+ ' '+users.LastName as CreatedBy,
			   PA.ProjectTypename as LoanType,
			 opa.ModifiedOn,
		 case when fhaRequest.IsLIHTC is null then '0'  else fhaRequest.IsLIHTC end as IsLIHTC,
			cast(0 as bit) as IsTaxExempted,fhaRequest.LoanAmount
	     FROM [$(DatabaseName)].dbo.opaform opa
	      join [$(DatabaseName)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(DatabaseName)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
	      join [$(TaskDB)].dbo.Task tk on tk.TaskinstanceId=opa.TaskInstanceId
		   left join [$(DatabaseName)].[dbo] .[Prod_ProjectType]  PA
           on PA. ProjecttypeId=opa.Projectactiontypeid
	       left join [$(DatabaseName)].dbo.HCP_Authentication users on users.UserID=opa.createdby
		   order by case when PA.ProjectTypename in ('Construction 241(a)','Construction NC','Construction SR') then fhaRequest.IsLIHTC end desc,fhaRequest.ModifiedOn desc
		  
		


	


GO

