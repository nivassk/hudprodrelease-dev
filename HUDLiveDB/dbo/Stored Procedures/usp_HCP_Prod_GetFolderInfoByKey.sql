﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetFolderInfoByKey]
(
@FolderKey int
)
AS

BEGIN

SELECT sf.[FolderKey]
      ,sf.[FolderName]
      ,sf.[ParentKey]
      ,sf.[SubfolderSequence]
	   ,ISNULL(sf.CreatedBy, 0 ) as CreatedBy
	  ,ISNULL(sf.CreatedOn, 0) as CreatedOn
	  ,0 as ViewTypeId
      
  FROM [$(TaskDB)].[dbo].[Prod_SubFolderStructure] sf where sf.[FolderKey] = @FolderKey 
  union
   
	SELECT f.[FolderKey]
      ,f.[FolderName]
      ,f.[ParentKey]
      ,f.[SubfolderSequence]
	   ,0 as CreatedBy
	  ,0 as CreatedOn
	  ,f.ViewTypeId
    
  FROM [$(TaskDB)].[dbo].[Prod_FolderStructure] f where f.[FolderKey] = @FolderKey 		  

		
end




GO

