﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetProductionUsers] as

SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.PreferredTimeZone,
	MR.RoleName, 	
	A.UserID
FROM [$(DatabaseName)].[dbo].[HCP_Authentication] A
INNER JOIN [$(DatabaseName)].[dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [$(DatabaseName)].[dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
where MR.RoleName='ProductionUser' or MR.RoleName='ProductionWlm' order by A.FirstName




GO

