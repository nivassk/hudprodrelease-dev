﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetRAIEdit]
(
@TaskInstanceId uniqueidentifier,
@PropertyId varchar(10) = null,
@FHANumber varchar(10) = null
)
AS
BEGIN

declare @PageTypeId int

 select @PageTypeId=(select top 1 PageTypeId from [$(TaskDB)].[dbo].[Task] where TaskInstanceId=@TaskInstanceId)
 if(@PageTypeId=18)

 begin
 -- This section only for Amendment
   --Get folder structure for the RAI files from parent task file ids   
       select distinct convert(nvarchar(max),fs.[FolderKey] )as id,
                                  fs.[FolderName] as folderNodeName,
                                 fs.[level] as [level],
                                  convert(nvarchar(max),fs.[ParentKey] )as parent,
                                  cast(0 as bit) as isLeaf,
                                  cast(0 as bit) as expanded,
                                  cast(1 as bit) as loaded,
                                  null as actionTaken,
                                  null as name,
                                  null as userRole,
                                  null as comment,
                                  null as [fileName],
                                  null as fileSize,
                                  null as uploadDate,
                                  null as submitDate,
                                  null as fileId,
                                  null as childFileId,
                                  cast(0 as bit) as editable,
                                  GETDATE() as actionDate,
                                  null as nfrComment,
                                  cast(0 as bit) as isFileHistory,
                                  cast(0 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  null as childFileName,
								  null as DocTypeID,
								convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
								convert(nvarchar(max),fs.FolderSortingNumber ) as FolderSortingNumber,
								fs.[SubfolderSequence] as SubfolderSequence 
    from [$(TaskDB)].dbo.Prod_FolderStructure fs 
    inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
    inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
       inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
       where ctf.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3 and fs.ViewTypeId =4
 union
  --Get subfolder structure for the RAI files from parent task file ids
			 select distinct convert(nvarchar(max),sfs.[FolderKey] )as id,
                                  sfs.[FolderName] as folderNodeName,
                                  1 as [level],
                                  convert(nvarchar(max),sfs.[ParentKey] )as parent,
                                  cast(0 as bit) as isLeaf,
                                  cast(0 as bit) as expanded,
                                  cast(1 as bit) as loaded,
                                  null as actionTaken,
                                  null as name,
                                  null as userRole,
                                  null as comment,
                                  null as [fileName],
                                  null as fileSize,
                                  null as uploadDate,
                                  null as submitDate,
                                  null as fileId,
                                  null as childFileId,
                                  cast(0 as bit) as editable,
                                  GETDATE() as actionDate,
                                  null as nfrComment,
                                  cast(0 as bit) as isFileHistory,
                                  cast(0 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  null as childFileName,
								  null as DocTypeID,
								convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
								sfs.FolderSortingNumber as FolderSortingNumber,
								sfs.[SubfolderSequence] as SubfolderSequence 
			 
			 from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
             inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
             inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
             inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
             where ctf.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber
			
			
       union

       --Get RAI files details from parent task file   and new version of file uploaded for RAI request
       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
                     tfparent.[Filename] as folderNodeName,
                     cast(1 as bit) as [level],
                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                                  'Request Addtional Information' as actionTaken,
                                (A.FirstName + ' ' + A.LastName) as name,
                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
                                  null as comment,
                                  tfparent.[FileName]  as [fileName],
                                  newFilesInChild.FileSize as fileSize,
                               newFilesInChild.UploadTime as uploadDate,
                             null as submitDate,
                             tfparent.FileId as fileId ,
                             newFilesInChild.FileId as childFileId,
                                cast(1 as bit) as editable,--to remove
                          tfparent.UploadTime as actionDate,
                       null as nfrComment,
                                  CAST( 1 as bit) as isFileHistory,
                                  CAST( 1 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  newFilesInChild.[FileName] as childFileName,
								  tfparent.[DocTypeID] as DocTypeID,
								  null as FolderKey,
								  null as FolderSortingNumber,
								  null as SubfolderSequence 
              from [$(TaskDB)].dbo.TaskFile tfparent
              --Table joins to get all file list in its parent task 
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
              inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			 
              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
              --Table joins to filtr and fet file list in its child task
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
              --Table joins to get Lender details
             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              --Table join to get latest version of uploaded file in its child task
              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
                                  from [$(TaskDB)].dbo.TaskFileHistory
                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3 and fs.ViewTypeId =4
              union

       --Get RAI subfolder files details from parent task file   and new version of file uploaded for RAI request
       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
                     tfparent.[Filename] as folderNodeName,
                     cast(1 as bit) as [level],
                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                                  'Request Addtional Information' as actionTaken,
                                (A.FirstName + ' ' + A.LastName) as name,
                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
                                  null as comment,
                                  tfparent.[FileName]  as [fileName],
                                  newFilesInChild.FileSize as fileSize,
                               newFilesInChild.UploadTime as uploadDate,
                             null as submitDate,
                             tfparent.FileId as fileId ,
                             newFilesInChild.FileId as childFileId,
                                cast(1 as bit) as editable,--to remove
                          tfparent.UploadTime as actionDate,
                       null as nfrComment,
                                  CAST( 1 as bit) as isFileHistory,
                                  CAST( 1 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  newFilesInChild.[FileName] as childFileName,
								  tfparent.[DocTypeID] as DocTypeID,
								  null as FolderKey,
								  null as FolderSortingNumber,
								  null as SubfolderSequence 
              from [$(TaskDB)].dbo.TaskFile tfparent
              --Table joins to get all file list in its parent task 
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
              --inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			  inner join [$(TaskDB)].dbo.Prod_SubFolderStructure sfs on sfs.[FolderKey] = tfm.[FolderKey]
              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
              --Table joins to filtr and fet file list in its child task
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
              --Table joins to get Lender details
             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              --Table join to get latest version of uploaded file in its child task
              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
                                  from [$(TaskDB)].dbo.TaskFileHistory
                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
              where pc.ChildTaskInstanceId = @TaskInstanceId 
              union 
              --Get New file request 
              select distinct  '0_1_00' as id, 
                                          'New File Request(s)' as folderNodeName,
                                         0 as [level],
                                         null as parent,
                                         cast(0 as bit) as isLeaf,
                                         cast(0 as bit) as expanded,
                                         cast(1 as bit) as loaded,
                                         'New File Request(s)' as actionTaken,
                                           (A.FirstName + ' ' + A.LastName) as name,
                                         r.RoleName as userRole,
                                         nfr.Comments as comment,
                                         null as [fileName],
                                         null as fileSize,
                                         null as uploadDate,
                                         nfr.RequestedOn  as submitDate,
                                         null as fileId,
                                         null as childFileId,
                                         cast(0 as bit) as editable,
                                         GETDATE() as actionDate,
                                         null as nfrComment,
                                         cast(0 as bit) as isFileHistory,
                                         cast(0 as bit) as isFileComment,
                                         cast(0 as bit) as isApprove,
                                         null as childFileName,
										  null as DocTypeID,
										  null as FolderKey,
										  0 as FolderSortingNumber,
										  null as SubfolderSequence 
              from [$(TaskDB)].[dbo].[NewFileRequest] nfr
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
              inner join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.TaskXrefid = pc.ParentTaskInstanceId
              inner join HCP_Authentication a on a.UserID = xref.AssignedTo
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              where pc.ChildTaskInstanceId = @TaskInstanceId and  r.RoleName in ('ProductionUser','ProductionWlm')  

              union 

              --Get folder structure for  uploaded file in new file request
              select distinct '0_1_'+ convert(nvarchar(max),fs.[FolderKey] )as id,
                                         fs.[FolderName] as folderNodeName,
                                         1 as [level],
                                         '0_1_00'as parent,
                                         cast(0 as bit) as isLeaf,
                                         cast(0 as bit) as expanded,
                                         cast(1 as bit) as loaded,
                                         'New File Request(s)' as actionTaken,
                                         null as name,
                                         null as userRole,
                                         null as comment,
                                         null as [fileName],
                                         null as fileSize,
                                         null as uploadDate,
                                         null as submitDate,
                                         null as fileId,
                                         null as childFileId,
                                         cast(0 as bit) as editable,
                                         GETDATE() as actionDate,
                                         null as nfrComment,
                                         cast(0 as bit) as isFileHistory,
                                         cast(0 as bit) as isFileComment,
                                         cast(0 as bit) as isApprove,
                                         null as childFileName,
										  null as DocTypeID,
										  convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
										  fs.FolderSortingNumber as FolderSortingNumber,
										  fs.[SubfolderSequence] as SubfolderSequence 
              from [$(TaskDB)].dbo.Prod_FolderStructure fs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.FolderKey !=1 and fs.FolderKey !=2  and fs.ViewTypeId!=2 and fs.ViewTypeId!=3 and fs.ViewTypeId =4
			  union 
              --Get subfolder structure for  uploaded file in new file request
            --  select distinct '0_1_'+ convert(nvarchar(max),sfs.[FolderKey] )as id,
            --                             sfs.[FolderName] as folderNodeName,
            --                             1 as [level],
            --                             '0_1_00'as parent,
            --                             cast(0 as bit) as isLeaf,
            --                             cast(0 as bit) as expanded,
            --                             cast(1 as bit) as loaded,
            --                             'New File Request(s)' as actionTaken,
            --                             null as name,
            --                             null as userRole,
            --                             null as comment,
            --                             null as [fileName],
            --                             null as fileSize,
            --                             null as uploadDate,
            --                             null as submitDate,
            --                             null as fileId,
            --                             null as childFileId,
            --                             cast(0 as bit) as editable,
            --                             GETDATE() as actionDate,
            --                             null as nfrComment,
            --                             cast(0 as bit) as isFileHistory,
            --                             cast(0 as bit) as isFileComment,
            --                             cast(0 as bit) as isApprove,
            --                             null as childFileName,
										  --null as DocTypeID,
										  --convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
										  --sfs.FolderSortingNumber as FolderSortingNumber,
										  --sfs.[SubfolderSequence] as SubfolderSequence 
            --  from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
            --  inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
            --  where pc.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber

            --  union 

              select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
                     tf.[Filename] as folderNodeName,
                    2 as [level],
                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                           'New File Upload'  as actionTaken,
                           (A.FirstName + ' ' + A.LastName) as name,
                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                           null as comment,
                           tf.FileName as [fileName],
                           FileSize as fileSize,
                           tf.UploadTime as uploadDate,
                           UploadTime as submitDate,
                           tf.FileId as fileId,
                           tf.FileId as childFileId,
                           CONVERT(BIT,0) as editable,
                           UploadTime as actionDate,
                           null as nfrComment,
                           CONVERT(BIT,0) as isFileHistory,
                           CONVERT(BIT,0) as isFileComment,
                           CONVERT(BIT,0) as isApprove,
                           tf.FileName as childFileName,
						    tf.DocTypeID as DocTypeID,
							pf.[FolderKey]  as FolderKey,
							pf.FolderSortingNumber as FolderSortingNumber,
							pf.[SubfolderSequence] as SubfolderSequence 
              from  [$(TaskDB)].dbo.TaskFile tf
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
              inner join [$(TaskDB)].[dbo].[Prod_FolderStructure] pf on pf.FolderKey = tfm.FolderKey
			  inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
              inner join webpages_UsersInRoles l on l.UserId = a.userid 
              inner join webpages_Roles r on r.roleid = l.roleid
              where ct.ChildTaskInstanceId = @TaskInstanceId and pf.FolderKey !=1 and pf.FolderKey !=2  and pf.ViewTypeId!=2 and pf.ViewTypeId!=3 and pf.ViewTypeId =4

Union 
-- Get new Files for subfolder
 select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
                     tf.[Filename] as folderNodeName,
                    2 as [level],
                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                           'New File Upload'  as actionTaken,
                           (A.FirstName + ' ' + A.LastName) as name,
                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                           null as comment,
                           tf.FileName as [fileName],
                           FileSize as fileSize,
                           tf.UploadTime as uploadDate,
                           UploadTime as submitDate,
                           tf.FileId as fileId,
                           tf.FileId as childFileId,
                           CONVERT(BIT,0) as editable,
                           UploadTime as actionDate,
                           null as nfrComment,
                           CONVERT(BIT,0) as isFileHistory,
                           CONVERT(BIT,0) as isFileComment,
                           CONVERT(BIT,0) as isApprove,
                           tf.FileName as childFileName,
						    tf.DocTypeID as DocTypeID,
							psf.[FolderKey]  as FolderKey,
							psf.FolderSortingNumber as FolderSortingNumber,
							psf.[SubfolderSequence] as SubfolderSequence 
              from  [$(TaskDB)].dbo.TaskFile tf
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
              inner join [$(TaskDB)].[dbo].[Prod_SubFolderStructure] psf on psf.FolderKey = tfm.FolderKey
              inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
              inner join webpages_UsersInRoles l on l.UserId = a.userid 
              inner join webpages_Roles r on r.roleid = l.roleid
              where ct.ChildTaskInstanceId = @TaskInstanceId 

Union 
select 
               convert(nvarchar(max),0 ) +'_' +convert(nvarchar(max),0)as id,
                     'Request Additional Submit' as folderNodeName,
                     cast(0 as bit) as [level],
                     null as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,

              'Request Additional Submit'  as actionTaken,
              (A.FirstName + ' ' + A.LastName) as name,
              CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                Notes as comment,
                '' as [fileName],
                     null as fileSize,
                     null as uploadDate,
                     starttime as submitDate,
                     null as fileId,
                     null as childFileId,
                     CONVERT(BIT,0) as editable,
                     starttime as actionDate,
                     null as nfrComment,
                     CONVERT(BIT,0) as isFileHistory,
                     CONVERT(BIT,0) as isFileComment,
                     CONVERT(BIT,0) as isApprove,
                     null as childFileName,
					  null as DocTypeID,
					  null as FolderKey,
					  0 as FolderSortingNumber,
					  null as SubfolderSequence 
       from [$(TaskDB)].dbo.Task t
       inner join HCP_Authentication a on a.UserName = t.AssignedTo
       left join webpages_UsersInRoles l on l.UserId = a.userid 
       left join webpages_Roles r on r.roleid = l.roleid
       where  t.TaskInstanceId = @TaskInstanceId
       and TaskStepId = 15

      order by id asc
      end

	  else

	  begin
	  -- this sectin is for remaining process like application/construction/closing
	   --Get folder structure for the RAI files from parent task file ids 
	   select distinct convert(nvarchar(max),fs.[FolderKey] )as id,
                                  fs.[FolderName] as folderNodeName,
                                 fs.[level] as [level],
                                  convert(nvarchar(max),fs.[ParentKey] )as parent,
                                  cast(0 as bit) as isLeaf,
                                  cast(0 as bit) as expanded,
                                  cast(1 as bit) as loaded,
                                  null as actionTaken,
                                  null as name,
                                  null as userRole,
                                  null as comment,
                                  null as [fileName],
                                  null as fileSize,
                                  null as uploadDate,
                                  null as submitDate,
                                  null as fileId,
                                  null as childFileId,
                                  cast(0 as bit) as editable,
                                  GETDATE() as actionDate,
                                  null as nfrComment,
                                  cast(0 as bit) as isFileHistory,
                                  cast(0 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  null as childFileName,
								  null as DocTypeID,
								convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
								convert(nvarchar(max),fs.FolderSortingNumber ) as FolderSortingNumber,
								fs.[SubfolderSequence] as SubfolderSequence 
    from [$(TaskDB)].dbo.Prod_FolderStructure fs 
    inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
    inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
       inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
       where ctf.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
 union
  --Get subfolder structure for the RAI files from parent task file ids
			 select distinct convert(nvarchar(max),sfs.[FolderKey] )as id,
                                  sfs.[FolderName] as folderNodeName,
                                  1 as [level],
                                  convert(nvarchar(max),sfs.[ParentKey] )as parent,
                                  cast(0 as bit) as isLeaf,
                                  cast(0 as bit) as expanded,
                                  cast(1 as bit) as loaded,
                                  null as actionTaken,
                                  null as name,
                                  null as userRole,
                                  null as comment,
                                  null as [fileName],
                                  null as fileSize,
                                  null as uploadDate,
                                  null as submitDate,
                                  null as fileId,
                                  null as childFileId,
                                  cast(0 as bit) as editable,
                                  GETDATE() as actionDate,
                                  null as nfrComment,
                                  cast(0 as bit) as isFileHistory,
                                  cast(0 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  null as childFileName,
								  null as DocTypeID,
								convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
								sfs.FolderSortingNumber as FolderSortingNumber,
								sfs.[SubfolderSequence] as SubfolderSequence 
			 
			 from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
             inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
             inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
             inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
             where ctf.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber
			
			
       union

       --Get RAI files details from parent task file   and new version of file uploaded for RAI request
       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
                     tfparent.[Filename] as folderNodeName,
                     cast(1 as bit) as [level],
                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                                  'Request Addtional Information' as actionTaken,
                                (A.FirstName + ' ' + A.LastName) as name,
                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
                                  null as comment,
                                  tfparent.[FileName]  as [fileName],
                                  newFilesInChild.FileSize as fileSize,
                               newFilesInChild.UploadTime as uploadDate,
                             null as submitDate,
                             tfparent.FileId as fileId ,
                             newFilesInChild.FileId as childFileId,
                                cast(1 as bit) as editable,--to remove
                          tfparent.UploadTime as actionDate,
                       null as nfrComment,
                                  CAST( 1 as bit) as isFileHistory,
                                  CAST( 1 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  newFilesInChild.[FileName] as childFileName,
								  tfparent.[DocTypeID] as DocTypeID,
								  null as FolderKey,
								  null as FolderSortingNumber,
								  null as SubfolderSequence 
              from [$(TaskDB)].dbo.TaskFile tfparent
              --Table joins to get all file list in its parent task 
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
              inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			 
              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
              --Table joins to filtr and fet file list in its child task
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
              --Table joins to get Lender details
             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              --Table join to get latest version of uploaded file in its child task
              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
                                  from [$(TaskDB)].dbo.TaskFileHistory
                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
              union

       --Get RAI subfolder files details from parent task file   and new version of file uploaded for RAI request
       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
                     tfparent.[Filename] as folderNodeName,
                     cast(1 as bit) as [level],
                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                                  'Request Addtional Information' as actionTaken,
                                (A.FirstName + ' ' + A.LastName) as name,
                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
                                  null as comment,
                                  tfparent.[FileName]  as [fileName],
                                  newFilesInChild.FileSize as fileSize,
                               newFilesInChild.UploadTime as uploadDate,
                             null as submitDate,
                             tfparent.FileId as fileId ,
                             newFilesInChild.FileId as childFileId,
                                cast(1 as bit) as editable,--to remove
                          tfparent.UploadTime as actionDate,
                       null as nfrComment,
                                  CAST( 1 as bit) as isFileHistory,
                                  CAST( 1 as bit) as isFileComment,
                                  cast(0 as bit) as isApprove,
                                  newFilesInChild.[FileName] as childFileName,
								  tfparent.[DocTypeID] as DocTypeID,
								  null as FolderKey,
								  null as FolderSortingNumber,
								  null as SubfolderSequence 
              from [$(TaskDB)].dbo.TaskFile tfparent
              --Table joins to get all file list in its parent task 
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
              --inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			  inner join [$(TaskDB)].dbo.Prod_SubFolderStructure sfs on sfs.[FolderKey] = tfm.[FolderKey]
              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
              --Table joins to filtr and fet file list in its child task
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
              --Table joins to get Lender details
             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              --Table join to get latest version of uploaded file in its child task
              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
                                  from [$(TaskDB)].dbo.TaskFileHistory
                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
              where pc.ChildTaskInstanceId = @TaskInstanceId 
              union 
              --Get New file request 
              select distinct  '0_1_00' as id, 
                                          'New File Request(s)' as folderNodeName,
                                         0 as [level],
                                         null as parent,
                                         cast(0 as bit) as isLeaf,
                                         cast(0 as bit) as expanded,
                                         cast(1 as bit) as loaded,
                                         'New File Request(s)' as actionTaken,
                                           (A.FirstName + ' ' + A.LastName) as name,
                                         r.RoleName as userRole,
                                         nfr.Comments as comment,
                                         null as [fileName],
                                         null as fileSize,
                                         null as uploadDate,
                                         nfr.RequestedOn  as submitDate,
                                         null as fileId,
                                         null as childFileId,
                                         cast(0 as bit) as editable,
                                         GETDATE() as actionDate,
                                         null as nfrComment,
                                         cast(0 as bit) as isFileHistory,
                                         cast(0 as bit) as isFileComment,
                                         cast(0 as bit) as isApprove,
                                         null as childFileName,
										  null as DocTypeID,
										  null as FolderKey,
										  0 as FolderSortingNumber,
										  null as SubfolderSequence 
              from [$(TaskDB)].[dbo].[NewFileRequest] nfr
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
              inner join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.TaskXrefid = pc.ParentTaskInstanceId
              inner join HCP_Authentication a on a.UserID = xref.AssignedTo
              inner join webpages_UsersInRoles l on l.UserId = a.userid
              inner join webpages_Roles r on r.roleid = l.roleid
              where pc.ChildTaskInstanceId = @TaskInstanceId and  r.RoleName in ('ProductionUser','ProductionWlm')  

              union 

              --Get folder structure for  uploaded file in new file request
              select distinct '0_1_'+ convert(nvarchar(max),fs.[FolderKey] )as id,
                                         fs.[FolderName] as folderNodeName,
                                         1 as [level],
                                         '0_1_00'as parent,
                                         cast(0 as bit) as isLeaf,
                                         cast(0 as bit) as expanded,
                                         cast(1 as bit) as loaded,
                                         'New File Request(s)' as actionTaken,
                                         null as name,
                                         null as userRole,
                                         null as comment,
                                         null as [fileName],
                                         null as fileSize,
                                         null as uploadDate,
                                         null as submitDate,
                                         null as fileId,
                                         null as childFileId,
                                         cast(0 as bit) as editable,
                                         GETDATE() as actionDate,
                                         null as nfrComment,
                                         cast(0 as bit) as isFileHistory,
                                         cast(0 as bit) as isFileComment,
                                         cast(0 as bit) as isApprove,
                                         null as childFileName,
										  null as DocTypeID,
										  convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
										  fs.FolderSortingNumber as FolderSortingNumber,
										  fs.[SubfolderSequence] as SubfolderSequence 
              from [$(TaskDB)].dbo.Prod_FolderStructure fs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.FolderKey !=1 and fs.FolderKey !=2  and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
			  union 
              --Get subfolder structure for  uploaded file in new file request
            --  select distinct '0_1_'+ convert(nvarchar(max),sfs.[FolderKey] )as id,
            --                             sfs.[FolderName] as folderNodeName,
            --                             1 as [level],
            --                             '0_1_00'as parent,
            --                             cast(0 as bit) as isLeaf,
            --                             cast(0 as bit) as expanded,
            --                             cast(1 as bit) as loaded,
            --                             'New File Request(s)' as actionTaken,
            --                             null as name,
            --                             null as userRole,
            --                             null as comment,
            --                             null as [fileName],
            --                             null as fileSize,
            --                             null as uploadDate,
            --                             null as submitDate,
            --                             null as fileId,
            --                             null as childFileId,
            --                             cast(0 as bit) as editable,
            --                             GETDATE() as actionDate,
            --                             null as nfrComment,
            --                             cast(0 as bit) as isFileHistory,
            --                             cast(0 as bit) as isFileComment,
            --                             cast(0 as bit) as isApprove,
            --                             null as childFileName,
										  --null as DocTypeID,
										  --convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
										  --sfs.FolderSortingNumber as FolderSortingNumber,
										  --sfs.[SubfolderSequence] as SubfolderSequence 
            --  from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
            --  inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
            --  where pc.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber

            --  union 

              select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
                     tf.[Filename] as folderNodeName,
                    2 as [level],
                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                           'New File Upload'  as actionTaken,
                           (A.FirstName + ' ' + A.LastName) as name,
                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                           null as comment,
                           tf.FileName as [fileName],
                           FileSize as fileSize,
                           tf.UploadTime as uploadDate,
                           UploadTime as submitDate,
                           tf.FileId as fileId,
                           tf.FileId as childFileId,
                           CONVERT(BIT,0) as editable,
                           UploadTime as actionDate,
                           null as nfrComment,
                           CONVERT(BIT,0) as isFileHistory,
                           CONVERT(BIT,0) as isFileComment,
                           CONVERT(BIT,0) as isApprove,
                           tf.FileName as childFileName,
						    tf.DocTypeID as DocTypeID,
							pf.[FolderKey]  as FolderKey,
							pf.FolderSortingNumber as FolderSortingNumber,
							pf.[SubfolderSequence] as SubfolderSequence 
              from  [$(TaskDB)].dbo.TaskFile tf
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
              inner join [$(TaskDB)].[dbo].[Prod_FolderStructure] pf on pf.FolderKey = tfm.FolderKey
			  inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
              inner join webpages_UsersInRoles l on l.UserId = a.userid 
              inner join webpages_Roles r on r.roleid = l.roleid
              where ct.ChildTaskInstanceId = @TaskInstanceId and pf.FolderKey !=1 and pf.FolderKey !=2  and pf.ViewTypeId!=2 and pf.ViewTypeId!=3

Union 
-- Get new Files for subfolder
 select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
                     tf.[Filename] as folderNodeName,
                    2 as [level],
                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,
                           'New File Upload'  as actionTaken,
                           (A.FirstName + ' ' + A.LastName) as name,
                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                           null as comment,
                           tf.FileName as [fileName],
                           FileSize as fileSize,
                           tf.UploadTime as uploadDate,
                           UploadTime as submitDate,
                           tf.FileId as fileId,
                           tf.FileId as childFileId,
                           CONVERT(BIT,0) as editable,
                           UploadTime as actionDate,
                           null as nfrComment,
                           CONVERT(BIT,0) as isFileHistory,
                           CONVERT(BIT,0) as isFileComment,
                           CONVERT(BIT,0) as isApprove,
                           tf.FileName as childFileName,
						    tf.DocTypeID as DocTypeID,
							psf.[FolderKey]  as FolderKey,
							psf.FolderSortingNumber as FolderSortingNumber,
							psf.[SubfolderSequence] as SubfolderSequence 
              from  [$(TaskDB)].dbo.TaskFile tf
              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
              inner join [$(TaskDB)].[dbo].[Prod_SubFolderStructure] psf on psf.FolderKey = tfm.FolderKey
              inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
              inner join webpages_UsersInRoles l on l.UserId = a.userid 
              inner join webpages_Roles r on r.roleid = l.roleid
              where ct.ChildTaskInstanceId = @TaskInstanceId 

Union 
select 
               convert(nvarchar(max),0 ) +'_' +convert(nvarchar(max),0)as id,
                     'Request Additional Submit' as folderNodeName,
                     cast(0 as bit) as [level],
                     null as parent,
                     cast(1 as bit) as isLeaf,
                     cast(0 as bit) as expanded,
                     cast(1 as bit) as loaded,

              'Request Additional Submit'  as actionTaken,
              (A.FirstName + ' ' + A.LastName) as name,
              CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
                Notes as comment,
                '' as [fileName],
                     null as fileSize,
                     null as uploadDate,
                     starttime as submitDate,
                     null as fileId,
                     null as childFileId,
                     CONVERT(BIT,0) as editable,
                     starttime as actionDate,
                     null as nfrComment,
                     CONVERT(BIT,0) as isFileHistory,
                     CONVERT(BIT,0) as isFileComment,
                     CONVERT(BIT,0) as isApprove,
                     null as childFileName,
					  null as DocTypeID,
					  null as FolderKey,
					  0 as FolderSortingNumber,
					  null as SubfolderSequence 
       from [$(TaskDB)].dbo.Task t
       inner join HCP_Authentication a on a.UserName = t.AssignedTo
       left join webpages_UsersInRoles l on l.UserId = a.userid 
       left join webpages_Roles r on r.roleid = l.roleid
       where  t.TaskInstanceId = @TaskInstanceId
       and TaskStepId = 15

      order by id asc
	  end
end

---------------old with out Amendment ---------------
--CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetRAIEdit]
--(
--@TaskInstanceId uniqueidentifier,
--@PropertyId varchar(10) = null,
--@FHANumber varchar(10) = null
--)
--AS
--BEGIN

--   --Get folder structure for the RAI files from parent task file ids   
--       select distinct convert(nvarchar(max),fs.[FolderKey] )as id,
--                                  fs.[FolderName] as folderNodeName,
--                                 fs.[level] as [level],
--                                  convert(nvarchar(max),fs.[ParentKey] )as parent,
--                                  cast(0 as bit) as isLeaf,
--                                  cast(0 as bit) as expanded,
--                                  cast(1 as bit) as loaded,
--                                  null as actionTaken,
--                                  null as name,
--                                  null as userRole,
--                                  null as comment,
--                                  null as [fileName],
--                                  null as fileSize,
--                                  null as uploadDate,
--                                  null as submitDate,
--                                  null as fileId,
--                                  null as childFileId,
--                                  cast(0 as bit) as editable,
--                                  GETDATE() as actionDate,
--                                  null as nfrComment,
--                                  cast(0 as bit) as isFileHistory,
--                                  cast(0 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  null as childFileName,
--								  null as DocTypeID,
--								convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
--								convert(nvarchar(max),fs.FolderSortingNumber ) as FolderSortingNumber,
--								fs.[SubfolderSequence] as SubfolderSequence 
--    from [$(TaskDB)].dbo.Prod_FolderStructure fs 
--    inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
--    inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
--       inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
--       where ctf.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
-- union
--  --Get subfolder structure for the RAI files from parent task file ids
--			 select distinct convert(nvarchar(max),sfs.[FolderKey] )as id,
--                                  sfs.[FolderName] as folderNodeName,
--                                  1 as [level],
--                                  convert(nvarchar(max),sfs.[ParentKey] )as parent,
--                                  cast(0 as bit) as isLeaf,
--                                  cast(0 as bit) as expanded,
--                                  cast(1 as bit) as loaded,
--                                  null as actionTaken,
--                                  null as name,
--                                  null as userRole,
--                                  null as comment,
--                                  null as [fileName],
--                                  null as fileSize,
--                                  null as uploadDate,
--                                  null as submitDate,
--                                  null as fileId,
--                                  null as childFileId,
--                                  cast(0 as bit) as editable,
--                                  GETDATE() as actionDate,
--                                  null as nfrComment,
--                                  cast(0 as bit) as isFileHistory,
--                                  cast(0 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  null as childFileName,
--								  null as DocTypeID,
--								convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
--								sfs.FolderSortingNumber as FolderSortingNumber,
--								sfs.[SubfolderSequence] as SubfolderSequence 
			 
--			 from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
--             inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
--             inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
--             inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
--             where ctf.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber
			
			
--       union

--       --Get RAI files details from parent task file   and new version of file uploaded for RAI request
--       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
--                     tfparent.[Filename] as folderNodeName,
--                     cast(1 as bit) as [level],
--                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                                  'Request Addtional Information' as actionTaken,
--                                (A.FirstName + ' ' + A.LastName) as name,
--                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
--                                  null as comment,
--                                  tfparent.[FileName]  as [fileName],
--                                  newFilesInChild.FileSize as fileSize,
--                               newFilesInChild.UploadTime as uploadDate,
--                             null as submitDate,
--                             tfparent.FileId as fileId ,
--                             newFilesInChild.FileId as childFileId,
--                                cast(1 as bit) as editable,--to remove
--                          tfparent.UploadTime as actionDate,
--                       null as nfrComment,
--                                  CAST( 1 as bit) as isFileHistory,
--                                  CAST( 1 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  newFilesInChild.[FileName] as childFileName,
--								  tfparent.[DocTypeID] as DocTypeID,
--								  null as FolderKey,
--								  null as FolderSortingNumber,
--								  null as SubfolderSequence 
--              from [$(TaskDB)].dbo.TaskFile tfparent
--              --Table joins to get all file list in its parent task 
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
--              inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			 
--              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--              --Table joins to filtr and fet file list in its child task
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
--              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
--              --Table joins to get Lender details
--             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              --Table join to get latest version of uploaded file in its child task
--              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
--                                  from [$(TaskDB)].dbo.TaskFileHistory
--                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
--              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
--              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
--              union

--       --Get RAI subfolder files details from parent task file   and new version of file uploaded for RAI request
--       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
--                     tfparent.[Filename] as folderNodeName,
--                     cast(1 as bit) as [level],
--                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                                  'Request Addtional Information' as actionTaken,
--                                (A.FirstName + ' ' + A.LastName) as name,
--                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
--                                  null as comment,
--                                  tfparent.[FileName]  as [fileName],
--                                  newFilesInChild.FileSize as fileSize,
--                               newFilesInChild.UploadTime as uploadDate,
--                             null as submitDate,
--                             tfparent.FileId as fileId ,
--                             newFilesInChild.FileId as childFileId,
--                                cast(1 as bit) as editable,--to remove
--                          tfparent.UploadTime as actionDate,
--                       null as nfrComment,
--                                  CAST( 1 as bit) as isFileHistory,
--                                  CAST( 1 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  newFilesInChild.[FileName] as childFileName,
--								  tfparent.[DocTypeID] as DocTypeID,
--								  null as FolderKey,
--								  null as FolderSortingNumber,
--								  null as SubfolderSequence 
--              from [$(TaskDB)].dbo.TaskFile tfparent
--              --Table joins to get all file list in its parent task 
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
--              --inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
--			  inner join [$(TaskDB)].dbo.Prod_SubFolderStructure sfs on sfs.[FolderKey] = tfm.[FolderKey]
--              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--              --Table joins to filtr and fet file list in its child task
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
--              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
--              --Table joins to get Lender details
--             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              --Table join to get latest version of uploaded file in its child task
--              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
--                                  from [$(TaskDB)].dbo.TaskFileHistory
--                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
--              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
--              where pc.ChildTaskInstanceId = @TaskInstanceId 
--              union 
--              --Get New file request 
--              select distinct  '0_1_00' as id, 
--                                          'New File Request(s)' as folderNodeName,
--                                         0 as [level],
--                                         null as parent,
--                                         cast(0 as bit) as isLeaf,
--                                         cast(0 as bit) as expanded,
--                                         cast(1 as bit) as loaded,
--                                         'New File Request(s)' as actionTaken,
--                                           (A.FirstName + ' ' + A.LastName) as name,
--                                         r.RoleName as userRole,
--                                         nfr.Comments as comment,
--                                         null as [fileName],
--                                         null as fileSize,
--                                         null as uploadDate,
--                                         nfr.RequestedOn  as submitDate,
--                                         null as fileId,
--                                         null as childFileId,
--                                         cast(0 as bit) as editable,
--                                         GETDATE() as actionDate,
--                                         null as nfrComment,
--                                         cast(0 as bit) as isFileHistory,
--                                         cast(0 as bit) as isFileComment,
--                                         cast(0 as bit) as isApprove,
--                                         null as childFileName,
--										  null as DocTypeID,
--										  null as FolderKey,
--										  0 as FolderSortingNumber,
--										  null as SubfolderSequence 
--              from [$(TaskDB)].[dbo].[NewFileRequest] nfr
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--              inner join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.TaskXrefid = pc.ParentTaskInstanceId
--              inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where pc.ChildTaskInstanceId = @TaskInstanceId and  r.RoleName in ('ProductionUser','ProductionWlm')  

--              union 

--              --Get folder structure for  uploaded file in new file request
--              select distinct '0_1_'+ convert(nvarchar(max),fs.[FolderKey] )as id,
--                                         fs.[FolderName] as folderNodeName,
--                                         1 as [level],
--                                         '0_1_00'as parent,
--                                         cast(0 as bit) as isLeaf,
--                                         cast(0 as bit) as expanded,
--                                         cast(1 as bit) as loaded,
--                                         'New File Request(s)' as actionTaken,
--                                         null as name,
--                                         null as userRole,
--                                         null as comment,
--                                         null as [fileName],
--                                         null as fileSize,
--                                         null as uploadDate,
--                                         null as submitDate,
--                                         null as fileId,
--                                         null as childFileId,
--                                         cast(0 as bit) as editable,
--                                         GETDATE() as actionDate,
--                                         null as nfrComment,
--                                         cast(0 as bit) as isFileHistory,
--                                         cast(0 as bit) as isFileComment,
--                                         cast(0 as bit) as isApprove,
--                                         null as childFileName,
--										  null as DocTypeID,
--										  convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
--										  fs.FolderSortingNumber as FolderSortingNumber,
--										  fs.[SubfolderSequence] as SubfolderSequence 
--              from [$(TaskDB)].dbo.Prod_FolderStructure fs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.FolderKey !=1 and fs.FolderKey !=2  and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
--			  union 
--              --Get subfolder structure for  uploaded file in new file request
--            --  select distinct '0_1_'+ convert(nvarchar(max),sfs.[FolderKey] )as id,
--            --                             sfs.[FolderName] as folderNodeName,
--            --                             1 as [level],
--            --                             '0_1_00'as parent,
--            --                             cast(0 as bit) as isLeaf,
--            --                             cast(0 as bit) as expanded,
--            --                             cast(1 as bit) as loaded,
--            --                             'New File Request(s)' as actionTaken,
--            --                             null as name,
--            --                             null as userRole,
--            --                             null as comment,
--            --                             null as [fileName],
--            --                             null as fileSize,
--            --                             null as uploadDate,
--            --                             null as submitDate,
--            --                             null as fileId,
--            --                             null as childFileId,
--            --                             cast(0 as bit) as editable,
--            --                             GETDATE() as actionDate,
--            --                             null as nfrComment,
--            --                             cast(0 as bit) as isFileHistory,
--            --                             cast(0 as bit) as isFileComment,
--            --                             cast(0 as bit) as isApprove,
--            --                             null as childFileName,
--										  --null as DocTypeID,
--										  --convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
--										  --sfs.FolderSortingNumber as FolderSortingNumber,
--										  --sfs.[SubfolderSequence] as SubfolderSequence 
--            --  from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
--            --  inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--            --  where pc.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber

--            --  union 

--              select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
--                     tf.[Filename] as folderNodeName,
--                    2 as [level],
--                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                           'New File Upload'  as actionTaken,
--                           (A.FirstName + ' ' + A.LastName) as name,
--                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                           null as comment,
--                           tf.FileName as [fileName],
--                           FileSize as fileSize,
--                           tf.UploadTime as uploadDate,
--                           UploadTime as submitDate,
--                           tf.FileId as fileId,
--                           tf.FileId as childFileId,
--                           CONVERT(BIT,0) as editable,
--                           UploadTime as actionDate,
--                           null as nfrComment,
--                           CONVERT(BIT,0) as isFileHistory,
--                           CONVERT(BIT,0) as isFileComment,
--                           CONVERT(BIT,0) as isApprove,
--                           tf.FileName as childFileName,
--						    tf.DocTypeID as DocTypeID,
--							pf.[FolderKey]  as FolderKey,
--							pf.FolderSortingNumber as FolderSortingNumber,
--							pf.[SubfolderSequence] as SubfolderSequence 
--              from  [$(TaskDB)].dbo.TaskFile tf
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
--              inner join [$(TaskDB)].[dbo].[Prod_FolderStructure] pf on pf.FolderKey = tfm.FolderKey
--			  inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
--              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
--              inner join webpages_UsersInRoles l on l.UserId = a.userid 
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where ct.ChildTaskInstanceId = @TaskInstanceId and pf.FolderKey !=1 and pf.FolderKey !=2  and pf.ViewTypeId!=2 and pf.ViewTypeId!=3

--Union 
---- Get new Files for subfolder
-- select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
--                     tf.[Filename] as folderNodeName,
--                    2 as [level],
--                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                           'New File Upload'  as actionTaken,
--                           (A.FirstName + ' ' + A.LastName) as name,
--                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                           null as comment,
--                           tf.FileName as [fileName],
--                           FileSize as fileSize,
--                           tf.UploadTime as uploadDate,
--                           UploadTime as submitDate,
--                           tf.FileId as fileId,
--                           tf.FileId as childFileId,
--                           CONVERT(BIT,0) as editable,
--                           UploadTime as actionDate,
--                           null as nfrComment,
--                           CONVERT(BIT,0) as isFileHistory,
--                           CONVERT(BIT,0) as isFileComment,
--                           CONVERT(BIT,0) as isApprove,
--                           tf.FileName as childFileName,
--						    tf.DocTypeID as DocTypeID,
--							psf.[FolderKey]  as FolderKey,
--							psf.FolderSortingNumber as FolderSortingNumber,
--							psf.[SubfolderSequence] as SubfolderSequence 
--              from  [$(TaskDB)].dbo.TaskFile tf
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
--              inner join [$(TaskDB)].[dbo].[Prod_SubFolderStructure] psf on psf.FolderKey = tfm.FolderKey
--              inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
--              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
--              inner join webpages_UsersInRoles l on l.UserId = a.userid 
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where ct.ChildTaskInstanceId = @TaskInstanceId 

--Union 
--select 
--               convert(nvarchar(max),0 ) +'_' +convert(nvarchar(max),0)as id,
--                     'Request Additional Submit' as folderNodeName,
--                     cast(0 as bit) as [level],
--                     null as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,

--              'Request Additional Submit'  as actionTaken,
--              (A.FirstName + ' ' + A.LastName) as name,
--              CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                Notes as comment,
--                '' as [fileName],
--                     null as fileSize,
--                     null as uploadDate,
--                     starttime as submitDate,
--                     null as fileId,
--                     null as childFileId,
--                     CONVERT(BIT,0) as editable,
--                     starttime as actionDate,
--                     null as nfrComment,
--                     CONVERT(BIT,0) as isFileHistory,
--                     CONVERT(BIT,0) as isFileComment,
--                     CONVERT(BIT,0) as isApprove,
--                     null as childFileName,
--					  null as DocTypeID,
--					  null as FolderKey,
--					  0 as FolderSortingNumber,
--					  null as SubfolderSequence 
--       from [$(TaskDB)].dbo.Task t
--       inner join HCP_Authentication a on a.UserName = t.AssignedTo
--       left join webpages_UsersInRoles l on l.UserId = a.userid 
--       left join webpages_Roles r on r.roleid = l.roleid
--       where  t.TaskInstanceId = @TaskInstanceId
--       and TaskStepId = 15

--      order by id asc
      
--end

--------------- Old--------------

--CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetRAIEdit]
--(
--@TaskInstanceId uniqueidentifier,
--@PropertyId varchar(10) = null,
--@FHANumber varchar(10) = null
--)
--AS
--BEGIN

--   --Get folder structure for the RAI files from parent task file ids   
--       select distinct convert(nvarchar(max),fs.[FolderKey] )as id,
--                                  fs.[FolderName] as folderNodeName,
--                                 fs.[level] as [level],
--                                  convert(nvarchar(max),fs.[ParentKey] )as parent,
--                                  cast(0 as bit) as isLeaf,
--                                  cast(0 as bit) as expanded,
--                                  cast(1 as bit) as loaded,
--                                  null as actionTaken,
--                                  null as name,
--                                  null as userRole,
--                                  null as comment,
--                                  null as [fileName],
--                                  null as fileSize,
--                                  null as uploadDate,
--                                  null as submitDate,
--                                  null as fileId,
--                                  null as childFileId,
--                                  cast(0 as bit) as editable,
--                                  GETDATE() as actionDate,
--                                  null as nfrComment,
--                                  cast(0 as bit) as isFileHistory,
--                                  cast(0 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  null as childFileName,
--								  null as DocTypeID,
--								convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
--								convert(nvarchar(max),fs.FolderSortingNumber ) as FolderSortingNumber,
--								fs.[SubfolderSequence] as SubfolderSequence 
--    from [$(TaskDB)].dbo.Prod_FolderStructure fs 
--    inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
--    inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
--       inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
--       where ctf.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
-- union
--  --Get subfolder structure for the RAI files from parent task file ids
--			 select distinct convert(nvarchar(max),sfs.[FolderKey] )as id,
--                                  sfs.[FolderName] as folderNodeName,
--                                  1 as [level],
--                                  convert(nvarchar(max),sfs.[ParentKey] )as parent,
--                                  cast(0 as bit) as isLeaf,
--                                  cast(0 as bit) as expanded,
--                                  cast(1 as bit) as loaded,
--                                  null as actionTaken,
--                                  null as name,
--                                  null as userRole,
--                                  null as comment,
--                                  null as [fileName],
--                                  null as fileSize,
--                                  null as uploadDate,
--                                  null as submitDate,
--                                  null as fileId,
--                                  null as childFileId,
--                                  cast(0 as bit) as editable,
--                                  GETDATE() as actionDate,
--                                  null as nfrComment,
--                                  cast(0 as bit) as isFileHistory,
--                                  cast(0 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  null as childFileName,
--								  null as DocTypeID,
--								convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
--								sfs.FolderSortingNumber as FolderSortingNumber,
--								sfs.[SubfolderSequence] as SubfolderSequence 
			 
--			 from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs 
--             inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
--             inner join [$(TaskDB)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
--             inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfile.TaskFileId
--             where ctf.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber
			
			
--       union

--       --Get RAI files details from parent task file   and new version of file uploaded for RAI request
--       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
--                     tfparent.[Filename] as folderNodeName,
--                     cast(1 as bit) as [level],
--                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                                  'Request Addtional Information' as actionTaken,
--                                (A.FirstName + ' ' + A.LastName) as name,
--                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
--                                  null as comment,
--                                  tfparent.[FileName]  as [fileName],
--                                  newFilesInChild.FileSize as fileSize,
--                               newFilesInChild.UploadTime as uploadDate,
--                             null as submitDate,
--                             tfparent.FileId as fileId ,
--                             newFilesInChild.FileId as childFileId,
--                                cast(1 as bit) as editable,--to remove
--                          tfparent.UploadTime as actionDate,
--                       null as nfrComment,
--                                  CAST( 1 as bit) as isFileHistory,
--                                  CAST( 1 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  newFilesInChild.[FileName] as childFileName,
--								  tfparent.[DocTypeID] as DocTypeID,
--								  null as FolderKey,
--								  null as FolderSortingNumber,
--								  null as SubfolderSequence 
--              from [$(TaskDB)].dbo.TaskFile tfparent
--              --Table joins to get all file list in its parent task 
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
--              inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
			 
--              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--              --Table joins to filtr and fet file list in its child task
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
--              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
--              --Table joins to get Lender details
--             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              --Table join to get latest version of uploaded file in its child task
--              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
--                                  from [$(TaskDB)].dbo.TaskFileHistory
--                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
--              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
--              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
--              union

--       --Get RAI subfolder files details from parent task file   and new version of file uploaded for RAI request
--       select distinct convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tfparent.[FileId] )as id,
--                     tfparent.[Filename] as folderNodeName,
--                     cast(1 as bit) as [level],
--                     convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                                  'Request Addtional Information' as actionTaken,
--                                (A.FirstName + ' ' + A.LastName) as name,
--                                 r.RoleName +'( '+  pv.[ViewName] +' )'  as userRole,
--                                  null as comment,
--                                  tfparent.[FileName]  as [fileName],
--                                  newFilesInChild.FileSize as fileSize,
--                               newFilesInChild.UploadTime as uploadDate,
--                             null as submitDate,
--                             tfparent.FileId as fileId ,
--                             newFilesInChild.FileId as childFileId,
--                                cast(1 as bit) as editable,--to remove
--                          tfparent.UploadTime as actionDate,
--                       null as nfrComment,
--                                  CAST( 1 as bit) as isFileHistory,
--                                  CAST( 1 as bit) as isFileComment,
--                                  cast(0 as bit) as isApprove,
--                                  newFilesInChild.[FileName] as childFileName,
--								  tfparent.[DocTypeID] as DocTypeID,
--								  null as FolderKey,
--								  null as FolderSortingNumber,
--								  null as SubfolderSequence 
--              from [$(TaskDB)].dbo.TaskFile tfparent
--              --Table joins to get all file list in its parent task 
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tfm.[TaskFileId] = tfparent.TaskFileId
--              --inner join [$(TaskDB)].dbo.Prod_FolderStructure fs on fs.[FolderKey] = tfm.[FolderKey]
--			  inner join [$(TaskDB)].dbo.Prod_SubFolderStructure sfs on sfs.[FolderKey] = tfm.[FolderKey]
--              inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
--              inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
--              --Table joins to filtr and fet file list in its child task
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.[ParentTaskInstanceId] = xref.TaskXrefid
--              inner join [$(TaskDB)].[dbo].[RequestAdditionalInfoFiles] ctf on ctf.TaskFileId = tfparent.TaskFileId and pc.ChildTaskInstanceId = ctf.ChildTaskInstanceId
--              --Table joins to get Lender details
--             inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--                     inner join  [$(TaskDB)].[dbo].[Prod_View] pv on pv.ViewId = xref.ViewId
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              --Table join to get latest version of uploaded file in its child task
--              left join (   select ParentTaskFileId,ChildTaskInstanceId,max( ChildTaskFileId) as ChildTaskFileId ,max(CreatedOn) as Maxdate 
--                                  from [$(TaskDB)].dbo.TaskFileHistory
--                                  group by ParentTaskFileId,ChildTaskInstanceId) fhChild on    ctf.ChildTaskInstanceId = fhChild.ChildTaskInstanceId and ctf.TaskFileId = fhChild.ParentTaskFileId
--              left join  [$(TaskDB)].dbo.TaskFile newFilesInChild on newFilesInChild.TaskInstanceId = fhChild.ChildTaskInstanceId and newFilesInChild.TaskFileId = fhChild.ChildTaskFileId
--              where pc.ChildTaskInstanceId = @TaskInstanceId 
--              union 
--              --Get New file request 
--              select distinct  '0_1_00' as id, 
--                                          'New File Request(s)' as folderNodeName,
--                                         0 as [level],
--                                         null as parent,
--                                         cast(0 as bit) as isLeaf,
--                                         cast(0 as bit) as expanded,
--                                         cast(1 as bit) as loaded,
--                                         'New File Request(s)' as actionTaken,
--                                           (A.FirstName + ' ' + A.LastName) as name,
--                                         r.RoleName as userRole,
--                                         nfr.Comments as comment,
--                                         null as [fileName],
--                                         null as fileSize,
--                                         null as uploadDate,
--                                         nfr.RequestedOn  as submitDate,
--                                         null as fileId,
--                                         null as childFileId,
--                                         cast(0 as bit) as editable,
--                                         GETDATE() as actionDate,
--                                         null as nfrComment,
--                                         cast(0 as bit) as isFileHistory,
--                                         cast(0 as bit) as isFileComment,
--                                         cast(0 as bit) as isApprove,
--                                         null as childFileName,
--										  null as DocTypeID,
--										  null as FolderKey,
--										  0 as FolderSortingNumber,
--										  null as SubfolderSequence 
--              from [$(TaskDB)].[dbo].[NewFileRequest] nfr
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--              inner join [$(TaskDB)].[dbo].[Prod_TaskXref] xref on xref.TaskXrefid = pc.ParentTaskInstanceId
--              inner join HCP_Authentication a on a.UserID = xref.AssignedTo
--              inner join webpages_UsersInRoles l on l.UserId = a.userid
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where pc.ChildTaskInstanceId = @TaskInstanceId and  r.RoleName in ('ProductionUser','ProductionWlm')  

--              union 

--              --Get folder structure for  uploaded file in new file request
--              select distinct '0_1_'+ convert(nvarchar(max),fs.[FolderKey] )as id,
--                                         fs.[FolderName] as folderNodeName,
--                                         1 as [level],
--                                         '0_1_00'as parent,
--                                         cast(0 as bit) as isLeaf,
--                                         cast(0 as bit) as expanded,
--                                         cast(1 as bit) as loaded,
--                                         'New File Request(s)' as actionTaken,
--                                         null as name,
--                                         null as userRole,
--                                         null as comment,
--                                         null as [fileName],
--                                         null as fileSize,
--                                         null as uploadDate,
--                                         null as submitDate,
--                                         null as fileId,
--                                         null as childFileId,
--                                         cast(0 as bit) as editable,
--                                         GETDATE() as actionDate,
--                                         null as nfrComment,
--                                         cast(0 as bit) as isFileHistory,
--                                         cast(0 as bit) as isFileComment,
--                                         cast(0 as bit) as isApprove,
--                                         null as childFileName,
--										  null as DocTypeID,
--										  convert(nvarchar(max),fs.[FolderKey] ) as FolderKey,
--										  fs.FolderSortingNumber as FolderSortingNumber,
--										  fs.[SubfolderSequence] as SubfolderSequence 
--              from [$(TaskDB)].dbo.Prod_FolderStructure fs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--              where pc.ChildTaskInstanceId = @TaskInstanceId and fs.FolderKey !=1 and fs.FolderKey !=2  and fs.ViewTypeId!=2 and fs.ViewTypeId!=3
--			  union 
--              --Get subfolder structure for  uploaded file in new file request
--              select distinct '0_1_'+ convert(nvarchar(max),sfs.[FolderKey] )as id,
--                                         sfs.[FolderName] as folderNodeName,
--                                         1 as [level],
--                                         '0_1_00'as parent,
--                                         cast(0 as bit) as isLeaf,
--                                         cast(0 as bit) as expanded,
--                                         cast(1 as bit) as loaded,
--                                         'New File Request(s)' as actionTaken,
--                                         null as name,
--                                         null as userRole,
--                                         null as comment,
--                                         null as [fileName],
--                                         null as fileSize,
--                                         null as uploadDate,
--                                         null as submitDate,
--                                         null as fileId,
--                                         null as childFileId,
--                                         cast(0 as bit) as editable,
--                                         GETDATE() as actionDate,
--                                         null as nfrComment,
--                                         cast(0 as bit) as isFileHistory,
--                                         cast(0 as bit) as isFileComment,
--                                         cast(0 as bit) as isApprove,
--                                         null as childFileName,
--										  null as DocTypeID,
--										  convert(nvarchar(max),sfs.[FolderKey] ) as FolderKey,
--										  sfs.FolderSortingNumber as FolderSortingNumber,
--										  sfs.[SubfolderSequence] as SubfolderSequence 
--              from [$(TaskDB)].dbo.Prod_SubFolderStructure sfs ,[$(TaskDB)].[dbo].[NewFileRequest] nfr
--              inner join [$(TaskDB)].[dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId 
--              where pc.ChildTaskInstanceId = @TaskInstanceId and sfs.ProjectNo = @PropertyId and sfs.FhaNo = @FHANumber

--              union 

--              select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
--                     tf.[Filename] as folderNodeName,
--                    2 as [level],
--                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                           'New File Upload'  as actionTaken,
--                           (A.FirstName + ' ' + A.LastName) as name,
--                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                           null as comment,
--                           tf.FileName as [fileName],
--                           FileSize as fileSize,
--                           tf.UploadTime as uploadDate,
--                           UploadTime as submitDate,
--                           tf.FileId as fileId,
--                           tf.FileId as childFileId,
--                           CONVERT(BIT,0) as editable,
--                           UploadTime as actionDate,
--                           null as nfrComment,
--                           CONVERT(BIT,0) as isFileHistory,
--                           CONVERT(BIT,0) as isFileComment,
--                           CONVERT(BIT,0) as isApprove,
--                           tf.FileName as childFileName,
--						    tf.DocTypeID as DocTypeID,
--							pf.[FolderKey]  as FolderKey,
--							pf.FolderSortingNumber as FolderSortingNumber,
--							pf.[SubfolderSequence] as SubfolderSequence 
--              from  [$(TaskDB)].dbo.TaskFile tf
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
--              inner join [$(TaskDB)].[dbo].[Prod_FolderStructure] pf on pf.FolderKey = tfm.FolderKey
--			  inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
--              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
--              inner join webpages_UsersInRoles l on l.UserId = a.userid 
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where ct.ChildTaskInstanceId = @TaskInstanceId and pf.FolderKey !=1 and pf.FolderKey !=2  and pf.ViewTypeId!=2 and pf.ViewTypeId!=3

--Union 
---- Get new Files for subfolder
-- select distinct  '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] ) +'_' +convert(nvarchar(max), tf.[FileId] )as id,
--                     tf.[Filename] as folderNodeName,
--                    2 as [level],
--                     '0_1_'+ convert(nvarchar(max),tfm.[FolderKey] )as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,
--                           'New File Upload'  as actionTaken,
--                           (A.FirstName + ' ' + A.LastName) as name,
--                           CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                           null as comment,
--                           tf.FileName as [fileName],
--                           FileSize as fileSize,
--                           tf.UploadTime as uploadDate,
--                           UploadTime as submitDate,
--                           tf.FileId as fileId,
--                           tf.FileId as childFileId,
--                           CONVERT(BIT,0) as editable,
--                           UploadTime as actionDate,
--                           null as nfrComment,
--                           CONVERT(BIT,0) as isFileHistory,
--                           CONVERT(BIT,0) as isFileComment,
--                           CONVERT(BIT,0) as isApprove,
--                           tf.FileName as childFileName,
--						    tf.DocTypeID as DocTypeID,
--							psf.[FolderKey]  as FolderKey,
--							psf.FolderSortingNumber as FolderSortingNumber,
--							psf.[SubfolderSequence] as SubfolderSequence 
--              from  [$(TaskDB)].dbo.TaskFile tf
--              inner join [$(TaskDB)].dbo.TaskFile_FolderMapping tfm on tf.TaskFileId = tfm.[TaskFileId]
--              inner join [$(TaskDB)].[dbo].[Prod_SubFolderStructure] psf on psf.FolderKey = tfm.FolderKey
--              inner join  [$(TaskDB)].dbo.ChildTaskNewFiles ct on tf.TaskFileId = ct.taskfileinstanceId
--              inner join HCP_Authentication a on a.UserID = tf.CreatedBy
--              inner join webpages_UsersInRoles l on l.UserId = a.userid 
--              inner join webpages_Roles r on r.roleid = l.roleid
--              where ct.ChildTaskInstanceId = @TaskInstanceId 

--Union 
--select 
--               convert(nvarchar(max),0 ) +'_' +convert(nvarchar(max),0)as id,
--                     'Request Additional Submit' as folderNodeName,
--                     cast(0 as bit) as [level],
--                     null as parent,
--                     cast(1 as bit) as isLeaf,
--                     cast(0 as bit) as expanded,
--                     cast(1 as bit) as loaded,

--              'Request Additional Submit'  as actionTaken,
--              (A.FirstName + ' ' + A.LastName) as name,
--              CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
--                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
--					 WHEN r.RoleName = 'InspectionContractor' then 'IC'
--                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole, 
--                Notes as comment,
--                '' as [fileName],
--                     null as fileSize,
--                     null as uploadDate,
--                     starttime as submitDate,
--                     null as fileId,
--                     null as childFileId,
--                     CONVERT(BIT,0) as editable,
--                     starttime as actionDate,
--                     null as nfrComment,
--                     CONVERT(BIT,0) as isFileHistory,
--                     CONVERT(BIT,0) as isFileComment,
--                     CONVERT(BIT,0) as isApprove,
--                     null as childFileName,
--					  null as DocTypeID,
--					  null as FolderKey,
--					  0 as FolderSortingNumber,
--					  null as SubfolderSequence 
--       from [$(TaskDB)].dbo.Task t
--       inner join HCP_Authentication a on a.UserName = t.AssignedTo
--       left join webpages_UsersInRoles l on l.UserId = a.userid 
--       left join webpages_Roles r on r.roleid = l.roleid
--       where  t.TaskInstanceId = @TaskInstanceId
--       and TaskStepId = 15

--      order by id asc
      
--end





--GO

