﻿
CREATE PROCEDURE [dbo].[usp_HCP_Delete_Script_For_Automation]
(
@FHANumber varchar(9)
)
AS
Begin try
	Begin transaction
		
		
		------==Live DB==-------------------------------------

		delete from [dbo].[Prod_NextStage] where FhaNumber = @FHANumber
		delete fr from [dbo].[Prod_SelectedAdditionalFinancingResources] fr 
		join Prod_FHANumberRequest fh on fr.FHANumberRequestId = fh.FHANumberRequestId where fh.FHANumber = @FHANumber
		delete sa from [dbo].[SharepointPortalAudit] sa 
		join Prod_SharepointScreen sh on sa.TaskInstanceId = sh.TaskinstanceId where sh.FHANumber = @FHANumber
		delete from [dbo].[Prod_SharepointScreen] where FHANumber = @FHANumber
		 
		-------==Task DB==---------------------------------
		delete pn from [$(TaskDB)].[dbo].[Prod_Note] pn 
		join [$(TaskDB)].dbo.Task tk on pn.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber
		delete gt from [$(TaskDB)].dbo.Prod_GroupTasks gt 
		join [$(TaskDB)].dbo.Task tk on gt.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber
		delete fm from [$(TaskDB)].dbo.TaskFile_FolderMapping fm 
		join [$(TaskDB)].dbo.TaskFile tf on fm.TaskFileId = tf.TaskFileId 
		join [$(TaskDB)].dbo.Task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber
	
		delete nf from [$(TaskDB)].[dbo].[NewFileRequest] nf
		join [$(TaskDB)].dbo.Prod_TaskXref tx on nf.Childtaskinstanceid = tx.TaskXrefid 
		join [$(TaskDB)].dbo.task tk on tk.TaskInstanceId = tx.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete pc from [$(TaskDB)].dbo.parentchildtask pc
		join [$(TaskDB)].dbo.Prod_TaskXref tx on pc.ParentTaskInstanceId = tx.TaskXrefid 
		join [$(TaskDB)].dbo.task tk on tk.TaskInstanceId = tx.TaskInstanceId where tk.FhaNumber = @FHANumber
		
		delete cf from [dbo].[ChildTaskNewFiles] cf
		join [$(TaskDB)].dbo.task tk on cf.ChildTaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete rf from [$(TaskDB)].dbo.[RequestAdditionalInfoFiles] rf
		join [$(TaskDB)].dbo.taskfile tf on rf.TaskFileId = tf.TaskFileId
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete th from [$(TaskDB)].[dbo].[TaskFileHistory] th 
		join [$(TaskDB)].dbo.taskfile tf on th.ParentTaskFileId = tf.TaskFileId
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete th from [$(TaskDB)].[dbo].[TaskFileHistory] th 
		join [$(TaskDB)].dbo.taskfile tf on th.ChildTaskFileId = tf.TaskFileId
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		

		delete rs from [$(TaskDB)].dbo.ReviewFileStatus rs 
		join [$(TaskDB)].dbo.taskfile tf on rs.TaskFileId = tf.TaskFileId
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete rc from [$(TaskDB)].[dbo].[ReviewFileComment] rc
		join [$(TaskDB)].dbo.taskfile tf on rc.FileTaskId = tf.TaskFileId
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete tk from [$(TaskDB)].dbo.taskfile tf
		join [$(TaskDB)].dbo.task tk on tf.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber


		------==Live DB==-------------------------------------

		delete op from opaform op
		join Prod_FHANumberRequest fr on op.FhaNumber = fr.FHANumber where fr.FHANumber = @FHANumber


		-------==TASK DB==--------------------------------------


		delete tx from [$(TaskDB)].dbo.prod_taskxref tx 
		join [$(TaskDB)].dbo.task tk on tx.TaskInstanceId = tk.TaskInstanceId where tk.FhaNumber = @FHANumber

		delete from [$(TaskDB)].dbo.task where FhaNumber = @FHANumber


		------==Live DB==-------------------------------------
		delete from Prod_FHANumberRequest where FhaNumber = @FHANumber


	Commit transaction
	
End try
Begin catch
	
	Rollback transaction
End catch

GO

