-- =============================================
-- Author:		David Coddington
-- Create date: 06/19/2015
-- Description:	Get AE's for WLM ID List
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Get_AEbyWLMlist] 
(
	@wlmListString NVARCHAR(100)
)
AS

DECLARE @useWLMList BIT
DECLARE @wlmList TABLE (HUD_WorkLoad_Manager_ID INT)


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO @wlmList SELECT * FROM dbo.fn_StringSplitterToInt(@wlmListString)
	IF EXISTS (SELECT * FROM @wlmList) 
	BEGIN
		SET @useWlmList = 1
	END
	ELSE
	BEGIN
		SET @useWlmList = 0
	END

SELECT DISTINCT pm.[HUD_Project_Manager_ID], pm.[HUD_Project_Manager_Name]
  FROM [dbo].[ProjectInfo] pri,
       [dbo].[HUD_Project_Manager] pm
WHERE (@useWlmList = 0 OR pri.HUD_WorkLoad_Manager_ID IN (SELECT * FROM @wlmList))
AND   pri.HUD_Project_Manager_ID is not null
AND   pm.[HUD_Project_Manager_ID] = pri.[HUD_Project_Manager_ID]
END


