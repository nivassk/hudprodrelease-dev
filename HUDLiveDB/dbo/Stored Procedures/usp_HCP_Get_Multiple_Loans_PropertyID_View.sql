﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Multiple_Loans_PropertyID_View]

AS
WITH Multiple_Loans_PropertyID (PropertyID,ProjectName,PropertyID_Count,OwnerCompanyType,OwnerContact,OwnerPhone)
AS
(
SELECT 
PI.PropertyID,
PI.ProjectName,
(SELECT Count(1) FROM [$(DatabaseName)].[dbo].[ProjectInfo] PI1 WHERE PI1.propertyid = PI.propertyid) AS PropertyID_Count,
PI.Owner_Company_Type as OwnerCompanyType,
PI.Owner_Contact_Indv_Fullname as OwnerContact,
Addr.PhonePrimary as OwnerPhone 
FROM [$(DatabaseName)].[dbo].[ProjectInfo] PI
LEFT JOIN [$(DatabaseName)].[dbo].[Address] Addr
ON Addr.AddressID = PI.Owner_AddressID
)

SELECT PropertyID, ProjectName, PropertyID_Count, OwnerCompanyType, OwnerContact, OwnerPhone 
FROM Multiple_Loans_PropertyID
WHERE PropertyID_Count > 1
GROUP BY PropertyID, ProjectName, PropertyID_Count, OwnerCompanyType, OwnerContact, OwnerPhone


