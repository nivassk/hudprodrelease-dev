﻿
create PROCEDURE [dbo].[usp_HCP_Prod_GetApplicationDetailsForSharePointScreen] 
( @TaskInstanceId uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 fr.*, ISNULL(fr.IsLIHTC,0) as IsLIHTC
	FROM [$(TaskDB)].dbo.Task tk
	JOIN Prod_FHANumberRequest fr
	ON tk.FhaNumber = fr.FHANumber
	WHERE tk.TaskInstanceId = @TaskInstanceId
	ORDER BY fr.ModifiedOn DESC
END


GO

