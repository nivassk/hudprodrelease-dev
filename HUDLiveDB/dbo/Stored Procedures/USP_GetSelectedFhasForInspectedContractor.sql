﻿CREATE  PROCEDURE [dbo].[USP_GetSelectedFhasForInspectedContractor](
  @userID int
)
AS
  SELECT fhaRequest.FHANumber,loanType.ProjectTypeName AS FHADescription
  FROM User_Lender user_lender
  join Prod_FHANumberRequest fhaRequest ON fhaRequest.FHANumber = user_lender.FHANumber
  join Prod_ProjectType loanType ON fhaRequest.ProjectTypeId = loanType.ProjectTypeId
  WHERE user_lender.User_ID=@userID

GO

