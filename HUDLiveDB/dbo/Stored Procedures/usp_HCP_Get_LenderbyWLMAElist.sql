
-- =============================================
-- Author:		David Coddington
-- Create date: 06/19/2015
-- Description:	Get Lenders's for AE ID List
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Get_LenderbyWLMAElist] 
(
	@aeListString NVARCHAR(100),
	@wlmListString NVARCHAR(100)
)
AS
DECLARE @useWLMList BIT
DECLARE @useAEList BIT
DECLARE @wlmList TABLE (HUD_WorkLoad_Manager_ID INT)
DECLARE @aeList TABLE (HUD_Project_Manager_ID INT)


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO @wlmList SELECT * FROM dbo.fn_StringSplitterToInt(@wlmListString)
	IF EXISTS (SELECT * FROM @wlmList) 
	BEGIN
		SET @useWlmList = 1
	END
	ELSE
	BEGIN
		SET @useWlmList = 0
	END

	INSERT INTO @aeList SELECT * FROM dbo.fn_StringSplitterToInt(@aeListString)
	IF EXISTS (SELECT * FROM @aeList) 
	BEGIN
		SET @useAeList = 1
	END
	ELSE
	BEGIN
		SET @useAeList = 0
	END

SELECT DISTINCT li.LenderID, li.Lender_Name "LenderName"
  FROM [dbo].[ProjectInfo] pri,
       [dbo].[LenderInfo] li
WHERE (@useWlmList = 0 OR pri.HUD_WorkLoad_Manager_ID IN (SELECT * FROM @wlmList))
AND   (@useAeList = 0 OR pri.HUD_Project_Manager_ID IN (SELECT * FROM @aeList))
AND   pri.LenderID is not null
AND   li.LenderID = pri.LenderID
END

