﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Portfolio_Detail]
(
@QuarterToken VARCHAR(6),
@PortfolioNumber VARCHAR(20)
)
AS
  SELECT 
		d.Portfolio_Name,
		d.Portfolio_Number,
		d.FhaLinked AS FHANumber,
		ISNULL(c.[ProjectName], d.[ProjectName]) AS ProjectName
      ,ISNULL(c.[ServiceName], d.[Lender_Mortgagee_Name]) AS ServiceName
      ,ISNULL(c.[LenderID], d.[LenderID]) AS LenderID
      ,ISNULL(c.[Servicer_ID], d.[ServicerID]) AS ServicerID
      ,c.[OperatorOwner]
      ,c.[PeriodEnding]
      ,c.[MonthsInPeriod]
      ,c.[FinancialStatementType]
      ,c.[UnitsInFacility]
      ,c.[PropertyID]
      ,c.[OperatingCash]
      ,c.[Investments]
      ,c.[ReserveForReplacementEscrowBalance]
      ,c.[AccountsReceivable]
      ,c.[CurrentAssets]
      ,c.[CurrentLiabilities]
      ,c.[TotalRevenues]
      ,c.[RentLeaseExpense]
      ,c.[DepreciationExpense]
      ,c.[AmortizationExpense]
      ,c.[TotalExpenses]
      ,c.[NetIncome]
      ,c.[ReserveForReplacementDeposit]
      ,c.[FHAInsuredPrincipalPayment]
      ,c.[FHAInsuredInterestPayment]
      ,c.[MortgageInsurancePremium]
      ,(SELECT TOP 1 ldi.[DataInserted] 
	    FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] ldi
	    WHERE ldi.[LDI_ID] = c.[LDI_ID]) AS DateInserted
      ,c.[HUD_Project_Manager_ID]
      ,c.[LDI_ID]
      ,c.[ReserveForReplacementBalancePerUnit]
      ,c.[WorkingCapital]
      ,c.[DebtCoverageRatio]
      ,c.[DaysCashOnHand]
      ,c.[DaysInAcctReceivable]
      ,c.[AvgPaymentPeriod]
      ,c.[WorkingCapitalScore]
      ,c.[DebtCoverageRatioScore]
      ,c.[DaysCashOnHandScore]
      ,c.[DaysInAcctReceivableScore]
      ,c.[AvgPaymentPeriodScore]
      ,c.[ScoreTotal]
      ,c.[ModifiedBy]
      ,c.[OnBehalfOfBy]
      ,c.[UserID]
      ,c.[Deleted_Ind]
      ,c.[ModifiedOn]
	FROM Lender_DataUpload_Live c
	RIGHT JOIN 
	(SELECT
		MAX(aa.LDP_ID) AS maxid,
		COUNT(1) AS count,
		b.Portfolio_Number,
		b.Portfolio_Name,
		b.ProjectName,
		b.LenderID,
		b.ServicerID,
		b.Lender_Mortgagee_Name,
		b.FHANumber as FhaLinked
	  FROM [dbo].[Lender_DataUpload_Live] aa
	  RIGHT JOIN ProjectInfo b
	  ON aa.FHANumber=b.FHANumber AND dbo.fn_hcp_getquartertoken(aa.periodending, aa.monthsinperiod) = @QuarterToken
	  RIGHT JOIN Lender_FHANumber lf
	  ON b.FHANumber = lf.FHANumber
	  AND b.LenderID = lf.LenderID
	  where FHA_EndDate IS NULL
	  GROUP BY b.Portfolio_Number, b.Portfolio_Name, b.FHANumber, b.Lender_Mortgagee_Name, b.ProjectName, b.LenderID, b.ServicerID
	  HAVING b.Portfolio_Number = @PortfolioNumber) d
	  ON c.FHANumber=d.FhaLinked AND c.LDP_ID = d.maxid 

