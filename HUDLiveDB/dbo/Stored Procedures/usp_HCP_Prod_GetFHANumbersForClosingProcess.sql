﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetFHANumbersForClosingProcess]
(
@lenderId int,
@PageTypeId int
)
AS

BEGIN

select PN.FhaNumber from Prod_NextStage PN join Prod_FHANumberRequest FR 
on PN.FhaNumber=FR.FHANumber 
join [$(TaskDB)].dbo.Task task 
on PN.FhaNumber=task.FhaNumber
where FR.LenderId= @lenderId
--and PN.NextPgTypeId=10
and task.PageTypeId=16 and task.TaskStepId =23 
and PN.FhaNumber not in (select FhaNumber from Prod_NextStage WHERE NextPgTypeId=@PageTypeId and NextPgTaskInstanceId IS NOT NULL)
		
end