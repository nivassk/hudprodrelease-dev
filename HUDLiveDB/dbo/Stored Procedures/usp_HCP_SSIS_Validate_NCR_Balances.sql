﻿
CREATE PROCEDURE [dbo].[usp_HCP_SSIS_Validate_NCR_Balances]

	@profile NVARCHAR(100),

	@result BIT OUTPUT	-- return 1 if validated, return 0 if any violations

AS

BEGIN

	IF (@profile IS NULL OR @profile = '')

	BEGIN

		SET @profile = 'Test'	-- 'C3_EXCEL_Validation' for 162.242.189.3

	END



	DECLARE @count INT

	DECLARE @OverMil BIT

	DECLARE @DiffBal BIT



	SET @OverMil = 0

	SET @DiffBal = 0

	SET @result = 1



	CREATE TABLE #NCREBalanceViolation

	(

		FHANumber NVARCHAR(15),

		PropertyId INT,

		LenderId INT,

		OldBalance DECIMAL(19,2),

		NewBalance DECIMAL(19,2),

		ViolationType INT	-- 0: Over $1M, 1: NCRE Balance changed

	);



	-- Check if balance is over $1M

	SELECT @count = count(fha_number_with_suffix) FROM Loans$ WHERE ncre_balance > 1000000.00



	IF(@count > 0)

	BEGIN

		INSERT INTO #NCREBalanceViolation(FHANumber, PropertyId, LenderId, OldBalance, NewBalance, ViolationType)

		SELECT lo.fha_number_with_suffix, lo.property_id, lo.servicing_mortgagee_id, npri.InitialNCREBalance, lo.ncre_balance, 0

		FROM Loans$ lo, NonCritical_ProjectInfo npri WHERE lo.ncre_balance > 1000000.00 AND lo.fha_number_with_suffix = npri.FhaNumber 

		AND lo.property_id = npri.PropertyId AND lo.servicing_mortgagee_id = npri.LenderId



		SET @OverMil = 1

	END



	-- Check if balance is different from initial balance

	SELECT @count = count(fha_number_with_suffix) FROM Loans$ lo, NonCritical_ProjectInfo npri WHERE lo.ncre_balance <> npri.InitialNCREBalance

	AND npri.InitialNCREBalance IS NOT NULL AND lo.fha_number_with_suffix = npri.FhaNumber AND lo.property_id = npri.PropertyId

	AND lo.servicing_mortgagee_id = npri.LenderId



	IF(@count > 0)

	BEGIN

		INSERT INTO #NCREBalanceViolation(FHANumber, PropertyId, LenderId, OldBalance, NewBalance, ViolationType)

		SELECT lo.fha_number_with_suffix, lo.property_id, lo.servicing_mortgagee_id, npri.InitialNCREBalance, lo.ncre_balance, 1

		FROM Loans$ lo, NonCritical_ProjectInfo npri WHERE lo.ncre_balance <> npri.InitialNCREBalance

		AND npri.InitialNCREBalance IS NOT NULL AND lo.fha_number_with_suffix = npri.FhaNumber 

		AND lo.property_id = npri.PropertyId AND lo.servicing_mortgagee_id = npri.LenderId



		SET @DiffBal = 1

	END



	-- Check if there's any violations

	SELECT @count = count(FHANumber) FROM #NCREBalanceViolation



	IF(@count > 0)

	BEGIN

		SET @result = 0



		DECLARE @subj NVARCHAR(255)

		DECLARE @body_text NVARCHAR(MAX)

		DECLARE @table NVARCHAR(MAX)



		SET @subj = 'Weekly Feed SSIS NCRE Balance Violation on ' + CONVERT(NVARCHAR(10), GETDATE(), 101) + ' (' + CAST(DB_NAME() AS NVARCHAR(30)) + ')'

		SET @body_text = '<P>SSIS package deployment on ' + CONVERT(NVARCHAR(10), GETDATE(), 101) + ' to ' + CAST(DB_NAME() AS NVARCHAR(30))

			+ ' for Weekly Feed has encountered records that violate the NCRE Balance business rules. '

			+ 'Inform HUD or make necessary changes to NCRE Balance data.</P><P>Refer to the table(s) below for more information.</P>'



		IF(@OverMil = 1)

		BEGIN

			SET @table = CAST((

				SELECT TD = nbv.FHANumber + '</TD><TD>' + CAST(nbv.PropertyId AS NVARCHAR(10)) + '</TD><TD>'

				+ CAST(nbv.LenderId AS NVARCHAR(10)) + '</TD><TD>' + CAST(nbv.OldBalance AS NVARCHAR(22)) + '</TD><TD>'

				+ CAST(nbv.NewBalance AS NVARCHAR(22))

				FROM #NCREBalanceViolation nbv WHERE nbv.ViolationType = 0

				FOR XML PATH('TR'), TYPE) AS NVARCHAR(MAX))



			SET @body_text = @body_text + '<P><SPAN STYLE="font-weight:bold">NCRE Balance(s) Over $1 Million</SPAN></P>'

				+ '<TABLE CELLPADDING="1" CELLSPACING="1" BORDER="1">'

				+ '<TR><TH>FHA Number</TH><TH>Property ID</TH><TH>Lender ID</TH><TH>Old NCRE Balance</TH>'

				+ '<TH>New NCRE Balance</TH></TR>'

				+ REPLACE(REPLACE(@table, '&lt;', '<' ), '&gt;', '>')

				+ '</TABLE>'

		END

			

		IF(@DiffBal = 1)

		BEGIN

			SET @table = CAST((

				SELECT TD = nbv.FHANumber + '</TD><TD>' + CAST(nbv.PropertyId AS NVARCHAR(10)) + '</TD><TD>'

				+ CAST(nbv.LenderId AS NVARCHAR(10)) + '</TD><TD>' + CAST(nbv.OldBalance AS NVARCHAR(22)) + '</TD><TD>'

				+ CAST(nbv.NewBalance AS NVARCHAR(22))

				FROM #NCREBalanceViolation nbv WHERE nbv.ViolationType = 1

				FOR XML PATH('TR'), TYPE) AS NVARCHAR(MAX))



			SET @body_text = @body_text 

				+ '<P><SPAN STYLE="font-weight:bold">New NCRE Balance(s) Different From Previous NCRE Balance(s)</SPAN></P>'

				+ '<TABLE CELLPADDING="1" CELLSPACING="1" BORDER="1">'

				+ '<TR><TH>FHA Number</TH><TH>Property ID</TH><TH>Lender ID</TH><TH>Old NCRE Balance</TH>'

				+ '<TH>New NCRE Balance</TH></TR>'

				+ REPLACE(REPLACE(@table, '&lt;', '<' ), '&gt;', '>')

				+ '</TABLE>'

		END



		SET @body_text = @body_text + '<P>Thank you,<BR/>C3 Systems HHCP Support</P>'



		EXEC msdb.dbo.sp_send_dbmail     

		@profile_name = @profile,

		@from_address = 'mmahimaluru@gmail.com',

		@recipients = 'cpark@c3-systems.com; mmahimaluru@c3-systems.com; rganapathy@c3-systems.com',

		@copy_recipients = 'ckendrick@c3-systems.com; NKumari@c3-systems.com;ukumar@c3-systems.com;Dkalyanasundaram@c3-systems.com;jblair@c3-systems.com; srahman@c3-systems.com',

		@subject = @subj,

		@body = @body_text,

		@body_format = 'HTML',

		@importance = 'High'

	END		

END

GO

