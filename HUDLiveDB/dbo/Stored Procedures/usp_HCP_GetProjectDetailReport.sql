
CREATE    PROCEDURE [dbo].[usp_HCP_GetProjectDetailReport]
@UserType NVARCHAR( 100 ),
@Username  NVARCHAR( 100 ),
@QueryDate NVARCHAR( 50 )
AS

Begin
declare @LenderIntermediate TABLE (

FHANumber nvarchar( 15 )  NULL,
ProjectName nvarchar( 100 )  NULL,
ServiceName nvarchar( 100 ) NULL,
UnitsInFacility INT NULL,
PeriodEnding NVARCHAR( 25 ) NULL,
MonthsInPeriod NVARCHAR( 5 ),
NOICumulative DECIMAL( 19 ,2 ) NULL,
QuaterlyNoi DECIMAL( 19 ,2 ) NULL,
NOIQuaterbyQuater DECIMAL( 19 ,2 ) NULL,
NOIPercentageChange DECIMAL( 19 ,2 ) NULL,
CumulativeRevenue DECIMAL( 19 ,2 ) NULL,
CumulativeExpenses DECIMAL( 19 ,2 ) NULL,
QuaterlyOperatingRevenue  DECIMAL (19 , 2) NULL,
QuarterlyOperatingExpense DECIMAL (19 , 2) NULL,
RevenueQuarterbyQuarter DECIMAL (19 , 2) NULL,
RevenuePercentageChange DECIMAL (19 , 2) NULL,
DSCRDifferencePerQuarter DECIMAL (19 , 2) NULL,
DSCRPercentageChange DECIMAL( 19 ,2 ) NULL,
CumulativeResidentDays DECIMAL (19 , 2) NULL,
QuarterlyResidentDays INT  NULL,
ADRperQuarter DECIMAL( 19 ,2 ) NULL,
ADRDifferencePerQuarter DECIMAL (19 , 2) NULL,
ADRPercentageChange DECIMAL( 19 ,2 ) NULL,
DebtCoverageRatio2 DECIMAL( 19 ,2 ) NULL,
AverageDailyRateRatio DECIMAL( 19 ,2 ) NULL,
QuarterlyDSCR DECIMAL( 19 ,2 ) NULL,
IsQtrlyORevError INT NULL,
IsQtrlyNOIError INT NULL,
IsQtrlyNOIPTError INT NULL,
IsQtrlyDSCRError INT NULL,
IsQtrlyDSCRPTError INT NULL,
IsADRError INT NULL,
IsProjectError int NULL,
fhainsuredprincipalInterestpayment DECIMAL ( 19 , 2 ) NULL,
DataInserted datetime NULL,
Ismissing int null,
QuarterlyMIP DECIMAL( 19 ,2 ) NULL,
QuarterlyFHAPI DECIMAL( 19 ,2 ) NULL,
MIPCumulative DECIMAL( 19 ,2 ) NULL
)

--Restrict acces to old data
declare @dt  Datetime
set @dt= convert (datetime , @QueryDate, 101 )


INSERT INTO @LenderIntermediate SELECT DISTINCT   FHANumber , ProjectName , ServiceName,    UnitsInFacility , PeriodEnding , MonthsInPeriod , NOICumulative , QuaterlyNoi , NOIQuaterbyQuater , NOIPercentageChange  , CumulativeRevenue, CumulativeExpenses , QuaterlyOperatingRevenue , QuarterlyOperatingExpense, RevenueQuarterbyQuarter ,RevenuePercentageChange , DSCRDifferencePerQuarter, DSCRPercentageChange , CumulativeResidentDays , CONVERT ( int, QuarterlyResidentDays ) as QuarterlyResidentDays, ADRperQuarter , ADRDifferencePerQuarter, ADRPercentageChange ,DebtCoverageRatio2 , AverageDailyRateRatio, QuarterlyDSCR ,IsQtrlyORevError , IsQtrlyNOIError, IsQtrlyNOIPTError , IsQtrlyDSCRError , IsQtrlyDSCRPTError , IsADRError , IsProjectError, fhainsuredprincipalInterestpayment ,DataInserted , Ismissing , QuarterlyMIP, QuarterlyFHAPI  ,MIPCumulative     
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate a join
( select FHANumber as FHA  , MonthsInPeriod as MIP , max ( DataInserted ) as createddt,max (  LDI_ID ) as LDIRecent, YEAR(   CONVERT ( DATETIME , PeriodEnding )) as YR
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate
group by FHANumber ,MonthsInPeriod , YEAR( CONVERT ( DATETIME , PeriodEnding ))) b
on a . FHANumber = b . FHA and a . DataInserted = b . createddt and    a. LDI_ID= b .LDIRecent and  CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt

union

SELECT DISTINCT   FHANumber ,ProjectName , ServiceName,    UnitsInFacility , PeriodEnding , MonthsInPeriod ,NOICumulative , QuaterlyNoi , NOIQuaterbyQuater , NOIPercentageChange  , CumulativeRevenue ,CumulativeExpenses , QuaterlyOperatingRevenue , QuarterlyOperatingExpense, RevenueQuarterbyQuarter , RevenuePercentageChange , DSCRDifferencePerQuarter ,DSCRPercentageChange , CumulativeResidentDays, CONVERT ( int, QuarterlyResidentDays ) as QuarterlyResidentDays, ADRperQuarter , ADRDifferencePerQuarter ,  ADRPercentageChange, DebtCoverageRatio2 , AverageDailyRateRatio , QuarterlyDSCR , IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError, IsADRError , IsProjectError, fhainsuredprincipalInterestpayment ,DataInserted , Ismissing, QuarterlyMIP ,QuarterlyFHAPI , MIPCumulative       
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate_InActive a join
( select FHANumber as FHA  , MonthsInPeriod as MIP , max ( DataInserted ) as createddt,max (  LDI_ID ) as LDIRecent, YEAR(   CONVERT ( DATETIME , PeriodEnding )) as YR
from [$(IntermDB)].dbo. Lender_DataUpload_Intermediate_InActive
group by FHANumber ,MonthsInPeriod , YEAR( CONVERT ( DATETIME , PeriodEnding ))) b
on a . FHANumber = b . FHA and a . DataInserted = b . createddt and a. LDI_ID= b .LDIRecent and   CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt

--SELECT * FROM  @LenderIntermediate
--SET @UserType='AccountExecutive'
--SET @Username ='EDWARD.E.CHLYSTEK@hud.gov'


IF (@UserType = 'InternalSpecialOptionUser')
BEGIN
SELECT LDI. MIPCumulative , LDI . QuarterlyFHAPI, LDI .QuarterlyMIP , LDI. Ismissing , p. PropertyID, DataInserted ,LI . Lender_Name as LenderName, p. Amortized_Unpaid_Principal_Bal as UnPaidBalance,isnull(p.CreditReview,' ') as CreditReview, fhainsuredprincipalInterestpayment, Case when IsNull (Portfolio_Number , '0')= '0' and IsNull( Portfolio_Name ,'0' )<> '0' Then 'NA_' + convert (varchar ( max), IsNull( Portfolio_Name ,'0' ))   Else IsNull (Portfolio_Number , '0')   END  as PortfolioNumber ,
       Case when IsNull( Portfolio_Name ,'0' )= '0' and IsNull (Portfolio_Number , '0')<> '0'  Then 'NA_' + convert ( nvarchar( max ), IsNull (Portfolio_Number , '0'))   Else IsNull (Portfolio_Name , '0')   END  as PortfolioName, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , COALESCE( IsQtrlyORevError ,0 ) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE (IsADRError , 0)   AS RowWiseError , '<font color="red">' + CAST ( COALESCE (IsQtrlyORevError , 0) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE( IsADRError ,0 ) as varchar( max )) + '</font>' AS  RowWiseErrorchar, LDI . FHANumber , LDI . ProjectName , LDI . ServiceName , wlm. HUD_WorkLoad_Manager_Name , ae . HUD_Project_Manager_Name , LDI . UnitsInFacility, CONVERT ( VARCHAR( 10 ),cast ( LDI . PeriodEnding as date ), 101 ) AS PeriodEnding , CONVERT ( int , LDI . MonthsInPeriod ) AS MonthsInPeriod   , LDI. NOICumulative , CASE WHEN LDI . QuaterlyNoi is null then    ISNULL ( CAST ( LDI. QuaterlyNoi as varchar ( max ))   , '-') when LDI . QuaterlyNoi is not NULL and IsQtrlyNOIError = 1 OR IsQtrlyNOIPTError = 1     THEN '<font color="red">' + ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' ) + '</font>' else  ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' )  END as QuaterlyNoi ,CASE WHEN LDI. NOIQuaterbyQuater is null then    ISNULL( CAST ( LDI . NOIQuaterbyQuater as varchar (max   ))  , '0' ) when LDI . NOIQuaterbyQuater is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError =1     THEN '<font color="red">' + ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max ))   , '-' )   END as NOIQuaterbyQuater   , CASE WHEN LDI. NOIPercentageChange is null then    ISNULL (CAST (LDI . NOIPercentageChange as varchar( max )) + '%' , '-') WHEN LDI . NOIPercentageChange is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError = 1 
  THEN 
      CASE WHEN LDI. NOIPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. NOIPercentageChange='0' THEN ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as NOIPercentageChange, LDI . CumulativeRevenue, LDI. CumulativeExpenses , CASE WHEN LDI . QuaterlyOperatingRevenue is null then   ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-' ) when LDI. QuaterlyOperatingRevenue is not NULL and IsQtrlyORevError  = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. QuaterlyOperatingRevenue as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-') END as QuaterlyOperatingRevenue , LDI . QuarterlyOperatingExpense , CASE WHEN LDI. RevenueQuarterbyQuarter is null then    ISNULL( CAST ( LDI . RevenueQuarterbyQuarter as varchar ( max )) , '0') when LDI . RevenueQuarterbyQuarter is not   NULL and IsQtrlyORevError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenueQuarterbyQuarter as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . RevenueQuarterbyQuarter as varchar( max ))   , '-') END as RevenueQuarterbyQuarter ,
CASE WHEN LDI . RevenuePercentageChange is null then   ISNULL (CAST (LDI . RevenuePercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . RevenuePercentageChange is not NULL and IsQtrlyORevError = 1    
  THEN 
      CASE WHEN LDI. RevenuePercentageChange='0' THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>' 
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. RevenuePercentageChange='0' THEN ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as RevenuePercentageChange  , CASE WHEN LDI . DSCRDifferencePerQuarter is null then    ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . DSCRDifferencePerQuarter is   not NULL and   IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError =1     THEN '<font color="red">' +  ISNULL (CAST (LDI . DSCRDifferencePerQuarter as varchar( max ))   , '-' ) + '</font>' else ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar (max ))  , '-' )    END as DSCRDifferencePerQuarter, CASE WHEN LDI . DSCRPercentageChange is null then   ISNULL (CAST (LDI . DSCRPercentageChange as varchar( max )) + '%' , '-')  

WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1 and LDI. DSCRPercentageChange is not null  
  THEN 
      CASE WHEN LDI. DSCRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. DSCRPercentageChange='0' THEN ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as DSCRPercentageChange , CONVERT ( int, LDI . CumulativeResidentDays ) as CumulativeResidentDays ,LDI . QuarterlyResidentDays, CASE WHEN LDI. ADRperQuarter is null then    ISNULL( CAST ( LDI. ADRperQuarter as varchar (max ))  , '-' ) when LDI . ADRperQuarter  is not NULL and   IsADRError= 1     THEN '<font color="red">' + ISNULL( CAST ( LDI . ADRperQuarter as varchar ( max ))   , '-') + '</font>' else ISNULL( CAST ( LDI . ADRperQuarter  as varchar (max ))  , '-' )   END as ADRperQuarter , CASE WHEN LDI. ADRDifferencePerQuarter is null then   ISNULL (CAST (LDI . ADRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . ADRDifferencePerQuarter is not NULL and IsADRError = 1      THEN '<font color="red">' +  ISNULL ( CAST ( LDI. ADRDifferencePerQuarter as varchar ( max ))   , '-') + '</font>' else ISNULL( CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))  , '-' )    END as ADRDifferencePerQuarter, CASE WHEN LDI . ADRPercentageChange is null then    ISNULL (CAST (LDI . ADRPercentageChange as varchar( max )) + '%' , '-') 
WHEN LDI . ADRPercentageChange is not NULL and IsADRError = 1   
  THEN 
      CASE WHEN LDI. ADRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . ADRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. ADRPercentageChange='0' THEN ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as ADRPercentageChange ,LDI . DebtCoverageRatio2, ldi . AverageDailyRateRatio, CASE WHEN LDI. QuarterlyDSCR is null then     ISNULL( CAST ( LDI. QuarterlyDSCR as varchar (max ))  , '-' ) when LDI . QuarterlyDSCR is not   NULL and IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1      THEN '<font color="red">' + ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max ))   , '-' )   END as QuarterlyDSCR, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , IsProjectError
FROM   @LenderIntermediate LDI
LEFT JOIN dbo .ProjectInfo p ON
LDI .FHANumber = p. FHANumber
LEFT JOIN dbo .LenderInfo LI ON
P .LenderID = LI. LenderID
JOIN User_Lender ul on ul .FHANumber = p. FHANumber
  JOIN HCP_Authentication auth on auth. userid = ul . User_ID
JOIN dbo. HUD_Project_Manager ae ON p . HUD_Project_Manager_ID = ae. HUD_Project_Manager_ID
JOIN address a ON a . AddressID = ae .AddressID
JOIN dbo. HUD_WorkLoad_Manager wlm ON p . HUD_WorkLoad_Manager_ID = wlm. HUD_WorkLoad_Manager_ID
WHERE   auth. username =  @username ORDER BY LDI .datainserted desc
END
Else IF ( @UserType = 'AccountExecutive' )
BEGIN
SELECT LDI. MIPCumulative , LDI . QuarterlyFHAPI, LDI .QuarterlyMIP , LDI. Ismissing , p. PropertyID, DataInserted ,LI . Lender_Name as LenderName, p. Amortized_Unpaid_Principal_Bal as UnPaidBalance,isnull(p.CreditReview,' ') as CreditReview, fhainsuredprincipalInterestpayment, Case when IsNull (Portfolio_Number , '0')= '0' and IsNull( Portfolio_Name ,'0' )<> '0' Then 'NA_' + convert (varchar ( max), IsNull( Portfolio_Name ,'0' ))   Else IsNull (Portfolio_Number , '0')   END  as PortfolioNumber ,
       Case when IsNull( Portfolio_Name ,'0' )= '0' and IsNull (Portfolio_Number , '0')<> '0'  Then 'NA_' + convert ( nvarchar( max ), IsNull (Portfolio_Number , '0'))   Else IsNull (Portfolio_Name , '0')   END  as PortfolioName, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , COALESCE( IsQtrlyORevError ,0 ) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE (IsADRError , 0)   AS RowWiseError , '<font color="red">' + CAST ( COALESCE (IsQtrlyORevError , 0) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE( IsADRError ,0 ) as varchar( max )) + '</font>' AS  RowWiseErrorchar, LDI . FHANumber , LDI . ProjectName , LDI . ServiceName , wlm. HUD_WorkLoad_Manager_Name , ae . HUD_Project_Manager_Name , LDI . UnitsInFacility, CONVERT ( VARCHAR( 10 ),cast ( LDI . PeriodEnding as date ), 101 ) AS PeriodEnding , CONVERT ( int , LDI . MonthsInPeriod ) AS MonthsInPeriod   , LDI. NOICumulative , CASE WHEN LDI . QuaterlyNoi is null then    ISNULL ( CAST ( LDI. QuaterlyNoi as varchar ( max ))   , '-') when LDI . QuaterlyNoi is not NULL and IsQtrlyNOIError = 1 OR IsQtrlyNOIPTError = 1     THEN '<font color="red">' + ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' ) + '</font>' else  ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' )  END as QuaterlyNoi ,CASE WHEN LDI. NOIQuaterbyQuater is null then    ISNULL( CAST ( LDI . NOIQuaterbyQuater as varchar (max   ))  , '0' ) when LDI . NOIQuaterbyQuater is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError =1     THEN '<font color="red">' + ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max ))   , '-' )   END as NOIQuaterbyQuater   , CASE WHEN LDI. NOIPercentageChange is null then    ISNULL (CAST (LDI . NOIPercentageChange as varchar( max )) + '%' , '-') WHEN LDI . NOIPercentageChange is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError = 1 
  THEN 
      CASE WHEN LDI. NOIPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. NOIPercentageChange='0' THEN ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as NOIPercentageChange , LDI . CumulativeRevenue, LDI. CumulativeExpenses , CASE WHEN LDI . QuaterlyOperatingRevenue is null then   ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-' ) when LDI. QuaterlyOperatingRevenue is not NULL and IsQtrlyORevError  = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. QuaterlyOperatingRevenue as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-') END as QuaterlyOperatingRevenue , LDI . QuarterlyOperatingExpense , CASE WHEN LDI. RevenueQuarterbyQuarter is null then    ISNULL( CAST ( LDI . RevenueQuarterbyQuarter as varchar ( max )) , '0') when LDI . RevenueQuarterbyQuarter is not   NULL and IsQtrlyORevError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenueQuarterbyQuarter as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . RevenueQuarterbyQuarter as varchar( max ))   , '-') END as RevenueQuarterbyQuarter ,
CASE WHEN LDI . RevenuePercentageChange is null then   ISNULL (CAST (LDI . RevenuePercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . RevenuePercentageChange is not NULL and IsQtrlyORevError = 1    
  THEN 
      CASE WHEN LDI. RevenuePercentageChange='0' THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>' 
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. RevenuePercentageChange='0' THEN ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as RevenuePercentageChange  , CASE WHEN LDI . DSCRDifferencePerQuarter is null then    ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . DSCRDifferencePerQuarter is   not NULL and   IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError =1     THEN '<font color="red">' +  ISNULL (CAST (LDI . DSCRDifferencePerQuarter as varchar( max ))   , '-' ) + '</font>' else ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar (max ))  , '-' )    END as DSCRDifferencePerQuarter, CASE WHEN LDI . DSCRPercentageChange is null then   ISNULL (CAST (LDI . DSCRPercentageChange as varchar( max )) + '%' , '-')  
WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1 and LDI. DSCRPercentageChange is not null  
  THEN 
      CASE WHEN LDI. DSCRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. DSCRPercentageChange='0' THEN ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as DSCRPercentageChange , CONVERT ( int, LDI . CumulativeResidentDays ) as CumulativeResidentDays, LDI . QuarterlyResidentDays , CASE WHEN LDI . ADRperQuarter is null then    ISNULL( CAST ( LDI . ADRperQuarter as varchar ( max ))   , '-') when LDI . ADRperQuarter is not NULL and   IsADRError= 1     THEN '<font color="red">' + ISNULL (CAST (LDI . ADRperQuarter as varchar( max ))   , '-' ) + '</font>' else ISNULL (CAST (LDI . ADRperQuarter as varchar( max ))   , '-' )  END as ADRperQuarter, CASE WHEN LDI. ADRDifferencePerQuarter is null then    ISNULL( CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))  , '0' ) when LDI . ADRDifferencePerQuarter is not NULL and IsADRError = 1       THEN '<font color="red">' + ISNULL ( CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))   , '-' ) + '</font>' else ISNULL (CAST (LDI . ADRDifferencePerQuarter as varchar( max ))   , '-' )    END as ADRDifferencePerQuarter , CASE WHEN LDI . ADRPercentageChange is null then     ISNULL( CAST ( LDI . ADRPercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . ADRPercentageChange is not NULL and IsADRError = 1   
  THEN 
      CASE WHEN LDI. ADRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . ADRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. ADRPercentageChange='0' THEN ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as ADRPercentageChange, LDI . DebtCoverageRatio2 , ldi . AverageDailyRateRatio, CASE WHEN LDI. QuarterlyDSCR is null  then    ISNULL( CAST ( LDI. QuarterlyDSCR as varchar (max ))  , '-' ) when LDI . QuarterlyDSCR  is not NULL and IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1      THEN '<font color="red">'  + ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max ))   , '-' )   END as QuarterlyDSCR, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , IsProjectError
FROM   @LenderIntermediate LDI
LEFT JOIN dbo .ProjectInfo p ON
LDI .FHANumber = p. FHANumber
LEFT JOIN dbo .LenderInfo LI ON
P .LenderID = LI. LenderID
JOIN dbo. HUD_Project_Manager ae ON p . HUD_Project_Manager_ID = ae. HUD_Project_Manager_ID
JOIN address a ON a . AddressID = ae .AddressID
JOIN dbo. HUD_WorkLoad_Manager wlm ON p . HUD_WorkLoad_Manager_ID = wlm. HUD_WorkLoad_Manager_ID
WHERE   a. email =  @Username ORDER BY LDI .datainserted desc
END

ELSE IF ( @UserType = 'WorkflowManager')
BEGIN
SELECT LDI. MIPCumulative , LDI. QuarterlyFHAPI, LDI .QuarterlyMIP , LDI. Ismissing ,p . PropertyID, DataInserted, LI. Lender_Name as LenderName , p. Amortized_Unpaid_Principal_Bal as UnPaidBalance,isnull(p.CreditReview,' ') as CreditReview, fhainsuredprincipalInterestpayment, Case when IsNull (Portfolio_Number , '0')= '0' and IsNull( Portfolio_Name ,'0' )<> '0' Then 'NA_' + convert (varchar ( max), IsNull( Portfolio_Name ,'0' ))   Else IsNull (Portfolio_Number , '0')   END  as PortfolioNumber ,
       Case when IsNull( Portfolio_Name ,'0' )= '0' and IsNull (Portfolio_Number , '0')<> '0'  Then 'NA_' + convert ( nvarchar( max ), IsNull (Portfolio_Number , '0'))   Else IsNull (Portfolio_Name , '0')   END  as PortfolioName, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , COALESCE( IsQtrlyORevError ,0 ) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE (IsADRError , 0)   AS RowWiseError , '<font color="red">' + CAST ( COALESCE (IsQtrlyORevError , 0) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE( IsADRError ,0 ) as varchar( max )) + '</font>' AS  RowWiseErrorchar, LDI . FHANumber , LDI . ProjectName , LDI . ServiceName , wlm. HUD_WorkLoad_Manager_Name , ae . HUD_Project_Manager_Name , LDI . UnitsInFacility, CONVERT ( VARCHAR( 10 ),cast ( LDI . PeriodEnding as date ), 101 ) AS PeriodEnding , CONVERT ( int , LDI . MonthsInPeriod ) AS MonthsInPeriod   , LDI. NOICumulative , CASE WHEN LDI . QuaterlyNoi is null then    ISNULL ( CAST ( LDI. QuaterlyNoi as varchar ( max ))   , '-') when LDI . QuaterlyNoi is not NULL and IsQtrlyNOIError = 1 OR IsQtrlyNOIPTError = 1     THEN '<font color="red">' + ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' ) + '</font>' else  ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' )  END as QuaterlyNoi ,CASE WHEN LDI. NOIQuaterbyQuater is null then    ISNULL( CAST ( LDI . NOIQuaterbyQuater as varchar (max   ))  , '0' ) when LDI . NOIQuaterbyQuater is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError =1     THEN '<font color="red">' + ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max ))   , '-' )   END as NOIQuaterbyQuater   , CASE WHEN LDI. NOIPercentageChange is null then    ISNULL (CAST (LDI . NOIPercentageChange as varchar( max )) + '%' , '-') WHEN LDI . NOIPercentageChange is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError = 1 
  THEN 
      CASE WHEN LDI. NOIPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. NOIPercentageChange='0' THEN ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as NOIPercentageChange , LDI . CumulativeRevenue, LDI. CumulativeExpenses , CASE WHEN LDI . QuaterlyOperatingRevenue is null then   ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-' ) when LDI. QuaterlyOperatingRevenue is not NULL and IsQtrlyORevError  = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. QuaterlyOperatingRevenue as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-') END as QuaterlyOperatingRevenue , LDI . QuarterlyOperatingExpense , CASE WHEN LDI. RevenueQuarterbyQuarter is null then    ISNULL( CAST ( LDI . RevenueQuarterbyQuarter as varchar ( max )) , '0') when LDI . RevenueQuarterbyQuarter is not   NULL and IsQtrlyORevError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenueQuarterbyQuarter as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . RevenueQuarterbyQuarter as varchar( max ))   , '-') END as RevenueQuarterbyQuarter ,
CASE WHEN LDI . RevenuePercentageChange is null then   ISNULL (CAST (LDI . RevenuePercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . RevenuePercentageChange is not NULL and IsQtrlyORevError = 1    
  THEN 
      CASE WHEN LDI. RevenuePercentageChange='0' THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>' 
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. RevenuePercentageChange='0' THEN ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as RevenuePercentageChange  , CASE WHEN LDI . DSCRDifferencePerQuarter is null then    ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . DSCRDifferencePerQuarter is   not NULL and   IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError =1     THEN '<font color="red">' +  ISNULL (CAST (LDI . DSCRDifferencePerQuarter as varchar( max ))   , '-' ) + '</font>' else ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar (max ))  , '-' )    END as DSCRDifferencePerQuarter, CASE WHEN LDI . DSCRPercentageChange is null then   ISNULL (CAST (LDI . DSCRPercentageChange as varchar( max )) + '%' , '-')  
WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1 and LDI. DSCRPercentageChange is not null  
  THEN 
      CASE WHEN LDI. DSCRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. DSCRPercentageChange='0' THEN ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as DSCRPercentageChange , CONVERT ( int, LDI . CumulativeResidentDays ) as CumulativeResidentDays ,LDI . QuarterlyResidentDays, CASE WHEN LDI. ADRperQuarter is null then    ISNULL( CAST ( LDI . ADRperQuarter as varchar ( max ))   , '-') when LDI. ADRperQuarter is not NULL and  IsADRError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. ADRperQuarter as varchar (max ))  , '-' ) + '</font>' else ISNULL (CAST (LDI . ADRperQuarter as varchar( max ))   , '-' )  END as ADRperQuarter, CASE WHEN LDI . ADRDifferencePerQuarter is null then   ISNULL (CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))  , '0' ) when LDI . ADRDifferencePerQuarter is not NULL and IsADRError = 1       THEN '<font color="red">' + ISNULL ( CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))   , '-' ) + '</font>' else ISNULL (CAST (LDI . ADRDifferencePerQuarter as varchar( max ))   , '-' )    END as ADRDifferencePerQuarter , CASE WHEN LDI . ADRPercentageChange is null then     ISNULL( CAST ( LDI . ADRPercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . ADRPercentageChange is not NULL and IsADRError = 1   
  THEN 
      CASE WHEN LDI. ADRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . ADRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. ADRPercentageChange='0' THEN ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as ADRPercentageChange, LDI . DebtCoverageRatio2 , ldi . AverageDailyRateRatio, CASE WHEN LDI. QuarterlyDSCR is null  then    ISNULL( CAST ( LDI. QuarterlyDSCR as varchar (max ))  , '-' ) when LDI . QuarterlyDSCR  is not NULL and IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1      THEN '<font color="red">'  + ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max ))   , '-' )   END as QuarterlyDSCR, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , IsProjectError
FROM   @LenderIntermediate LDI
LEFT JOIN dbo .ProjectInfo p ON
LDI .FHANumber = p. FHANumber
LEFT JOIN dbo .LenderInfo LI ON
P .LenderID = LI. LenderID

JOIN dbo. HUD_Project_Manager ae ON p . HUD_Project_Manager_ID = ae. HUD_Project_Manager_ID

JOIN dbo. HUD_WorkLoad_Manager wlm ON p . HUD_WorkLoad_Manager_ID = wlm. HUD_WorkLoad_Manager_ID
JOIN address a ON a . AddressID = wlm .AddressID
WHERE   a. email =  @Username
ORDER BY LDI .datainserted desc
--AND P. HUD_WorkLoad_Manager_ID = (SELECT HUD_WorkLoad_Manager_ID FROM [$(DatabaseName)].dbo. HUD_WorkLoad_Manager WHERE AddressID =(SELECT AddressID FROM [$(DatabaseName)].dbo. Address WHERE Email = @Username )) ORDER BY LDI .FHANumber, CONVERT ( DATETIME, LDI .PeriodEnding ), ldi.MonthsInPeriod asc
END

ELSE IF ( @UserType = 'HUDAdmin' OR @UserType = 'SuperUser' OR @UserType = 'HUDDirector' )
BEGIN
SELECT LDI. MIPCumulative , LDI . QuarterlyFHAPI, LDI .QuarterlyMIP , LDI. Ismissing , p. PropertyID, DataInserted ,LI . Lender_Name as LenderName, p. Amortized_Unpaid_Principal_Bal as UnPaidBalance,isnull(p.CreditReview,' ') as CreditReview, fhainsuredprincipalInterestpayment, Case when IsNull (Portfolio_Number , '0')= '0' and IsNull( Portfolio_Name ,'0' )<> '0' Then 'NA_' + convert (varchar ( max), IsNull( Portfolio_Name ,'0' ))   Else IsNull (Portfolio_Number , '0')   END  as PortfolioNumber ,
       Case when IsNull( Portfolio_Name ,'0' )= '0' and IsNull (Portfolio_Number , '0')<> '0'  Then 'NA_' + convert ( nvarchar( max ), IsNull (Portfolio_Number , '0'))   Else IsNull (Portfolio_Name , '0')   END  as PortfolioName, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , COALESCE( IsQtrlyORevError ,0 ) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE (IsADRError , 0)   AS RowWiseError , '<font color="red">' + CAST ( COALESCE (IsQtrlyORevError , 0) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE( IsADRError ,0 ) as varchar( max )) + '</font>' AS  RowWiseErrorchar, LDI . FHANumber , LDI . ProjectName , LDI . ServiceName , wlm. HUD_WorkLoad_Manager_Name , ae . HUD_Project_Manager_Name , LDI . UnitsInFacility, CONVERT ( VARCHAR( 10 ),cast ( LDI . PeriodEnding as date ), 101 ) AS PeriodEnding , CONVERT ( int , LDI . MonthsInPeriod ) AS MonthsInPeriod   , LDI. NOICumulative , CASE WHEN LDI . QuaterlyNoi is null then    ISNULL ( CAST ( LDI. QuaterlyNoi as varchar ( max ))   , '-') when LDI . QuaterlyNoi is not NULL and IsQtrlyNOIError = 1 OR IsQtrlyNOIPTError = 1     THEN '<font color="red">' + ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' ) + '</font>' else  ISNULL (CAST (LDI . QuaterlyNoi as varchar( max ))   , '-' )  END as QuaterlyNoi ,CASE WHEN LDI. NOIQuaterbyQuater is null then    ISNULL( CAST ( LDI . NOIQuaterbyQuater as varchar (max   ))  , '0' ) when LDI . NOIQuaterbyQuater is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError =1     THEN '<font color="red">' + ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . NOIQuaterbyQuater as varchar( max ))   , '-' )   END as NOIQuaterbyQuater   , CASE WHEN LDI. NOIPercentageChange is null then    ISNULL (CAST (LDI . NOIPercentageChange as varchar( max )) + '%' , '-') WHEN LDI . NOIPercentageChange is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError = 1 
  THEN 
      CASE WHEN LDI. NOIPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. NOIPercentageChange='0' THEN ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as NOIPercentageChange , LDI . CumulativeRevenue, LDI. CumulativeExpenses , CASE WHEN LDI . QuaterlyOperatingRevenue is null then   ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-' ) when LDI. QuaterlyOperatingRevenue is not NULL and IsQtrlyORevError  = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. QuaterlyOperatingRevenue as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . QuaterlyOperatingRevenue as varchar( max ))   , '-') END as QuaterlyOperatingRevenue , LDI . QuarterlyOperatingExpense , CASE WHEN LDI. RevenueQuarterbyQuarter is null then    ISNULL( CAST ( LDI . RevenueQuarterbyQuarter as varchar ( max )) , '0') when LDI . RevenueQuarterbyQuarter is not   NULL and IsQtrlyORevError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenueQuarterbyQuarter as varchar (max ))  , '-' ) + '</font>'   else ISNULL (CAST (LDI . RevenueQuarterbyQuarter as varchar( max ))   , '-') END as RevenueQuarterbyQuarter ,
CASE WHEN LDI . RevenuePercentageChange is null then   ISNULL (CAST (LDI . RevenuePercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . RevenuePercentageChange is not NULL and IsQtrlyORevError = 1    
  THEN 
      CASE WHEN LDI. RevenuePercentageChange='0' THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>' 
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. RevenuePercentageChange='0' THEN ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as RevenuePercentageChange  , CASE WHEN LDI . DSCRDifferencePerQuarter is null then    ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . DSCRDifferencePerQuarter is   not NULL and   IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError =1     THEN '<font color="red">' +  ISNULL (CAST (LDI . DSCRDifferencePerQuarter as varchar( max ))   , '-' ) + '</font>' else ISNULL ( CAST ( LDI. DSCRDifferencePerQuarter as varchar (max ))  , '-' )    END as DSCRDifferencePerQuarter, CASE WHEN LDI . DSCRPercentageChange is null then   ISNULL (CAST (LDI . DSCRPercentageChange as varchar( max )) + '%' , '-')  
WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1 and LDI. DSCRPercentageChange is not null  
  THEN 
      CASE WHEN LDI. DSCRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. DSCRPercentageChange='0' THEN ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as DSCRPercentageChange , CONVERT ( int, LDI . CumulativeResidentDays ) as CumulativeResidentDays ,LDI . QuarterlyResidentDays, CASE WHEN LDI. ADRperQuarter is null then    ISNULL( CAST ( LDI . ADRperQuarter as varchar ( max ))   , '-') when LDI. ADRperQuarter is not NULL and  IsADRError = 1     THEN '<font color="red">' + ISNULL ( CAST ( LDI. ADRperQuarter as varchar (max ))  , '-' ) + '</font>' else ISNULL ( CAST ( LDI. ADRperQuarter as varchar (max ))  , '-' )   END as ADRperQuarter , CASE WHEN LDI. ADRDifferencePerQuarter is null then    ISNULL ( CAST ( LDI. ADRDifferencePerQuarter as varchar ( max ))   , '0') when LDI . ADRDifferencePerQuarter is not NULL and IsADRError = 1       THEN '<font color="red">' + ISNULL ( CAST ( LDI. ADRDifferencePerQuarter as varchar (max ))   , '-' ) + '</font>' else ISNULL (CAST (LDI . ADRDifferencePerQuarter as varchar( max ))   , '-' )    END as ADRDifferencePerQuarter , CASE WHEN LDI . ADRPercentageChange is null then     ISNULL( CAST ( LDI . ADRPercentageChange as varchar ( max )) + '%' , '-' ) WHEN LDI . ADRPercentageChange is not NULL and IsADRError = 1   
  THEN 
      CASE WHEN LDI. ADRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . ADRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. ADRPercentageChange='0' THEN ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as ADRPercentageChange, LDI . DebtCoverageRatio2 , ldi . AverageDailyRateRatio, CASE WHEN LDI. QuarterlyDSCR is null  then    ISNULL( CAST ( LDI. QuarterlyDSCR as varchar (max ))  , '-' ) when LDI . QuarterlyDSCR  is not NULL and IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1      THEN '<font color="red">'  + ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max )) , '-' ) + '</font>' else ISNULL (CAST (LDI . QuarterlyDSCR as varchar( max ))   , '-' )   END as QuarterlyDSCR, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , IsProjectError
FROM   @LenderIntermediate LDI
LEFT JOIN dbo .ProjectInfo p ON
LDI .FHANumber = p. FHANumber
LEFT JOIN dbo .LenderInfo LI ON
P .LenderID = LI. LenderID
JOIN dbo. HUD_Project_Manager ae ON p . HUD_Project_Manager_ID = ae. HUD_Project_Manager_ID
JOIN address a ON a . AddressID = ae .AddressID
JOIN dbo. HUD_WorkLoad_Manager wlm ON p . HUD_WorkLoad_Manager_ID = wlm. HUD_WorkLoad_Manager_ID ORDER BY LDI . datainserted desc
END

ELSE IF ( @UserType = 'LenderAccountRepresentative' OR @UserType = 'LenderAccountManager' OR  @UserType = 'BackupAccountManager' )
BEGIN


DECLARE @LenderId INT
SElect @LenderId= lender .Lender_ID FROM dbo. HCP_Authentication auth
JOIN dbo. webpages_UsersInRoles uir ON auth . UserID = uir .UserId
JOIN dbo. webpages_Roles roles ON roles . RoleId = uir .RoleId
JOIN dbo. User_Lender lender ON lender . User_ID = uir. UserId
JOIN dbo. LenderInfo linfo ON linfo . LenderID = lender .Lender_ID
--WHERE auth.UserName = 'rahmansharminlender@gmail.com'
WHERE auth. UserName =@Username

--SELECT FHANumber FROM dbo.ProjectInfo WHERE lenderid = 30542

SELECT LDI. MIPCumulative , LDI . QuarterlyFHAPI, LDI .QuarterlyMIP , LDI. Ismissing ,'<font color="red">' + CAST ( COALESCE (IsQtrlyORevError , 0) + COALESCE (IsQtrlyNOIError , 0) + COALESCE ( IsQtrlyNOIPTError, 0 ) + COALESCE ( IsQtrlyDSCRError, 0 ) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE( IsADRError ,0 ) as varchar( max )) + '</font>' AS RowWiseErrorchar, p .PropertyID , DataInserted, LI .Lender_Name as LenderName, p. Amortized_Unpaid_Principal_Bal as UnPaidBalance,isnull(p.CreditReview,' ') as CreditReview, fhainsuredprincipalInterestpayment, Case when IsNull (Portfolio_Number , '0')= '0' and IsNull( Portfolio_Name ,'0' )<> '0' Then 'NA_' +  convert (varchar ( max), IsNull( Portfolio_Name ,'0' ))   Else IsNull (Portfolio_Number , '0')    END  as PortfolioNumber ,
       Case when IsNull( Portfolio_Name ,'0' )= '0' and IsNull (Portfolio_Number , '0')<> '0'  Then 'NA_' + convert ( nvarchar( max ), IsNull (Portfolio_Number , '0'))   Else IsNull (Portfolio_Name , '0')   END  as PortfolioName,   IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , COALESCE( IsQtrlyORevError ,0 ) + COALESCE (IsQtrlyNOIError , 0) + COALESCE (IsQtrlyNOIPTError , 0) + COALESCE (IsQtrlyDSCRError , 0) + COALESCE (IsQtrlyDSCRPTError , 0)+ COALESCE (IsADRError , 0)   AS RowWiseError , LDI. FHANumber , LDI . ProjectName, LDI. ServiceName , wlm .HUD_WorkLoad_Manager_Name , ae. HUD_Project_Manager_Name ,LDI . UnitsInFacility, CONVERT( VARCHAR (10 ), cast( LDI .PeriodEnding as date ), 101) AS PeriodEnding , CONVERT ( int , LDI . MonthsInPeriod ) AS MonthsInPeriod , LDI. NOICumulative , CASE WHEN IsQtrlyNOIError = 1 OR IsQtrlyNOIPTError = 1 THEN '<font color="red">' + CAST ( LDI. QuaterlyNoi as varchar (max ))  + '</font>' ELSE CAST ( LDI. QuaterlyNoi as varchar (max )) END as QuaterlyNoi ,CASE WHEN IsQtrlyNOIError = 1 or IsQtrlyNOIPTError =1 AND LDI. NOIQuaterbyQuater is not NULL  THEN '<font color="red">' + ISNULL (CAST ( LDI. NOIQuaterbyQuater as varchar (max ))    , '-' ) + '</font>' ELSE ISNULL( CAST( LDI .NOIQuaterbyQuater as varchar ( max ))   , '0' ) END as NOIQuaterbyQuater   , CASE WHEN LDI . NOIPercentageChange is not NULL and IsQtrlyNOIError = 1 or IsQtrlyNOIPTError = 1 
  THEN 
      CASE WHEN LDI. NOIPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . NOIPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. NOIPercentageChange='0' THEN ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. NOIPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as NOIPercentageChange  , ISNULL( CAST ( LDI . NOIPercentageChange as varchar ( max )) + '%' , '-' ) AS NOIPercentageChange  , LDI .CumulativeRevenue , LDI. CumulativeExpenses , CASE WHEN IsQtrlyORevError = 1 THEN '<font color="red">' + CAST( LDI . QuaterlyOperatingRevenue as varchar( max)) + '</font>' ELSE CAST(   LDI. QuaterlyOperatingRevenue as varchar ( max )) END as QuaterlyOperatingRevenue , LDI. QuarterlyOperatingExpense ,CASE WHEN IsQtrlyORevError = 1   THEN '<font color="red">' + ISNULL (CAST ( LDI. RevenueQuarterbyQuarter as varchar (max ))  , '-' ) + '</font>' ELSE ISNULL (CAST (LDI . RevenueQuarterbyQuarter as varchar( max ))   , '0' ) END as RevenueQuarterbyQuarter ,
CASE WHEN LDI . RevenuePercentageChange is not NULL and IsQtrlyORevError = 1    
  THEN 
      CASE WHEN LDI. RevenuePercentageChange='0' THEN '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>' 
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. RevenuePercentageChange='0' THEN ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. RevenuePercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as RevenuePercentageChange , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError =1 AND LDI. DSCRDifferencePerQuarter !=NULL  THEN '<font color="red">' + ISNULL (CAST ( LDI. DSCRDifferencePerQuarter as varchar (max ))  , '-' ) + '</font>' ELSE ISNULL (CAST (LDI . DSCRDifferencePerQuarter as varchar( max  ))   , '0' ) END as DSCRDifferencePerQuarter, 
CASE 
WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1 and LDI. DSCRPercentageChange is not null  
  THEN 
      CASE WHEN LDI. DSCRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL (CAST (LDI . DSCRPercentageChange as varchar ( max )) + '%' , '-') + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. DSCRPercentageChange='0' THEN ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. DSCRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as DSCRPercentageChange , CONVERT ( int, LDI . CumulativeResidentDays ) as CumulativeResidentDays, LDI . QuarterlyResidentDays , CASE WHEN IsADRError = 1   THEN '<font color="red">' + ISNULL ( CAST (LDI . ADRperQuarter as varchar( max ))   , '-' ) + '</font>' ELSE ISNULL (CAST (LDI . ADRperQuarter as varchar( max ))   , '-' ) END as ADRperQuarter, CASE  WHEN IsADRError = 1 and LDI . ADRDifferencePerQuarter is not NULL    THEN '<font color="red">' + ISNULL ( CAST ( LDI . ADRDifferencePerQuarter as varchar ( max ))   , '-' ) + '</font>' ELSE  ISNULL ( CAST ( LDI . ADRDifferencePerQuarter as varchar ( max )) , '0') END as ADRDifferencePerQuarter, CASE WHEN LDI . ADRPercentageChange is not NULL and IsADRError = 1   
  THEN 
      CASE WHEN LDI. ADRPercentageChange='0' THEN  '<font color="red">' + ISNULL (CAST (LDI . ADRPercentageChange as varchar ( max ))  , '-') + '</font>'
       ELSE '<font color="red">' + ISNULL ( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' , '-' ) + '</font>'
	  END

 ELSE 
    CASE WHEN LDI. ADRPercentageChange='0' THEN ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) ,'-' )
    ELSE ISNULL( CAST ( LDI. ADRPercentageChange as varchar (max )) + '%' ,'-' ) end
  
END as ADRPercentageChange, LDI . DebtCoverageRatio2 , ldi . AverageDailyRateRatio , CASE WHEN IsQtrlyDSCRError = 1 or IsQtrlyDSCRPTError = 1  THEN '<font color="red">' + CAST ( LDI . QuarterlyDSCR as varchar ( max )) + '</font>' ELSE CAST (LDI . QuarterlyDSCR as varchar( max )) END as QuarterlyDSCR, IsQtrlyORevError, IsQtrlyNOIError ,IsQtrlyNOIPTError , IsQtrlyDSCRError, IsQtrlyDSCRPTError ,IsADRError , IsProjectError
FROM   @LenderIntermediate LDI
LEFT JOIN dbo .ProjectInfo p ON
LDI .FHANumber = p. FHANumber and p . lenderid= @LenderId
LEFT JOIN dbo .LenderInfo LI ON
P .LenderID = LI. LenderID
JOIN dbo. HUD_Project_Manager ae ON p . HUD_Project_Manager_ID = ae. HUD_Project_Manager_ID
JOIN address a ON a . AddressID = ae .AddressID
JOIN dbo. HUD_WorkLoad_Manager wlm ON p . HUD_WorkLoad_Manager_ID = wlm. HUD_WorkLoad_Manager_ID
WHERE LDI. FHANumber IN (   SELECT DISTINCT FHANumber FROM dbo. ProjectInfo WHERE lenderid =  @LenderId)
ORDER BY LDI .datainserted desc
END

End
  


GO

