
-- =============================================
-- Author:		David Coddington
-- Create date: 4/29/15
-- Description:	Generic Reporting query for Ad-Hoc
--              Reports.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Get_Adhoc_Detail]
(
@wlmListString NVARCHAR(MAX),
@aeListString NVARCHAR(MAX),
@minPeriodEnding DATETIME,
@maxPeriodEnding DATETIME,
@FhaNumberBegin NVARCHAR(9),
@lenderListString NVARCHAR(MAX),
@activeSelected INT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @wlmList TABLE (HUD_WorkLoad_Manager_ID INT)
	DECLARE @useWlmList INT;

	INSERT INTO @wlmList SELECT * FROM dbo.fn_StringSplitterToInt(@wlmListString)
	IF EXISTS (SELECT * FROM @wlmList) 
	BEGIN
		SET @useWlmList = 1
	END
	ELSE
	BEGIN
		SET @useWlmList = 0
	END

	DECLARE @aeList TABLE (HUD_Account_Executive_ID INT)
	DECLARE @useAeList INT;

	INSERT INTO @aeList SELECT * FROM dbo.fn_StringSplitterToInt(@aeListString)
	IF EXISTS (SELECT * FROM @aeList) 
	BEGIN
		SET @useAeList = 1
	END
	ELSE
	BEGIN
		SET @useAeList = 0
	END

	DECLARE @lenderList TABLE (HUD_Lender_ID INT)
	DECLARE @useLenderList INT;

	INSERT INTO @lenderList SELECT * FROM dbo.fn_StringSplitterToInt(@lenderListString)
	IF EXISTS (SELECT * FROM @lenderList) 
	BEGIN
		SET @useLenderList = 1
	END
	ELSE
	BEGIN
		SET @useLenderList = 0
	END

    -- Insert statements for procedure here
SELECT @minPeriodEnding
,@maxPeriodEnding
,pri.Portfolio_Number	"Portfolio_Number"
      ,pri.Portfolio_Name	"Portfolio_Name"
      ,pri.FHANumber	"FHA_Number"
      ,pri.ProjectName	"Project_Name"
      ,pri.PropertyID	"Property_Id"
	  ,pir.PropertyName "Property_Name"
       ,AddrProperty.Title	"Property_Title"
       ,AddrProperty.Organization	"Property_Organization"
       ,AddrProperty.Email	"Property_eMail"
       ,AddrProperty.PhonePrimary	"Property_Primary_Phone"
       ,AddrProperty.PhoneAlternate	"Property_Alternate_Phone"
       ,AddrProperty.Fax	"Property_FAX"
      ,AddrProperty.AddressLine1	"Property_Address_Line1"
       ,AddrProperty.AddressLine2	"Property_Address_Line2"
       ,AddrProperty.City	"Property_City"
       ,AddrProperty.StateCode	"Property_State"
       ,AddrProperty.ZIP	"Property_Zip"
       ,AddrProperty.ZIP4_Code	"Property_Zip+4"
	   ,pri.LenderID "LenderID"
       ,li.Lender_Name	"Lender_Name"
       ,pri.Lender_Mortgagee_Name	"Lender_Mortgagee_Name"
       ,AddrLender.Title	"Lender_Title"
       ,AddrLender.Organization	"Lender_Organization"
       ,AddrLender.Email	"Lender_eMail"
       ,AddrLender.PhonePrimary	"Lender_Primary_Phone"
       ,AddrLender.PhoneAlternate	"Lender_Alternate_Phone"
       ,AddrLender.Fax	"Lender_FAX"
       ,AddrLender.AddressLine1	"Lender_Address_Line1"
       ,AddrLender.AddressLine2	"Lender_Address_Line2"
       ,AddrLender.City	"Lender_City"
       ,AddrLender.StateCode	"Lender_State"
       ,AddrLender.ZIP	"Lender_Zip"
       ,AddrLender.ZIP4_Code	"Lender_Zip+4"
	   ,pri.ServicerID	"Servicer_ID"
       ,si.ServicerName	"Servicer_Name"
       ,pri.Servicing_Mortgagee_Name	"Servicing_Mortgagee_Name"
       ,AddrServicer.Title	"Servicer_Title"
       ,AddrServicer.Organization	"Servicer_Organization"
       ,AddrServicer.Email	"Servicer_eMail"
       ,AddrServicer.PhonePrimary	"Servicer_Primary_Phone"
       ,AddrServicer.PhoneAlternate	"Servicer_Alternate_Phone"
       ,AddrServicer.Fax	"Servicer_FAX"
       ,AddrServicer.AddressLine1	"Servicer_Address_Line1"
       ,AddrServicer.AddressLine2	"Servicer_Address_Line2"
       ,AddrServicer.City	"Servicer_City"
       ,AddrServicer.StateCode	"Servicer_State"
       ,AddrServicer.ZIP	"Servicer_Zip"
       ,AddrServicer.ZIP4_Code	"Servicer_Zip_4"
       ,pri.ManagementID	
       ,wlmi.HUD_WorkLoad_Manager_Name	"WorkLoad_Manager_Name"
       ,AddrWlm.Title	"WorkLoad_Manager_Title"
       ,AddrWlm.Organization	"WorkLoad_Manager_Organization"
       ,AddrWlm.Email	"WorkLoad_Manager_eMail"
       ,AddrWlm.PhonePrimary	"WorkLoad_Manager_Primary_Phone"
       ,AddrWlm.PhoneAlternate	"WorkLoad_Manager_Alternate_Phone"
       ,AddrWlm.Fax	"WorkLoad_Manager_FAX"
       ,AddrWlm.AddressLine1	"WorkLoad_Manager_Address_Line1"
      ,AddrWlm.AddressLine2	"WorkLoad_Manager_Address_Line2"
       ,AddrWlm.City	"WorkLoad_Manager_City"
       ,AddrWlm.StateCode	"WorkLoad_Manager_State"
       ,AddrWlm.ZIP	"WorkLoad_Manager_Zip"
       ,AddrWlm.ZIP4_Code	"WorkLoad_Manager_Zip+4"
       ,aei.HUD_Project_Manager_Name	"Account_Executive_Name"
       ,AddrAe.Title	"Account_Executive_Title"
       ,AddrAe.Organization	"Account_Executive_Organization"
       ,AddrAe.Email	"Account_Executive_eMail"
       ,AddrAe.PhonePrimary	"Account_Executive_Primary_Phone"
       ,AddrAe.PhoneAlternate	"Account_Executive_Alternate_Phone"
       ,AddrAe.Fax	"Account_Executive_FAX"
       ,AddrAe.AddressLine1	"Account_Executive_Address_Line1"
       ,AddrAe.AddressLine2	"Account_Executive_Address_Line2"
       ,AddrAe.City	"Account_Executive_City"
       ,AddrAe.StateCode	"Account_Executive_State"
       ,AddrAe.ZIP	"Account_Executive_Zip"
       ,AddrAe.ZIP4_Code	"Account_Executive_Zip+4"
       ,pri.Primary_Loan_Code	"Primary_Loan_Code"
       ,pri.Initial_Endorsement_Date	"Initial_Endorsement_Date"
       ,pri.Final_Endorsement_Date	"Final_Endorsement_Date"
       ,pri.Field_Office_Status_Name	"Field_Office_Status_Name"
       ,pri.Soa_Code	"SOA_Code"
       ,pri.Soa_Numeric_Name	"Soa_Numeric_Name"
       ,pri.Soa_Description_Text	"SOA_Description"
       ,pri.Troubled_Code	"Troubled_Code"
       ,pri.Troubled_status_update_date	"Troubled_Status_Update_Date"
       ,pri.Reac_Last_Inspection_Score	"REAC_Last_Inspection_Score"
       ,pri.Reac_Last_Inspection_Date	"REAC_Last_Inspection_Date"
       ,pri.Mddr_In_Default_Status_Name	"Mddr_In_Default_Status_Name"
       ,pri.Is_Active_Dec_Case_Ind	"Is_Active_Dec_Case_Ind"
       ,pri.Original_Loan_Amount	"Original_Loan_Amount"
       ,pri.Original_Interest_Rate	"Original_Interest_Rate"
       ,pri.Amortized_Unpaid_Principal_Bal	"Amortized_Unpaid_Principal_Bal"
       ,pri.Is_Nursing_Home_Ind	"Is_Nursing_Home_Ind"
       ,pri.Is_Board_And_Care_Ind	"Is_Board_And_Car_Ind"
       ,pri.Is_Assisted_Living_Ind	"Is_Assisted_Living_Ind"
       ,pri.Deleted_Ind	"Deleted_Ind"
       ,pri.Owner_Company_Type	"Owner_Company_Type"
       ,pri.Owner_Legal_Structure_Name	"Owner_Legal_Structure_Name"
       ,pri.Owner_Organization_Name	"Owner_Organization_Name"
       ,AddrOwner.Title	"Owner_Title"
       ,AddrOwner.Organization	"Owner_Organization"
       ,AddrOwner.Email	"Owner_eMail"
       ,AddrOwner.PhonePrimary	"Owner_Primary_Phone"
       ,AddrOwner.PhoneAlternate	"Owner_Alternate_Phone"
       ,AddrOwner.Fax	"Owner_FAX"
       ,AddrOwner.AddressLine1	"Owner_Address_Line1"
       ,AddrOwner.AddressLine2	"Owner_Address_Line2"
       ,AddrOwner.City	"Owner_City"
       ,AddrOwner.StateCode	"Owner_State"
       ,AddrOwner.ZIP	"Owner_Zip"
       ,AddrOwner.ZIP4_Code	"Owner_Zip_4"
       ,pri.Owner_Contact_Indv_Fullname	"Owner_Contact_Indv_Fullname"
       ,pri.Owner_Contact_Indv_Title	"Owner_Contact_Indv_Title"
--       ,AddrOwnerCntct.Title	"Owner_Contact_Title"
       ,AddrOwnerCntct.Organization	"Owner_Contact_Organization"
       ,AddrOwnerCntct.Email	"Owner_Contact_eMail"
       ,AddrOwnerCntct.PhonePrimary	"Owner_Contact_Primary_Phone"
       ,AddrOwnerCntct.PhoneAlternate	"Owner_Contact_Alternate_Phone"
       ,AddrOwnerCntct.Fax	"Owner_Contact_FAX"
       ,AddrOwnerCntct.AddressLine1	"Owner_Contact_Address_Line1"
       ,AddrOwnerCntct.AddressLine2	"Owner_Contact_Address_Line2"
       ,AddrOwnerCntct.City	"Owner_Contact_City"
       ,AddrOwnerCntct.StateCode	"Owner_Contact_State"
       ,AddrOwnerCntct.ZIP	"Owner_Contact_Zip"
       ,AddrOwnerCntct.ZIP4_Code	"Owner_Contact_Zip_4"
       ,pri.Mgmt_Agent_Company_Type	"Management_Agent_Company_Type"
       ,pri.Mgmt_Agent_org_name	"Mgmt_Agent_org_name"
       ,AddrMngmntAgnt.Title	"Management_Agent_Title"
       ,AddrMngmntAgnt.Organization	"Management_Agent_Organization"
       ,AddrMngmntAgnt.Email	"Management_Agent_eMail"
       ,AddrMngmntAgnt.PhonePrimary	"Management_Agent_Primary_Phone"
       ,AddrMngmntAgnt.PhoneAlternate	"Management_Agent_Alternate_Phone"
       ,AddrMngmntAgnt.Fax	"Management_Agent_FAX"
       ,AddrMngmntAgnt.AddressLine1	"Management_Agent_Address_Line1"
       ,AddrMngmntAgnt.AddressLine2	"Management_Agent_Address_Line2"
       ,AddrMngmntAgnt.City	"Management_Agent_City"
       ,AddrMngmntAgnt.StateCode	"Management_Agent_State"
       ,AddrMngmntAgnt.ZIP	"Management_Agent_Zip"
       ,AddrMngmntAgnt.ZIP4_Code	"Management_Agent_Zip_4"
       ,pri.Mgmt_Contact_FullName	"Mgmt_Contact_FullName"
       ,pri.Mgmt_Contact_Indv_Title	"Mgmt_Contact_Indv_Title"
       ,AddrMngmntCntct.Title	"Management_Contact_Title"
       ,AddrMngmntCntct.Organization	"Management_Contact_Organization"
       ,AddrMngmntCntct.Email	"Management_Contact_eMail"
       ,AddrMngmntCntct.PhonePrimary	"Management_Contact_Primary_Phone"
       ,AddrMngmntCntct.PhoneAlternate	"Management_Contact_Alternate_Phone"
       ,AddrMngmntCntct.Fax	"Management_Contact_FAX"
       ,AddrMngmntCntct.AddressLine1	"Management_Contact_Address_Line1"
       ,AddrMngmntCntct.AddressLine2	"Management_Contact_Address_Line2"
       ,AddrMngmntCntct.City	"Management_Contact_City"
       ,AddrMngmntCntct.StateCode	"Management_Contact_State"
       ,AddrMngmntCntct.ZIP	"Management_Contact_Zip"
       ,AddrMngmntCntct.ZIP4_Code	"Management_Contact_Zip_4"
       ,ld.OperatorOwner	"OperatorOwner"
       ,ld.PeriodEnding	"PeriodEnding"
       ,ld.MonthsInPeriod	"MonthsInPeriod"
       ,ld.FinancialStatementType	"FinancialStatementType"
       ,ld.UnitsInFacility	"UnitsInFacility"
       ,ld.OperatingCash	"OperatingCash"
       ,ld.Investments	"Investments"
       ,ld.ReserveForReplacementEscrowBalance	"ReserveForReplacementEscrowBalance"
       ,ld.AccountsReceivable	"AccountsReceivable"
       ,ld.CurrentAssets	"CurrentAssets"
       ,ld.CurrentLiabilities	"CurrentLiabilities"
       ,ld.TotalRevenues	"TotalRevenues"
       ,ld.RentLeaseExpense	"RentLeaseExpense"
       ,ld.DepreciationExpense	"DepreciationExpense"
       ,ld.AmortizationExpense	"AmortizationExpense"
       ,ld.TotalExpenses	"TotalExpenses"
       ,ld.NetIncome	"NetIncome"
      ,ld.ReserveForReplacementDeposit	"ReserveForReplacementDeposit"
       ,ld.FHAInsuredPrincipalPayment	"FHAInsuredPrincipalPayment"
       ,ld.FHAInsuredInterestPayment	"FHAInsuredInterestPayment"
       ,ld.MortgageInsurancePremium	"MortgageInsurancePremium"
      ,ild.DataInserted	"DateInserted"
       ,ld.ReserveForReplacementBalancePerUnit	"ReserveForReplacementBalancePerUnit"
       ,ld.WorkingCapital	"Working_apital"
       ,ld.DebtCoverageRatio	"DebtCoverageRatio"
       ,ld.DaysCashOnHand	"DaysCashOnHand"
       ,ld.DaysInAcctReceivable	"DaysInAcctReceivable"
       ,ld.AvgPaymentPeriod	"AveragePaymentPeriod"
       ,ld.WorkingCapitalScore	"WorkingCapitalScore"
       ,ld.DebtCoverageRatioScore	"DebtCoverageRatioScore"
       ,ld.DaysCashOnHandScore	"Days_Cash_On_Hand_Score"
       ,ld.DaysInAcctReceivableScore	"DaysInAcctReceivableScore"
       ,ld.AvgPaymentPeriodScore	"AveragePaymentPeriodScore"
       ,ld.ScoreTotal	"ScoreTotal"
	
  FROM (SELECT * FROM [dbo].[Lender_DataUpload_Live] 	
        WHERE ldp_id in (SELECT MAX([LDP_ID]) ldp_id	
                         FROM [dbo].[Lender_DataUpload_Live]	
                         WHERE FHANumber like @FhaNumberBegin + '%'	
						 AND   PeriodEnding >= @minPeriodEnding
						 AND   PeriodEnding <= @maxPeriodEnding
                         GROUP BY [FHANumber])) as ld	
  INNER JOIN [dbo].[ProjectInfo] pri	
  ON  pri.FHANumber = ld.FHANumber
  AND (@useWlmList = 0 OR pri.HUD_WorkLoad_Manager_ID IN (SELECT * FROM @wlmList))
  AND (@useAeList = 0 OR pri.HUD_Project_Manager_ID IN (SELECT * FROM @aeList))
  AND (@useLenderList = 0 OR pri.LenderID IN (SELECT * FROM @lenderList))
  AND (   @activeSelected = 0 
       OR (@activeSelected = 1 AND pri.Deleted_Ind IS NULL) 
	   OR (@activeSelected = 2 AND pri.Deleted_Ind IS NOT NULL))
  LEFT JOIN [dbo].[PropertyInfo_iREMS] pir	
  ON pir.PropertyID = pri.PropertyID
  LEFT JOIN [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] ild	
  ON ild.LDI_ID = ld.LDI_ID	
  LEFT JOIN [dbo].[LenderInfo] li	
  ON li.LenderID = pri.LenderID	
  LEFT JOIN [dbo].[Address] AddrLender	
  ON li.AddressID = AddrLender.AddressID
  LEFT JOIN [dbo].[HCP_Authentication] la
  ON la.AddressID = li.AddressID
  LEFT JOIN [dbo].[ServicerInfo] si	
  ON si.ServicerID = pri.ServicerID	
  LEFT JOIN [dbo].[Address] AddrServicer	
  ON si.AddressID = AddrServicer.AddressID	
  LEFT JOIN [dbo].[HUD_WorkLoad_Manager] wlmi	
  ON wlmi.HUD_WorkLoad_Manager_ID = pri.HUD_WorkLoad_Manager_ID	
  LEFT JOIN [dbo].[Address] AddrWlm	
  ON wlmi.AddressID = AddrWlm.AddressID	
  LEFT JOIN [dbo].[HUD_Project_Manager] aei	
  ON aei.HUD_Project_Manager_ID = pri.HUD_Project_Manager_ID	
  LEFT JOIN [dbo].[Address] AddrAe	
  ON aei.AddressID = AddrAe.AddressID	
  LEFT JOIN [dbo].[Address] AddrProperty	
  ON AddrProperty.AddressID = pri.Property_AddressID	
  LEFT JOIN [dbo].[Address] AddrOwner	
  ON AddrOwner.AddressID = pri.Owner_AddressID	
  LEFT JOIN [dbo].[Address] AddrOwnerCntct	
  ON AddrOwnerCntct.AddressID = pri.Owner_Contact_AddressID	
  LEFT JOIN [dbo].[Address] AddrMngmntAgnt	
  ON AddrMngmntAgnt.AddressID = pri.Mgmt_Agent_AddressID	
  LEFT JOIN [dbo].[Address] AddrMngmntCntct	
  ON AddrMngmntCntct.AddressID = pri.Mgmt_Contact_AddressID	

END

