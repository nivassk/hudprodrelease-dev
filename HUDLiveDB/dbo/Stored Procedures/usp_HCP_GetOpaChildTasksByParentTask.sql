﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetOpaChildTasksByParentTask]
(
@TaskInstanceId  uniqueidentifier
)
AS

SELECT t.[TaskId]
      ,t.[TaskInstanceId]
      ,t.[SequenceId]
      ,t.[AssignedBy]
      ,t.[AssignedTo]
      ,t.[DueDate]
      ,t.[StartTime]
      ,t.[Notes]
      ,t.[TaskStepId]
      ,t.[DataKey1]
      ,t.[DataKey2]
      ,t.[DataStore1]
      ,t.[DataStore2]
      ,t.[TaskOpenStatus]
      ,t.[IsReassigned]
 from  [$(TaskDB)].dbo.Task t
inner join   [$(TaskDB)].dbo.ParentChildTask pc on t.TaskInstanceId = pc.ChildTaskInstanceId
inner join (select lt.TaskInstanceId, max(lt.StartTime) as MaxDate 
			 from [$(TaskDB)].dbo.[Task] lt  group by lt.TaskInstanceId ) l on l.TaskInstanceId = t.TaskInstanceId and t.StartTime = l.MaxDate
where pc.ParentTaskInstanceId = @TaskInstanceId 




GO

