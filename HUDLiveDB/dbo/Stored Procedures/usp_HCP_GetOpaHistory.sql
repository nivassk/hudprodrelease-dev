
CREATE PROCEDURE [dbo].[usp_HCP_GetOpaHistory]
(
@TaskInstanceId uniqueidentifier
)
AS

BEGIN

if exists(select 1 from [$(TaskDB)].dbo.Task  where TaskInstanceId = @TaskInstanceId)
BEGIN

--Add user id input parameter
--Add user role
select distinct-- 'File Upload' as actionTaken,
                     CASE WHEN rsl.[StatusId] =1then 'File Upload ( '+ rsl.[StatusName] +' )'
                     WHEN  rsl.[StatusId] = 2 then 'File Upload ( '+ rsl.[StatusName] +' )'
                     WHEn  rsl.[StatusId] = 3then 'File Upload ( '+ rsl.[StatusName] +' )'
                     WHEn  rsl.[StatusId] = 4 then 'File RAI ( '+ rsl.[StatusName] +' )'
                     WHEN  rsl.[StatusId] =5 then 'File Approved ( '+ rsl.[StatusName] +' )' end as actionTaken,

                     CASE WHEN rsl.[StatusId] =1then (aLender.FirstName + ' ' + aLender.LastName)
                     WHEN  rsl.[StatusId] = 2 then (aLender.FirstName + ' ' + aLender.LastName)
                     WHEn  rsl.[StatusId] = 3then (aLender.FirstName + ' ' + aLender.LastName)
                     WHEn  rsl.[StatusId] = 4 then (aAE.FirstName + ' ' + aAE.LastName)
                     WHEN  rsl.[StatusId] =5 then (aAE.FirstName + ' ' + aAE.LastName) end  as name,

                CASE WHEN rsl.[StatusId] =1 then CASE WHEN rLender.RoleName = 'LenderAccountRepresentative' then 'LAR'
															WHEN rLender.RoleName= 'BackupAccountManager' then 'BAM'
															WHEn rLender.RoleName= 'LenderAccountManager' then 'LAM'
															WHEN rLender.RoleName= 'AccountExecutive' then 'AE'
															WHEN rLender.RoleName= 'OperatorAccountRepresentative' then 'OAR'  end
                     WHEN  rsl.[StatusId] = 2 then  CASE WHEN rLender.RoleName = 'LenderAccountRepresentative' then 'LAR'
															WHEN rLender.RoleName= 'BackupAccountManager' then 'BAM'
															WHEn rLender.RoleName= 'LenderAccountManager' then 'LAM'
															WHEN rLender.RoleName= 'AccountExecutive' then 'AE'
															WHEN rLender.RoleName= 'OperatorAccountRepresentative' then 'OAR'  end
                     WHEn  rsl.[StatusId] = 3 then CASE WHEN rLender.RoleName = 'LenderAccountRepresentative' then 'LAR'
															WHEN rLender.RoleName= 'BackupAccountManager' then 'BAM'
															WHEn rLender.RoleName= 'LenderAccountManager' then 'LAM'
															WHEN rLender.RoleName= 'AccountExecutive' then 'AE'
															WHEN rLender.RoleName= 'OperatorAccountRepresentative' then 'OAR'  end
                     WHEn  rsl.[StatusId] = 4 then CASE WHEN rAE.RoleName = 'LenderAccountRepresentative' then 'LAR'
															WHEN rAE.RoleName= 'BackupAccountManager' then 'BAM'
															WHEn rAE.RoleName= 'LenderAccountManager' then 'LAM'
															WHEN rAE.RoleName= 'AccountExecutive' then 'AE'
															WHEN rAE.RoleName= 'OperatorAccountRepresentative' then 'OAR'  end
                     WHEN  rsl.[StatusId] =5  then CASE WHEN rAE.RoleName = 'LenderAccountRepresentative' then 'LAR'
															WHEN rAE.RoleName= 'BackupAccountManager' then 'BAM'
															WHEn rAE.RoleName= 'LenderAccountManager' then 'LAM'
															WHEN rAE.RoleName= 'AccountExecutive' then 'AE'
															WHEN rAE.RoleName= 'OperatorAccountRepresentative' then 'OAR'  end end as userRole,
       
                     null as comment,

                     null as submitDate,
                     CAST(
                           CASE 
                                  WHEN tfchild.TaskFileId is null 
                                         THEN 0 
                                  ELSE 1
                           END as bit) as isFileHistory,
              CAST(
                           CASE 
                                  WHEN rfcFinal.Comment is null 
                                         THEN 0 
                                  ELSE 1
                           END as bit) as isFileComment,
          tfparent.TaskFileId as taskFileId,
          tfparent.FileId as fileId ,
           COALESCE(tfchild.[FileId], tfparent.[FileId]) as [downloadFileId] ,
          COALESCE(tfchild.[FileName], tfparent.[FileName]) as [fileName] ,
         reverse(Substring(reverse(tfparent.[FileName]), Charindex('.', reverse(tfparent.[FileName]))+1,Len(tfparent.[FileName]))) as fileRename,
         null as isFileRenamed,
         reverse(left(reverse(tfparent.[FileName]), charindex('.', reverse(tfparent.[FileName])) -1)) as fileExt,
         COALESCE(tfchild.fileSize, tfparent.fileSize) as fileSize,
          COALESCE(tfchild.UploadTime, tfparent.UploadTime) as uploadDate,
          CAST(
                           CASE 
                                  WHEN rf.[Status] = 4
                                         THEN 1 
                                  ELSE 0
                           END as bit) as isReqAddInfo,
                           CAST(
                           CASE 
                                  WHEN rf.[Status] = 5
                                         THEN 1 
                                  ELSE 0
                           END as bit) as isApprove,
                           
                           raiComments=CASE 
                                  WHEN rf.[Status] = 4
                                         THEN rfcFinal.Comment  
                                  WHEN rf.[Status] = 5
                                         THEN COALESCE(rfcFinal.Comment  , '') 
                                  ELSE ''
                                  end,

              rf.[Status] as fileReviewStatusId,
              rsl.StatusName as fileReviewStatusName,
              COALESCE(tfchild.UploadTime, tfparent.UploadTime)  as actionDate
from [$(TaskDB)].dbo.TaskFile tfparent
inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
inner join [$(DatabaseName)].dbo.HCP_Authentication aLender on aLender.UserName = t.AssignedBy
inner join [$(DatabaseName)].dbo.HCP_Authentication aAE on aAE.UserName = t.AssignedTo
inner join [$(DatabaseName)].dbo.webpages_UsersInRoles lLender on lLender.UserId = aLender.userid
inner join [$(DatabaseName)].dbo.webpages_UsersInRoles lAE on lAE.UserId = aAE.userid
inner join [$(DatabaseName)].dbo.webpages_Roles rLender on rLender.roleid = lLender.roleid
inner join [$(DatabaseName)].dbo.webpages_Roles rAE on rAE.roleid = lAE.roleid
inner join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId
inner join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
                     inner join 
                     (select FileTaskId, max(CreatedOn) as MaxDate 
                      from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId
left join (select tf1.FileId, tf1.TaskFileId,tf1.[FileName],tf1.FileData,tf1.fileSize,tf1.UploadTime,tf2.ParentTaskFileId 
              from [$(TaskDB)].dbo.TaskFile tf1
              inner join ( select   tfh.ChildTaskFileId ,tfh.ParentTaskFileId
                                                       from [$(TaskDB)].dbo.TaskFile tfc 
                                                       inner join [$(TaskDB)].dbo.TaskFileHistory tfh on tfc.TaskFileId = tfh.ParentTaskFileId
                                                       inner join  (select tfpf.ParentTaskFileId, max(tfpf.CreatedOn) as MaxDate
                                                                           from [$(TaskDB)].dbo.TaskFileHistory tfpf
                                                                           group by tfpf.ParentTaskFileId) basetable on basetable.ParentTaskFileId = tfh.ParentTaskFileId 
                                                                                                                                                  and basetable.MaxDate = tfh.CreatedOn ) tf2 on tf1.TaskFileId = tf2.ChildTaskFileId
            ) tfchild on tfparent.TaskFileId = tfchild.ParentTaskFileId
where tfparent.TaskInstanceId = @TaskInstanceId AND rLender.RoleName <> 'HUDAdmin' AND rAE.RoleName <> 'HUDAdmin'

union
	select distinct 'New File Request' as actionTaken,
					(A.FirstName + ' ' + A.LastName) as name,
	                'AE' as userRole,
					nfr.[Comments] as comment,
					nfr.[RequestedOn] as submitDate,
					null as isFileHistory,
					null as isFileComment,
					null as taskFileId,
					null as fileId,
					null as [downloadFileId] ,
					null as [fileName],
					null as [fileRename],
					null as isFileRenamed,
					null as fileExt,
					null as fileSize,
					null as uploadDate,
					null as isReqAddInfo,
					null as isApprove,
					null as raiComments,
					null as fileReviewStatusId,
					null as fileReviewStatusName,
					nfr.[RequestedOn]  as actionDate
	from [$(TaskDB)].dbo.NewFileRequest nfr
	inner join HCP_Authentication a on a.UserID = nfr.[RequestedBy]
	inner join [$(TaskDB)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = nfr.[ChildTaskInstanceId]
	where pc.ParentTaskInstanceId = @TaskInstanceId
	


Union

	select case when t.TaskStepId = 14 then 'OPA Submit'
			when t.TaskStepId = 15 and o.RequestStatus = 2 then 'OPA Approve'
			when t.TaskStepId = 15 and o.RequestStatus = 5 then 'OPA Deny'
			when t.TaskStepId = 15 then 'Request Additional Submit'
			when t.TaskStepId = 16 then 'Request Addtional Information'
			end as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' 
			else 'AE' end as userRole,	
			t.Notes as comment,
			t.starttime as submitDate,
			null as isFileHistory,
			null as isFileComment,
			null as taskFileId,
			null as fileId,
			null as [downloadFileId] ,
			null as [fileName],
			null as [fileRename],
			null as isFileRenamed,
			null as fileExt,
			null as fileSize,
			null as uploadDate,
			null as isReqAddInfo,
			null as isApprove,
			null as raiComments,
			null as fileReviewStatusId,
			null as fileReviewStatusName,
			t.starttime as actionDate
	from [$(TaskDB)].dbo.task t
	left join [$(DatabaseName)].dbo.opaform o on t.TaskInstanceId = o.TaskInstanceId
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	left join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid and  t.TaskStepId in (14,15)
	left join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	where( t.TaskInstanceId = @TaskInstanceId or
		t.TaskInstanceId in ( select pt.ChildTaskInstanceId from [$(TaskDB)].dbo.ParentChildTask pt where pt.ParentTaskInstanceId =@TaskInstanceId ))
		and t.TaskStepId in (14,15) and t.SequenceId in (0,1)
	order by actionDate desc

end
else
BEGIN

select 'File Upload' as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' end as userRole,
			null as comment,
			[FileName] as [fileName],
			fileSize as fileSize,
			uploadTime as uploadDate,
			null as submitDate,
			fileId,
			case when t.TaskInstanceId is null then CONVERT(BIT,1)
				 when t.TaskStepId = 16 and t.StartTime < tf.UploadTime then CONVERT(BIT,1)  
				 else CONVERT(BIT,0) end as editable,
			uploadTime as actionDate
	from [$(TaskDB)].dbo.TaskFile tf
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserID = tf.CreatedBy
	inner join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid
	inner join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	left join
		(select top 1 * from [$(TaskDB)].dbo.Task where TaskInstanceId = @TaskInstanceId order by SequenceId desc) t on t.TaskInstanceId = tf.TaskInstanceId
	where tf.TaskInstanceId = @TaskInstanceId

Union

	select case when t.TaskStepId = 14 then 'OPA Submit'
			when t.TaskStepId = 15 and o.RequestStatus = 2 then 'OPA Approve'
			when t.TaskStepId = 15 and o.RequestStatus = 5 then 'OPA Deny'
			when t.TaskStepId = 16 then 'Request Addtional Information'
			end as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' 
			else 'AE' end as userRole,	
			Notes as comment,
			null as [fileName],
			null as fileSize,
			null as uploadDate,
			starttime as submitDate,
			null as fileId,
			CONVERT(BIT,0) as editable,
			starttime as actionDate
	from [$(DatabaseName)].dbo.opaform o 
	inner join [$(TaskDB)].dbo.task t on t.TaskInstanceId = o.TaskInstanceId
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	left join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid and t.TaskStepId = 14
	left join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	where o.TaskInstanceId = @TaskInstanceId

    order by actionDate desc
end

end

GO

