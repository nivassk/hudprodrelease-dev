﻿
CREATE PROCEDURE [dbo].[usp_HCP_Verify_PropertyID_iRems]
(
@LenderID INT,
@UserID INT
)
AS

declare @DateInserted DateTime 
SET @DateInserted = (SELECT MAX(DataInserted) FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] WHERE LenderID = @LenderID AND UserID = @UserID );

WITH Lender_Verifcation (ProjectName,ServiceName,LenderID,PeriodEnding,MonthsInPeriod,FHANumber,OperatingCash,Investments,ReserveForReplacementEscrowBalance,AccountsReceivable,CurrentAssets,CurrentLiabilities,TotalRevenues,RentLeaseExpense,DepreciationExpense,AmortizationExpense,TotalExpenses,NetIncome,MortgageInsurancePremium,UserID,DataInserted)
AS
(
SELECT 
		LDI.ProjectName,
		LDI.ServiceName,
		LDI.LenderID,
		LDI.PeriodEnding,
		LDI.MonthsInPeriod,
		LDI.FHANumber,
		LDI.OperatingCash,
		LDI.Investments,
		LDI.ReserveForReplacementEscrowBalance,
		LDI.AccountsReceivable,
		LDI.CurrentAssets,
		LDI.CurrentLiabilities,
		LDI.TotalRevenues,
		LDI.RentLeaseExpense,
		LDI.DepreciationExpense,
		LDI.AmortizationExpense,
		LDI.TotalExpenses,
		LDI.NetIncome,
		LDI.MortgageInsurancePremium,
		LDI.UserID, 
		LDI.DataInserted 
FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] LDI
WHERE LDI.LenderID = @LenderID AND LDI.UserID = @UserID AND LDI.DataInserted = @DateInserted
)

--UPDATE LDI1
--SET LDI1.PropertyID = PII.PropertyID
--FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] LDI1 INNER JOIN Lender_Verifcation LV
--ON LDI1.FHANumber = LV.FHANumber INNER JOIN [$(DatabaseName)].[dbo].[PropertyID_FHANumber_iRems] PII
--ON  PII.FHANumber = LV.FHANumber
--WHERE LDI1.LenderID = @LenderID AND LDI1.UserID = @UserID AND LDI1.DataInserted = @DateInserted
  
UPDATE LDI1
SET LDI1.PropertyID = PII.PropertyID, LDI1.ProjectName = FHAi.FHADescription, LDI1.ServiceName = LI.Lender_Name
FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] LDI1 INNER JOIN Lender_Verifcation LV ON LDI1.FHANumber = LV.FHANumber 
INNER JOIN [$(DatabaseName)].[dbo].[PropertyID_FHANumber_iRems] PII ON  PII.FHANumber = LV.FHANumber
INNER JOIN [$(DatabaseName)].[dbo].[FHAInfo_iREMS] FHAi ON  FHAi.FHANumber = LV.FHANumber
INNER JOIN [$(DatabaseName)].[dbo].[LenderInfo] LI ON  LI.LenderID = LV.LenderID
WHERE LDI1.LenderID = @LenderID AND LDI1.UserID = @UserID AND LDI1.DataInserted = @DateInserted
  



GO

