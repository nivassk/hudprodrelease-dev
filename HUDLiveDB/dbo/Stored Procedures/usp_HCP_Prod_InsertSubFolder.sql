﻿
--CREATE PROCEDURE [dbo].[usp_HCP_Prod_InsertSubFolder]
--(
-- @ParentKey int,
-- @FhaNo varchar(10), 
-- @SubfolderSequence varchar(10),
-- @ProjectNo varchar(10)
  
--)
--AS

--BEGIN

--INSERT INTO [$(TaskDB)].[dbo].[Prod_SubFolderStructure]
--           (
--		    FolderName,
--			ParentKey,
--			ViewTypeId,
--			FolderSortingNumber,
--			SubfolderSequence,
--			FhaNo,
--			ProjectNo 
--		   )
--           ( SELECT 
		       
--                 FolderName
--				,@ParentKey
--				,ViewTypeId
--				,FolderSortingNumber
--				,@SubfolderSequence
--				,@FhaNo
--				,@ProjectNo
--			 FROM [$(TaskDB)].[dbo].[Prod_FolderStructure] where folderkey = @ParentKey) 
 		
--end




--GO

CREATE PROCEDURE [dbo].[usp_HCP_Prod_InsertSubFolder]
(
 @ParentKey int,
 @FhaNo varchar(10), 
 @SubfolderSequence varchar(10),
 @ProjectNo varchar(10),
 @CreatedBy int,
 @CreatedOn datetime ,
  @PageTypeId int,
 @GroupTaskInstanceId uniqueidentifier
)
AS

BEGIN

INSERT INTO [$(TaskDB)].[dbo].[Prod_SubFolderStructure]
           (
		    FolderName,
			ParentKey,
			ViewTypeId,
			FolderSortingNumber,
			SubfolderSequence,
			FhaNo,
			ProjectNo,
			CreatedBy,
			CreatedOn,
			PageTypeId,
			GroupTaskInstanceId
		   )
           ( SELECT 
		       
                 FolderName
				,@ParentKey
				,ViewTypeId
				,FolderSortingNumber
				,@SubfolderSequence
				,@FhaNo
				,@ProjectNo
				,@CreatedBy
				,@CreatedOn
				,@PageTypeId 
				,@GroupTaskInstanceId 
			 FROM [$(TaskDB)].[dbo].[Prod_FolderStructure] where folderkey = @ParentKey) 
 		
end


