﻿
CREATE TABLE [dbo].[HUD_Project_Manager_Portfolio](
	[HUD_Project_Manager_Portfolio_ID] [int] IDENTITY(1,1) NOT NULL,
	[HUD_Project_Manager_ID] [int] NULL,
	[ProjectID_FHANumber] [nvarchar](15) NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
 CONSTRAINT [PK_HUD_Project_Manager_Portfolio] PRIMARY KEY CLUSTERED 
(
	[HUD_Project_Manager_Portfolio_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HUD_Project_Manager_Portfolio]  WITH CHECK ADD  CONSTRAINT [FK_HUD_Project_Manager_Portfolio_FHAInfo_iREMS] FOREIGN KEY([ProjectID_FHANumber])
REFERENCES [dbo].[FHAInfo_iREMS] ([FHANumber])
GO

ALTER TABLE [dbo].[HUD_Project_Manager_Portfolio] CHECK CONSTRAINT [FK_HUD_Project_Manager_Portfolio_FHAInfo_iREMS]
GO

ALTER TABLE [dbo].[HUD_Project_Manager_Portfolio]  WITH CHECK ADD  CONSTRAINT [FK_HUD_Project_Manager_Portfolio_HUD_Project_Manager] FOREIGN KEY([HUD_Project_Manager_ID])
REFERENCES [dbo].[HUD_Project_Manager] ([HUD_Project_Manager_ID])
GO

ALTER TABLE [dbo].[HUD_Project_Manager_Portfolio] CHECK CONSTRAINT [FK_HUD_Project_Manager_Portfolio_HUD_Project_Manager]
GO

