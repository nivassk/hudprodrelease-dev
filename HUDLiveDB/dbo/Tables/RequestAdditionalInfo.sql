﻿
CREATE TABLE [dbo].[RequestAdditionalInfo](
	[RequestAdditionalInfoId] [uniqueidentifier] NOT NULL,
	[RequestInstanceId] [uniqueidentifier] NOT NULL,
	[ParentFormId] [uniqueidentifier] NOT NULL,
	[FhaNumber] [varchar](15) NOT NULL,
	[ProjectActionTypeId] [int] NULL,
	[RequestStatus] [int] NULL,
	[Comments] [varchar](500) NULL,
	[MytaskId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestAdditionalInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RequestAdditionalInfo] ADD  DEFAULT (newid()) FOR [RequestAdditionalInfoId]
GO

ALTER TABLE [dbo].[RequestAdditionalInfo]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[RequestAdditionalInfo]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

