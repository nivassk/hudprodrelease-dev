﻿CREATE TABLE [dbo].[ProdLender_LookUp](
	[LenderId] [int] NOT NULL,
	[IsProdLender] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL
) ON [PRIMARY]

GO
