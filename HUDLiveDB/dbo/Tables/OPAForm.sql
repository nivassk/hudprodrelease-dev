﻿
CREATE TABLE [dbo].[OPAForm](
	[ProjectActionFormId] [uniqueidentifier] NOT NULL,
	[FhaNumber] [varchar](15) NOT NULL,
	[PropertyName] [varchar](100) NOT NULL,
	[RequestDate] [datetime] NOT NULL,
	[RequesterName] [varchar](100) NULL,
	[ServicerSubmissionDate] [datetime] NULL,
	[ProjectActionStartDate] [datetime] NULL,
	[ProjectActionTypeId] [int] NOT NULL,
	[RequestStatus] [int] NULL,
	[MytaskId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ServicerComments] [varchar](500) NULL,
	[AEComments] [varchar](500) NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[IsAddressChange] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProjectActionFormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OPAForm] ADD  DEFAULT ((0)) FOR [IsAddressChange]
GO

ALTER TABLE [dbo].[OPAForm]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[OPAForm]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

