﻿
CREATE TABLE [dbo].[HCP_EmailTemplateMessages](
	[MessageTemplateId] [int] NOT NULL,
	[EmailTypeId] [int] NOT NULL,
	[MessageSubject] [nvarchar](256) NOT NULL,
	[MessageBody] [nvarchar](max) NOT NULL,
	[KeyWords] [nvarchar](256) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_HCP_EmailTemplateMessages] PRIMARY KEY CLUSTERED 
(
	[MessageTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[HCP_EmailTemplateMessages]  WITH CHECK ADD  CONSTRAINT [fk_HCP_EmailTemplateMessages_HCP_Authentication_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[HCP_EmailTemplateMessages] CHECK CONSTRAINT [fk_HCP_EmailTemplateMessages_HCP_Authentication_CreatedBy]
GO

ALTER TABLE [dbo].[HCP_EmailTemplateMessages]  WITH CHECK ADD  CONSTRAINT [fk_HCP_EmailTemplateMessages_HCP_Authentication_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[HCP_EmailTemplateMessages] CHECK CONSTRAINT [fk_HCP_EmailTemplateMessages_HCP_Authentication_ModifiedBy]
GO

