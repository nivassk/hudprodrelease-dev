﻿
CREATE TABLE [dbo].[Lender_FHANumber](
	[LenderID] [int] NOT NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[FHA_StartDate] [datetime] NULL,
	[FHA_EndDate] [datetime] NULL,
 CONSTRAINT [PK_LenderID_FHANumber] PRIMARY KEY CLUSTERED 
(
	[LenderID] ASC,
	[FHANumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

