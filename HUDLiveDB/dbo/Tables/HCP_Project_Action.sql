﻿
CREATE TABLE [dbo].[HCP_Project_Action](
	[ProjectActionID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectActionName] [varchar](100) NOT NULL,
	[ProjectActionLongName] [varchar](200) NOT NULL,
	[AE_DTC] [int] NULL,
	[Hours] [int] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
	[PageTypeId] [tinyint] NOT NULL,
 CONSTRAINT [PK_HCP_Project_Action] PRIMARY KEY CLUSTERED 
(
	[ProjectActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HCP_Project_Action] ADD  DEFAULT ((3)) FOR [PageTypeId]
GO

