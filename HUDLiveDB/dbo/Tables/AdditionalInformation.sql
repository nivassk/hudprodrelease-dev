﻿
CREATE TABLE [dbo].[AdditionalInformation](
	[AdditionalInformationId] [uniqueidentifier] NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[AEComment] [varchar](500) NULL,
	[LenderComment] [varchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[OrderNumber] [int] NULL,
	[ParentFormID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_AdditionalInformation] PRIMARY KEY CLUSTERED 
(
	[AdditionalInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AdditionalInformation]  WITH CHECK ADD  CONSTRAINT [FK__Additiona__Creat__1960B67E] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[AdditionalInformation] CHECK CONSTRAINT [FK__Additiona__Creat__1960B67E]
GO

ALTER TABLE [dbo].[AdditionalInformation]  WITH CHECK ADD  CONSTRAINT [FK__Additiona__Modif__1A54DAB7] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[AdditionalInformation] CHECK CONSTRAINT [FK__Additiona__Modif__1A54DAB7]
GO

