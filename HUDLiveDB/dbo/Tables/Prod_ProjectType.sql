﻿
CREATE TABLE [dbo].[Prod_ProjectType](
	[ProjectTypeId] [int] NOT NULL,
	[ProjectTypeName] [nvarchar](100) NULL,
	[IsotherloanType] [bit] NULL,
	[PageTypeId] [int] NULL,
 CONSTRAINT [PK__Prod_Pro__6F245E6300339012] PRIMARY KEY CLUSTERED 
(
	[ProjectTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_ProjectType] ADD  CONSTRAINT [DF_Prod_ProjectType_PageTypeId]  DEFAULT ((0)) FOR [PageTypeId]
GO

