﻿
CREATE TABLE [dbo].[NonCriticalRequestExtensions](
	[NonCriticalRequestExtensionId] [uniqueidentifier] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[LenderId] [int] NOT NULL,
	[FhaNumber] [nvarchar](15) NOT NULL,
	[ClosingDate] [datetime] NULL,
	[ExtensionPeriod] [int] NULL,
	[ExtensionRequestStatus] [int] NULL,
	[Comments] [nvarchar](500) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[TaskId] [int] NULL,
 CONSTRAINT [PK_NonCriticalRequestExtensions] PRIMARY KEY CLUSTERED 
(
	[NonCriticalRequestExtensionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

