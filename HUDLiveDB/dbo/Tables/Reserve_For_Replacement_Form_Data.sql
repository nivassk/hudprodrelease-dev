﻿
CREATE TABLE [dbo].[Reserve_For_Replacement_Form_Data](
	[R4RId] [uniqueidentifier] NOT NULL,
	[BorrowersRequestDate] [datetime] NOT NULL,
	[ServicerSubmissionDate] [datetime] NOT NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[PropertyName] [nvarchar](100) NOT NULL,
	[PropertyId] [int] NOT NULL,
	[IsRequestForAdvance] [bit] NOT NULL,
	[NumberOfUnits] [int] NOT NULL,
	[ReserveAccountBalance] [decimal](19, 2) NOT NULL,
	[ReserveAccountBalanceAsOfDate] [datetime] NOT NULL,
	[TotalPurchaseAmount] [decimal](19, 2) NOT NULL,
	[TotalRequestedAmount] [decimal](19, 2) NOT NULL,
	[TotalApprovedAmount] [decimal](19, 2) NOT NULL,
	[IsSingleItemExceedsFiftyThousand] [bit] NOT NULL,
	[IsPurchaseYearOlderThanR4RRequest] [bit] NOT NULL,
	[IsCommentEntered] [bit] NOT NULL,
	[ServicerRemarks] [nvarchar](500) NULL,
	[HudRemarks] [nvarchar](500) NULL,
	[AdditionalRemarks] [nvarchar](500) NULL,
	[RequestStatus] [int] NOT NULL,
	[AttachmentDescription] [nchar](500) NULL,
	[TaskId] [int] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[Deleted_Ind] [bit] NULL,
	[IsRemodelingProposed] [bit] NOT NULL,
	[IsMortgagePaymentsCovered] [bit] NOT NULL,
	[IsLenderDelegate] [bit] NULL,
	[IsSuspension] [bit] NULL,
	TaskInstanceId uniqueidentifier null,
 CONSTRAINT [PK_Reserve_For_Replacement_Form_Data] PRIMARY KEY CLUSTERED 
(
	[R4RId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data] ADD  DEFAULT ((0)) FOR [IsRemodelingProposed]
GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data] ADD  DEFAULT ((0)) FOR [IsMortgagePaymentsCovered]
GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_For_Replacement_Form_Data_FHAInfo_iREMS] FOREIGN KEY([FHANumber])
REFERENCES [dbo].[FHAInfo_iREMS] ([FHANumber])
GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data] CHECK CONSTRAINT [FK_Reserve_For_Replacement_Form_Data_FHAInfo_iREMS]
GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_For_Replacement_Form_Data_PropertyInfo_iREMS] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[PropertyInfo_iREMS] ([PropertyID])
GO

ALTER TABLE [dbo].[Reserve_For_Replacement_Form_Data] CHECK CONSTRAINT [FK_Reserve_For_Replacement_Form_Data_PropertyInfo_iREMS]
GO

