﻿
CREATE TABLE [dbo].[CheckListQuestions](
	[CheckListId] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid()),
	[QuestionId] [int] NULL,
	[Question] [varchar](500) NULL,
	[Level] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[DisplayOrderId] [int] NULL,
	[ProjectActionId] [int] NULL,
	[ParentQuestionId] [uniqueidentifier] NULL,
	[IsAttachmentRequired] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CheckListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

