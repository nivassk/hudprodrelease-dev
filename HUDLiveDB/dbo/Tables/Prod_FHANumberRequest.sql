﻿
CREATE TABLE [dbo].[Prod_FHANumberRequest](
	[FHANumberRequestId] [uniqueidentifier] NOT NULL,
	[FHANumber] [nvarchar](15) NULL,
	[ProjectName] [nvarchar](100) NULL,
	[ProjectTypeId] [int] NULL,
	[CurrentLoanTypeId] [int] NULL,
	[ActivityTypeId] [int] NULL,
	[LoanTypeId] [int] NULL,
	[BorrowerTypeId] [int] NULL,
	[IsAddressCorrect] [bit] NULL,
	[PropertyAddressId] [int] NULL,
	[CongressionalDtId] [int] NULL,
	[LenderId] [int] NULL,
	[LenderContactName] [nvarchar](100) NULL,
	[LenderContactAddressId] [int] NULL,
	[LoanAmount] [decimal](19, 2) NULL,
	[SkilledNursingBeds] [int] NULL,
	[SkilledNursingUnits] [int] NULL,
	[AssistedLivingBeds] [int] NULL,
	[AssistedLivingUnits] [int] NULL,
	[BoardCareBeds] [int] NULL,
	[BoardCareUnits] [int] NULL,
	[MemoryCareBeds] [int] NULL,
	[MemoryCareUnits] [int] NULL,
	[IndependentBeds] [int] NULL,
	[IndependentUnits] [int] NULL,
	[IsMidLargePortfolio] [bit] NULL,
	[IsMasterLeaseProposed] [bit] NULL,
	[Portfolio_Name] [nvarchar](100) NULL,
	[Portfolio_Number] [int] NULL,
	[Comments] [varchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[OtherBeds] [int] NULL,
	[OtherUnits] [int] NULL,
	[MortgageInsuranceTypeId] [int] NULL,
	[ConstructionStageTypeId] [int] NULL,
	[CMSStarRating] [int] NULL,
	[ProposedInterestRate] [decimal](19, 2) NULL,
	[IsNewOrExistOrNAPortfolio] [int] NOT NULL,
	[IsPortfolioRequired] [bit] NOT NULL,
	[IsCreditReviewRequired] [bit] NOT NULL,
	[IsFhaInsertComplete] [bit] NULL,
	[IsPortfolioComplete] [bit] NULL,
	[IsCreditReviewAttachComplete] [bit] NULL,
	[InsertFHAComments] [varchar](500) NULL,
	[PortfolioComments] [varchar](500) NULL,
	[CreditreviewComments] [varchar](500) NULL,
	[CreditReviewDate] [datetime] NULL,
	[IsReadyForApplication] [bit] NOT NULL,
	[RequestDeny] [bit] NOT NULL,
	[TaskinstanceId] [uniqueidentifier] NOT NULL,
	[PropertyId] [int] NULL,
	[RequestStatus] [int] NULL,
	[CurrentFHANumber] [nvarchar](15) NULL,
	[RequestSubmitDate] [datetime] NULL,
	[DenyComments] [varchar](500) NULL,
	[LenderContactEmail] [varchar](500) NULL,
	[LenderContactPhone] [varchar](500) NULL,
	[IsLIHTC] [bit] NULL,
	[CancelComments] [varchar](500) NULL,
	[IsPortfolioCancelled] [bit] NOT NULL,
	[IsCreditCancelled] [bit] NOT NULL,
	[One80Warning] [bit] NULL,
	[Two70Warning] [bit] NULL,
 [AssignedDate] DATETIME NULL, 
    CONSTRAINT [PK_Prod_FHANumberRequest] PRIMARY KEY CLUSTERED 
(
	[FHANumberRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_FHANumberRequest] ADD  CONSTRAINT [DF__Prod_FHAN__IsPor__2AC04CAA]  DEFAULT ('0') FOR [IsPortfolioRequired]
GO

ALTER TABLE [dbo].[Prod_FHANumberRequest] ADD  CONSTRAINT [DF__Prod_FHAN__IsCre__2BB470E3]  DEFAULT ('0') FOR [IsCreditReviewRequired]
GO

ALTER TABLE [dbo].[Prod_FHANumberRequest] ADD  CONSTRAINT [DF__Prod_FHAN__IsPor__2CA8951C]  DEFAULT ((0)) FOR [IsPortfolioCancelled]
GO

ALTER TABLE [dbo].[Prod_FHANumberRequest] ADD  CONSTRAINT [DF__Prod_FHAN__IsCre__2D9CB955]  DEFAULT ((0)) FOR [IsCreditCancelled]
GO

