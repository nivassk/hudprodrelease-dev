﻿
CREATE TABLE [dbo].[PDRUserPreference](
	[PDRUserPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[PDRColumnID] [int] NOT NULL,
	[Visibility] [bit] NOT NULL,
 CONSTRAINT [PK_PDRUserPreference] PRIMARY KEY CLUSTERED 
(
	[PDRUserPreferenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PDRUserPreference]  WITH CHECK ADD  CONSTRAINT [FK_PDRUserPreference_HCP_Authentication] FOREIGN KEY([UserID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[PDRUserPreference] CHECK CONSTRAINT [FK_PDRUserPreference_HCP_Authentication]
GO

ALTER TABLE [dbo].[PDRUserPreference]  WITH CHECK ADD  CONSTRAINT [FK_PDRUserPreference_PDRColumn] FOREIGN KEY([PDRColumnID])
REFERENCES [dbo].[PDRColumn] ([PDRColumnID])
GO

ALTER TABLE [dbo].[PDRUserPreference] CHECK CONSTRAINT [FK_PDRUserPreference_PDRColumn]
GO

