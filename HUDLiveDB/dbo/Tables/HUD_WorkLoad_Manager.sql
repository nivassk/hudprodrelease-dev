﻿
CREATE TABLE [dbo].[HUD_WorkLoad_Manager](
	[HUD_WorkLoad_Manager_ID] [int] IDENTITY(1,1) NOT NULL,
	[HUD_WorkLoad_Manager_Name] [nvarchar](100) NOT NULL,
	[AddressID] [int] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_HUD_WorkLoad_Manager] PRIMARY KEY CLUSTERED 
(
	[HUD_WorkLoad_Manager_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager]  WITH CHECK ADD  CONSTRAINT [FK_HUD_WorkLoad_Manager_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO

ALTER TABLE [dbo].[HUD_WorkLoad_Manager] CHECK CONSTRAINT [FK_HUD_WorkLoad_Manager_Address]
GO

