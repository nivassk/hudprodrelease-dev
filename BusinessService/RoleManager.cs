﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web;


namespace HUDHealthcarePortal.BusinessService
{
    public static class RoleManager
    {

        public static string[] GetUserRoles(string userName)
        {
            if (string.IsNullOrEmpty(UserPrincipal.Current.UserRole))
            {
                return Roles.GetRolesForUser(userName);
            }
            var roles = new string[1];
            roles[0] = UserPrincipal.Current.UserRole;
            return roles;
        }

        public static bool IsAdminRole(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.HUDAdmin.ToString("g"))
                || roles.Contains(HUDRole.LenderAccountManager.ToString("g"))
                || roles.Contains(HUDRole.BackupAccountManager.ToString("g"))
                || roles.Contains(HUDRole.SuperUser.ToString("g"));
        }

        public static bool IsSuperUserRole(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.HUDAdmin.ToString("g"))
                || roles.Contains(HUDRole.HUDDirector.ToString("g"))
                || roles.Contains(HUDRole.SuperUser.ToString("g"));
        }

        public static bool IsWorkloadManagerRole(string userName)
        {
            var roles = GetUserRoles(userName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            return roles.Contains(HUDRole.WorkflowManager.ToString("g"));
        }

        public static bool IsAccountExecutiveRole(string userName)
        {
            var roles = GetUserRoles(userName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            return roles.Contains(HUDRole.AccountExecutive.ToString("g"));
        }

        public static bool IsAccountExecutiveOrWorkloadManagerRole(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.AccountExecutive.ToString("g")) || roles.Contains(HUDRole.WorkflowManager.ToString("g")) || roles.Contains(HUDRole.InternalSpecialOptionUser.ToString("g"));
        }

        public static bool IsInternalSpecialOptionUser(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.InternalSpecialOptionUser.ToString("g"));
        }

        public static bool IsReviewer(string userName)
        {
            var roles = GetUserRoles(userName);
            //string roles = "";
            //roles = SessionHelper.SessionExtract<string>(SessionHelper.SESSION_KEY_LOGINUSERROLE);
            return roles != null && (roles.Contains(HUDRole.Reviewer.ToString("g")));
        }

        public static bool IsUserOperatorAccountRepresentative(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.OperatorAccountRepresentative.ToString("g"));
        }

        public static bool IsLenderAdmin(string userName)
        {
            var roles = GetUserRoles(userName);
            
            return roles.Contains(HUDRole.LenderAccountManager.ToString("g"))
                || roles.Contains(HUDRole.BackupAccountManager.ToString("g"));
        }

        public static bool IsHudAdmin(string userName)
        {
            var roles = GetUserRoles(userName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes

            return roles.Contains(HUDRole.HUDAdmin.ToString("g"))
                || roles.Contains(HUDRole.SuperUser.ToString("g"));
        }

        public static bool IsHudDirector(string userName)
        {
            var roles = GetUserRoles(userName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            return roles.Contains(HUDRole.HUDDirector.ToString("g"));
        }

        public static bool IsHudAttorney(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.Attorney.ToString("g"));
        }

        public static bool IsInternalUser(string userName)
        {
            var roles = GetUserRoles(userName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            return roles.Contains(HUDRole.HUDAdmin.ToString("g"))
                || roles.Contains(HUDRole.SuperUser.ToString("g"))
                || roles.Contains(HUDRole.AccountExecutive.ToString("g"))
                || roles.Contains(HUDRole.Attorney.ToString("g"))
                || roles.Contains(HUDRole.HUDDirector.ToString("g"))
                || roles.Contains(HUDRole.WorkflowManager.ToString("g"));
        }

        public static bool IsCurrentOperatorAccountRepresentative()
        {
            var roles = GetUserRoles(UserPrincipal.Current.UserName);
            return roles != null && roles.Contains(HUDRole.OperatorAccountRepresentative.ToString("g"));
        }


        public static bool IsCurrentSpecialOptionUser()
        {
            var roles = GetUserRoles(UserPrincipal.Current.UserName);
            HttpContext.Current.Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            HttpContext.Current.Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            return roles != null && roles.Contains(HUDRole.Servicer.ToString("g"));
        }

        public static bool IsCurrentUserLenderRoles()
        {
            var roles = GetUserRoles(UserPrincipal.Current.UserName);
            return roles != null && (roles.Contains(HUDRole.LenderAccountRepresentative.ToString("g"))
                                     || roles.Contains(HUDRole.LenderAccountManager.ToString("g"))
                                     || roles.Contains(HUDRole.BackupAccountManager.ToString("g"))
                                     || roles.Contains(HUDRole.OperatorAccountRepresentative.ToString("g"))
                                     || roles.Contains(HUDRole.Servicer.ToString("g")));
        }

        public static bool IsLenderAdmin(string[] userRoles)
        {
            return userRoles.Contains("LenderAccountManager") || userRoles.Contains("BackupAccountManager");
        }

        public static bool IsCurrentUserLenderAdmin()
        {
            return IsLenderAdmin(UserPrincipal.Current.UserName);
        }

        public static bool IsUserLenderRole(string userName)
        {
            var roles = Roles.GetRolesForUser(userName);//GetUserRoles(userName);
            return roles != null && (roles.Contains(HUDRole.LenderAccountRepresentative.ToString("g"))
                                     || roles.Contains(HUDRole.LenderAccountManager.ToString("g"))
                                     || roles.Contains(HUDRole.BackupAccountManager.ToString("g"))
                                     || roles.Contains(HUDRole.Servicer.ToString("g")));
        }

        public static IList<MenuRestrictionsViewmodel> GetMenuRestrictedMenuLenders()
        {
            return new LenderInfoRepository().GetMenuRestrictedLenders(); //lenderInfoRepository.GetMenuRestrictedLenders();
        }

        public static bool IsMenuRestricted(int lenderId, string menuTitle)
        {
            var rLenders = new LenderInfoRepository().GetMenuRestrictedLenders();

            var result = (from item in rLenders
                          where (item.LenderId == lenderId && item.MenuName == menuTitle)
                          select item).ToList();

            if (result.Count() > 0) return true;
            else return false;
        }

        public static bool IsProductionUser(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.ProductionUser.ToString("g"));
        }

        public static bool IsProductionWLM(string userName)
        {
            var roles = GetUserRoles(userName);
            return roles.Contains(HUDRole.ProductionWlm.ToString("g"));
        }

        public static bool IsUserLamBamLar(string userName)
        {
            var roles = Roles.GetRolesForUser(userName);//GetUserRoles(userName);
            return roles != null && (roles.Contains(HUDRole.LenderAccountRepresentative.ToString("g"))
                                     || roles.Contains(HUDRole.LenderAccountManager.ToString("g"))
                                     || roles.Contains(HUDRole.BackupAccountManager.ToString("g")));
        }

        public static bool IsHudUser(string userName)
        {
            return (IsHudAdmin(userName) || IsHudDirector(userName) || IsWorkloadManagerRole(userName)
                || IsAccountExecutiveRole(userName) || IsHudAttorney(userName) || IsReviewer(userName)
                || IsProductionUser(userName) || IsProductionWLM(userName));
        }
        public static bool IsInspectionContractor(string userName)
        {
            var roles = GetUserRoles(userName);
           return roles.Contains(HUDRole.InspectionContractor.ToString("g"));
        }

        public static string[] GetUsersByRole(string roleName)
        {
            var users = Roles.GetUsersInRole(roleName);
            return users;
        }
    }
}
