﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System.Collections.Generic;

namespace HUDHealthcarePortal.BusinessService
{
    public class LookupManager : ILookupManager
    {
        public IEnumerable<SecurityQuestionLookup> GetAllSecurityQuestions()
        {
            var secQuestionRepo = new SecQuestionRepository();
            return secQuestionRepo.GetAllSecQuestions();
        }

        public IEnumerable<EmailTypeLookup> GetAllEmailTypes()
        {
            var emailTypeRepo = new EmailTypeRepository();
            return emailTypeRepo.GetAllEmailTypes();
        }

        public IEnumerable<KeyValuePair<int, string>> GetAllLenders()
        {
            var lenderRepo = new LenderRepository();
            return lenderRepo.GetAllLenders();
        }
    }
}
