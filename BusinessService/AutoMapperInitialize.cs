﻿using HUDHealthcarePortal.Repository;

namespace HUDHealthcarePortal.BusinessService
{
    public static class AutoMapperInitialize
    {
        public static void Initialize()
        {
            AutoMapperRepositoryConfig.Configure();
        }
    }
}
