﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using Repository.Production;
using HUDHealthcarePortal.BusinessService;

namespace BusinessService.Production
{
   public  class ProductionQueueManager:IProductionQueueManager
   {
       private UnitOfWork unitOfWork;
       private UnitOfWork uitOfWorkTask;
       private IUserInfoRepository userInfoRepository;
       private ITaskRepository taskRepository;
       private IProd_TaskXrefRepository prodTaskXrefRepository;
       private IFHANumberRequestRepository fhaNumberRequestRepository;
       private IProd_ViewRepository taskviewRepository;
        private IProd_Form290TaskRepository prod_Form290TaskRepository;


       public ProductionQueueManager()
       {
           unitOfWork = new UnitOfWork(DBSource.Live);
           uitOfWorkTask = new UnitOfWork(DBSource.Task);
           userInfoRepository = new UserInfoRepository(unitOfWork);
           taskRepository = new TaskRepository(uitOfWorkTask);
           prodTaskXrefRepository = new Prod_TaskXrefRepository(uitOfWorkTask);
           fhaNumberRequestRepository = new FHANumberRequestRepository(unitOfWork);
           taskviewRepository = new Prod_ViewRepository(uitOfWorkTask);
            prod_Form290TaskRepository = new Prod_From290TaskRepository(uitOfWorkTask);
       }
        public List<UserInfoModel> GetProductionUsers()
        {
            var productionUser = userInfoRepository.GetProductionUsers().ToList();
            return productionUser;
        }

		/// <summary>
		/// get productionWlmUsers
		/// </summary>
		/// <returns></returns>
		public List<UserInfoModel> GetProductionWLMUsers()
		{
			var productionWlmUsers = userInfoRepository.GetProductionWLMUsers().ToList();
			return productionWlmUsers;
		}
		public IEnumerable<TaskModel> GetProductionTasks()
        {
            return taskRepository.GetProductionTasks();
        }
        public IEnumerable<TaskModel> GetProductionQueues()
        {
            return taskRepository.GetProductionQueues();
        }
        public IEnumerable<TaskModel> GetProductionTaskByType(int type)
        {
            return taskRepository.GetProductionTaskByType(type);
        }
        public UserViewModel GetUserById(int? userId)
       {
           if (userId == null)
               return new UserViewModel();
           return userInfoRepository.GetUserInfoById((int) userId);

       }

       public IEnumerable<TaskModel> GetProductionTaskByStatus(int status)
       {
           return taskRepository.GetProductionTaskByStatus(status);
       }

       public IEnumerable<TaskModel> GetProductionTaskByTypeAndStatus(int type, int status)
       {
           return taskRepository.GetProductionTaskByTypeAndStatus(type, status);
       }

       public bool AssignProductionFhaInsert(ProductionTaskAssignmentModel model)
       {
           var subTasks = prodTaskXrefRepository.GetProductionSubTasks(model.TaskInstanceId).ToList();
           if (subTasks.Any())
           {
               var unAssigned = subTasks.Count(m => m.AssignedTo == null);
               if (unAssigned < 1)
                   model.IsChildAssigned = true;
           }
           else
           {
               model.IsChildAssigned = true;
           }

           return taskRepository.AssignProductionFhaInsert(model);
       }

       public IEnumerable<TaskModel> GetProductionQueueByType(int type)
       {
           return taskRepository.GetProductionQueueByType(type);
       }

       public IEnumerable<ProductionQueueLenderInfo> GetFhaRequests(List<Guid> taskInstances)
       {
          return fhaNumberRequestRepository.GetFhaRequests(taskInstances);
       }

       public IEnumerable<ProductionMyTaskModel> GetProductionTaskByUserName(string userName,string role)
       {
           return taskRepository.GetProductionTaskByUserName(userName,role);
       }


       public List<UserInfoModel> GetUnAssignedProductionUsers()
       {
           var productionUser = userInfoRepository.GetUnAssignedProductionUsers().ToList();
           return productionUser;
       }


       public List<Prod_ViewModel> GetUnAssignedViewsbyTaskInstanceId(int TypeID, Guid taskinstanceid)
       {
           return taskviewRepository.GetUnAssignedViewsbyTaskInstanceId(TypeID, taskinstanceid);
       }


       public UserInfoModel GetUserInfoByUsername(string userName)
       {
           return userInfoRepository.GetUserInfoByUsername(userName);
       }
       public int GetMyOpenProdTasks()
       {
           var currentUser = UserPrincipal.Current;
            var taskList = taskRepository.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole);
            if (RoleManager.IsUserLenderRole(currentUser.UserName))
            {
                return taskList.Where(m => m.StatusId == (int)TaskStep.Request
                  || m.StatusId == (int)TaskStep.OPAAddtionalInformation).Count();
            }
            else
            {
                return taskList.Where(m => m.StatusId == (int)TaskStep.InProcess || m.StatusId == (int)TaskStep.Form290Request).Count();
            }
           
       }


       public List<UserInfoModel> GetUnAssignedExternalReviewers()
       {
           var unassignedUser = userInfoRepository.GetUnAssignedExternalReviewers().ToList();
           return unassignedUser;
       }

        public List<UserInfoModel> GetALLInternalSpecialOptionUsers()
        {
            var unassignedUser = userInfoRepository.GetALLInternalSpecialOptionUsers().ToList();
            return unassignedUser;
        }


        public IEnumerable<FilteredProductionTasksModel> GetFilteredProductionTasks(int productionType, DateTime dateFrom, DateTime dateTo, int lenderId, int loanType)
       {
           return taskRepository.GetFilteredProductionTasks(productionType, dateFrom, dateTo, lenderId, loanType);
       }


       public UserViewModel  GetUserById(int userId)
       {
           return userInfoRepository.GetUserInfoById(userId);
       }


       public IEnumerable<ApplicationAndClosingTypeModel> GetApplicationAndClosingType(Guid taskInstanceId)
       {
           return taskRepository.GetApplicationAndClosingType(taskInstanceId);
       }


       public IEnumerable<TaskModel> GetCompletedFHAsAndNotAssignedChild()
       {
           return taskRepository.GetCompletedFHAsAndNotAssignedChild();
       }

        public Guid CreateForm290Task(Prod_Form290TaskModel model)
        {
           return prod_Form290TaskRepository.AddForm290Task(model);
        }

        public Guid AddTask(TaskModel model)
        {
            return taskRepository.AddForm290Task(model);
        }
        public Prod_Form290TaskModel GetForm290TaskBySingleTaskInstanceID(Guid taskInstanceID)
        {
            return prod_Form290TaskRepository.GetForm290TaskBySingleTaskInstanceID(taskInstanceID);
        }
        public List<Prod_Form290TaskModel> GetForm290TaskByTaskInstanceID(List<Guid> taskInstanceIDs)
        {   
            return prod_Form290TaskRepository.GetForm290TaskByTaskInstanceID(taskInstanceIDs);
        }

        public Prod_Form290TaskModel GetFrom290TaskByClosingTaskInstanceID(Guid ClosingTaskInstanceID)
        {
            return prod_Form290TaskRepository.GetFrom290TaskByClosingTaskInstanceID(ClosingTaskInstanceID);
        }

        public bool AssignForm290Task(ProductionTaskAssignmentModel model)
        {
            return taskRepository.AssignForm290Task(model);
        }
    }
}
