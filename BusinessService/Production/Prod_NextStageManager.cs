﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using Repository.Interfaces;

namespace BusinessService.Production
{
    public class Prod_NextStageManager : IProd_NextStageManager
    {
        private UnitOfWork unitOfWorkTask;
        private IProd_NextStageRepository prod_NextStageRepository;
        private ITaskRepository taskRepository;


        public Prod_NextStageManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Live);

            prod_NextStageRepository = new Prod_NextStageRepository(unitOfWorkTask);
            taskRepository = new TaskRepository(unitOfWorkTask);
            
        }


        public Guid AddNextStage(Prod_NextStageModel model)
        {
            return prod_NextStageRepository.AddNextStage(model);
        }

        public List<string> GetFhasforNxtStage(int pagetypeid, int lenderId)
        {
            return prod_NextStageRepository.GetFhasforNxtStage(pagetypeid, lenderId);
        }

        public bool UpdateTaskInstanceIdForNxtStage(Prod_NextStageModel model)
        {
            return prod_NextStageRepository.UpdateTaskInstanceIdForNxtStage(model);
        }

		/// <summary>
		/// get currenttaskinstanceid from taskinstanceid
		/// </summary>
		/// <param name="guidCurrentTaskinstanceid"></param>
		/// <returns></returns>
		public Guid? GetNxtStageTaskid(Guid guidCurrentTaskinstanceid)
		{
			return prod_NextStageRepository.GetNxtStageTaskid(guidCurrentTaskinstanceid);
		}

		/// <summary>
		/// get completedtaskinstanceid from taskinstanceid
		/// </summary>
		/// <param name="guidCurrentTaskinstanceid"></param>
		/// <returns></returns>
		public Guid? GetCompletedStageTaskid(Guid guidCurrentTaskinstanceid)
		{
			return prod_NextStageRepository.GetCompletedStageTaskid(guidCurrentTaskinstanceid);
		}
	}
}
