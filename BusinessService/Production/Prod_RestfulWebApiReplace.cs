﻿using BusinessService.Interfaces.Production;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessService.Production
{
    public class Prod_RestfulWebApiReplace:IProd_RestfulWebApiReplace
    {
        private IProd_RestfulWebApiReplaceRepository WebApiDocumentReplaceRepository;
         public Prod_RestfulWebApiReplace()
        {

            WebApiDocumentReplaceRepository = new Prod_RestfulWebApiReplaceRepository();
        }
         public RestfulWebApiResultModel ReplaceDocument(RestfulWebApiUpdateModel fileInfo, string token, HttpPostedFileBase myFile)
        
        {
            return WebApiDocumentReplaceRepository.ReplaceDocumentInfo(fileInfo, token, myFile);
        }
    }
}
