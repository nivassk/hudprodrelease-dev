﻿using HUDHealthcarePortal.Model;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IOPAManager
    {

        List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType);
        //List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId);
    }
}
