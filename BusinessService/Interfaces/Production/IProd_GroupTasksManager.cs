﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_GroupTasksManager
    {
        Guid AddProdGroupTasks(Prod_GroupTasksModel model);
        void UpdateGroupTask(Prod_GroupTasksModel model);
        bool IsGroupTaskAvailable(Guid taskInstanceId);
        Prod_GroupTasksModel GetGroupTaskByTaskInstanceId(Guid taskInstanceId);
      Prod_GroupTasksModel GetGroupTaskAByTaskInstanceId(Guid TaskInstanceId);
        void UpdateProdGroupTask(OPAViewModel projectActionViewModel);
        void Checkin(OPAViewModel AppProcessModel);
        List<Prod_GroupTasksModel> GetGroupTask(int userid);
        void UnlockGroupTask(int taskId);
        Prod_GroupTasksModel GetGroupTaskAById(int TaskId);
    }
}
