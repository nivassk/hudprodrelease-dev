﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_RestfulWebApiUpdate
    {
        RestfulWebApiResultModel UpdateDocument(RestfulWebApiUpdateModel fileInfo, string token);
    }
}
