﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_RestfulWebApiDocumentUpload
    {

        RestfulWebApiResultModel AssetManagementUploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
        RestfulWebApiResultModel UploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
        RestfulWebApiResultModel uploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary);
        RestfulWebApiResultModel uploadCopiedFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary, string filename);
    }
}
