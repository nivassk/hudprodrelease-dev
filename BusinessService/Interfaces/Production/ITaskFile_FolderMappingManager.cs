﻿using HUDHealthcarePortal.Model;
using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
   public  interface ITaskFile_FolderMappingManager
    {
       int AddTaskFile_FolderMapping(TaskFile_FolderMappingModel model);
       IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys, Guid InstanceId);
    }
}
