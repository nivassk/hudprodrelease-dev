﻿using System;
using Model;
using System.Collections;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;

namespace BusinessService.Interfaces
{
    public interface  INonCriticalRequestExtensionManager
    {
        NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhaNumber);
        List<string> GetAllowedNonCriticalFhaByLenderUserId(int lenderUserId);
        Guid SaveNonCriticalRequestExtension(NonCriticalRequestExtensionModel model);
        void UpdateNonCriticalRequestExtension(NonCriticalRequestExtensionModel model);
        void UpdateTaskId(NonCriticalRequestExtensionModel model);
        TaskModel GetLatestTaskId(Guid taskInstanceId);
    }
}