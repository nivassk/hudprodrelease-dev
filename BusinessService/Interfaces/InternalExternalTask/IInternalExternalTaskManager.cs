﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.InternalExternalTask;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces.InternalExternalTask
{
   public interface IInternalExternalTaskManager
    {
        Guid AddInternalExternalTask(InternalExternalTaskModel internalExternalTask);
        bool UpdateInternalExternalTask(InternalExternalTaskModel internalExternalTask);

        InternalExternalTaskModel FindByTaskInstanceID(Guid taskInstanceID);
        InternalExternalTaskModel FindByFHANuber(string fhaNumber);

        List<InternalExternalTaskModel> GetAllTasks();
        AdditionalPropertyInfo GetPropertyInfoWithAEandWLM(string fhaNumber);
        List<String> GetAvailableFHANumbers();
        InternalExternalTaskModel GetNotCompletedTask(int pageTypeID, string fhaNumber);
        UserViewModel GetUserInfoById(int? userId);
        bool UpdateNoiTask(InternalExternalTaskModel model);

        string GetUserNameById(int? pUserId); //skumar-form290-get user name from hcp_authentication b userid
    }
}
