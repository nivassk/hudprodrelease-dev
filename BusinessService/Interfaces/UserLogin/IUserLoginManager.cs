﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.UserLogin
{
  
    public interface IUserLoginManager
    {
        Guid SaveUserLogin(UserLoginModel model);
        void UpdatUserLogin(UserLoginModel opaViewModel);
        List<UserLoginModel> GetLoginHistory(int userid);
    }
}
