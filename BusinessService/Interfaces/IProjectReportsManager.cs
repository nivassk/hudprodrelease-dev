﻿using System;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using Model;

namespace BusinessService.Interfaces
{
    public interface IProjectReportsManager
    {
        ProjectReportModel GetSuperUserProjectSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetWLMProjectSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetAEProjectSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetProjectLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetSuperUserLargeLoanSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetWLMLargeLoanSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetAELargeLoanSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetLargeLoanLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate);
        ProjectReportModel GetSuperUserRatioExceptionSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetWLMRatioExceptionSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetAERatioExceptionSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetRatioExceptionLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetProjectReportSummary(ReportLevel reportLevel, HUDRole hudRole, string userName, string wlmId, string aeId,
            string lenderId, ReportType reportType, DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetProjectReportDetail(string wlmId, string aeId, string lenderId, ReportType reportType,
            DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        string GetWlmId(string userName);
        string GetAeId(string userName);
    }
}