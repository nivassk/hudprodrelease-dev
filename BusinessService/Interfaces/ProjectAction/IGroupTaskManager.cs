﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using Model.ProjectAction;

namespace BusinessService.ProjectAction
{
    public interface IGroupTaskManager
    {
        int AddNewGroupTask(GroupTaskModel groupTaskModel);
        bool CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber);
        void UpdateGroupTask(ProjectActionViewModel projectActionViewModel);
        PaginateSortModel<GroupTaskModel> GetGroupTasksForLender(int lenderId, int page, string sort,
            SqlOrderByDirecton sortDir);
        GroupTaskModel GetGroupTaskById(int taskId);
        void UnlockGroupTask(int taskId);
        void UpdateGroupTaskForCheckout(GroupTaskModel groupTaskModel);
        ProjectActionViewModel GetProjectActionViewModelFromGroupTask(int taskId);
        //Extending Methods For OPA Form
        OPAViewModel GetOPAProjectActionViewModelFromGroupTask(int taskId);
        void LockGroupTask(int taskId);
        void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel);

        GroupTaskModel GroupTaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber);
        bool CheckIfFileExistsByGroupTaskId(int groupTaskId);
    }
}
