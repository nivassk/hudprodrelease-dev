﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System.Collections.Generic;
using Repository.Interfaces;

namespace HUDHealthcarePortal.BusinessService
{
    public class MultiLoanManager : IMultiLoanManager
    {
        private readonly IMultiLoanRepository multiLoanRepository;

        public MultiLoanManager()
        {
            UnitOfWork unitOfWorkLive = new UnitOfWork(DBSource.Live);
            multiLoanRepository = new MultipleLoanRepository(unitOfWorkLive);
        }

        IEnumerable<MultiLoanModel> IMultiLoanManager.GetMultiLoans()
        {
            return multiLoanRepository.GetMultiLoans();
        }

        public IEnumerable<MultiLoanDetailModel> GetMultiLoansDetailByPropertyID(int propertyId)
        {
            return multiLoanRepository.GetMultiLoansDetailByPropertyID(propertyId);
        }
    }
}
