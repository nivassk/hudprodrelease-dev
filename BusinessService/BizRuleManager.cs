﻿using System;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using System.Globalization;

namespace HUDHealthcarePortal.BusinessService
{
    public class BizRuleManager
    {
        public UploadStatusFlag GetUploadStatusFlag(string monthsInPeriod, string periodEnding)
        {
            // parse data
            int iMonthInPeriod;
            DateTime dtPeriodEnding;
            var bMonthInPeriodOK = int.TryParse(monthsInPeriod, out iMonthInPeriod);
            var bPeriodEndingOK = DateTime.TryParse(periodEnding, out dtPeriodEnding);
            if (!bMonthInPeriodOK || !bPeriodEndingOK)
                return UploadStatusFlag.BadData;

            //validating quarterly and year-end financial rules
            int iDaysInDifference = 0;
            
            DateTime dCurrentDate = DateTime.Now;
            TimeSpan tDateDifference = dCurrentDate - dtPeriodEnding;
            iDaysInDifference = tDateDifference.Days;
            if (iMonthInPeriod == 3 || iMonthInPeriod == 6 || iMonthInPeriod == 9)
            {
                if (iDaysInDifference > 60)
                {
                    return UploadStatusFlag.Late;
                }
            }
            else if (iMonthInPeriod == 12)
            {
                if (iDaysInDifference > 90)
                {
                    return UploadStatusFlag.Late;
                }
            }
            return UploadStatusFlag.NotLate;
        }

        // put all HUD business rules here
        // e.g. lock up period of upload
        public string UploadFieldsCannotEmpty(FormUploadModel model)
        {
            string displayErrorrs = String.Empty;
            DateTime periodEnding = DateTime.MinValue;
            if (model == null)
                return "Please fill in all empty fields.";
            if (string.IsNullOrEmpty(model.FHANumber))
                displayErrorrs = "FHA Number cannot be empty.<br/>";
            if ( !model.LenderID.HasValue )
                displayErrorrs += "Service ID cannot be empty.<br/>";
            if (String.IsNullOrEmpty(model.ProjectName))
                displayErrorrs += "Project Name cannot be empty.<br/>";
            if (string.IsNullOrEmpty(model.PeriodEnding) || !DateTime.TryParseExact(model.PeriodEnding, "d", CultureInfo.InvariantCulture, DateTimeStyles.None, out periodEnding ) )
                displayErrorrs += "Period Ending should be in MM/DD/YYYY format.<br/>";
            if (String.IsNullOrEmpty(model.MonthsInPeriod) )
                displayErrorrs += "Months in Period cannot be empty.<br/>";
            if (!model.TotalRevenues.HasValue)
                displayErrorrs += "Total Revenues cannot be empty.<br/>";
            if (!model.TotalExpenses.HasValue)
                displayErrorrs += "Total Operating Expenses cannot be empty.<br/>";
            if (!model.ActualNumberOfResidentDays.HasValue )
                displayErrorrs += "Actual Number of Resident Days cannot be empty.<br/>";
            if ( !model.FHAInsuredPrincipalInterestPayment.HasValue )
                displayErrorrs += "FHA Insured Principal & Interest Payment cannot be empty.<br/>";
            if (!model.MortgageInsurancePremium.HasValue)
                displayErrorrs += "MIP cannot be empty.<br/>";
                
            return displayErrorrs;
        }

        public bool CheckForEmptyR4RFormFields(ReserveForReplacementFormModel model)
        {
            if (model == null)
                return false;
            if (string.IsNullOrEmpty(model.FHANumber) || string.IsNullOrEmpty(model.PropertyName) || string.IsNullOrEmpty(model.PropertyAddress.AddressLine1))
                return false;
            if (string.IsNullOrEmpty(model.PropertyAddress.City) || string.IsNullOrEmpty(model.PropertyAddress.StateCode) || string.IsNullOrEmpty(model.PropertyAddress.ZIP))
                return false;
            if (!model.NumberOfUnits.HasValue || !model.ReserveAccountBalance.HasValue || !model.ConfirmReserveAccountBalance.HasValue)
                return false;
            if (!model.TotalPurchaseAmount.HasValue || !model.TotalRequestedAmount.HasValue)
                return false;
            
            
            return true;
        }
    }
}
