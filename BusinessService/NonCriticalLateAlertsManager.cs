﻿using System.Collections.Generic;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository;
using Repository.Interfaces;

namespace BusinessService
{
    public class NonCriticalLateAlertsManager : INonCriticalLateAlertsManager
    {
        private readonly INonCriticalLateAlertsRepository nonCriticalLateAlertsRepository;
        private readonly INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository;
        private readonly UnitOfWork unitOfWorkLive;

        public NonCriticalLateAlertsManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            nonCriticalLateAlertsRepository = new NonCriticalLateAlertsRepository(unitOfWorkLive);
            nonCriticalProjectInfoRepository = new NonCriticalProjectInfoRepository();
        }

        public List<NonCriticalLateAlertsModel> GetNonCriticalLateAlertsToSend(string date)
        {
            return nonCriticalProjectInfoRepository.GetNonCriticalLateAlertsToSend(date);
        }

        public void SaveNonCriticalLateAlerts(IList<NonCriticalLateAlertsModel> models)
        {
            nonCriticalLateAlertsRepository.SaveNonCriticalLateAlerts(models);
        }
    }
}