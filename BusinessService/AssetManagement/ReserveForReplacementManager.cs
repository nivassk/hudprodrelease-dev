﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Repository;
using Repository.Interfaces;

namespace BusinessService.AssetManagement
{
    public class ReserveForReplacementManager : IReserveForReplacementManager
    {
        private IProjectInfoRepository projectInfoRepository;
        private IReserveForReplacementRepository reserveForReplacementRepository;
        private ITaskRepository taskRepository;
        private UnitOfWork unitOfWorkLive;
        private IDisclaimerMsgRepository disclaimerMsgRepository;

        public ReserveForReplacementManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            projectInfoRepository = new ProjectInfoRepository(unitOfWorkLive);
            reserveForReplacementRepository = new ReserveForReplacementRepository(unitOfWorkLive);
            taskRepository = new TaskRepository();
            disclaimerMsgRepository = new DisclaimerMsgRepository(unitOfWorkLive);// User Story 1901

        }
        // User Story 1901
        public string GetDisclaimerMsgByPageType(string pageTypeName)
        {
            return disclaimerMsgRepository.GetDisclaimerMsgByPageType(pageTypeName);
        }

        public PropertyInfoModel GetPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetPropertyInfo(fhaNumber);
        }
        public ReserveForReplacementFormModel GetApprovedAmountByFHANumber(string fhaNumber)
        {
            return reserveForReplacementRepository.GetApprovedAmountByFHANumber(fhaNumber);
        }

        public List<string> GetAllFHANumbersListForR4R()
        {
            return reserveForReplacementRepository.GetAllFHANumbersListForR4R();
        }

        public void UpdateApprovedAmount(decimal changeApprovedAmount, string FHANumber, string HudRemarks)
        {
            reserveForReplacementRepository.UpdateApprovedAmount(changeApprovedAmount, FHANumber, HudRemarks);
            unitOfWorkLive.Save();
        }

        public int GetAeUserIdByFhaNumber(string fhaNumber)
        {
            return projectInfoRepository.GetAeUserIdByFhaNumber(fhaNumber);
        }

        public string GetAeEmailByFhaNumber(string fhaNumber)
        {
            return projectInfoRepository.GetAeEmailByFhaNumber(fhaNumber);
        }

		public ReserveForReplacementFormModel GetR4RFormByFHANumber(string fhaNumber)
		{
			return reserveForReplacementRepository.GetR4RFormByFHANumber(fhaNumber);
		}
		 

		public IEnumerable<ReserveForReplacementFormModel> GetR4RFormByPropertyId(int propertyId)
        {
            return reserveForReplacementRepository.GetR4RFormByPropertyId(propertyId);
        }

        public Guid SaveR4RForm(ReserveForReplacementFormModel model)
        {
            try
            {
                Guid r4rId = reserveForReplacementRepository.SaveR4RForm(model);
                unitOfWorkLive.Save();
                return r4rId;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void UpdateR4RForm(ReserveForReplacementFormModel model)
        {
            reserveForReplacementRepository.UpdateR4RForm(model);
            unitOfWorkLive.Save();
        }

        public ReserveForReplacementFormModel GetR4RbyTaskInstanceId(Guid taskInstanceId)
        {
            return reserveForReplacementRepository.GetR4RbyTaskInstanceId(taskInstanceId);
        }
        public TaskModel GetLatestTaskId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId);
        }

        /// <summary>
        /// update the R4R with latest task id
        /// </summary>
        public void UpdateTaskId(ReserveForReplacementFormModel model)
        {
            reserveForReplacementRepository.UpdateTaskId(model);
            unitOfWorkLive.Save();
        }

        public List<string> GetSubmitedFHANumbers(int lenderID)
        {
            return reserveForReplacementRepository.GetSubmitedFHANumbers(lenderID);
        }
    }
}
