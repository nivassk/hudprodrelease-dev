﻿


CREATE PROCEDURE [dbo].[usp_HCP_Get_QuarterlyUploadReport_View]
(
@LenderId int	-- lender id: -1 gets all lenders
)
AS
SET FMTONLY OFF
DECLARE @dtNow DATETIME
DECLARE @firstDayOfQrtRecent DATETIME
SET @dtNow = GETDATE()
SELECT @firstDayOfQrtRecent = DATEADD(quarter,DATEDIFF(quarter,0,@dtNow),0);

CREATE TABLE #UploadStatus
(
    LenderID INT PRIMARY KEY,
	LenderName NVARCHAR(100), 
    TotalExpected INT, 
    Received INT, 
    Missing AS (TotalExpected - Received)
);

WITH List_Unique_LenderID (LenderID)
AS
(
SELECT DISTINCT LDI.LenderID 
FROM [HCP_Live_db].[dbo].[Lender_FHANumber] AS LDI
)

INSERT INTO #UploadStatus
SELECT 
LUL.LenderID, 
LI.Lender_Name,
(SELECT COUNT(*) FROM [HCP_Live_db].[dbo].[Lender_FHANumber] LF WHERE LF.LenderID =  LUL.LenderID) AS TOTALEXPECTED,
(SELECT COUNT(*) FROM [HCP_Live_db].[dbo].[Lender_FHANumber] LF 
INNER JOIN [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] LDI1 
ON LF.LenderID = LDI1.LenderID
AND LF.FHANumber = LDI1.FHANumber
INNER JOIN
(
	SELECT MAX(FHANumber) AS MaxFHA, MAX(DataInserted) as MaxDate
	FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate]
	GROUP BY LenderID, FHANumber
) b
ON LDI1.FHANumber = b.MaxFHA AND LDI1.DataInserted = b.MaxDate  
WHERE LF.LenderID = LUL.LenderID 
AND LDI1.DataInserted >= @firstDayOfQrtRecent) AS RECEIVED
FROM List_Unique_LenderID LUL
INNER JOIN [HCP_Live_db].[dbo].[LenderInfo] LI
ON LUL.LenderID = LI.LenderID AND ISNULL(LI.Deleted_Ind, 0) = 0
AND (@LenderId = -1 OR LUL.LenderID = @LenderId)
  
SELECT a.LenderID, a.LenderName, a.TotalExpected, a.Received, a.Missing FROM #UploadStatus a

SET FMTONLY ON
