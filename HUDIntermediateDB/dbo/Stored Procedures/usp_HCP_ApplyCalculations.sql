CREATE PROCEDURE [dbo].[usp_HCP_ApplyCalculations]
@QueryDate NVARCHAR( 50 )
AS

BEGIN try
        Begin TRANSACTION
  
declare @LDI_ID varchar ( 11 ), @MIP decimal, @PEYear nvarchar( 50 ),@PE DATETIME , @CurrentYear int ,@FHANumber varchar (30 ), @NOICumulative DECIMAL(19,2) ,@IMDate DATETIME
declare @dt  Datetime


set @dt= convert( datetime , @QueryDate , 101 )


select @LDI_ID = min( LDI_ID ) from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a  where CONVERT ( VARCHAR ( 10 ), cast ( a. datainserted as date),  101 )>@dt  and    a. IsCalcProcessed is null

while @LDI_ID is not null
begin
select @MIP = MonthsInPeriod from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID
select   @PEYear = YEAR( CONVERT( DATETIME , PeriodEnding)) from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID
select @PE=    CONVERT( DATETIME , PeriodEnding) from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID
select @FHANumber = FHANumber from  [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID

--Print @LDI_ID
print @PEYear
print @MIP
print @FHANumber

--Start of  Logic of Change in  NOI
declare @TempData_NOI TABLE (
LDI_ID INT,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod decimal  NULL,
QuaterlyNoi DECIMAL(19,2) NULL,
DataInserted nvarchar( 50 ) NULL,
NOIQuaterbyQuater DECIMAL(19,2) NULL,
NOIPercentageChange DECIMAL(19,2) NULL

)


IF (@MIP = 3)
BEGIN
SET @IMDate= @pe
END
ELSE IF ( @MIP = 6 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 3, @pe )
END
ELSE IF ( @MIP = 9 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 6, @pe )
END
ELSE IF ( @MIP = 12 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 9, @pe )
End


Declare @NOIQuaterbyQuater DECIMAL(19,2) , @NOIPercentageChange DECIMAL(19,2)
delete from @TempData_NOI
insert into @TempData_NOI  SELECT DISTINCT a. LDI_ID , FHANumber , MonthsInPeriod, QuaterlyNOI, DataInserted, NOIQuaterbyQuater ,NOIPercentageChange from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a
  where    a . FHANumber= @FHANumber and CONVERT ( DATETIME , a .PeriodEnding )BETWEEN     @IMDate AND @PE and a .MonthsInPeriod <= @MIP AND a .HasCalculated = 1 and CONVERT (VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt

--Select * from @TempData_NOI

--Calculation of noi Qatar By Qatar

-- select COALESCE(MAX(QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod =3
IF (@MIP = 3)
Begin
  --set @NOIQuaterbyQuater =NULL

  set @NOIQuaterbyQuater =( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)
  End
 

ELSE IF ( @MIP = 6 )
begin
if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 3)
  BEGIN
  --set @NOIQuaterbyQuater =NULL
    set @NOIQuaterbyQuater =( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)
  END

  ELSE
--select COALESCE(MAX(QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod =3
    set @NOIQuaterbyQuater = ( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (QuaterlyNoi ), 0) from @TempData_NOI where MonthsInPeriod = 3 )
        Print @NOIQuaterbyQuater
        End
ELSE IF ( @MIP = 9 )
begin

if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 6)
  BEGIN
 -- set @NOIQuaterbyQuater =NULL
   set @NOIQuaterbyQuater =( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)
  END

  Else
--select COALESCE(MAX(QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod =6
  set @NOIQuaterbyQuater = ( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (QuaterlyNoi ), 0) from @TempData_NOI where MonthsInPeriod = 6 )
  Print @NOIQuaterbyQuater
  end
ELSE IF ( @MIP = 12 )
begin

if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 9)
  BEGIN
  --set @NOIQuaterbyQuater =NULL
    set @NOIQuaterbyQuater =( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)
  END

  Else
--select COALESCE(MAX(QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod =9
set @NOIQuaterbyQuater= (select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)-(   select COALESCE (MAX (QuaterlyNoi ), 0) from @TempData_NOI where MonthsInPeriod = 9 )
Print @NOIQuaterbyQuater
End
--Logic to Update NOI QuarterbyQuarter
Update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set NOIQuaterbyQuater = @NOIQuaterbyQuater, IsCalcProcessed =1 where LDI_ID= @LDI_ID

---Calculation of NOI Percentage Change

IF (@MIP = 3)
   set @NOIPercentageChange =NULL
 

ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 3)
  BEGIN
  set @NOIPercentageChange =NULL
  END
  Else
    IF ( ISNULL(( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod = 3), 0 ) != 0 )
    BEGIN
        DECLARE @A DECIMAL(19,2) ,@B DECIMAL(19,2)
        select @A= QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP
        select @B= QuaterlyNoi from @TempData_NOI where MonthsInPeriod= 3

        --select COALESCE(MAX(QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod =3
print ((@A - @B)/ @B )*100
        set @NOIPercentageChange = (( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod= @MIP)-( select COALESCE (MAX (QuaterlyNoi ), 0) from @TempData_NOI where MonthsInPeriod = 3 ) )/( select COALESCE (MAX (QuaterlyNoi ), 0) from @TempData_NOI where MonthsInPeriod= 3 )*100
    END
    ELSE
    BEGIN
        set @NOIPercentageChange= 0
    END
ELSE IF ( @MIP = 9 )

if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 6)
  BEGIN
  set @NOIPercentageChange =NULL
  END

  ELSE
 
  IF ( ISNULL(( select QuaterlyNoi from @TempData_NOI where MonthsInPeriod = 6), 0 ) != 0 )
    BEGIN
        set @NOIPercentageChange = (( select COALESCE (MAX ( QuaterlyNoi), 0 ) from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuaterlyNoi), 0 ) from @TempData_NOI where MonthsInPeriod = 6 ) )/( select COALESCE (MAX ( QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod = 6 )* 100

    END
    ELSE
    BEGIN
        set @NOIPercentageChange= 0
    END
ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_NOI where MonthsInPeriod = 9)
  BEGIN
  set @NOIPercentageChange =NULL
  END

  ELSE
IF (ISNULL ((select QuaterlyNoi from @TempData_NOI where MonthsInPeriod = 9 ), 0 ) != 0 )
    BEGIN
        set @NOIPercentageChange= ((select COALESCE( MAX( QuaterlyNoi ), 0 ) from @TempData_NOI where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuaterlyNoi), 0 ) from @TempData_NOI where MonthsInPeriod = 9 ) )/( select COALESCE (MAX ( QuaterlyNoi), 0) from @TempData_NOI where MonthsInPeriod = 9 )* 100

    END
    ELSE
    BEGIN
        set @NOIPercentageChange= 0
    END

        Print @NOIPercentageChange
        --Logic to Update NOI Noi Percentage Change
Update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set NOIPercentageChange = @NOIPercentageChange, IsCalcProcessed =1 where LDI_ID= @LDI_ID
        --Ending Logic of Change in  NOI
----------------------------------------------------------------------------------------------------------------------------------
        --Start Of Second Block---

        --Start Logic of Change in  Revenue

--Need To Change the below logic in
--update [dbo].[Lender_DataUpload_Intermediate] set QuaterlyOperatingRevenue=TotalRevenues

--Calculation of Noi Cumulative
declare @TempData_Revenue TABLE (
LDI_ID INT,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod decimal  NULL,
QuaterlyOperatingRevenue DECIMAL(19,2) NULL,
DataInserted nvarchar( 50 ) NULL,
RevenueQuaterbyQuater DECIMAL(19,2) NULL,
RevenuePercentageChange DECIMAL(19,2) NULL
)

Declare @RevenueQuaterbyQuater DECIMAL(19,2) , @RevenuePercentageChange DECIMAL(19,2)
delete from @TempData_Revenue
insert into @TempData_Revenue  select DISTINCT a. LDI_ID ,FHANumber , MonthsInPeriod, QuaterlyOperatingRevenue ,  DataInserted , RevenueQuarterbyQuarter, RevenuePercentageChange 
from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a
  where    a . FHANumber= @FHANumber and CONVERT ( DATETIME , a .PeriodEnding )BETWEEN     @IMDate AND @PE and a .MonthsInPeriod <= @MIP AND a .HasCalculated = 1 and CONVERT (VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt

--select * from @TempData_Revenue


--Calculation of Revenue Qatar By Qatar

IF (@MIP = 3)
   --set @RevenueQuaterbyQuater =NULL

  set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)
 
  -- COALESCE(MAX(QuaterlyOperatingRevenue), 0)
ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 3)
  BEGIN
 -- set @RevenueQuaterbyQuater =NULL
   set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)
  END

  ELSE
    set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)-( select COALESCE (MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod =3 )
ELSE IF ( @MIP = 9 )

if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 6)
  BEGIN
  --set @RevenueQuaterbyQuater =NULL

    set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)
  END
  Else
    set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)-( select COALESCE (MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod =6 )

ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 9)
  BEGIN
 -- set @RevenueQuaterbyQuater =NULL

   set @RevenueQuaterbyQuater = ( select COALESCE (MAX (QuaterlyOperatingRevenue ), 0) from  @TempData_Revenue where MonthsInPeriod = @MIP)
  END

  ELSE

set @RevenueQuaterbyQuater= (select COALESCE( MAX( QuaterlyOperatingRevenue ), 0 ) from @TempData_Revenue where MonthsInPeriod =@MIP )-( select COALESCE( MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod =9 )
--Updating Revenue Quater by Quater
Update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set RevenueQuarterbyQuarter= @RevenueQuaterbyQuater, IsCalcProcessed =1 where LDI_ID = @LDI_ID
print @RevenueQuaterbyQuater

--End of Calculation of  Revenue Qatar By Qatar


---Calculation of Revenue Percentage Change

IF (@MIP = 3)
   set @RevenuePercentageChange =NULL
 

ELSE IF ( @MIP = 6 )
--COALESCE(MAX(QuaterlyOperatingRevenue), 0)

if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 3)
  BEGIN
  set @RevenuePercentageChange =NULL
  END

  ELSE

    IF ( ISNULL(( select QuaterlyOperatingRevenue from @TempData_Revenue where MonthsInPeriod = 3), 0 ) != 0 )
    BEGIN
        set @RevenuePercentageChange = (( select COALESCE ( MAX ( QuaterlyOperatingRevenue),  0 ) from @TempData_Revenue where MonthsInPeriod =@MIP )-( select COALESCE ( MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod = 3) )/(select COALESCE( MAX ( QuaterlyOperatingRevenue ), 0 ) from @TempData_Revenue where MonthsInPeriod= 3  )* 100
    END
    ELSE
    BEGIN
        set @RevenuePercentageChange= 0
    END
ELSE IF ( @MIP = 9 )
 
if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 6)
  BEGIN
  set @RevenuePercentageChange =NULL
  END

  ELSE


  IF ( ISNULL(( select QuaterlyOperatingRevenue from @TempData_Revenue where MonthsInPeriod = 6), 0 ) != 0 )
    BEGIN
        set @RevenuePercentageChange = (( select COALESCE ( MAX ( QuaterlyOperatingRevenue),  0 ) from @TempData_Revenue where MonthsInPeriod =@MIP )-( select COALESCE ( MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod = 6) )/(select COALESCE( MAX ( QuaterlyOperatingRevenue ), 0 ) from @TempData_Revenue where MonthsInPeriod= 6  )* 100

    END
    ELSE
    BEGIN
        set @RevenuePercentageChange= 0
    END
ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_Revenue where MonthsInPeriod = 9)
  BEGIN
  set @RevenuePercentageChange =NULL
  END

  ELSE

IF (ISNULL ((select QuaterlyOperatingRevenue from @TempData_Revenue where MonthsInPeriod = 9), 0 ) != 0 )
    BEGIN
        set @RevenuePercentageChange= ((select COALESCE( MAX ( QuaterlyOperatingRevenue), 0 )  from @TempData_Revenue where MonthsInPeriod =@MIP )-( select COALESCE ( MAX ( QuaterlyOperatingRevenue), 0 ) from @TempData_Revenue where MonthsInPeriod = 9) )/(select COALESCE( MAX ( QuaterlyOperatingRevenue ), 0 ) from @TempData_Revenue where MonthsInPeriod= 9  )* 100

    END
    ELSE
    BEGIN
        set @RevenuePercentageChange= 0
    END

        --Updating Revenue Percentage Change in Revenue
    Update [$(DatabaseName)].[dbo].[Lender_DataUpload_Intermediate] set RevenuePercentageChange= @RevenuePercentageChange, IsCalcProcessed =1 where LDI_ID = @LDI_ID
        Print @RevenuePercentageChange

        ------------------------------------------------  END OF REVENUE  -----------------------------------------------------             --

        --------------------------------------------------------------------------------------------------------------------------------------
        --Logic for Change in Average Daily Rate and Resident Days
        --Calculation of Noi Cumulative


--Update  [dbo].[Lender_DataUpload_Intermediate] set QuarterlyResidentDays= ActualNumberOfResidentDays
declare @TempData_ADR TABLE (
LDI_ID INT,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod decimal  NULL,
QuarterlyResidentDays decimal NULL,
DataInserted nvarchar( 50 ) NULL,
CumulativeResidentDays decimal NULL,
ADRperQuarter DECIMAL( 19 ,2 ) NULL,
ADRDifferencePerQuarter DECIMAL (19 , 2) NULL,
ADRPercentageChange DECIMAL( 19 ,2 ) NULL
)

Declare @ADRDifferencePerQuarter DECIMAL ( 19, 2 ) , @ADRPercentageChange DECIMAL ( 19 , 2)
Delete from @TempData_ADR
insert into @TempData_ADR  select DISTINCT a. LDI_ID ,FHANumber , MonthsInPeriod, QuarterlyResidentDays , DataInserted , CumulativeResidentDays ,ADRperQuarter , ADRDifferencePerQuarter, ADRPercentageChange from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a
  where    a . FHANumber= @FHANumber and CONVERT ( DATETIME , a .PeriodEnding )BETWEEN     @IMDate AND @PE and a .MonthsInPeriod <= @MIP AND a .HasCalculated = 1 and CONVERT (VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt


--select * from @TempData_ADR

--Calculation of Adr Difference per Qatar

IF (@MIP = 3)
   --set @ADRDifferencePerQuarter =NULL

  set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )
 
  --COALESCE(MAX(ADRperQuarter), 0)
ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 3)
  BEGIN
  --set @ADRDifferencePerQuarter =NULL
  
  set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )
  END

  ELSE

    set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 3 )
ELSE IF ( @MIP = 9 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 6)
  BEGIN
  --set @ADRDifferencePerQuarter =NULL
  
  set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )
  END

  ELSE

    set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 6 )

ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 9)
  BEGIN
  --set @ADRDifferencePerQuarter =NULL
  
  set @ADRDifferencePerQuarter = ( select COALESCE (MAX (ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod= @MIP )
  END

  ELSE

set @ADRDifferencePerQuarter= (select COALESCE( MAX( ADRperQuarter ), 0 ) from @TempData_ADR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 9 )

print @ADRDifferencePerQuarter

--Updating Logic for ADRDifferencePerQuarter
  update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set ADRDifferencePerQuarter= @ADRDifferencePerQuarter, IsCalcProcessed =1 where LDI_ID = @LDI_ID
--End of Calculation of Adr Difference per Qatar

---Calculation of ADR Percentage Change
--COALESCE(MAX(ADRperQuarter), 0)
IF (@MIP = 3)
   set @ADRPercentageChange =NULL
 
ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 3)
  BEGIN
  set @ADRPercentageChange =NULL
  END

  ELSE

    IF ( ISNULL(( select ADRperQuarter from @TempData_ADR where MonthsInPeriod = 3), 0) != 0.00 )
    BEGIN
        set @ADRPercentageChange = ((select COALESCE( MAX( ADRperQuarter ), 0 ) from @TempData_ADR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 3 ) )/( select COALESCE (MAX ( ADRperQuarter), 0) from @TempData_ADR where MonthsInPeriod = 3 )* 100
    END
    ELSE
    BEGIN
        set @ADRPercentageChange= 0
    END
ELSE IF ( @MIP = 9 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 6)
  BEGIN
  set @ADRPercentageChange =NULL
  END

  ELSE
 
  IF ( ISNULL(( select ADRperQuarter from @TempData_ADR where MonthsInPeriod = 6), 0 ) != 0.00 )
    BEGIN
        set @ADRPercentageChange = (( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = @MIP )-( select COALESCE ( MAX( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 6 ) )/( select COALESCE ( MAX ( ADRperQuarter ), 0) from @TempData_ADR where MonthsInPeriod = 6 )* 100
    END
    ELSE
    BEGIN
        --print 'nothin'
        set @ADRPercentageChange= 0
    END
ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_ADR where MonthsInPeriod = 9)
  BEGIN
  set @ADRPercentageChange =NULL
  END

  ELSE
IF (ISNULL ((select ADRperQuarter from @TempData_ADR where MonthsInPeriod = 9 ), 0 ) != 0.00 )
    BEGIN
        set @ADRPercentageChange= ((select COALESCE( MAX( ADRperQuarter ), 0 ) from @TempData_ADR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( ADRperQuarter), 0 ) from @TempData_ADR where MonthsInPeriod = 9 ) )/( select COALESCE (MAX ( ADRperQuarter), 0) from @TempData_ADR where MonthsInPeriod = 9 )* 100

    END
    ELSE
    BEGIN
        set @ADRPercentageChange= 0
    END

        Print @ADRPercentageChange
--Updating Logic for ADR Percentage Change
  update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set ADRPercentageChange= @ADRPercentageChange, IsCalcProcessed =1 where LDI_ID = @LDI_ID


         ---------------------------------------------------------------------------------------------
--Calculation of DSCR
--Update [dbo].[Lender_DataUpload_Intermediate] set QuarterlyFHAPI=FHAInsuredPrincipalInterestPayment
--Update [dbo].[Lender_DataUpload_Intermediate] set QuarterlyMIP=MonthsInPeriod
--Update [dbo].[Lender_DataUpload_Intermediate] set QuarterlyDSCR=DebtCoverageRatio2
--Update [dbo].[Lender_DataUpload_Intermediate] set QuarterlyOperatingExpense=TotalExpenses



declare @TempData_DSCR TABLE (
LDI_ID INT,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod decimal  NULL,
DataInserted nvarchar( 50 ) NULL,
TotalExpenses DECIMAL(19,2) NULL,
CumulativeExpenses DECIMAL(19,2) NULL,
FHAInsuredPrincipalInterestPayment DECIMAL(19,2) NULL,
FHAPICumulative DECIMAL(19,2) NULL,
QuarterlyFHAPI DECIMAL(19,2) null,
QuarterlyMIP DECIMAL(19,2) NULL,
MIPCumulative DECIMAL(19,2) NULL,
DebtCoverageRatio2 DECIMAL(19,2) NULL,
DSCRDifferencePerQuarter DECIMAL (19 , 2) NULL,
DSCRPercentageChange DECIMAL( 19 ,2 ) null,
QuarterlyDSCR DECIMAL( 19 ,2 ) NULL
)

Declare @DSCRDifferencePerQuarter DECIMAL ( 19, 2 ),@DSCRPercentageChange DECIMAL (19 , 2)
Delete from @TempData_DSCR

Insert into @TempData_DSCR  select DISTINCT a. LDI_ID ,FHANumber , MonthsInPeriod , DataInserted , TotalExpenses, CumulativeExpenses , FHAInsuredPrincipalInterestPayment, FHAPICumulative ,QuarterlyFHAPI , QuarterlyMIP, MIPCumulative , DebtCoverageRatio2 , DSCRDifferencePerQuarter, DSCRPercentageChange , QuarterlyDSCR from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a
  where    a . FHANumber= @FHANumber and CONVERT ( DATETIME , a .PeriodEnding )BETWEEN     @IMDate AND @PE and a .MonthsInPeriod <= @MIP AND a .HasCalculated = 1 and CONVERT (VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt


--Select * from @TempData_DSCR

--Calculation of DSCR Difference Per Quarter
--COALESCE(MAX(QuarterlyDSCR), 0)
IF (@MIP = 3)
   --set @DSCRDifferencePerQuarter =NULL
 set @DSCRDifferencePerQuarter =( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )

ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 3)
  BEGIN
  --set @DSCRDifferencePerQuarter =NULL
   set @DSCRDifferencePerQuarter =( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )
  END

  ELSE

    set @DSCRDifferencePerQuarter = ( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from  @TempData_DSCR where MonthsInPeriod = 3 )
ELSE IF ( @MIP = 9 )




if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 6)
  BEGIN
  --set @DSCRDifferencePerQuarter =NULL
   set @DSCRDifferencePerQuarter =( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )
  END

  ELSE

    set @DSCRDifferencePerQuarter = ( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from  @TempData_DSCR where MonthsInPeriod = 6 )

ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 9)
  BEGIN
  --set @DSCRDifferencePerQuarter =NULL
   set @DSCRDifferencePerQuarter =( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= @MIP )
  END

  ELSE
set @DSCRDifferencePerQuarter= (select COALESCE( MAX( QuarterlyDSCR ), 0 ) from @TempData_DSCR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from  @TempData_DSCR where MonthsInPeriod = 9 )

print @DSCRDifferencePerQuarter
--Updating  @DSCRDifferencePerQuarter
Update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set DSCRDifferencePerQuarter= @DSCRDifferencePerQuarter, IsCalcProcessed =1 where LDI_ID = @LDI_ID

---Calculation of DSCR Percentage Change
--COALESCE(MAX(QuarterlyDSCR), 0)
IF (@MIP = 3)
   set @DSCRPercentageChange =NULL
 

ELSE IF ( @MIP = 6 )

if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 3)
  BEGIN
  set @DSCRPercentageChange =NULL
  END

  ELSE


    IF ( ISNULL(( select QuarterlyDSCR from @TempData_DSCR where MonthsInPeriod = 3), 0) != 0.00 )
    BEGIN
        set @DSCRPercentageChange = (( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from @TempData_DSCR where MonthsInPeriod = @MIP )-( select COALESCE ( MAX( QuarterlyDSCR), 0 ) from @TempData_DSCR where MonthsInPeriod = 3 ) )/( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= 3 )*100
    END
    ELSE
    BEGIN
        set @DSCRPercentageChange= 0
    END
ELSE IF ( @MIP = 9 )

if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 6)
  BEGIN
  set @DSCRPercentageChange =NULL
  END

  ELSE
 
  IF ( ISNULL(( select QuarterlyDSCR from @TempData_DSCR where MonthsInPeriod = 6), 0) != 0.00 )
    BEGIN
        set @DSCRPercentageChange = (( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from @TempData_DSCR where MonthsInPeriod = @MIP )-( select COALESCE ( MAX( QuarterlyDSCR), 0 ) from @TempData_DSCR where MonthsInPeriod = 6 ) )/( select COALESCE (MAX (QuarterlyDSCR ), 0) from @TempData_DSCR where MonthsInPeriod= 6 )*100
   END
    ELSE
    BEGIN
        set @DSCRPercentageChange= 0
    END
ELSE IF ( @MIP = 12 )

if NOT exists ( select * from @TempData_DSCR where MonthsInPeriod = 9)
  BEGIN
  set @DSCRPercentageChange =NULL
  END

  ELSE


IF (ISNULL ((select QuarterlyDSCR from @TempData_DSCR where MonthsInPeriod = 9 ), 0) != 0.00  )
    BEGIN
        set @DSCRPercentageChange= ((select COALESCE( MAX( QuarterlyDSCR ), 0 ) from @TempData_DSCR where MonthsInPeriod= @MIP )-( select COALESCE (MAX ( QuarterlyDSCR), 0 ) from  @TempData_DSCR where MonthsInPeriod = 9 ) )/( select COALESCE ( MAX ( QuarterlyDSCR), 0 ) from @TempData_DSCR where MonthsInPeriod =9 )* 100

    END
    ELSE
    BEGIN
        set @DSCRPercentageChange= 0
    END

        Print @DSCRPercentageChange

--Updating DSCR Percentage Change
Update [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] set DSCRPercentageChange= @DSCRPercentageChange, IsCalcProcessed =1 where LDI_ID = @LDI_ID


select @LDI_ID = min ( LDI_ID ) from  [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] ()  a  where a. LDI_ID > @LDI_ID and CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt and    a. IsCalcProcessed is null

END

Commit transaction
      
End try
Begin catch
 
        print ERROR_MESSAGE ()

        Rollback transaction
End catch
