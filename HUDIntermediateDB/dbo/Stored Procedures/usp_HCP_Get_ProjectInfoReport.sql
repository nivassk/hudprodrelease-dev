﻿CREATE PROCEDURE [dbo].[usp_HCP_Get_ProjectInfoReport]
(
	@Username nvarchar(max)
)
AS
SELECT TOP 100 u.ProjectName,
       u.ServiceName,
       --u.PeriodEnding,
       --u.UnitsInFacility,
       --u.MonthsInPeriod,
       --u.DataInserted,
       u.FHANumber
       --u.TotalRevenues,
       --u.TotalExpenses,
       --u.FHAInsuredPrincipalInterestPayment,
       --u.MortgageInsurancePremium,
       --u.ActualNumberOfResidentDays,
       --u.DebtCoverageRatio2,
       --u.AverageDailyRateRatio,
       --u.NOIRatio
FROM [dbo].[Lender_DataUpload_Intermediate] u

GO
