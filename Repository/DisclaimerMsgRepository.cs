﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;


namespace HUDHealthcarePortal.Repository
{
    public class DisclaimerMsgRepository : BaseRepository<usp_HCP_GetDisclaimerMsgByPageType_Result>, IDisclaimerMsgRepository
    {
        public DisclaimerMsgRepository() 
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public DisclaimerMsgRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        // User Story 1901
        public string GetDisclaimerMsgByPageType(string pageTypeName)
        {
            var context = this.Context as HCP_live;
            string disclaimerMsg = string.Empty;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFhasByLenderIds, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetDisclaimerMsgByPageType_Result>("[usp_HCP_GetDisclaimerMsgByPageType]",
                new { PageTypeName = pageTypeName }).ToList();
           
            foreach (var value in results)
            {
               disclaimerMsg = disclaimerMsg+ value.DisclaimerTextDescription;
            }

            return disclaimerMsg;

           // return Mapper.Map<IEnumerable<usp_HCP_GetDisclaimerMsgByPageType_Result>, List<string>>(results);
           // return results;
        }
    }
}
