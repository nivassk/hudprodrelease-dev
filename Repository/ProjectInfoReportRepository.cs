﻿using AutoMapper;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ProjectInfoReportRepository : BaseRepository<usp_HCP_Get_ProjectInfoReport_Result>, IProjectInfoReportRepository
    {

        public ProjectInfoReportRepository()
            : base(new UnitOfWork(DBSource.Intermediate))
        {
            
        }

        public ProjectInfoReportRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<ProjectInfoViewModel> GetProjectInfoReport(string username)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in ProjectInfoViewModel, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_ProjectInfoReport_Result>("usp_HCP_Get_ProjectInfoReport",
                new { Username = username }).ToList();
            return Mapper.Map<IEnumerable<ProjectInfoViewModel>>(results);
        }

    }
}
