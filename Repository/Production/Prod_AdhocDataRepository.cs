﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using HUDHealthcarePortal.Repository;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using Repository.Interfaces.Production;
using EntityObject.Entities.HCP_live.Programmability;
using AutoMapper;

namespace Repository.Production
{
    public class Prod_AdhocDataRepository : BaseRepository<Prod_AdhocDataModel>, IProd_AdhocDataRepository
    {
        private UnitOfWork unitOfWorkLive;

        public Prod_AdhocDataRepository():base(new UnitOfWork(DBSource.Live))
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
        }

        public Prod_AdhocDataRepository(UnitOfWork unitOfWork): base(unitOfWork){ }

        public IList<Prod_AdhocDataModel> GetProdDataForAdhocDataDownload()
        {
            var resultList = new List<Prod_AdhocDataModel>();
            try
            {

                var context = (HCP_live)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in GetProdDataForAdhocDataDownload, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetAllProdDataResult>("usp_HCP_Prod_GetAllProdData", null).ToList();
                resultList = Mapper.Map<IList<Prod_AdhocDataModel>>(results).ToList();

            }
            catch (Exception ex)
            {

            }
            return resultList;
        }
    }
}
