﻿using AutoMapper;
using EntityObject.Entities.HCP_task;
using EntityObject.Entities.HCP_task.Programmability;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
    public class TaskFile_FolderMappingRepository : BaseRepository<TaskFile_FolderMapping>, ITaskFile_FolderMappingRepository
    {
       
          public TaskFile_FolderMappingRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

          public TaskFile_FolderMappingRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public int AddTaskFile_FolderMapping(TaskFile_FolderMappingModel model)
        {
            var context = (HCP_task)this.Context;
            var foldermapping = Mapper.Map<TaskFile_FolderMapping>(model);
            var itemExists = (from n in context.TaskFile_FolderMapping where n.TaskFileId == model.TaskFileId select n).SingleOrDefault();
            if (itemExists != null)
            {
                itemExists.FolderKey = model.FolderKey;
                this.Update(itemExists);
            }
            else
            {
                this.InsertNew(foldermapping);
            }

            context.SaveChanges();
            return foldermapping.fileFolderId;
        }


        public IEnumerable<Prod_FolderStructureModel> GetFolderbyViewId(int ViewId)
        {
            try
            {

                var context = (HCP_task)this.Context;

                var folderstructure = (from n in context.Prod_FolderStructure where n.ViewTypeId == ViewId select n).ToList();
                var result = Mapper.Map<IEnumerable<Prod_FolderStructureModel>>(folderstructure);
                return result;
            }

            catch (Exception ex)
            {

            }
            return null;
        }

        public IEnumerable<Prod_FolderStructureModel> GetFolderListbyTaskInstanceId(Guid taskInstanceId)
        {
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var folderList = new List<Prod_FolderStructureModel>();

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_FolderList_Result>("usp_HCP_Prod_GetOpaHistory_FolderList",
                new
                {
                    TaskInstanceId = taskInstanceId,

                }).ToList();
                folderList = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistory_FolderList_Result>, IEnumerable<Prod_FolderStructureModel>>(results.OrderBy(x=>x.FolderSortingNumber)).ToList();


                return folderList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public IEnumerable<OPAViewModel> GetProdReviewersTaskStatus(Guid taskInstanceId)
        {
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var taskList = new List<OPAViewModel>();

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId_Result>("usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId",
                new
                {
                    TaskInstanceId = taskInstanceId,

                }).ToList();
                taskList = Mapper.Map<IEnumerable<usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId_Result>, IEnumerable<OPAViewModel>>(results).ToList();


                return taskList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public IEnumerable<OPAViewModel> GetProdPendingRAI(Guid taskInstanceId)
        {
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var pendingRAIList = new List<OPAViewModel>();

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetPendingRAIByTaskInstanceId_Result>("usp_HCP_Prod_GetPendingRAIByTaskInstanceId",
                new
                {
                    TaskInstanceId = taskInstanceId,
                  
                }).ToList();
                pendingRAIList = Mapper.Map<IEnumerable<usp_HCP_Prod_GetPendingRAIByTaskInstanceId_Result>, IEnumerable<OPAViewModel>>(results).ToList();


                return pendingRAIList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys,Guid InstanceId)
        {
            try
            {
                var context = (HCP_task)this.Context;

                var results = (from c in context.TaskFiles
                             join o in context.TaskFile_FolderMapping on c.TaskFileId equals o.TaskFileId
                             where c.TaskInstanceId==InstanceId && folderkeys.Contains(o.FolderKey)
                             select c).ToList();
                //var results = context.TaskFile_FolderMapping.Where(x => folderkeys.Contains(x.FolderKey)).ToList();

                var result = Mapper.Map<IEnumerable<TaskFileModel>>(results);
                return result;
            }

            catch (Exception ex)
            {

            }
            return null;
            
            //try
            //{

            //    var context = (HCP_task)this.Context;

            //    var folderstructure = (from n in context.Prod_FolderStructure 
            //                           join tm in context.TaskFile_FolderMapping on n.FolderKey equals tm.FolderKey
            //                           join tf in context.TaskFiles on   tm.TaskFileId equals tf.TaskFileId
            //                           where tf.TaskInstanceId == taskInstanceId
            //                           select n).ToList();
            //    var result = Mapper.Map<IEnumerable<Prod_FolderStructureModel>>(folderstructure);
            //    return result;
            //}

            //catch (Exception ex)
            //{

            //}
            //return null;
        }
        public IEnumerable<DocumentTypeModel> GetMappedDocTypesbyFolder(int folderkey)
        {
            try
            {
                var context = (HCP_task)this.Context;

                // link query not giving exact result for some folders -- BUG 281 in TFS
                context.Database.CommandTimeout = 600;

                var results = context.Database.SqlQuerySimple<DocumentTypeModel>("usp_HCP_Prod_FileNameByFolderKeyForDropDown",
                new
                {
                    FolderKey = folderkey,
                }).ToList();


               // var DistinctItems = results.GroupBy(x => x.DocumentTypeId).Select(y => y.First());
                var result = Mapper.Map<IEnumerable<DocumentTypeModel>>(results);
                //return result.OrderBy(m => m.DocumentType);
                return result;
                #region old code
                //var results = (from c in context.DocumentTypes
                //               join f in context.Prod_FolderStructure on c.FolderKey equals f.FolderKey
                //               where folderkey == f.FolderKey
                //               select c).ToList().Union
                // (from c in context.DocumentTypes
                //  join sf in context.Prod_SubFolderStructure on c.FolderKey equals sf.ParentKey
                //  where folderkey == sf.FolderKey
                //  select c).ToList();
                //var DistinctItems = results.GroupBy(x => x.DocumentTypeId).Select(y => y.First());
                //}).ToList();
                //var DistinctItems = results.GroupBy(
                //p => p.DocumentTypeId,
                //p => p.DocumentType,
                //(key, g) => new { DocumentTypeId = key, DocumentType = g.ToList() });
                #endregion
            }

            catch (Exception ex)
            {

            }
            return null;

            
        }
    }
}
