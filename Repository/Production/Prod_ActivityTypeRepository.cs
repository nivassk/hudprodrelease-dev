﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_ActivityTypeRepository : BaseRepository<Prod_ActivityType>, IProd_ActivityTypeRepository
    {
        public Prod_ActivityTypeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public Prod_ActivityTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IList<ActivityTypeModel> GetAllActivityTypes()
        {
            var activityTypes = this.GetAll().ToList();
            return Mapper.Map<IList<Prod_ActivityType>, IList<ActivityTypeModel>>(activityTypes);
        }
    }
}
