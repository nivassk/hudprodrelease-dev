﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_SharePointAccountExecutivesRepository:BaseRepository<Prod_SharePointAccountExecutives>, IProd_SharePointAccountExecutivesRepository
    {
        public Prod_SharePointAccountExecutivesRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public Prod_SharePointAccountExecutivesRepository(): base(new UnitOfWork(DBSource.Live))
        {
        }

        public IList<Prod_SharePointAccountExecutivesViewModel> GetAllSharePointAEs()
        {
            var results = GetAll().ToList();
            return Mapper.Map<List<Prod_SharePointAccountExecutivesViewModel>>(results);
        }
    }
}
