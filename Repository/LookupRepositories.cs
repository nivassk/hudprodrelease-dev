﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System.Collections.Generic;
using System.Linq;

namespace HUDHealthcarePortal.Repository
{
    // put lookup tables here. static tables, such as roles, states etc
    // all should in the form of key value pair lists
    // will add cache feature for lookup table later
    public class RoleRepository : BaseRepository<webpages_Roles>
    {
        public RoleRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public RoleRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<KeyValuePair<int, string>> GetRoles()
        {
            // don't call ToList, it will cause linq to execute db call
            // usse IEnumberable to delay calls (so you can use results in other linq operations)
            return this.GetAll().Select(p => new KeyValuePair<int, string>(p.RoleId, p.RoleName));
        }
    }

    public class LenderRepository : BaseRepository<LenderInfo>
    {
        public LenderRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public LenderRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<KeyValuePair<int, string>> GetAllLenders()
        {
            return this.GetAll().Select(p => new KeyValuePair<int, string>(p.LenderID, p.Lender_Name));
        }
    }

    public class SecQuestionRepository : BaseLookupCache<SecurityQuestion>
    {
        public SecQuestionRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public SecQuestionRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        protected override string IdColName
        {
            get
            {
                return "SecurityQuestionID";
            }
        }

        protected override string ValColName
        {
            get
            {
                return "SecurityQuestionDescription";
            }
        }

        public IEnumerable<SecurityQuestionLookup> GetAllSecQuestions()
        {
            var entityQuestions = this.GetLookupList();
            return Mapper.Map<IEnumerable<SecurityQuestionLookup>>(entityQuestions);
        }
    }

    public class EmailTypeRepository : BaseLookupCache<HCP_EmailType>
    {
        public EmailTypeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public EmailTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        protected override string IdColName
        {
            get
            {
                return "EmailTypeId";
            }
        }

        protected override string ValColName
        {
            get
            {
                return "EmailTypeId";
            }
        }

        protected override string CdColName
        {
            get
            {
                return "EmailTypeCd";
            }
        }

        public IEnumerable<EmailTypeLookup> GetAllEmailTypes()
        {
            var entityQuestions = this.GetLookupList();
            return Mapper.Map<IEnumerable<EmailTypeLookup>>(entityQuestions);
        }
    }
}
