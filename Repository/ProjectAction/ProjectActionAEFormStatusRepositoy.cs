﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.ProjectAction;

namespace Repository.ProjectAction
{
    public class ProjectActionAEFormStatusRepositoy : BaseRepository<ProjectActionAEFormStatus>, IProjectActionAEFormStatusRepository 
    {
        public ProjectActionAEFormStatusRepositoy()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public ProjectActionAEFormStatusRepositoy(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public Dictionary<Guid, int?> GetProjectActionAEFormStatus(Guid projectActionFormId)
        {
            var context = (HCP_live) this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetProjectActionAEFormStatus, please pass in correct context in unit of work.");

            var projectActionStatus = (from n in context.ProjectActionAEFormStatus 
                                           where n.ProjectActionFormId == projectActionFormId
                                           select n);
            var projectActionAEFormStatuses = projectActionStatus.Where(item => item != null).ToDictionary(item => item.CheckListId, item => item.RequestStatus);
            return projectActionAEFormStatuses;
        }

        public bool IsApprovalStatusTrueForAllQuestions(Guid projectActionFormId)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetApprovalStatusForAllQuestions, please pass in correct context in unit of work.");

            var projectActionStatus = (from n in context.ProjectActionAEFormStatus
                                       where n.ProjectActionFormId == projectActionFormId
                                       select n);
            var totalCount = projectActionStatus.Count();
            var approvedCount = projectActionStatus.Count(p => p.RequestStatus == (int)StatusForProjectActionAEForm.A);
            if (totalCount == approvedCount) return true;
            return false;
        }

        public IList<ProjectActionAEFormStatus> GetProjectActionAEFormStatusList(Guid projectActionFormId)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetProjectActionAEFormStatusList, please pass in correct context in unit of work.");

            var projectActionStatus = (from n in context.ProjectActionAEFormStatus
                                       where n.ProjectActionFormId == projectActionFormId
                                       select n);

            return projectActionStatus.ToList();
        }

        public void UpdateAECheckListItem(ProjectActionAEFormStatus projectActionAEFormStatus)
        {
            this.Update(projectActionAEFormStatus);
        }
    }
}
