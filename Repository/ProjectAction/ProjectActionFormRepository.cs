﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.ProjectAction;
using Model;
using AutoMapper;
namespace Repository.ProjectAction
{
    public class ProjectActionFormRepository : BaseRepository<ProjectActionForm>, IProjectActionFormRepository 
    {
         public ProjectActionFormRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

         public ProjectActionFormRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

         public ProjectActionViewModel  GetProjectAction(Guid projectActionFormId)
         {
             var context = (HCP_live)this.Context;
             var projectActionForm = (from n in context.ProjectActionForm
                                      where n.ProjectActionFormId == projectActionFormId
                                      select n).FirstOrDefault();

             return Mapper.Map<ProjectActionForm, ProjectActionViewModel>(projectActionForm);
         }
        //Move the below two methods to projectAction Repository
         public IEnumerable<KeyValuePair<int, string>> GetProjectActionTypes()
         {
             var context = (HCP_live) this.Context;
            var  query = (from p in context.HCP_Project_Action
                 select  p).ToList();


            IEnumerable<KeyValuePair<int, string>> result = query.Select(p => new KeyValuePair<int, string>(p.ProjectActionID , p.ProjectActionName)).OrderBy(p => p.Value);
             return result;

         }

       public string GetProjectActionName(int projectActionId)
        {
            var context = (HCP_live)this.Context;
           return (from p in context.HCP_Project_Action
                   where p.ProjectActionID == projectActionId
               select p).FirstOrDefault().ProjectActionName;
        }

        public Guid SaveProjectActionRequestForm(ProjectActionViewModel projectActionViewModel)
        {
            var context = (HCP_live)this.Context;
            var projectActionForm = Mapper.Map<ProjectActionForm>(projectActionViewModel);
            this.InsertNew(projectActionForm);
            return projectActionForm.ProjectActionFormId;
        }

        public void UpdateProjectActionRequestForm(ProjectActionViewModel projectActionViewModel)
        {
            var context = (HCP_live)this.Context;          

            var projectActionForm = (from n in context.ProjectActionForm
                                     where n.ProjectActionFormId == projectActionViewModel.ProjectActionFormId
                                     select n).FirstOrDefault();

            projectActionForm.RequestStatus = projectActionViewModel.RequestStatus;
            projectActionForm.AEComments = projectActionViewModel.AEComments;
            projectActionForm.ModifiedOn = DateTime.UtcNow;
            projectActionForm.ModifiedBy = projectActionViewModel.ModifiedBy; 
            this.Update(projectActionForm);
        }

        public void UpdateTaskId(ProjectActionViewModel model)
        {
            var context = (HCP_live)this.Context;

            var projectActionForm = (from n in context.ProjectActionForm 
                                      where n.ProjectActionFormId  == model.ProjectActionFormId 
                                      select n).FirstOrDefault();

            projectActionForm.MyTaskId = model.MytaskId;
            this.Update(projectActionForm);
         }

        public ProjectActionViewModel GetProjectActionRequestId(Guid formId)
        {
            var context = (HCP_live)this.Context;
            var projectActionForm = (from n in context.ProjectActionForm
                                     where n.ProjectActionFormId == formId
                                     select n).FirstOrDefault();

            return Mapper.Map<ProjectActionViewModel>(projectActionForm);
        }

        public void InsertAEChecklistStatus(ProjectActionViewModel projectActionViewModel)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in InsertAEChecklistStaus, please pass in correct context in unit of work.");

            context.Database.ExecuteSqlCommandSimple("usp_HCP_InsertAEChecklistStatus", new
            {
                ProjectActionFormId = projectActionViewModel.ProjectActionFormId,
                ProjectActionTypeId = projectActionViewModel.ProjectActionTypeId,
                UserId = projectActionViewModel.ModifiedBy 
            });
           
        }

        public void UpdateAEChecklistStatus(ProjectActionViewModel projectActionViewModel, IProjectActionAEFormStatusRepository projectActionAeFormStatusRepository)
        {
            var context = (HCP_live)this.Context;
            var aeCheckList = projectActionAeFormStatusRepository.GetProjectActionAEFormStatusList(projectActionViewModel.ProjectActionFormId).ToList();
            var questions = projectActionViewModel.Questions.ToList();

            if (projectActionViewModel.Questions.Count > 0)
            {
                aeCheckList.ForEach(chk =>
                {
                    var question = questions.Find(
                        aq =>
                            aq.CheckListId == chk.CheckListId &&
                            projectActionViewModel.ProjectActionFormId == chk.ProjectActionFormId);
                    int? status = null;

                    if (question != null)
                    {
                        if (question.Status == "A") status = (int) StatusForProjectActionAEForm.A;
                        chk.ModifiedBy = projectActionViewModel.ModifiedBy;
                        chk.RequestStatus = status;
                        projectActionAeFormStatusRepository.UpdateAECheckListItem(chk);
                    }

                });
            }

        }

        public bool IsProjectActionApprovedOrDenied(Guid projectActionFormId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in InsertAEChecklistStaus, please pass in correct context in unit of work.");

            var results = (from n in context.ProjectActionForm
                where n.ProjectActionFormId == projectActionFormId
                select n).FirstOrDefault();
            if (results!= null)
            {
                if (results.RequestStatus == (int)RequestStatus.Approve || results.RequestStatus == (int)RequestStatus.Deny)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
