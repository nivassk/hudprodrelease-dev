﻿using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;

namespace Repository
{
    public class UserWorkloadProjectManagerRepository : BaseRepository<User_WLM_PM>, IUserWorkloadProjectManagerRepository
    {
        public UserWorkloadProjectManagerRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public UserWorkloadProjectManagerRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<User_WLM_PM> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public void AddNewUser(UserViewModel model)
        {
            this.InsertNew(Mapper.Map<User_WLM_PM>(model));
        }

    }
}
