﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUploadStatusDetailRepository
    {
        IEnumerable<UploadStatusDetailModel> GetUploadStatusDetailByLenderId(string username, string usertype,int lendId, int monthsInPeriod, int year);
    }
}
