﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace Repository.Interfaces.Production
{
    public interface IProd_SharePointAccountExecutivesRepository
    {
        IList<Prod_SharePointAccountExecutivesViewModel> GetAllSharePointAEs();
    }
}
