﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Repository.Interfaces.Production
{
   public interface IProd_RestfulWebApiReplaceRepository
    {
       RestfulWebApiResultModel ReplaceDocumentInfo(RestfulWebApiUpdateModel DocumentInfo, string token, HttpPostedFileBase file);
    }
}
