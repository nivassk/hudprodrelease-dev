﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using System.Web;

namespace Repository.Interfaces.Production
{
  public interface IProd_RestfulWebApiUploadRepository
    {
        
      RestfulWebApiResultModel AssetManagementUploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
      RestfulWebApiResultModel UploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
      RestfulWebApiResultModel uploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary);
      RestfulWebApiResultModel uploadCopiedFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary, string filename);
    }
}
