﻿using System;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IHUDManagerRepository
    {
        Tuple<HUDManagerModel, HUDManagerModel, string> GetHUDManagersByFhaNumber(string fhaNumber);
        HUDManagerModel GetAccountExecutiveByFHANumber(string fhaNumber);
    }
}
