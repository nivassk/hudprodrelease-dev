﻿using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUploadStatusRepository
    {
        System.Collections.Generic.IEnumerable<UploadStatusViewModel> GetUploadStatusByLenderId(int lendId);

        System.Collections.Generic.IEnumerable<UploadStatusViewModel> GetUploadStatusDetailByQuarter(string username, string usertype, int lenderId,
            int monthsInPeriod, int year);
    }
}
