﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using Model;

namespace Repository.Interfaces
{
   public  interface IPDRColumnRepository:IJoinable<PDRColumn>
    {
        int AddNewColumn(PDRColumnViewModel model);
        PDRColumnViewModel GetColumnByID(int id);
        void UpdateColumn(PDRColumnViewModel model);
        IEnumerable<PDRColumnViewModel> GetAllPdrColumns();
    }
}
