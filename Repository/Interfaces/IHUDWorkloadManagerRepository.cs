﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IHUDWorkloadManagerRepository
    {
        IEnumerable<KeyValuePair<int, string>> GetWorkloadManagers();
        UserViewModel GetWorkloadManagerDetail(int id);
        IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId);
        IList<HUDManagerModel> GetSubordinateISOUsByWLMId(string wlmId);
        IList<HUDManagerModel> GetRegisteredAEsByWLMId(string wlmId, IHUDAccountExecutiveRepository aeRepository);
    }
}
