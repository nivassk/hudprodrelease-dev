﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IAdhocReportRepository
    {
        IEnumerable<AdhocReportModel> GetAdhocReport(string wlmList,
                                                     string aeList,
                                                     DateTime minPeriodEnd,
                                                     DateTime maxPeriodEnd, 
                                                     string partialFhaNumber, 
                                                     string lenderList, 
                                                     int activeAllSelected);

        IEnumerable<KeyValuePair<int, string>> GetAEsbyWLMList(string wlmList);
        IEnumerable<KeyValuePair<int, string>> GetLendersbyWLMAEList(string wlmList, string aeList);

    }
}
