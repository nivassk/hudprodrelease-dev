﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IMultiLoanRepository
    {
        IEnumerable<MultiLoanModel> GetMultiLoans();
        IEnumerable<MultiLoanDetailModel> GetMultiLoansDetailByPropertyID(int propertyId);
    }
}
