﻿using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IAddressRepository : IJoinable<Address>
    {
        int AddNewAddress(AddressModel model);
        AddressModel GetAddressByID(int id);
        void UpdateAddress(AddressModel model);
        AddressModel GetAddressByUserName(string email);
        void updateAddressSP(AddressModel model);
    }
}
