﻿namespace HUDHealthcarePortal.Repository.Interfaces.AssetManagement
{
    using System;
    using System.Collections.Generic;
    using EntityObject.Entities.HCP_live;
    using HUDHealthcarePortal.Model.AssetManagement;
    using Repository.Interfaces;
    public interface INonCriticalRequestExtensionRepository
    {
        Guid SaveNonCriticalRequestExtension(NonCriticalRequestExtensionModel  model);

        void UpdateNonCriticalRequestExtension(NonCriticalRequestExtensionModel model);

        void UpdateTaskId(NonCriticalRequestExtensionModel model);

        NonCriticalRequestExtensionModel GetNCRExtensionFormById(Guid ncrId);
    }
}