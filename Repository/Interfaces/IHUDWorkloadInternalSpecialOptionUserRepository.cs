﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IHUDWorkloadInternalSpecialOptionUserRepository
    {
        void AddNewUser(UserViewModel model);
        void UpdateUser(UserViewModel model);
        int GetWorkloadManagerIdFromInternalSpecialOptionUserId(int isouId);
        string GetWorkloadManagerEmailFromInternalSpecialOptionUserId(int isouId);
        IList<HUDManagerModel> GetISOUIdsByWLMId(string wlmId);
        string GetISOUNameById(int isouId);
        string GetISOUEmailById(int isouId);
        List<string> GetISOUFhaNumbers(int isouId);
        List<int> GetNCREs(List<int> taskIds);
        List<int> GetNCREExts(List<int> taskIds);
    }
}
