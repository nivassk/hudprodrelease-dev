﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IDataUploadLiveRepository : IJoinable<Lender_DataUpload_Live>
    {
        System.Collections.Generic.IEnumerable<ExcelUploadView_Model> GetUploadedData(int monthsInPeriod, int year);
        ExcelUploadView_Model GetProjectDetailByLDI_ID(int ldiID);
        IEnumerable<DerivedUploadData> GetUploadedDataByScoreRange(decimal minVal, decimal maxVal);
        PaginateSortModel<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByScoreRangeForProjectReport(decimal minVal, decimal maxVal);
        IEnumerable<DerivedUploadData> GetUploadedDataByScoreRangeAndQuarter(decimal minVal, decimal maxVal,
           int monthsInPeriod, int year, ILenderFhaRepository lenderFhaRepository);
        IEnumerable<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByScoreRangeAndQuarterBySearch(
            decimal minVal, decimal maxVal, ICommentRepository commentRepo, int monthsInPeriod, int year);
        PaginateSortModel<ExcelUploadView_Model> GetUploadedDataByScoreRangeWithPageSort(decimal minVal, decimal maxVal,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum);
        PaginateSortModel<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByScoreRangeWithPageSort(decimal minVal, decimal maxVal,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentRepository commentRepo, ILenderFhaRepository lenderFhaRepository, int monthsInPeriod, int year);

        PaginateSortModel<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByScoreRangeWithPageSort(
            decimal minVal, decimal maxVal,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentRepository commentRepo,
            DateTime endDayOfQtr);
        void UpdateLenderPropertyInfo(int userId, int lenderId);
    }
}
