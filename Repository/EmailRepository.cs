﻿using System.Data.Entity;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using Repository.Interfaces;
using Repository;

namespace HUDHealthcarePortal.Repository
{
    public class EmailRepository : BaseRepository<HCP_Email>, IEmailRepository
    {
        public EmailRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public EmailRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void SaveEmail(EmailModel email)
        {
            var emailEntity = Mapper.Map<HCP_Email>(email);
            AttachNew(emailEntity);
        }

        public int SaveEmailAndGetID(EmailModel email)
        {
            var emailEntity = Mapper.Map<HCP_Email>(email);
            AttachNew(emailEntity);
            Context.SaveChanges();
            Context.Entry(emailEntity).Reload();

            return emailEntity.EmailId;
        }
    }
}