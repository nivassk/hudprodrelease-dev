﻿using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Collections.Generic;

namespace Repository
{
	public class HUDSignatureRepository : BaseRepository<HUDSignature>, IHUDSignatureRepository
	{
		public HUDSignatureRepository()
			: base(new UnitOfWork(DBSource.Live))
		{
		}
		public HUDSignatureRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		
		public int AddNewSignature(HUDSignatureModel pModel)
		{
			var sign = Mapper.Map<HUDSignature>(pModel);
			this.InsertNew(sign);
			
			UnitOfWork.Save();
			return sign.Id;
		}

		public HUDSignatureModel GetHUDSignatureByUserId(int pId)
		{
			//var model = this.GetByIDFast(pId);
			var objHUDSignature = this.Find(o => o.UserId == pId).FirstOrDefault();
			if (objHUDSignature != null)
			{
				HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel { Id = objHUDSignature.Id, UserId = objHUDSignature.UserId, Signature = objHUDSignature.Signature };

				//return Mapper.Map<HUDSignatureModel>(objHUDSignatureModel);
				return objHUDSignatureModel;
			}
			return new HUDSignatureModel();
		}

		public void UpdateHUDSignatureByUserId(HUDSignatureModel pModel)
		{
			if (pModel.UserId >0)
			{
				var objHUDSignature = this.Find(o => o.UserId == pModel.UserId).FirstOrDefault();
				//var sign = this.GetByIDFast(pModel.Id.Value);
				objHUDSignature.Signature = pModel.Signature;
				//Mapper.Map<HUDSignature>(sign);
				this.Update(objHUDSignature);
				UnitOfWork.Save();
			}
		}

		
		
		
	}
}
