﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class HUDWorkloadInternalSpecialOptionUserRepository : 
        BaseRepository<HUD_WorkloadManager_InternalSpecialOptionUser>, IHUDWorkloadInternalSpecialOptionUserRepository
    {
        public HUDWorkloadInternalSpecialOptionUserRepository() : base(new UnitOfWork(DBSource.Live))
        {
        }

        public HUDWorkloadInternalSpecialOptionUserRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public void AddNewUser(UserViewModel model)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetWorkloadManagerEmailFromInternalSpecialOptionUserId, please pass in correct context in unit of work.");
            HUD_WorkloadManager_InternalSpecialOptionUser isou = new HUD_WorkloadManager_InternalSpecialOptionUser();
            isou.HUD_WorkloadManagerId = model.SelectedWorkloadManagerId ?? default(int);
            isou.InternalSpecialOptionUserId = model.UserID;
            context.HUD_WorkloadManager_InternalSpecialOptionUser.Add(isou);
            context.SaveChanges();
        }

        public void UpdateUser(UserViewModel model)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetWorkloadManagerEmailFromInternalSpecialOptionUserId, please pass in correct context in unit of work.");
            var isou = (from i in context.HUD_WorkloadManager_InternalSpecialOptionUser
                        where i.InternalSpecialOptionUserId == model.UserID
                        select i).FirstOrDefault();
            if (isou != null)
            {
                context.HUD_WorkloadManager_InternalSpecialOptionUser.Remove(isou);
            }
            var isou2 = new HUD_WorkloadManager_InternalSpecialOptionUser();
            isou2.HUD_WorkloadManagerId = model.SelectedWorkloadManagerId ?? default(int);
            isou2.InternalSpecialOptionUserId = model.UserID;
            context.HUD_WorkloadManager_InternalSpecialOptionUser.Add(isou2);
            context.SaveChanges();
        }

        public IList<HUDManagerModel> GetISOUIdsByWLMId(string wlmId)
        {
            if (wlmId == null) { wlmId = string.Empty; }
            List<string> wlmTempArray = new List<string>(wlmId.Split(",".ToCharArray()));
            int[] wlmIntArray = new int[wlmTempArray.Count];
            for (int i = 0; i < wlmTempArray.Count; i++)
            {
                if (wlmTempArray[i] != "") wlmIntArray[i] = Convert.ToInt32(wlmTempArray[i]);
            }
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetISOUsByWLMId, please pass in correct context in unit of work.");
            var hudISOUIds = (from w in context.HUD_WorkLoad_Manager
                              join i in context.HUD_WorkloadManager_InternalSpecialOptionUser on w.HUD_WorkLoad_Manager_ID equals i.HUD_WorkloadManagerId
                              join a in context.HCP_Authentication on i.InternalSpecialOptionUserId equals a.UserID
                              where wlmIntArray.Contains(w.HUD_WorkLoad_Manager_ID)
                              select new { Manager_Name = a.FirstName + " " + a.LastName, ModifiedOn = a.ModifiedOn,
                                  Deleted_Ind = a.Deleted_Ind, ISOU_ID = i.InternalSpecialOptionUserId, AddressId = a.AddressID })
                              .ToList();
            var hudProjectMgrs = new List<HUDManagerModel>();
            foreach (var item in hudISOUIds)
            {
                HUDManagerModel isou = new HUDManagerModel();
                isou.Manager_ID = 0;
                isou.Manager_Name = item.Manager_Name;
                isou.AddressID = item.AddressId;
                isou.ModifiedOn = item.ModifiedOn;
                isou.ManagerType = HUDManagerType.InternalSpecialOptionUser;
                isou.ISOU_ID = item.ISOU_ID;
                hudProjectMgrs.Add(isou);
            }
            return hudProjectMgrs;
        }

        public int GetWorkloadManagerIdFromInternalSpecialOptionUserId(int isouId)
        {
            var wlm = this.Find(p => p.InternalSpecialOptionUserId == isouId).FirstOrDefault();
            if (wlm != null) return wlm.HUD_WorkloadManagerId;
            return 0;
        }

        public string GetWorkloadManagerEmailFromInternalSpecialOptionUserId(int isouId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetWorkloadManagerEmailFromInternalSpecialOptionUserId, please pass in correct context in unit of work.");
            var results = (from h in context.HUD_WorkloadManager_InternalSpecialOptionUser
                             join w in context.HUD_WorkLoad_Manager on h.HUD_WorkloadManagerId equals w.HUD_WorkLoad_Manager_ID
                             join a in context.Addresses on w.AddressID equals a.AddressID
                           where h.InternalSpecialOptionUserId == isouId
                           select a.Email).First();
            return results;
        }

        public string GetISOUNameById(int isouId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetISOUNameById, please pass in correct context in unit of work.");
            var name = (from i in context.HUD_WorkloadManager_InternalSpecialOptionUser 
                          join a in context.HCP_Authentication on i.InternalSpecialOptionUserId equals a.UserID
                        where a.UserID == isouId
                        select a.FirstName + " " + a.LastName).FirstOrDefault();
            return name;
        }

        public string GetISOUEmailById(int isouId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetISOUNameById, please pass in correct context in unit of work.");
            var email = (from i in context.HUD_WorkloadManager_InternalSpecialOptionUser
                        join a in context.HCP_Authentication on i.InternalSpecialOptionUserId equals a.UserID
                        where a.UserID == isouId
                        select a.UserName).FirstOrDefault();
            return email;
        }

        public List<string> GetISOUFhaNumbers(int isouId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetISOUFhaNumbers, please pass in correct context in unit of work.");
            var fhas = (from f in context.User_Lender
                        where f.User_ID == isouId
                        select f.FHANumber).ToList<string>();
            return fhas;
        }

        public List<int> GetNCREs(List<int> taskIds)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetNCREs, please pass in correct context in unit of work.");
            int[] tasks = taskIds.ToArray();
            var ncreTaskIds = (from f in context.NonCriticalRepairsRequest
                               where tasks.Contains(f.TaskId ?? default(int))
                               select f.TaskId ?? default(int)).ToList<int>();
            return ncreTaskIds;
        }

        public List<int> GetNCREExts(List<int> taskIds)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetNCREs, please pass in correct context in unit of work.");
            int[] tasks = taskIds.ToArray();
            var ncreTaskIds = (from f in context.NonCriticalRequestExtension
                               where tasks.Contains(f.TaskId ?? default(int))
                               select f.TaskId ?? default(int)).ToList<int>();
            return ncreTaskIds;
        }
    }
}
