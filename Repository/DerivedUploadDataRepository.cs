﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;


namespace HUDHealthcarePortal.Repository
{
    public class DerivedUploadDataRepository : DerivedEntityRepoBase<DerivedUploadData>
    {
        public DerivedUploadDataRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public DerivedUploadDataRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        protected override string SqlQueryPageSort
        {
            get 
            {
                return ";WITH tblDerived AS" +
                        "(" +
                        "SELECT c.*,  " +
                            "CASE WHEN e.FHANumber IS NULL THEN 0" +
                            "ELSE 1 " +
                            "END AS [HasComment]" +
                        "FROM ( " +
                            "SELECT b.* " +
                                "FROM ( " +
                                    "SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS [ROW_NUMBER], " +
                                    "a.* " +
                                    "FROM [Lender_DataUpload_Live] a" +
                            ") b" +
                        ") c" +
                        "LEFT JOIN [Comment] e ON c.FHANumber = e.FHANumber" +
                        ")" +

                        ",tblCnt AS" +
                        "(" +
                        "SELECT COUNT(*) RowCnt FROM tblDerived" +
                        ")" +

                        "SELECT * FROM tblDerived, tblCnt" +
                        "WHERE [ROW_NUMBER] BETWEEN {1} AND {2}" +
                        "ORDER BY [ROW_NUMBER] " + "{3}";
            }
        }
    }
}
