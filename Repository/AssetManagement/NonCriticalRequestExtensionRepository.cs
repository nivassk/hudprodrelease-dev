﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository.AssetManagement
{
    public class NonCriticalRequestExtensionRepository : BaseRepository<NonCriticalRequestExtension>, INonCriticalRequestExtensionRepository
    {
         public NonCriticalRequestExtensionRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// insert non critical request on submit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Guid  SaveNonCriticalRequestExtension(NonCriticalRequestExtensionModel model)
        {
            var nonCriticalRepairsRequest = Mapper.Map<NonCriticalRequestExtension>(model);
            this.InsertNew(nonCriticalRepairsRequest);
            return nonCriticalRepairsRequest.NonCriticalRequestExtensionId;
        }

        /// <summary>
        /// updated the existing Non critical request
        /// </summary>
        /// <param name="model"></param>
        public void UpdateNonCriticalRequestExtension(NonCriticalRequestExtensionModel model)
        {
            var context = (HCP_live)this.Context;
            var nonCriticalExtensionRequest = (from n in context.NonCriticalRequestExtension  
                                        where n.NonCriticalRequestExtensionId  == model.NonCriticalRequestExtensionId 
                                        select n).FirstOrDefault();
            nonCriticalExtensionRequest.ModifiedBy = model.ModifiedBy;
            nonCriticalExtensionRequest.ModifiedOn = model.ModifiedOn;
            nonCriticalExtensionRequest.ExtensionRequestStatus  = model.ExtensionRequestStatus;
            nonCriticalExtensionRequest.ExtensionPeriod  = model.ExtensionPeriod;
            nonCriticalExtensionRequest.Comments = model.Comments;
            nonCriticalExtensionRequest.TaskId = model.TaskId;
            if (model.ExtensionRequestStatus == (int) RequestExtensionStatus.Approve)
            {
                var noncriticalProjectInfo = (from n in context.NonCritical_ProjectInfo
                    where
                        n.FHANumber == model.FHANumber && n.LenderID == model.LenderID &&
                        n.PropertyID == model.PropertyID
                    select n).FirstOrDefault();

                noncriticalProjectInfo.EndingDate = model.EndDate;
                noncriticalProjectInfo.ExtensionApprovalDate = model.ModifiedOn;
                noncriticalProjectInfo.ModifiedBy = model.ModifiedBy;
            }

           // Context.SaveChanges();
        }

        /// <summary>
        /// Updated the lastest TaskId, this would be useful to find the latest task details
        /// </summary>
        /// <param name="model"></param>
        public void  UpdateTaskId(NonCriticalRequestExtensionModel model)
        {
            var context = (HCP_live)this.Context;

            var nonCriticalExtensionRequest = (from n in context.NonCriticalRequestExtension
                                      where n.NonCriticalRequestExtensionId  == model.NonCriticalRequestExtensionId
                                      select n).FirstOrDefault();
            
             nonCriticalExtensionRequest.TaskId  = model.TaskId;
            this.Update(nonCriticalExtensionRequest); 
        }

        public NonCriticalRequestExtensionModel GetNCRExtensionFormById(Guid ncrId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNCRFormById, please pass in correct context in unit of work.");

            var result = (from n in context.NonCriticalRequestExtension 
                          where n.NonCriticalRequestExtensionId  == ncrId
                          select n).FirstOrDefault();

            return Mapper.Map<NonCriticalRequestExtension, NonCriticalRequestExtensionModel>(result);
        }
    }
    }
 