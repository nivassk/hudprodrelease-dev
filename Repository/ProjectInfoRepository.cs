﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Runtime.ConstrainedExecution;
using System.Security.Policy;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Linq;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using Model;
using Repository.Interfaces;
using System;
using Repository;

namespace HUDHealthcarePortal.Repository
{
    /// <summary>
    /// this class is never meant to be called by upper level layers, so don't need to have an interface
    /// </summary>
    public class ProjectInfoRepository : BaseRepository<ProjectInfo>,IProjectInfoRepository
    {
        public ProjectInfoRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public ProjectInfoRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<ProjectInfo> DataToJoin
        {
            get { return DbQueryRoot; }
        }
        /// <summary>
        /// fha number is unique identifier for project info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ProjectInfo GetProjectInfoByFhaNumber(string fhaNumber)
        {
            var projectInfo = new ProjectInfo();

            try
            {
                projectInfo = this.Find(p => p.FHANumber.Equals(fhaNumber)).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return projectInfo;
        }

        public IEnumerable<ProjectInfoModel> GetProjectInfoListAssociatedToWorkloadManager(int workloadManagerId)
        {
            var projectInfoList = this.Find(p => p.HUD_WorkLoad_Manager_ID == workloadManagerId).ToList();
            return Mapper.Map<List<ProjectInfo>, List<ProjectInfoModel>>(projectInfoList);
        }

        public IEnumerable<ProjectInfoModel> GetProjectInfoListAssociatedToAccountExecutive(int accountExecutiveId)
        {
            var projectInfoList = this.Find(p => p.HUD_Project_Manager_ID == accountExecutiveId).ToList();
            return Mapper.Map<List<ProjectInfo>, List<ProjectInfoModel>>(projectInfoList);
        }
        
        
        public PropertyInfoModel GetPropertyInfo(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            var results = new PropertyInfoModel();
            try
            {
                //     results = (from p in GetAll()
                //                   join lf in context.Lender_FHANumber on p.FHANumber equals lf.FHANumber
                //                   join a in context.Addresses on p.Property_AddressID equals a.AddressID into pa
                //                   from x in pa.DefaultIfEmpty()
                //                   where p.LenderID == lf.LenderID
                //                   && lf.FHA_EndDate == null && p.FHANumber == fhaNumber
                //                   orderby p.ModifiedOn descending
                //                   select new PropertyInfoModel()
                //                   {
                //                       PropertyId = p.PropertyID,
                //                       PropertyName = p.ProjectName,
                //                       ReacScore = p.Reac_Last_Inspection_Score,
                //                       TroubledCode = p.Troubled_Code,
                //                       ActiveDecCaseInd = p.Is_Active_Dec_Case_Ind,
                //                       StreetAddress = x != null ? x.AddressLine1 : string.Empty,
                //                       City = x != null ? x.City : string.Empty,
                //                       State = x != null ? x.StateCode : string.Empty,
                //                       Zipcode = x != null ? x.ZIP : string.Empty,
                //                       InitialEndorsementDate = p.Initial_Endorsement_Date.HasValue ? p.Initial_Endorsement_Date.Value.ToShortDateString() : string.Empty,
                //                       AddressId = x != null ? x.AddressID : 0
                //                   }).Distinct().First();
                results = context.Database.SqlQuerySimple<PropertyInfoModel>("usp_HCP_GetPropertyInfo",
                   new
                   {
                       fhaNumber = fhaNumber
                   }).Distinct().FirstOrDefault();
            }
            catch (Exception ex)
            {
                
            }

            return results;
        }

       public int GetAeUserIdByFhaNumber(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            IList<int> result = new List<int>();

            try
            {
                result = (from pf in GetAll()
                          join pm in context.HUD_Project_Manager on pf.HUD_Project_Manager_ID equals pm.HUD_Project_Manager_ID
                          join uwp in context.User_WLM_PM on pm.HUD_Project_Manager_ID equals uwp.HUD_Project_Manager_ID
                          where pf.FHANumber == fhaNumber
                          select uwp.UserID).ToList();
                
            }
            catch (Exception ex)
            {

            }
            var enumerable = result as IList<int> ?? result.ToList();
            return enumerable.Any()?enumerable.Last():0;
        }

        public string GetAeEmailByFhaNumber(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            string result = null;
            try
            {
                result = context.Database.SqlQuerySimple<string>("usp_HCP_GetAeEmailByFhaNumber",
                   new
                   {
                       fhaNumber = fhaNumber
                   }).Distinct().FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return result;
        }


        public PropertyInfoModel GetProdPropertyInfo(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            var results = new PropertyInfoModel();
            try
            {
                results = (from lfha in context.Prod_FHANumberRequest
                           join pt in context.Prod_ProjectType on lfha.ProjectTypeId equals pt.ProjectTypeId
                           join a in context.Addresses on lfha.PropertyAddressId equals a.AddressID into pa
                           from x in pa.DefaultIfEmpty()
                           where lfha.FHANumber == fhaNumber
                           select new PropertyInfoModel()
                           {
                               PropertyId = lfha.PropertyId,
                               PropertyName = lfha.ProjectName,
                               StreetAddress = x != null ? x.AddressLine1 : string.Empty,
                               City = x != null ? x.City : string.Empty,
                               State = x != null ? x.StateCode : string.Empty,
                               Zipcode = x != null ? x.ZIP : string.Empty,
                               ProjectType = pt != null ? pt.ProjectTypeName : string.Empty,
                               ProjectTypeID = pt.ProjectTypeId,
                               IsAddressChange = lfha.IsAddressCorrect

                           }).Distinct().First();

                //////////////////
                results = (from PI in context.ProjectInfoes
                            
                           
                           where PI.FHANumber == fhaNumber
                           select new PropertyInfoModel()
                           {
                               PropertyId = PI.PropertyID,
                               PropertyName = PI.ProjectName                       

                           }).Distinct().First();

                /////////////////
            }
            catch (Exception ex)
            {

               
            }

            return results;
        }

        public PropertyInfoModel GetAssetManagementPropertyInfo(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            var results = new PropertyInfoModel();
            try
            {            
                results = (from PI in context.ProjectInfoes where PI.FHANumber == fhaNumber
                           select new PropertyInfoModel()
                           {
                               PropertyId = PI.PropertyID,
                               PropertyName = PI.ProjectName
                           }).Distinct().First();
            }
            catch (Exception ex)
            {
            }
            return results;
        }

        public AdditionalPropertyInfo GetPropertyInfoWithAEandWLM(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            var results = new AdditionalPropertyInfo();
            try
            {
                results = (from p in GetAll()
                           join lf in context.Lender_FHANumber on p.FHANumber equals lf.FHANumber
                           join ae in context.HUD_Project_Manager on p.HUD_Project_Manager_ID equals ae.HUD_Project_Manager_ID
                           join wlm in context.HUD_WorkLoad_Manager on p.HUD_WorkLoad_Manager_ID equals wlm.HUD_WorkLoad_Manager_ID
                           join a in context.Addresses on p.Property_AddressID equals a.AddressID into pa
                           from x in pa.DefaultIfEmpty()
                           where p.LenderID == lf.LenderID
                           && lf.FHA_EndDate == null && p.FHANumber == fhaNumber
                           orderby p.ModifiedOn descending
                           select new AdditionalPropertyInfo()
                           {
                               PropertyId = p.PropertyID,
                               PropertyName = p.ProjectName,
                               ReacScore = p.Reac_Last_Inspection_Score,
                               TroubledCode = p.Troubled_Code,
                               ActiveDecCaseInd = p.Is_Active_Dec_Case_Ind,
                               StreetAddress = x != null ? x.AddressLine1 : string.Empty,
                               City = x != null ? x.City : string.Empty,
                               State = x != null ? x.StateCode : string.Empty,
                               Zipcode = x != null ? x.ZIP : string.Empty,
                               InitialEndorsementDate = p.Initial_Endorsement_Date.HasValue ? p.Initial_Endorsement_Date.Value.ToShortDateString() : string.Empty,
                               AddressId = x != null ? x.AddressID : 0,
                               LenderOrg = p.Servicing_Mortgagee_Name,
                               LenderID = p.LenderID ?? 0,
                               AssignedAeName = ae.HUD_Project_Manager_Name,
                               AssignedWLMName = wlm.HUD_WorkLoad_Manager_Name
                           }).Distinct().First();
            }
            catch (Exception ex)
            {

            }

            return results;
        }
    }
}
