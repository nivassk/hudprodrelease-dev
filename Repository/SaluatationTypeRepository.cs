﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class SaluatationTypeRepository : BaseRepository<SaluatationType>, ISaluatationTypeRepository
	{
		public SaluatationTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public SaluatationTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<SaluatationType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<SaluatationTypeModel> GetSaluatationTypes()
		{
			var saluatationTypes = this.GetAll().ToList();			
			return Mapper.Map<List<SaluatationTypeModel>>(saluatationTypes);
		}
		public string GetSaluatationTypeName(int pId)
		{
			//var d = this.GetByID(pId);
			SaluatationType objSaluatationType = this.GetByID(pId);
			if(objSaluatationType!= null)
				return objSaluatationType.Saluatation;
			return null;
		}

	}
}



