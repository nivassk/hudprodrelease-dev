﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Core;
using Repository;
using Repository.Interfaces;
using Model;

namespace HUDHealthcarePortal.Repository
{

    public class AdminManagerUsersRepository : BaseRepository<usp_HCP_Get_Admin_ManageUsers_View_Result>, IAdminManagerUsersRepository
    {
        public AdminManagerUsersRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public AdminManagerUsersRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<UserViewModel> GetDataAdminManageUser(string searchText, int page, string sort, SqlOrderByDirecton sortdir)
        {
           var  userModel = new UserModel();

            var context = this.Context as HCP_live;
            var addressRepo = new AddressRepository(this.UnitOfWork);
            var titleRepo = new ReviewerTitlesRepository(this.UnitOfWork);
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_Admin_ManageUsers_View_Result>("usp_HCP_Get_Admin_ManageUsers_View").ToList();

            userModel.USerReportGridModel  = new PaginateSortModel<UserViewModel>();
            userModel.USerReportGridModel.Entities = Mapper.Map<IEnumerable<usp_HCP_Get_Admin_ManageUsers_View_Result>, IEnumerable<UserViewModel>>(results).ToList();
            userModel.USerReportGridModel.TotalRows = userModel.USerReportGridModel.Entities.Count();
            userModel.USerReportGridModel.PageSize = 10;
            userModel.UserReportGridlList = userModel.USerReportGridModel.Entities.ToList();

            if (page == 0)
            {
                userModel.USerReportGridModel.PageSize = userModel.USerReportGridModel.TotalRows;
            }

            userModel.USerReportGridModel = PaginateSort.SortAndPaginate(userModel.USerReportGridModel.Entities.AsQueryable(), sort, sortdir, userModel.USerReportGridModel.PageSize, page);


            
           // var users =  Mapper.Map<IEnumerable<UserViewModel>>(results);
            foreach (var item in userModel.UserReportGridlList)
            {
                var rm = new ReviewerTitlesModel();
                if(item.TitleId!=null)
                {
                    rm= titleRepo.GetReviewerTitle(item.TitleId.Value);
                }
                item.AddressModel = item.AddressID.HasValue ? addressRepo.GetAddressByID(item.AddressID.Value) : new AddressModel();
                item.AddressModel.Title = rm.Reviewer_Name;
                if (!string.IsNullOrEmpty(item.SelectedTimezone))
                {
                    var enumTimezone = EnumType.Parse<HUDTimeZone>(item.SelectedTimezone);
                    item.SelectedTimezone = EnumType.GetEnumDescription(enumTimezone);                    
                }
            }


           return String.IsNullOrEmpty(searchText) ? userModel.UserReportGridlList : userModel.UserReportGridlList.Where(p => (p.UserName != null && p.UserName.ToLower().Contains(searchText.ToLower())) ||
          (p.FirstName != null && p.FirstName.ToLower().Contains(searchText.ToLower())) ||
          (p.MiddleName != null && p.MiddleName.ToLower().Contains(searchText.ToLower())) ||
          (p.LastName != null && p.LastName.ToLower().Contains(searchText.ToLower())) ||
          (p.Title != null && p.Title.ToLower().Contains(searchText.ToLower())) ||
          (p.Organization != null && p.Organization.ToLower().Contains(searchText.ToLower())) ||
          (p.LenderName != null && p.LenderName.ToLower().Contains(searchText.ToLower())) ||
          (p.PhoneNumber != null && p.PhoneNumber.ToLower().Contains(searchText.ToLower())) ||
          (p.AddressLine1 != null && p.AddressLine1.ToLower().Contains(searchText.ToLower())) ||
          (p.City != null && p.City.ToLower().Contains(searchText.ToLower())) ||
          (p.ZipCode != null && p.ZipCode.ToLower().Contains(searchText.ToLower())) ||
          (p.EmailAddress != null && p.EmailAddress.ToLower().Contains(searchText.ToLower())));
        }
    }
}
