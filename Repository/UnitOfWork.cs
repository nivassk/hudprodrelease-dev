﻿using EntityObject.Entities.HCP_intermediate;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using System;
using System.Data.Entity;

namespace HUDHealthcarePortal.Repository
{
    //public class UnitOfWork : IUnitOfWork
    public class UnitOfWork : IDisposable
    {
        private DbContext _context;
        public UnitOfWork(DBSource dbSource)
        {
            if (dbSource == DBSource.Intermediate)
                _context = new HCP_intermediate();
            else if (dbSource == DBSource.Live)
                _context = new HCP_live();
            else
                _context = new HCP_task();
            // default lazy loading enabled is false, don't turn on
            // use explicit eager loading when necessary
            // lazy loading causes issue in serializing entities
            //this.Context.ContextOptions.LazyLoadingEnabled = true;
        }

        internal DbContext Context
        {
            get { return _context; }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void  Save()
        {
            _context.SaveChanges();
        }
    }
}
