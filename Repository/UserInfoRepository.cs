﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class UserInfoRepository : BaseRepository<usp_HCP_AuthenticateInitial_LogIn_Result>, IUserInfoRepository
    {
        public UserInfoRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UserInfoRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public UserInfoModel GetUserInfoByUsername(string sUserName)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetUserInfoByUsername, please pass in correct context in unit of work.");
            usp_HCP_AuthenticateInitial_LogIn_Result userResult =
                context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
                    "usp_HCP_AuthenticateInitial_LogIn", new {UserName = sUserName}).FirstOrDefault();
            return Mapper.Map<usp_HCP_AuthenticateInitial_LogIn_Result, UserInfoModel>(userResult);
        }

        public List<UserInfoModel> GetProductionUsers()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetUserInfoByUsername, please pass in correct context in unit of work.");
            List<usp_HCP_AuthenticateInitial_LogIn_Result> userResult =
                context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
                    "usp_HCP_GetProductionUsers").ToList();
            return Mapper.Map<List<usp_HCP_AuthenticateInitial_LogIn_Result>, List<UserInfoModel>>(userResult);
        }

		/// <summary>
		/// get wlm users
		/// </summary>
		/// <returns></returns>
		public List<UserInfoModel> GetProductionWLMUsers()
		{
			var context = this.Context as HCP_live;
			if (context == null)
				throw new InvalidCastException("context is not from db live in GetUserInfoByUsername, please pass in correct context in unit of work.");
			List<usp_HCP_AuthenticateInitial_LogIn_Result> userResult =
				context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
					"usp_HCP_GetProductionWLMUsers").ToList();
			return Mapper.Map<List<usp_HCP_AuthenticateInitial_LogIn_Result>, List<UserInfoModel>>(userResult);
		}
		public UserViewModel GetUserInfoById(int userId)
        {
            var context = this.Context as HCP_live;
            var result = (from a in context.HCP_Authentication
                          where a.UserID == userId
                          select (a)
                 ).FirstOrDefault();
            return Mapper.Map<UserViewModel>(result);
        }



        public List<UserInfoModel> GetUnAssignedProductionUsers()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetUnAssignedProductionUsers, please pass in correct context in unit of work.");
            List<usp_HCP_AuthenticateInitial_LogIn_Result> userResult =
                context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
                    "usp_HCP_GetUnAssignedProductionUsers").ToList();
            return Mapper.Map<List<usp_HCP_AuthenticateInitial_LogIn_Result>, List<UserInfoModel>>(userResult);
        }
        public List<UserInfoModel> GetUnAssignedExternalReviewers()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetUnAssignedProductionUsers, please pass in correct context in unit of work.");
            List<usp_HCP_AuthenticateInitial_LogIn_Result> userResult =
                context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
                    "usp_HCP_GetUnAssignedExternalReviewers").ToList();
            return Mapper.Map<List<usp_HCP_AuthenticateInitial_LogIn_Result>, List<UserInfoModel>>(userResult);
        }

        public List<UserInfoModel> GetALLInternalSpecialOptionUsers()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetUnAssignedProductionUsers, please pass in correct context in unit of work.");
            List<usp_HCP_AuthenticateInitial_LogIn_Result> userResult =
                context.Database.SqlQuerySimple<usp_HCP_AuthenticateInitial_LogIn_Result>(
                    "usp_HCP_GetAllInternalSpecialOptionUsers").ToList();
            return Mapper.Map<List<usp_HCP_AuthenticateInitial_LogIn_Result>, List<UserInfoModel>>(userResult);
        }
    }
}
