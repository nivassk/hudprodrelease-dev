﻿
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{

    public class ReportsRepository : BaseRepository<usp_HCP_Get_Admin_ManageUsers_View_Result>, IReportsRepository
    {
        public ReportsRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public ReportsRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<ReportsModel> GetMultipleLoanProperty()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_Multiple_Loans_PropertyID_Result>("usp_HCP_Get_Multiple_Loans_PropertyID").ToList();
            return Mapper.Map<IEnumerable<ReportsModel>>(results);
        }
    }
}
