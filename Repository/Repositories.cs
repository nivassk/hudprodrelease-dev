﻿using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;
using HUDHealthcarePortal.Model;
using System;
using AutoMapper;
namespace HUDHealthcarePortal.Repository
{
    // for simple repository classes, put in this file,
    // for repositories need override base class and add features
    // put in seperate files

    public class LenderInfoRepository : BaseRepository<LenderInfo>, ILenderInfoRepository
    {
        public LenderInfoRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public LenderInfoRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<KeyValuePair<int, string>> GetAllLenders()
        {
            var result = this.GetAll()
            .Where(p=> p.Deleted_Ind.GetValueOrDefault(false) == false)
                .Select(p => new KeyValuePair<int, string>(p.LenderID, p.Lender_Name)).OrderBy(p => p.Value);
            return result;
        }

        public IEnumerable<KeyValuePair<int, string>> GetLenderDetail(int lenderId)
        {
            return this.GetAll()
             .Where(p => (p.LenderID == lenderId))
                 .Select(p => new KeyValuePair<int, string>(p.LenderID, p.Lender_Name)).OrderBy(p => p.Value);
        }

        public IList<MenuRestrictionsViewmodel> GetMenuRestrictedLenders()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetMenuRestrictedLenders, please pass in correct context in unit of work.");

            var results = (from r in context.Menu_Restrictions
                          select r).ToList();
            //return Mapper.Map<Menu_Restrictions, MenuRestrictionsViewmodel>(results).;
             return Mapper.Map<List<Menu_Restrictions>, List<MenuRestrictionsViewmodel>>(results);
        }

        public string GetLenderName(int lenderId)
        {
            return this.Find(p => p.LenderID == lenderId).First().Lender_Name;
        }

    }

    public class ServicerInfoRepository : BaseRepository<ServicerInfo>, IServicerInfoRepository
    {
        public ServicerInfoRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public ServicerInfoRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<KeyValuePair<int, string>> GetAllServicers()
        {
            return this.GetAll().Select(p => new KeyValuePair<int, string>(p.ServicerID, p.ServicerName)).OrderBy(p => p.Value);
        }
    }

    public class StatesRepository : BaseRepository<State>, IStatesRepository
    {
        public StatesRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public StatesRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public List<KeyValuePair<string, string>> GetAllStates()
        {
            return this.GetAll().Select(p => new KeyValuePair<string, string>(p.StateCode, p.StateName)).ToList();
        }
    }

    public class UserInRoleRepository : BaseRepository<webpages_UsersInRoles>, IUserInRoleRepository
    {
        public UserInRoleRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UserInRoleRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// key = userid, value = roleid
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<int, int>> GetUsersInRoles()
        {
            return this.GetAll().Select(p => new KeyValuePair<int, int>(p.UserId, p.RoleId));
        }

        public bool IsUserProductionWlm(int userId)
        {
            var context = this.Context as HCP_live;
            var result = (from ur in context.webpages_UsersInRoles 
                            join rl in context.webpages_Roles on ur.RoleId equals rl.RoleId
                          where rl.RoleName == "ProductionWlm" && ur.UserId == userId//Enum.GetName(typeof(HUDRole),HUDRole.ProductionWlm).ToString()
                          select ur).FirstOrDefault();
            return result != null?true:false;

        }
    }
}
