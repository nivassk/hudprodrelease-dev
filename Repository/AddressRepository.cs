﻿using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Collections.Generic;

namespace Repository
{
    public class AddressRepository : BaseRepository<Address>, IAddressRepository
    {
        public AddressRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public AddressRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Address> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public int AddNewAddress(AddressModel model)
        {
            var address = Mapper.Map<Address>(model);
            this.InsertNew(address);
            UnitOfWork.Save();
            return address.AddressID;
        }

        public AddressModel GetAddressByID(int id)
        {
            var model = this.GetByIDFast(id);
            if (model != null)
            {
                model.AddressLine1 = RemoveZeros(model.AddressLine1);
                model.AddressLine2 = RemoveZeros(model.AddressLine2);
                model.City = RemoveZeros(model.City);
                model.Email = model.Email;
                model.Fax = RemoveZeros(model.Fax);
                model.Organization = RemoveZeros(model.Organization);
                model.PhoneAlternate = RemoveZeros(model.PhoneAlternate);
                model.PhonePrimary = RemoveZeros(model.PhonePrimary);
                model.StateCode = RemoveZeros(model.StateCode);
                model.Title = RemoveZeros(model.Title);
                model.ZIP = RemoveZeros(model.ZIP);
                model.ZIP4_Code = RemoveZeros(model.ZIP4_Code);


                return Mapper.Map<AddressModel>(model);
            }
            return new AddressModel();
        }

        public void UpdateAddress(AddressModel model)
        {
            if (model.AddressID != null)
            {
                var address = this.GetByIDFast(model.AddressID.Value);
                address.AddressLine1 =  model.AddressLine1;
                address.AddressLine2 = model.AddressLine2;
                address.City = model.City;
                address.Email = model.Email;
                address.Fax = model.Fax;
                address.Organization = model.Organization;
                address.PhoneAlternate = model.PhoneAlternate;
                address.PhonePrimary = model.PhonePrimary;
                address.StateCode = model.StateCode;
                address.Title = model.Title;
                address.ZIP = model.ZIP;
                address.ZIP4_Code = model.ZIP4_Code;
                //Mapper.Map<Address>(model);
                this.Update(address);
            }
        }

        private string RemoveZeros(string addString)
        {
            if (addString == null || addString == "0" || addString == "00") return string.Empty; 
            return addString;
        }
        public AddressModel GetAddressByUserName(string email)
        {
            var addressess = this.Find(m => m.Email == email).FirstOrDefault();
            return Mapper.Map<Address, AddressModel>(addressess);

            
        }
        public void updateAddressSP(AddressModel model)
        {
            if (model.AddressID != null)
            {
                var address = this.GetByIDFast(model.AddressID.Value);
                address.AddressLine1 = model.AddressLine1;               
                address.City = model.City;
                address.Email = model.Email;                                        
                address.StateCode = model.StateCode;              
                address.ZIP = model.ZIP;
                this.Update(address);
                UnitOfWork.Save();

    }
}
    }
}
