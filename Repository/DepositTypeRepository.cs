﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class DepositTypeRepository : BaseRepository<DepositType>, IDepositTypeRepository
	{
		public DepositTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public DepositTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<DepositType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<DepositTypeModel> GetDepositTypes()
		{
			var depositTypes = this.GetAll().ToList();
			return Mapper.Map<List<DepositTypeModel>>(depositTypes);
		}
		public string GetDepositTypeName(int pId)
		{
			DepositType objDepositType = this.GetByID(pId);
			if (objDepositType != null)
				return objDepositType.Deposit;
			return null;
		}

	}
}
