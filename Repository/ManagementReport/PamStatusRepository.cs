﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Core;
using Model;

namespace Repository.ManagementReport
{
   public partial class PamStatusRepository : BaseRepository<PamStatus>,IPamStatusRepository
   {

        public PamStatusRepository() : base(new UnitOfWork(DBSource.Live))
        {
            
        }

        public PamStatusRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            
        }

       public IEnumerable<PamStatusViewModel> GetAllPamStausesByModuleId(int moduleId)
       {
           var context = (HCP_live) this.Context;

           return (from p in context.PamStatus
               where p.ModuleId == moduleId
               select new PamStatusViewModel()
               {
                PamStatusId = p.PamStatusId,
                StatusDescription = p.StatusDescription,
                displayorder = p.displayorder
               }).OrderBy(s=>s.displayorder);
       }
   }

}
