﻿using System;
using Core;
using Core.Utilities;

namespace HUDHealthcarePortal.Core.Utilities
{
    public static class DateHelper
    {
        public static int DaysFromBeginningOfYearTill(DateTime datetime)
        {
            DateTime beginningOfYear = new DateTime(datetime.Year, 1, 1);
            return (datetime - beginningOfYear).Days + 1;
        }

        public static string GetCurrentQuarter()
        {
            //DateTime today = lenderFhaRepository.GetLatestQuarter();
            var today = DateTime.Now;
            var currentQtr = "";
            var month = today.Month;
            var year = today.Year;
            if (month <= 3)
                currentQtr = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("4")) + "/" + Convert.ToString(year - 1);
            else if (month <= 6)
                currentQtr = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("1")) + "/" + Convert.ToString(year);
            else if (month <= 9)
                currentQtr = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("2")) + "/" + Convert.ToString(year);
            else if (month <= 12)
                currentQtr = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("3")) + "/" + Convert.ToString(year);

            return currentQtr;
        }

        public static Tuple<int, int> GetValuesFromSelectedQuarter(string selectedQuarter)
        {
            if (selectedQuarter != null)
            {
                var result = selectedQuarter.Split(new[] {'/'});
                // var monthsInPeriod = EnumType.EnumToValue(EnumType.Parse<ReportingPeriod>(result[0]));
                return new Tuple<int, int>(int.Parse(result[0]), int.Parse(result[1]));
            }
            return null;
        }

        public static string GetSelectedQuarterText(string selectedQuarter)
        {
            if (selectedQuarter != null)
            {
                var result = selectedQuarter.Split(new[] { '/' });
                var monthsInPeriod = EnumType.EnumToValue(EnumType.Parse<ReportingPeriod>(result[0]));
                return monthsInPeriod + " " + result[1];
            }
            return null;
        }
    }
}