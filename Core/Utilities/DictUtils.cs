﻿using System.Collections.Generic;

namespace HUDHealthcarePortal.Core.Utilities
{
    public static class DictionaryUtilities
    {
        public static TItem GetOrCreate<TKey, TItem>(this Dictionary<TKey, TItem> dict, TKey key) where TItem : new()
        {
            if (!dict.ContainsKey(key))
            {
                dict[key] = new TItem();
            }
            return dict[key];
        }

        public static void SafeAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value, bool bOverwritePrevious)
        {
            if (dict.ContainsKey(key))
            {
                if (bOverwritePrevious)
                {
                    dict.Remove(key);
                    dict.Add(key, value);
                }
            }
            else
            {
                dict.Add(key, value);
            }
        }
    }
}
