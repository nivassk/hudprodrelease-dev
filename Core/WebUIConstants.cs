﻿namespace Core
{
    public static class WebUiConstants
    {
        public const string IntermediateConnectionName = "HCP_Intermediate";
        public const string LiveConnectionName = "HCP_Live";
        public const string Comma = ",";
        public const bool ShowPrintIcon = false;
    }
}