﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Xml;
using System.Data.SqlClient;
using Core;

namespace HUDHealthcarePortal.Core
{
    [Serializable]
    public class UserData
    {
        #region Private Data
        private DateTime _mDtCache = DateTime.UtcNow;
        private int _mIUserId;
        private int? _mILenderId;
        private int? _mIServicerId;
        private string _mSUserName;
        private string _mSUserRole;
        private string _mSFirstName;
        private string _mSLastName;
        private string _mSTimezone;
        private bool _mIsotherAe;
        private string[] _mRoles;
        #endregion Private Data

        #region Public Properties

        public bool IsotherAe
        {
            get { return _mIsotherAe; }
            protected set { _mIsotherAe = value; }
        }

        public int UserId
        {
            get { return _mIUserId; }
            protected set { _mIUserId = value; }
        }

        public int? LenderId
        {
            get { return _mILenderId; }
            protected set { _mILenderId = value; }
        }

        public int? ServicerId
        {
            get { return _mIServicerId; }
            protected set { _mIServicerId = value; }
        }

        public string UserName
        {
            get { return _mSUserName; }
            protected set { _mSUserName = value; }
        }

        public string FirstName
        {
            get { return _mSFirstName; }
            protected set { _mSFirstName = value; }
        }

        public string LastName
        {
            get { return _mSLastName; }
            protected set { _mSLastName = value; }
        }

        public string Timezone
        {
            get { return _mSTimezone; }
            protected set { _mSTimezone = value; }
        }
        
        public string UserRole
        {
            get { return _mSUserRole; }
            internal set { _mSUserRole = value; }
        }
        /// <summary>
        /// Roles are populated after user login in controller
        /// </summary>
        public string[] Roles
        {
            get { return _mRoles; }
            set { _mRoles = value; }
        }
        #endregion Public Properties

        public UserData(string logOn)
        {
            Initialize(logOn);
        }

        public void Initialize(string sUserName)
        {
            _mSUserName = sUserName;
            LoadUserData(sUserName);
        }

        /// <summary>
        /// check to see if the user data is expired from cache by comparing the cache time
        /// and the current time.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsExpireFromCache()
        {
            bool bIsExpire = false;
            int iTTLSeconds = 60; //default to 60 seconds
            if (ConfigurationManager.AppSettings["UserPrincipalDataTTL"] != null)
                iTTLSeconds = XmlConvert.ToInt32(ConfigurationManager.AppSettings["UserPrincipalDataTTL"]);

            DateTime dtNow = DateTime.UtcNow;
            TimeSpan timeSpan = dtNow.Subtract(_mDtCache);
            if (timeSpan.TotalSeconds > iTTLSeconds)
                bIsExpire = true;
            return bIsExpire;
        }

        #region Protected Methods

        protected static List<TVal> GetListByKey<TKey, TVal>(IDictionary<TKey, List<TVal>> dict, TKey key)
        {
            List<TVal> list;
            dict.TryGetValue(key, out list);
            if (list == null)
            {
                list = new List<TVal>();
                dict.Add(key, list);
            }
            return list;
        }

        protected static void AddToListByKey<TKey, TVal>(IDictionary<TKey, List<TVal>> dict, TKey key, TVal val)
        {
            var list = GetListByKey(dict, key);
            if (!list.Contains(val))
                list.Add(val);
        }

        #endregion Protected Methods

        #region Private Methods

        private void LoadUserData(string sUserName)
{
            Debugger.OutputDebugString("Load UserData from database. User=[{0}]", sUserName);
            InitState();
            if (sUserName == UserPrincipal.ANONYMOUS_NAME)
                return;

            try
            {
                // want to put UserData in Core but don't want core to reference entity framework context
                // instead of using  context to call sp, use sql client instead, so we have better layered structure
                string sConnection_HCP_Live_db = ConfigurationManager.ConnectionStrings[WebUiConstants.LiveConnectionName].ConnectionString.ToString();
                using (SqlConnection conn = new SqlConnection(sConnection_HCP_Live_db))
                {
                    SqlCommand command = new SqlCommand();
                    //int iUserID = UserPrincipal.Current.UserId;
                    conn.Open();

                    //command1.CommandType = CommandType.Text;
                    command.Connection = conn;
                    command.CommandText = "usp_HCP_AuthenticateInitial_LogIn";
                    command.CommandType = CommandType.StoredProcedure;

                    // Add the input parameter and set its properties.
                    SqlParameter parameter = new SqlParameter();

                    //Lender ID
                    parameter.ParameterName = "@UserName";
                    parameter.SqlDbType = SqlDbType.VarChar;
                    parameter.Direction = ParameterDirection.Input;

                    //Add the parameter to the Parameters collection - User Name.
                    command.Parameters.AddWithValue("UserName", sUserName);
                    //command.ExecuteNonQuery();

                    using (SqlDataReader rdr = command.ExecuteReader(CommandBehavior.CloseConnection)) 
                    {
                        rdr.Read();
                        _mIUserId = rdr.GetInt32(rdr.GetOrdinal("UserID"));
                        _mILenderId = GetNullableSqlInt(rdr, rdr.GetOrdinal("LenderID"));
                        _mIServicerId = GetNullableSqlInt(rdr, rdr.GetOrdinal("ServicerID"));
                        _mSUserName = GetNullableSqlString(rdr, rdr.GetOrdinal("UserName"));
                        _mSUserRole = GetNullableSqlString(rdr, rdr.GetOrdinal("RoleName"));
                        _mSFirstName = GetNullableSqlString(rdr, rdr.GetOrdinal("FirstName"));
                        _mSLastName = GetNullableSqlString(rdr, rdr.GetOrdinal("LastName"));
                        _mSTimezone = GetNullableSqlString(rdr, rdr.GetOrdinal("PreferredTimeZone"));
                        _mIsotherAe = GetNullableSqlBool(rdr, rdr.GetOrdinal("IsotherAe"));
                        rdr.Close(); 
                    } 
                    
                }

                //using (HCP_Live_dbEntities context = new HCP_Live_dbEntities())
                //{
                //    var user = context.usp_HCP_AuthenticateInitial_LogIn(sUserName).FirstOrDefault();
                //    m_iUserId = user.UserID;
                //    m_iLenderId = user.LenderID;
                //    m_iServicerId = user.ServicerID;
                //    m_sUserName = user.UserName;
                //    m_sUserRole = user.RoleName;
                //    m_sFirstName = user.FirstName;
                //    m_sLastName = user.LastName; 
                //}
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.StackTrace + string.Format(CultureInfo.InvariantCulture, "User name: {0} cannot load user data", sUserName));
            }
        }

        private void InitState()
        {
            //reset the cache loading time
            _mDtCache = DateTime.UtcNow;
            //m_iCompanyId = int.MinValue;
            _mIUserId = int.MinValue;
        }

        private static int? GetNullableSqlInt(SqlDataReader dataReader, int fieldIndex)
        {
            object value = dataReader.GetValue(fieldIndex);
            return value is DBNull ? (int?)null : dataReader.GetInt32(fieldIndex);
        }

        private static string GetNullableSqlString(SqlDataReader dataReader, int fieldIndex)
        {
            object value = dataReader.GetValue(fieldIndex);
            return value is DBNull ? null : dataReader.GetString(fieldIndex);
        }

        private static bool GetNullableSqlBool(SqlDataReader dataReader, int fieldIndex)
        {
            object value = dataReader.GetBoolean(fieldIndex);
            return value is DBNull ? false : dataReader.GetBoolean(fieldIndex);
        }
        
        #endregion Private Methods
    } 

    #region UserDataCache class

    internal class UserDataCache
    {
        private static readonly Hashtable m_oUserDataCache = new Hashtable();

        public static bool Contains(string sUserName)
        {
            return m_oUserDataCache.Contains(sUserName);
        }

        public static UserData GetUserData(string sUserName)
        {
            return m_oUserDataCache[sUserName] as UserData;
        }

        public static void AddUserData(string sUserName, UserData oUserData)
        {
            lock (m_oUserDataCache)
            {
                m_oUserDataCache[sUserName] = oUserData;
            }
        }

        public static void RemoveFromCache(string name)
        {
            lock (m_oUserDataCache)
            {
                m_oUserDataCache.Remove(name);
            }
        }
    }

    #endregion UserDataCache class
}
