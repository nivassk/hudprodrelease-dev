﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using Core;
using HUDHealthcarePortal.Model;
using Model;
using SpreadsheetGear;
using System.Text.RegularExpressions;
using BusinessService.Interfaces;
using Model.Production;
using BusinessService.Interfaces.Production;
using BusinessService.Production;

namespace HUDHealthcarePortal.Helpers
{
    public static class ExcelAndPrintHelper
    {
        
        public static IRange GetExcelWorksheetRange(IWorkbook workbook, string worksheetName)
        {
            var worksheet = workbook.Worksheets["Sheet1"];
            worksheet.Name = worksheetName;
            var cells = worksheet.Cells;
            return cells;
        }

        public static void GetHeaderValueForProjectReport(IRange cells, string reportType)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            if (reportType == "PortfolioSummary")
            {
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Portfolio Name";
                cells[row, col].ColumnWidth = 10;
                cells[row, col++].Value = "Count";
            }
            else
            {
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Project Name";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Lender Name";
            }

            //if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
            //{
            //    cells[row, col++].Value = "Score";
            //}
            //if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
            //{
            //    cells[row, col].ColumnWidth = 22;
            //    cells[row, col++].Value = "Operator/Owner";
            //    cells[row, col].ColumnWidth = 22;
            //    cells[row, col++].Value = "Financial Statement Type";
            //}
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Units in Facility";
            if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
            {
                cells[row, col].ColumnWidth = 15;
                cells[row, col++].Value = "Period Ending";
                cells[row, col].ColumnWidth = 15;
                cells[row, col++].Value = "Months In Period";
                cells[row, col].ColumnWidth = 15;
                cells[row, col++].Value = "FHA Number";
            }

            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Operating Cash";
            //cells[row, col].ColumnWidth = 15;
            //cells[row, col++].Value = "Investments";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "RFR Escrow Balance";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Accounts Receivable";
            //cells[row, col].ColumnWidth = 15;
            //cells[row, col++].Value = "Current Assets";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Current Liabilities";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Total Revenues";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Rent Lease Expense";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Depreciation Expense";
            //cells[row, col].ColumnWidth = 22;
            //cells[row, col++].Value = "Amortization Expense";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Total Operating Expenses";
            //cells[row, col].ColumnWidth = 15;
            //cells[row, col++].Value = "Net Income";
            //cells[row, col].ColumnWidth = 25;
            //cells[row, col].WrapText = true;
            //cells[row, col++].Value = "Reserve for Replacement Deposit";
            cells[row, col].ColumnWidth = 28;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "FHA Insured Principal & Interest Payment";
            //cells[row, col].ColumnWidth = 28;
            //cells[row, col++].Value = "FHA Insured Interest Payment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Mortgage Insurance Premium (MIP)";
            cells[row, col].ColumnWidth = 15;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Actual Number of Resident Days";
            //cells[row, col].WrapText = true;
            //cells[row, col].ColumnWidth = 28;
            //cells[row, col].WrapText = true;
            //cells[row, col++].Value = "Reserve for Replacement Balance per unit";
            cells[row, col].ColumnWidth = 25;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Debt Service Coverage Ratio";
            cells[row, col].ColumnWidth = 15;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Average Daily rate";
            cells[row, col].ColumnWidth = 22;
            cells[row, col++].Value = "NOI Ratio";
            //cells[row, col].ColumnWidth = 25;
            //cells[row, col++].Value = "Days in Accounts Receivable";
            //cells[row, col].ColumnWidth = 25;
            //cells[row, col++].Value = "Average Payment Period";
            //if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
            //{
            //    cells[row, col].ColumnWidth = 25;
            //    cells[row, col++].Value = "Debt Coverage Ratio Score";
            //    cells[row, col].ColumnWidth = 22;
            //    cells[row, col++].Value = "Working Capital Score";
            //    cells[row, col].ColumnWidth = 28;
            //    cells[row, col++].Value = "Days Cash on Hand Score";
            //    cells[row, col].ColumnWidth = 28;
            //    cells[row, col].WrapText = true;
            //    cells[row, col++].Value = "Days in Accounts Receivable Score";
            //    cells[row, col].ColumnWidth = 28;
            //    cells[row, col++].Value = "Average Payment Period Score";
            //}
            if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
            {
                cells[row, col].ColumnWidth = 22;
                cells[row, col++].Value = "Date Inserted";
            }
            if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
            {
                if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                {
                    cells["A1:N1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                    cells["A1:N1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                }
                else
                {
                    cells["A1:AE1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                    cells["A1:AE1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                }
            }
            else
            {
                if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                {
                    cells["A1:N1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                    cells["A1:N1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                }
                else
                {
                    cells["A1:AD1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                    cells["A1:AD1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                }
            }
        }

        public static int SetDataValuestoExcelForProjectReport(List<DerivedUploadData> uploadedDataFiltered, IRange cells, string reportType)
        {
            int row = 1;
            int col = 0;
            if (uploadedDataFiltered != null)
                foreach (var rec in uploadedDataFiltered)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    if (reportType == "PortfolioSummary")
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col++].Value = rec.Portfolio_Name;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.Portfolio_Count;
                    }
                    else
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col++].Value = rec.ProjectName;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col++].Value = rec.ServiceName;
                    }
                    //if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
                    //{
                    //    cells[row, col++].Value = rec.ScoreTotal;
                    //}

                    //cells[row, col++].Value = rec.UnitsInFacility;
                    if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                    {
                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.PeriodEnding;
                        cells[row, col++].Value = rec.MonthsInPeriod;
                        cells[row, col++].Value = rec.FHANumber;
                    }
                    cells[row, col].NumberFormat = "#,###0";
                    cells[row, col++].Value = rec.TotalRevenues;

                    cells[row, col].NumberFormat = "#,###0";
                    cells[row, col++].Value = rec.TotalExpenses;
                    cells[row, col].NumberFormat = "#,###0";
                    cells[row, col++].Value = rec.FHAInsuredPrincipalInterestPayment;
                    cells[row, col].NumberFormat = "#,###0";
                    cells[row, col++].Value = rec.MortgageInsurancePremium;
                    cells[row, col].NumberFormat = "#,###0";
                    cells[row, col++].Value = rec.ActualNumberOfResidentDays;
                    cells[row, col].NumberFormat = "#,##0.0";
                    // Math.Round(rec.DebtCoverageRatio2,1)

                    string.Format("{0:0}", rec.NOIRatio.ToString());
                    cells[row, col++].Value = string.Format("{0:0}", rec.DebtCoverageRatio2.ToString());
                    cells[row, col].NumberFormat = "#,##0.0";
                    cells[row, col++].Value = string.Format("{0:0}", rec.AverageDailyRateRatio.ToString()); ;
                    cells[row, col].NumberFormat = "#,##0.0";
                    cells[row, col++].Value = string.Format("{0:0}", rec.NOIRatio.ToString()); ;




                    if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                    {
                        if (rec.DataInserted != null)
                        {
                            cells[row, col].NumberFormat = "mm/dd/yyyy hh:mm:ss AM/PM";
                            cells[row, col].Value = TimezoneManager.GetPreferredTimeFromUtc(rec.DataInserted ?? DateTime.MinValue);
                        }
                        //col++ -- If adding columns
                    }
                    if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
                    {
                        if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                        {
                            cells["A" + (row + 1) + ":AN" + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                        }
                        else
                        {
                            cells["A" + (row + 1) + ":AE" + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                        }
                    }
                    else
                    {
                        if (reportType == "ProjectReport" || reportType == "PortfolioDetail")
                        {
                            cells["A" + (row + 1) + ":AN" + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                        }
                        else
                        {
                            cells["A" + (row + 1) + ":AD" + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                        }
                    }
                    row++;
                }
            return row;
        }

        public static void GetHeaderValuesForUploadStatus(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 35;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Total Expected";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Received";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Missing";
            cells["A1:D1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:D1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static int SetDataValuesForUploadStatus(List<UploadStatusViewModel> uploadedData,
            IRange cells)
        {
            int row = 1;
            int col = 0;
            if (uploadedData != null)
                foreach (var rec in uploadedData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.LenderName;
                    cells[row, col].WrapText = true;
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.TotalExpected;
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.Received;
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.Missing;
                    row++;
                }
            cells["A1:D" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void GetHeaderValuesForUploadStatusDetail(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 35;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Name";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "FHA Number";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Uploaded by";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Uploaded on";
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "Received";
            cells["A1:F1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:F1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static int SetDataValuesForUploadStatusDetail(List<UploadStatusDetailModel> uploadedData,
            IRange cells)
        {
            int row = 1;
            int col = 0;
            if (uploadedData != null)
                foreach (var rec in uploadedData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.Lender_Name;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.ProjectName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.FHANumber;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.UserName;
                    cells[row, col].NumberFormat = "mm/dd/yyyy hh:mm:ss AM/PM";
                    cells[row, col++].Value = rec.DateInserted.ToMyTimeFromUtc();
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.ReceivedYN;
                    row++;
                }
            cells["A1:F" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void GetHeaderValuesForMissingProjectDetail(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 35;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "FHA Number";

            cells["A1:C1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:C1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static int SetDataValuesForMissingProjectDetail(List<ReportViewModel> uploadedData,
            IRange cells)
        {
            int row = 1;
            int col = 0;
            if (uploadedData != null)
                foreach (var rec in uploadedData)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col].WrapText = true;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LenderName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.ProjectName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.FhaNumber;
                    row++;
                }
            cells["A1:C" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void GetHeaderValuesForMyUploadReport(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 35;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "Date Inserted";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "User Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "Lender ID";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Total FHA Number";
            cells["A1:E1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:E1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static int SetDataValuesForMyUploadReport(List<ReportExcelUpload_Model> uploadedData,
            IRange cells)
        {
            int row = 1;
            int col = 0;
            if (uploadedData != null)
                foreach (var rec in uploadedData)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col].WrapText = true;
                    cells[row, col].NumberFormat = "mm/dd/yyyy hh:mm:ss AM/PM";
                    cells[row, col++].Value = TimezoneManager.GetPreferredTimeFromUtc(rec.DataInserted);
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.UserName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;

                    cells[row, col++].Value = rec.Lender_Name;
                    cells[row, col].NumberFormat = "#";
                    cells[row, col++].Value = rec.LenderID;
                    cells[row, col].NumberFormat = "#";
                    cells[row, col++].Value = rec.Total_FHANumber;
                    row++;
                }
            cells["A1:E" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }


        public static void GetHeaderValuesForMissingProjectReport(IRange cells, string userType)
        {
            var row = 0;
            var col = 0;
            string colName = "D";
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            if (userType == "SuperUserWLM")
            {
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Workload Manager Name";
                colName = "E";
            }
            if (userType == "WLM")
            {
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Account Executive Name";
                colName = "E";
            }
            if (userType == "SuperUser" || userType == "SuperUserWLM" || userType == "WLM")
            {
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Number of Projects";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Expected Number of Reports";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Received Number of Reports";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Missing Number of Reports";
            }

            if (userType == "AE")
            {
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Property Name";
                cells[row, col].ColumnWidth = 15;
                cells[row, col++].Value = "FHA Number";
                cells[row, col].ColumnWidth = 20;
                cells[row, col++].Value = "Quarter Missing";
                cells[row, col].ColumnWidth = 20;
                cells[row, col++].Value = "Quarter End Date";
            }
            cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }


        public static void GetHeaderProjectDetailSuperUserHighLevel(ReportHeaderModel reportHeaderModel, IRange cells, string userType)
        {
            var row = 0;
            var col = 0;
            string colName = "C";
            string sheetName = cells.Worksheet.Name;
            var row2 = 0;
            var col2 = 0;
            var row3 = 0;
            var col3 = 1;
            var row4 = 0;
            var col4 = 0;
            var row5 = 0;
            var col5 = 1;
            var row6 = 5;
            var col6 = 1;

            if (sheetName == "Header Details")
            {
                cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row5, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;


                cells[row2, col2].ColumnWidth = 35;
                cells[row2, col2].CurrentRegion.HorizontalAlignment = HAlign.Left;
                cells[row2, col2].CurrentRegion.Font.Bold = true;
                cells[row2++, col2].Value = "Report Name";
                cells[row3, col3].ColumnWidth = 35;
                cells[row3, col3].CurrentRegion.Font.Bold = true;
                cells[row3++, col3].Value = EnumType.GetEnumDescription(reportHeaderModel.ReportType);

                cells[row2, col2].ColumnWidth = 35;
                cells[row2++, col2].Value = "Report Created By";
                cells[row3++, col3].Value = reportHeaderModel.CreatedBy.ToString();
                cells[row2, col2].ColumnWidth = 35;
                cells[row2++, col2].Value = "Report Generated as of";
                cells[row3++, col3].Value = DateTime.Today.AddDays(-1).ToShortDateString();
                cells[row2, col2].ColumnWidth = 35;
                cells[row2++, col2].Value = "Total Number of Projects";
                cells[row3++, col3].Value = reportHeaderModel.TotalProperties;
                cells[row2, col2].ColumnWidth = 50;
                cells[row2++, col2].Value = "Total Number of Projects with Exceptions";
                cells[row3++, col3].Value = reportHeaderModel.TotalPropertieswithException;
            }

            else
            {

                cells[row, col].HorizontalAlignment = HAlign.Center;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
                cells[row, col].EntireRow.RowHeight = 30;
                //System.Drawing.Color.Black;
                cells[row, col].EntireRow.Font.Bold = true;

                if (userType == "SuperUser" || userType == "SuperUserWLM" || userType == "WLM")
                {

                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "Work Load Manager";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Number of Reports";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Number of Reports with Exceptions";
                }


                cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

            }



        }



        public static void GetHeaderProjectDetail(ReportHeaderModel reportHeaderModel, IRange cells, string userType, string[] listPe, int[] listQtrint, string[] listPFNo, string[] lispfname, int[] listPIDint, string[] listFHA)
        {
            string sheetName = cells.Worksheet.Name;
            bool SelectAll = false;


            var row2 = 0;
            var col2 = 0;
            var row3 = 0;
            var col3 = 1;
            var row4 = 0;
            var col4 = 0;
            var row5 = 0;
            var col5 = 1;
            var row6 = 5;
            var col6 = 1;



            var row = 0;
            var col = 0;
            string colName = "AL";
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;

            if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
            {
                if (sheetName == "Header Details")
                {
                    cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row5, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                    /* cells[row4++, col4].CurrentRegion.Font.Bold = true;
                     cells[row4++, col4].CurrentRegion.Font.Bold = true;
                     cells[row4++, col4].CurrentRegion.Font.Bold = true;
                     cells[row4++, col4].CurrentRegion.Font.Bold = true;
                     cells[row4++, col4].CurrentRegion.Font.Bold = true; */

                    cells[row2, col2].ColumnWidth = 35;
                    cells[row2, col2].CurrentRegion.HorizontalAlignment = HAlign.Left;
                    cells[row2++, col2].Value = "Report Name";
                    cells[row3, col3].ColumnWidth = 35;
                    cells[row3++, col3].Value = EnumType.GetEnumDescription(reportHeaderModel.ReportType);
                    if (userType == "LenderAccountManager" || userType == "LenderAccountRepresentative" || userType == "BackupAccountManager")
                    {
                        cells[row2, col2].ColumnWidth = 35;
                        cells[row2++, col2].Value = "Lender Name";
                        cells[row3++, col3].Value = reportHeaderModel.LenderName;

                    }

                    else if (userType == "AccountExecutive")
                    {

                        cells[row2, col2].ColumnWidth = 35;
                        cells[row2++, col2].Value = "AccountExecutive";
                        cells[row3++, col3].Value = reportHeaderModel.HUD_Project_Manager_Name;

                    }
                    else if (userType == "WorkflowManager")
                    {

                        cells[row2, col2].ColumnWidth = 35;
                        cells[row2++, col2].Value = "Work Load Manager";
                        cells[row3++, col3].Value = reportHeaderModel.HUD_WorkLoad_Manager_Name;

                    }

                    cells[row2, col2].ColumnWidth = 35;
                    cells[row2++, col2].Value = "Report Created By";
                    cells[row3++, col3].Value = reportHeaderModel.CreatedBy.ToString();
                    cells[row2, col2].ColumnWidth = 35;
                    cells[row2++, col2].Value = "Report Generated as of";
                    cells[row3++, col3].Value = DateTime.Today.AddDays(-1);
                    cells[row2, col2].ColumnWidth = 35;
                    cells[row2++, col2].Value = "Total Number of Projects";
                    cells[row3++, col3].Value = reportHeaderModel.TotalProperties;
                    cells[row2, col2].ColumnWidth = 50;
                    cells[row2++, col2].Value = "Total Number of Projects with Exceptions";
                    cells[row3++, col3].Value = reportHeaderModel.TotalPropertieswithException;


                    cells[row2++, col2].Value = "Portfolio Number";
                    cells[row2++, col2].Value = "Portfolio Name";
                    cells[row2++, col2].Value = "Project ID";
                    cells[row2++, col2].Value = "FHA Number";
                    cells[row2++, col2].Value = "Period Ending";
                    cells[row2++, col2].Value = "Months in Period";


                    if (listPFNo.Length == 0 && lispfname.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0 && listPe.Length == 0 && listQtrint.Length == 0)
                    {
                        SelectAll = true;
                    }



                    if (listPFNo.Length == 0 && SelectAll == true)
                    {
                        cells[row3, col3].NumberFormat = "@";
                        cells[row3++, col3].Value = "All Selected";

                    }
                    else
                    {
                        string ListPFNo = string.Join(", ", listPFNo);
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        cells[row3, col3].NumberFormat = "@";
                        cells[row3++, col3].Value = ListPFNo;

                    }



                    if (lispfname.Length == 0 && SelectAll == true)
                    {
                        cells[row3++, col3].Value = "All Selected";

                    }
                    else
                    {

                        string Lispfname = string.Join(", ", lispfname);
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        cells[row3++, col3].Value = Lispfname;

                    }



                    if (listPIDint.Length == 0 && SelectAll == true)
                    {
                        cells[row3, col3].NumberFormat = "@";
                        cells[row3++, col3].Value = "All Selected";


                    }
                    else
                    {
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        cells[row3, col3].NumberFormat = "@";
                        string ListPIDint = string.Join(", ", listPIDint);
                        cells[row3++, col3].Value = ListPIDint;

                    }


                    if (listFHA.Length == 0 && SelectAll == true)
                    {
                        cells[row3++, col3].Value = "All Selected";

                    }
                    else
                    {
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        cells[row3, col3].NumberFormat = "@";
                        string ListFHA = string.Join(", ", listFHA);
                        cells[row3++, col3].Value = ListFHA;

                    }



                    if (listPe.Length == 0 && SelectAll == true)
                    {
                        cells[row3++, col3].Value = "All Selected";

                    }
                    else
                    {
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        string ListPe = string.Join(", ", listPe);
                        cells[row3++, col3].Value = ListPe;
                    }


                    if (listQtrint.Length == 0 && SelectAll == true)
                    {
                        cells[row3++, col3].Value = "All Selected";
                    }
                    else
                    {
                        cells[row3, col3].CurrentRegion.HorizontalAlignment = HAlign.Left;
                        string ListQtrint = string.Join(", ", listQtrint);
                        cells[row3++, col3].Value = ListQtrint;
                    }


                    cells["B:B"].EntireColumn.WrapText = true;

                }
                else if (sheetName == "ProjectDetail SuperUser" || sheetName == "ProjectDetail Lender" || sheetName == "ProjectDetail WLM" || sheetName == "ProjectDetail AE" || sheetName == "ProjectDetail ISU" || sheetName == "ProjectError ISU")
                {
                    cells[row, col].HorizontalAlignment = HAlign.Center;
                    cells[row, col].EntireRow.RowHeight = 30;
                    cells[row, col].EntireRow.Font.Bold = true;

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Project ID";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "FHA Number";

                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "Project Name";




                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Portfolio Number";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Portfolio Name";






                    //cells[row, col++].Value = "Lender Name";
                    //cells[row, col].ColumnWidth = 35;


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Credit Review";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Period Ending";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Months In Period";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Unpaid Principal Balance";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Total Revenue";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Total Operating Expenses";


                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "FHA Insured Principal & Interest Payment";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "MIP";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Actual Number of Resident Days";

                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "Servicer Name";

                    cells[row, col].ColumnWidth = 35;
                    cells["P:P"].CurrentRegion.HorizontalAlignment = HAlign.Center;
                    cells[row, col++].Value = "Workload Manager";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Account Executive";


                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "QuarterlyFHAPI";













                    //cells[row, col].ColumnWidth = 35;
                    //cells[row, col++].Value = "Units in Facility";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Cumulative";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly NOI";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Quarter/Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Percentage Change";





                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Rev";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Operating  Exp";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Rev Quarter/Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Rev Percentage Change";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Cumulative DSCR";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qrtly DSCR";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "DSCR Difference Per Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "DSCR Percentage Change";



                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qrtly Resident Days";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Cumulative Average Daily Rate (ADR)";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Average Daily Rate (ADR)";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "ADR Difference Per Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "ADR Percentage Change";





                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "QuaterlyMIP";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Exception Count";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Date Inserted";



                    cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                    cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                }
            }

        }

        public static void GetHeaderCurQrtr(IRange cells, string userType)
        {
            var row = 0;
            var col = 0;
            string colName = "F";
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;

            if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
            {
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "FHA Number";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Project Name";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Servicer Name";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Workload Manager";

                //cells[row, col].ColumnWidth = 35;
                //cells[row, col++].Value = "Units in Facility";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Period Ending";


                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Months In Period";


            }


            cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static void GetHeaderErrorDetail(ReportModel reportModel, IRange cells, string userType, string role)
        {
            var row = 0;
            var col = 0;

            //New Data

            string sheetName = cells.Worksheet.Name;
            var row2 = 0;
            var col2 = 0;
            var row3 = 0;
            var col3 = 1;
            var row4 = 0;
            var col4 = 0;
            var row5 = 0;
            var col5 = 1;
            var row6 = 5;
            var col6 = 1;


            if (sheetName == "Header Details")
            {
                cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row5++, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row5, col5].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;
                cells[row6++, col6].CurrentRegion.HorizontalAlignment = HAlign.Right;

                cells[row2, col2].ColumnWidth = 35;
                cells[row2, col2].CurrentRegion.HorizontalAlignment = HAlign.Left;
                cells[row2, col2].CurrentRegion.Font.Bold = true;
                cells[row2++, col2].Value = "Report Name";
                cells[row3, col3].ColumnWidth = 35;
                cells[row3, col3].CurrentRegion.Font.Bold = true;
                cells[row3++, col3].Value = EnumType.GetEnumDescription(reportModel.ReportHeaderModel.ReportType);

                cells[row2, col2].ColumnWidth = 35;
                cells[row2++, col2].Value = "Report Created By";
                cells[row3++, col3].Value = reportModel.ReportHeaderModel.CreatedBy.ToString();
                cells[row2, col2].ColumnWidth = 35;
                cells[row2++, col2].Value = "Report Generated as of";
                cells[row3++, col3].Value = DateTime.Today.AddDays(-1).ToShortDateString();
                if (role == HUDRole.WorkflowManager.ToString())
                {
                    cells[row2++, col2].Value = "Workload Manager";
                    cells[row3++, col3].Value = reportModel.ReportGridModelList[0].HUD_WorkLoad_Manager_Name;
                }

                else if (role == HUDRole.AccountExecutive.ToString())
                {
                    cells[row2++, col2].Value = "Account Executive";
                    cells[row3++, col3].Value = reportModel.ReportGridModelList[0].HUD_Project_Manager_Name;
                }

                else if (role == HUDRole.InternalSpecialOptionUser.ToString())
                {
                    cells[row2++, col2].Value = "Internal Special Option User";
                    cells[row3++, col3].Value = UserPrincipal.Current.UserName;
                }

                //cells[row2++, col2].Value = "Total Number of Projects";
                //cells[row3++, col3].Value = reportHeaderModel.TotalProperties;
                cells[row2, col2].ColumnWidth = 50;


                cells[row2++, col2].Value = "Total Number of Projects with Exceptions";
                cells[row3, col3].Font.Color = Color.FromArgb(255, 0, 0);
                cells[row3++, col3].Value = reportModel.ReportHeaderModel.TotalPropertieswithException;
            }

            else
            {
                string colName = "AL";
                cells[row, col].HorizontalAlignment = HAlign.Center;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
                cells[row, col].EntireRow.RowHeight = 30;
                //System.Drawing.Color.Black;
                cells[row, col].EntireRow.Font.Bold = true;

                if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "AccountExecutive" || userType == "InternalSpecialOptionUser")
                {
                    cells[row, col].HorizontalAlignment = HAlign.Center;
                    cells[row, col].EntireRow.RowHeight = 30;
                    cells[row, col].EntireRow.Font.Bold = true;

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Project ID";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "FHA Number";

                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "Project Name";



                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Portfolio Number";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Portfolio Name";



                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Credit Review";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Period Ending";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Months In Period";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Unpaid Principal Balance";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Total Revenue";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Total Operating Expenses";


                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "FHA Insured Principal & Interest Payment";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "MIP";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Actual Number of Resident Days";

                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "Servicer Name";

                    cells[row, col].ColumnWidth = 35;
                    cells["P:P"].CurrentRegion.HorizontalAlignment = HAlign.Center;
                    cells[row, col++].Value = "Workload Manager";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Account Executive";


                    cells[row, col].ColumnWidth = 40;
                    cells[row, col++].Value = "QuarterlyFHAPI";

                    //cells[row, col].ColumnWidth = 35;
                    //cells[row, col++].Value = "Units in Facility";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Cumulative";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly NOI";


                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Quarter/Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "NOI Percentage Change";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Rev";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Operating  Exp";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Rev Quarter/Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Rev Percentage Change";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Cumulative DSCR";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qrtly DSCR";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "DSCR Difference Per Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "DSCR Percentage Change";



                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qrtly Resident Days";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Cumulative Average Daily Rate (ADR)";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Qtrly Average Daily Rate (ADR)";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "ADR Difference Per Quarter";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "ADR Percentage Change";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "QuaterlyMIP";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Exception Count";

                    cells[row, col].ColumnWidth = 35;
                    cells[row, col++].Value = "Date Inserted";


                }


                cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

            }


        }

        public static void GetHeaderPTDetail(IRange cells, string userType)
        {
            var row = 0;
            var col = 0;
            string colName = "J";
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;


            if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
            {
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "FHA Number";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Project Name";
                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Servicer Name";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Workload Manager";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Account Executive";

                //cells[row, col].ColumnWidth = 35;
                //cells[row, col++].Value = "Units in Facility";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Period Ending";


                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Months In Period";


                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Qtrly NOI";

                cells[row, col].ColumnWidth = 35;
                cells[row, col++].Value = "Qrtly DSCR";


            }


            cells["A1:" + colName + "1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:" + colName + "1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static string StripHTML(string input = "")
        {
            if (!String.IsNullOrEmpty(input))
            {
                return Regex.Replace(input, "<.*?>", String.Empty);
            }
            else
            {
                return "";
            }

        }
        public static void ColorPink(IRange cells, int row, int col, ReportGridModel rec)
        {
            if (rec.Ismissing != null && rec.Ismissing == 1)
            {
                cells[row, col].Font.Color = Color.FromArgb(0, 0, 139);
                cells[row, col].Font.Italic = true;
            }
        }

        public static void HideColumns(IWorkbook workbook, IEnumerable<PDRUserPreferenceViewModel> userPreference)
        {

            // IEnumerable<PDRUserPreferenceViewModel> list;
            //= new IEnumerable<PDRUserPreferenceViewModel>();
            //List<int> list = new List<int>();
            //list.Add(1);
            //list.Add(2);

            foreach (var value in userPreference)
            {
                value.Visibility = true;
                switch (value.PDRColumnID)
                {

                    case 1:
                        workbook.Worksheets[0].Cells["D:D"].Columns.Hidden = value.Visibility;
                        break;
                    case 2:
                        workbook.Worksheets[0].Cells["E:E"].Columns.Hidden = value.Visibility;
                        break;
                    case 3:
                        workbook.Worksheets[0].Cells["F:F"].Columns.Hidden = value.Visibility;
                        break;
                    case 4:
                        workbook.Worksheets[0].Cells["G:G"].Columns.Hidden = value.Visibility;
                        break;
                    case 5:
                        workbook.Worksheets[0].Cells["H:H"].Columns.Hidden = value.Visibility;
                        break;
                    case 6:
                        workbook.Worksheets[0].Cells["I:I"].Columns.Hidden = value.Visibility;
                        break;
                    case 7:
                        workbook.Worksheets[0].Cells["J:J"].Columns.Hidden = value.Visibility;
                        break;
                    case 8:
                        workbook.Worksheets[0].Cells["K:K"].Columns.Hidden = value.Visibility;
                        break;
                    case 9:
                        workbook.Worksheets[0].Cells["L:L"].Columns.Hidden = value.Visibility;
                        break;
                    case 10:
                        workbook.Worksheets[0].Cells["M:M"].Columns.Hidden = value.Visibility;
                        break;
                    case 11:
                        workbook.Worksheets[0].Cells["N:N"].Columns.Hidden = value.Visibility;
                        break;
                    case 12:
                        workbook.Worksheets[0].Cells["O:O"].Columns.Hidden = value.Visibility;
                        break;
                    case 13:
                        workbook.Worksheets[0].Cells["P:P"].Columns.Hidden = value.Visibility;
                        break;
                    case 14:
                        workbook.Worksheets[0].Cells["Q:Q"].Columns.Hidden = value.Visibility;
                        break;
                    case 15:
                        workbook.Worksheets[0].Cells["R:R"].Columns.Hidden = value.Visibility;
                        break;
                    case 16:
                        workbook.Worksheets[0].Cells["S:S"].Columns.Hidden = value.Visibility;
                        break;
                    case 17:
                        workbook.Worksheets[0].Cells["T:T"].Columns.Hidden = value.Visibility;
                        break;
                    case 18:
                        workbook.Worksheets[0].Cells["U:U"].Columns.Hidden = value.Visibility;
                        break;
                    case 19:
                        workbook.Worksheets[0].Cells["V:V"].Columns.Hidden = value.Visibility;
                        break;
                    case 20:
                        workbook.Worksheets[0].Cells["W:W"].Columns.Hidden = value.Visibility;
                        break;
                    case 21:
                        workbook.Worksheets[0].Cells["X:X"].Columns.Hidden = value.Visibility;
                        break;
                    case 22:
                        workbook.Worksheets[0].Cells["Y:Y"].Columns.Hidden = value.Visibility;
                        break;
                    case 23:
                        workbook.Worksheets[0].Cells["Z:Z"].Columns.Hidden = value.Visibility;
                        break;
                    case 24:
                        workbook.Worksheets[0].Cells["AA:AA"].Columns.Hidden = value.Visibility;
                        break;
                    case 25:
                        workbook.Worksheets[0].Cells["AB:AB"].Columns.Hidden = value.Visibility;
                        break;
                    case 26:
                        workbook.Worksheets[0].Cells["AC:AC"].Columns.Hidden = value.Visibility;
                        break;
                    case 27:
                        workbook.Worksheets[0].Cells["AD:AD"].Columns.Hidden = value.Visibility;
                        break;
                    case 28:
                        workbook.Worksheets[0].Cells["AE:AE"].Columns.Hidden = value.Visibility;
                        break;
                    case 29:
                        workbook.Worksheets[0].Cells["AF:AF"].Columns.Hidden = value.Visibility;
                        break;
                    case 30:
                        workbook.Worksheets[0].Cells["AG:AG"].Columns.Hidden = value.Visibility;
                        break;
                    case 31:
                        workbook.Worksheets[0].Cells["AH:AH"].Columns.Hidden = value.Visibility;
                        break;
                    case 32:
                        workbook.Worksheets[0].Cells["AI:AI"].Columns.Hidden = value.Visibility;
                        break;
                    case 33:
                        workbook.Worksheets[0].Cells["AJ:AJ"].Columns.Hidden = value.Visibility;
                        break;
                    case 34:
                        workbook.Worksheets[0].Cells["AK:AK"].Columns.Hidden = value.Visibility;
                        break;
                    case 35:
                        workbook.Worksheets[0].Cells["AL:AL"].Columns.Hidden = value.Visibility;
                        break;
                }

            }


        }
        public static int SetDataValuesForProjectDetail(ReportModel reportModel,
          IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            // cells["A:A"].Columns.Hidden = false;


            string colName = "AL";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;

                    if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
                    {


                        cells[row, col].NumberFormat = "@";

                        //ColorPink(cells, row, col, rec);

                        //Property ID
                        //ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.PropertyID;

                        //FHA Number 

                        //ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.FhaNumber;

                        //Project Name
                        cells["C:C"].EntireColumn.WrapText = true;
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ProjectName;

                        //Portfolio Number
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.PortfolioNumber;

                        //Portfolio Name
                        //ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.PortfolioName;

                        //Credit Review
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.CreditReview;
                        //Period Ending 

                        //ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.PeriodEnding;

                        //Months in Period

                        //ColorPink(cells, row, col, rec);
                        if (rec.MonthsInPeriod != 0)
                        {
                            cells[row, col].NumberFormat = "#0";
                            cells[row, col++].Value = rec.MonthsInPeriod;
                        }
                        else
                        {
                            cells[row, col].NumberFormat = "#0";
                            cells[row, col++].Value = " ";

                        }

                        //UPB

                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.UnPaidBalance;

                        //Total Revenue
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.CumulativeRevenue;

                        //Total Expenses

                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.CumulativeExpenses;


                        //fhainsuredprincipalInterestpayment
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.fhainsuredprincipalInterestpayment;


                        //MIP
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.MIPCumulative;

                        //Actual number of ResidentDays
                        // ColorPink(cells, row, col, rec);
                        cells[row, col++].Value = rec.CumulativeResidentDays;

                        //Servicer Name
                        cells["N:N"].EntireColumn.WrapText = true;
                        //ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ServiceName;

                        //WLM
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_WorkLoad_Manager_Name;


                        //PM
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_Project_Manager_Name;

                        //QuarterlyFHAPI

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.QuarterlyFHAPI;


                        //NOICumulative

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.NOICumulative;


                        //QuaterlyNoi

                        if (row == 1)
                        {
                            cells[row, col].NumberFormat = "#0";


                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuaterlyNoi);
                        }
                        else
                        {
                            ColorPink(cells, row, col, rec);
                            cells[row, col].NumberFormat = "#,###0.00";
                            if (rec.QuaterlyNoi == "-")
                            {
                                cells[row, col].EntireColumn.HorizontalAlignment = HAlign.Center;
                            }

                            if (rec.QuaterlyNoi != null && rec.QuaterlyNoi.Contains("<font"))
                            {
                                cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                            }
                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuaterlyNoi);

                        }

                        //NOIQuaterbyQuater

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.NOIQuaterbyQuater == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        //cells["P:P"].EntireColumn.HorizontalAlignment = HAlign.Left;
                        if (rec.NOIQuaterbyQuater != null && rec.NOIQuaterbyQuater.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.NOIQuaterbyQuater);


                        //NOIPercentageChange
                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0.00%";
                        if (rec.NOIPercentageChange == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        if (rec.NOIPercentageChange != null && rec.NOIPercentageChange.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        if (rec.NOIPercentageChange == "0" || rec.NOIPercentageChange == "0.00%")
                        {
                            cells[row, col].NumberFormat = "0.00";
                        }

                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.NOIPercentageChange);


                        //QuaterlyOperatingRevenue
                        if (row == 1)
                        {
                            cells[row, col].NumberFormat = "#0";

                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuaterlyOperatingRevenue);
                        }
                        else
                        {
                            ColorPink(cells, row, col, rec);
                            cells[row, col].NumberFormat = "#,###0.00";
                            if (rec.QuaterlyOperatingRevenue == "-")
                            {
                                cells[row, col].HorizontalAlignment = HAlign.Center;
                            }
                            if (rec.QuaterlyOperatingRevenue != null && rec.QuaterlyOperatingRevenue.Contains("<font"))
                            {
                                cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                            }
                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuaterlyOperatingRevenue);
                        }


                        //QuarterlyOperatingExpense
                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.QuarterlyOperatingExpense != null && rec.QuarterlyOperatingExpense.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML((rec.QuarterlyOperatingExpense.ToString()));


                        //RevenueQuarterbyQuarter
                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.RevenueQuarterbyQuarter == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        if (rec.RevenueQuarterbyQuarter != null && rec.RevenueQuarterbyQuarter.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.RevenueQuarterbyQuarter);


                        //RevenuePercentageChange
                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0.00%";
                        if (rec.RevenuePercentageChange == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }


                        if (rec.RevenuePercentageChange != null && rec.RevenuePercentageChange.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        if (rec.RevenuePercentageChange == "0" || rec.RevenuePercentageChange == "0.00%")
                        {
                            cells[row, col].NumberFormat = "0.00";
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.RevenuePercentageChange);

                        //Cumulative DSCR
                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.DebtCoverageRatio2 != null && rec.DebtCoverageRatio2.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = rec.DebtCoverageRatio2;


                        //QuarterlyDSCR

                        if (row == 1)
                        {
                            cells[row, col].NumberFormat = "#0";

                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuarterlyDSCR);
                        }

                        else
                        {
                            ColorPink(cells, row, col, rec);
                            cells[row, col].NumberFormat = "#,###0.00";
                            if (rec.QuarterlyDSCR == "-")
                            {
                                cells[row, col].HorizontalAlignment = HAlign.Center;
                            }
                            if (rec.QuarterlyDSCR != null && rec.QuarterlyDSCR.ToString().Contains("<font"))
                            {
                                cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                            }


                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuarterlyDSCR);
                        }



                        //DSCRDifferencePerQuarter

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.DSCRDifferencePerQuarter == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        if (rec.DSCRDifferencePerQuarter != null && rec.DSCRDifferencePerQuarter.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.DSCRDifferencePerQuarter);


                        //DSCRPercentageChange

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#0.00%";
                        if (rec.DSCRPercentageChange == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }

                        if (rec.DSCRPercentageChange != null && rec.DSCRPercentageChange.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        if (rec.DSCRPercentageChange == "0" || rec.DSCRPercentageChange == "0.00%")
                        {
                            cells[row, col].NumberFormat = "0.00";
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.DSCRPercentageChange);


                        //QuarterlyResidentDays

                        ColorPink(cells, row, col, rec);
                        // cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.QuarterlyResidentDays;


                        //Cumulative ADR
                        ColorPink(cells, row, col, rec);

                        cells[row, col].NumberFormat = "#,###0.00";
                        cells[row, col++].Value = rec.AverageDailyRateRatio;

                        //Quarterly ADR

                        if (row == 1)
                        {
                            cells[row, col].NumberFormat = "#0";

                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.ADRperQuarter);
                        }
                        else
                        {
                            ColorPink(cells, row, col, rec);
                            cells[row, col].NumberFormat = "#,###0.00";
                            if (rec.ADRperQuarter == "-")
                            {
                                cells[row, col].HorizontalAlignment = HAlign.Center;
                            }
                            if (rec.ADRperQuarter != null && rec.ADRperQuarter.Contains("<font"))
                            {
                                cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                            }
                            //cells["AD:AD"].EntireColumn.HorizontalAlignment = HAlign.Right;

                            cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.ADRperQuarter);
                        }

                        //ADRDifferencePerQuarter
                        ColorPink(cells, row, col, rec);
                        //cells["AE:AE"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.ADRDifferencePerQuarter == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        if (rec.ADRDifferencePerQuarter != null && rec.ADRDifferencePerQuarter.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.ADRDifferencePerQuarter);

                        //ADRPercentageChange

                        ColorPink(cells, row, col, rec);
                        //cells["AF:AF"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        //cells["S:Y"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        //cells["AA:AC"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        //cells["AG:AH"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells[row, col].NumberFormat = "#0.00%";
                        if (rec.ADRPercentageChange == "-")
                        {
                            cells[row, col].HorizontalAlignment = HAlign.Center;
                        }
                        if (rec.ADRPercentageChange != null && rec.ADRPercentageChange.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        if (rec.ADRPercentageChange == "0" || rec.ADRPercentageChange == "0.00%")
                        {
                            cells[row, col].NumberFormat = "0.00";
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.ADRPercentageChange);


                        //QuarterlyMIP

                        ColorPink(cells, row, col, rec);
                        cells[row, col].NumberFormat = "#,###0.00";

                        cells[row, col++].Value = rec.QuarterlyMIP;


                        //Exception Count
                        // ColorPink(cells, row, col, rec);
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        cells[row, col++].Value = rec.RowWiseError;


                        //Date Inserted

                        //cells["AL:AL"].EntireColumn.HorizontalAlignment = HAlign.Left;
                        //  ColorPink(cells, row, col, rec);
                        if (rec.DataInserted != default(DateTime))
                        {
                            cells[row, col].NumberFormat = "mm/dd/yyyy h:mm:ss AM/PM";

                            cells[row, col++].Value = TimezoneManager.GetPreferredTimeFromUtc(rec.DataInserted);
                        }
                        else
                        {
                            cells[row, col].NumberFormat = "#0";

                            cells[row, col++].Value = "";
                        }






                    }

                    row++;
                }

            cells[1, 19].Interior.Color = Color.FromArgb(195, 195, 229);
            cells[1, 19].Font.Color = Color.FromArgb(255, 0, 0);
            cells[1, 19].Font.Bold = true;

            cells[1, 22].Interior.Color = Color.FromArgb(195, 195, 229);
            cells[1, 22].Font.Color = Color.FromArgb(255, 0, 0);
            cells[1, 22].Font.Bold = true;

            cells[1, 27].Interior.Color = Color.FromArgb(195, 195, 229);
            cells[1, 27].Font.Color = Color.FromArgb(255, 0, 0);
            cells[1, 27].Font.Bold = true;

            cells[1, 32].Interior.Color = Color.FromArgb(195, 195, 229);
            cells[1, 32].Font.Color = Color.FromArgb(255, 0, 0);
            cells[1, 32].Font.Bold = true;

            cells[1, 36].Interior.Color = Color.FromArgb(195, 195, 229);
            cells[1, 36].Font.Color = Color.FromArgb(255, 0, 0);
            cells[1, 36].Font.Bold = true;

            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();

                cells[row, col].EntireRow.Font.Bold = true;


                cells[row, 0].Value = "Total number of Properties:";

                cells[row, 1].Value = ProjectCnt - 1;
                /*cells[row, 14].Value = reportModel.ReportHeaderModel.TotalNoiError;
                cells[row, 19].Value = reportModel.ReportHeaderModel.TotalOpRevError;
                cells[row, 24].Value = reportModel.ReportHeaderModel.TotalDSCRError;
                cells[row, 30].Value = reportModel.ReportHeaderModel.TotalADRError;
                cells[row, 33].Value = reportModel.ReportHeaderModel.TotalColError;
                */


            }

            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }



        public static int SetDataValuesForCurQrtrAEReport(ReportModel reportModel,
     IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            string colName = "F";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;

                    if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
                    {


                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.FhaNumber;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ProjectName;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ServiceName;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_WorkLoad_Manager_Name;


                        //cells[row, col].NumberFormat = "#0";
                        //cells[row, col++].Value = rec.UnitsInFacility;


                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.PeriodEnding;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.MonthsInPeriod;


                    }

                    row++;
                }

            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
                var UnitsInFacility = reportModel.ReportGridModelList.Sum(o => o.UnitsInFacility);

                row = row++;
                cells[row, 0].Value = "Total number of Properties:";
                cells[row, 1].Value = ProjectCnt;
                // cells[row, 4].Value = UnitsInFacility;
            }
            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int SetDataValuesForErrorDetail(ReportModel reportModel,
     IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            string colName = "N";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;

                    if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "InternalSpecialOptionUser")
                    {


                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.FhaNumber;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ProjectName;
                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.PeriodEnding;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.MonthsInPeriod;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_WorkLoad_Manager_Name;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_Project_Manager_Name;

                        cells["G:G"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells["I:I"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells["K:K"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells["M:M"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells[row, col].NumberFormat = "#0.00%";
                        if (rec.RevenuePercentageChange != null && rec.RevenuePercentageChange.Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.RevenuePercentageChange);


                        cells[row, col].NumberFormat = "#,###0.00";
                        if (rec.CumulativeRevenue != null && rec.CumulativeRevenue.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = rec.CumulativeRevenue;

                        cells[row, col].NumberFormat = "#0.00%";

                        if (rec.NOIPercentageChange != null && rec.NOIPercentageChange.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.NOIPercentageChange);

                        cells[row, col].NumberFormat = "#,###0.00";


                        if (rec.NOICumulative != null && rec.NOICumulative.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = rec.NOICumulative;

                        cells[row, col].NumberFormat = "#0.00%";

                        if (rec.DSCRPercentageChange != null && rec.DSCRPercentageChange.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.DSCRPercentageChange);

                        cells[row, col].NumberFormat = "#,###0.00";

                        if (rec.DebtCoverageRatio2 != null && rec.DebtCoverageRatio2.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }


                        cells[row, col++].Value = (rec.DebtCoverageRatio2);

                        cells[row, col].NumberFormat = "#0.00%";

                        if (rec.ADRPercentageChange != null && rec.ADRPercentageChange.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.ADRPercentageChange);

                        cells[row, col].NumberFormat = "#,###0.00";

                        if (rec.AverageDailyRateRatio != null && rec.AverageDailyRateRatio.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }
                        cells[row, col++].Value = rec.AverageDailyRateRatio;




                    }

                    row++;
                }

            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
                row = row++;
                cells[row, 0].Value = "Total Number of Properties with Exceptions:";
                cells[row, 1].Value = ErrorCnt;
            }
            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }


        public static int SetDataValuesForPTDetail(ReportModel reportModel,
         IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            string colName = "J";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;


                    if (userType == "AccountExecutive" || userType == "WorkflowManager" || userType == "SuperUser" || userType == "LenderAccountRepresentative" || userType == "LenderAccountManager" || userType == "BackupAccountManager" || userType == "InternalSpecialOptionUser")
                    {


                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.FhaNumber;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ProjectName;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ServiceName;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_WorkLoad_Manager_Name;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.HUD_Project_Manager_Name;

                        //cells[row, col].NumberFormat = "#0";
                        //cells[row, col++].Value = rec.UnitsInFacility;


                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.PeriodEnding;

                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.MonthsInPeriod;


                        cells[row, col].NumberFormat = "#,###0.00";

                        if (rec.QuaterlyNoi != null && rec.QuaterlyNoi.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuaterlyNoi);


                        cells[row, col].NumberFormat = "#,###0.00";

                        if (rec.QuarterlyDSCR != null && rec.QuarterlyDSCR.ToString().Contains("<font"))
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                        }

                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.QuarterlyDSCR);




                    }

                    row++;
                }
            var FilteredList = reportModel.ReportGridModelList.Where(x => x.IsQtrlyDSCRPTError == 1 || x.IsQtrlyNOIPTError == 1).Distinct().ToList();
            reportModel.ReportGridModelList = null;
            reportModel.ReportGridModelList = FilteredList;
            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
                row = row++;
                cells[row, 0].Value = "Total number of Properties:";
                cells[row, 1].Value = ProjectCnt;
            }


            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int SetDataValuesForProjectDetailSuperUserHighLevel(ReportModel reportModel,
           IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            string colName = "C";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;

                    if (userType == "SuperUser" || userType == "SuperUserWLM" || userType == "WLM")
                    {


                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = ExcelAndPrintHelper.StripHTML(rec.WLMName.ToString().Replace("&nbsp;", " ").Trim());
                        cells[row, col].NumberFormat = "#0";
                        if (rec.isWLM == 1)
                        {
                            cells[row, col++].Value = rec.ProjectCountWLM;
                        }
                        else
                        {
                            cells[row, col++].Value = rec.ProjectCountAE;
                        }


                        cells[row, col].NumberFormat = "#0";
                        if (rec.isWLM == 1)
                        {
                            cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                            cells[row, col++].Value = rec.ErrorCountWLM;
                        }
                        else
                        {

                            cells[row, col++].Value = rec.ErrorCountAE;
                        }




                    }

                    row++;
                }

            if (reportModel.ReportGridModelList != null)
            {
                row = row++;
                var ProjectCnt = (from od in reportModel.ReportGridModelList where od.isWLM == 1 select od.ProjectCountWLM).Sum();
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.isWLM == 1 select od.ErrorCountWLM).Sum();
                cells[row, 0].Value = "Total:";
                cells[row, 1].NumberFormat = "#0";
                cells[row, 1].Value = ProjectCnt;
                cells[row, 2].NumberFormat = "#0";
                cells[row, 2].Value = ErrorCnt;
                cells[row, 2].Font.Color = Color.FromArgb(255, 0, 0);
            }



            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int SetDataValuesForMissingProjectReport(ReportModel reportModel,
            IRange cells, string userType)
        {
            int row = 1;
            int col = 0;
            string colName = "D";
            if (reportModel.ReportGridModelList != null)
                foreach (var rec in reportModel.ReportGridModelList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    if (userType == "SuperUserWLM")
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.HudWorkloadManagerName;
                        colName = "E";
                    }
                    if (userType == "WLM")
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.HudProjectManagerName;
                        colName = "E";
                    }
                    if (userType == "SuperUser" || userType == "SuperUserWLM" || userType == "WLM")
                    {
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.NumberOfProjects;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ExpectedNumber;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.ReceivedNumber;
                        cells[row, col].NumberFormat = "#0";
                        cells[row, col++].Value = rec.MissingNumber;
                    }
                    if (userType == "AE")
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.ProjectName;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.FhaNumber;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.MissingQuarter;
                        cells[row, col].NumberFormat = "mm/dd/yyyy";
                        cells[row, col++].Value = rec.QuarterEndDate;
                    }
                    row++;
                }
            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int GetHeaderValuesForProjectDetail(ProjectReportModel reportModel, List<String> selectedRatioExceptions, IRange cells)
        {
            var row = 0;
            var col = 0;
            string colName = "N";

            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Report Name:";
            col++;
            cells[row++, col].Value = EnumType.GetEnumDescription(reportModel.ReportHeaderModel.ReportType);
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Report Generated as of:";
            col++;
            cells[row++, col].Value = DateTime.Today.ToShortDateString();
            if (!String.IsNullOrEmpty(reportModel.ReportHeaderModel.HudWorkloadManagerName))
            {
                col = 0;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                cells[row, col++].Value = "Workload Manager Name:";
                col++;
                cells[row++, col].Value = reportModel.ReportHeaderModel.HudWorkloadManagerName;
            }
            if (!String.IsNullOrEmpty(reportModel.ReportHeaderModel.HudProjectManagerName))
            {
                col = 0;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                cells[row, col++].Value = "Account Executive Name:";
                col++;
                cells[row++, col].Value = reportModel.ReportHeaderModel.HudProjectManagerName;
            }
            if (!String.IsNullOrEmpty(reportModel.ReportHeaderModel.LenderName))
            {
                col = 0;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                cells[row, col++].Value = "Lender Name:";
                col++;
                cells[row++, col].Value = reportModel.ReportHeaderModel.LenderName;
            }
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Minimum Upload Date:";
            col++;
            cells[row++, col].Value = reportModel.MinUploadDate;
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Maximum Upload Date:";
            col++;
            cells[row++, col].Value = reportModel.MaxUploadDate;

            if (reportModel.ReportHeaderModel.ReportType.ToString("g").Equals("RatioExceptionsReport"))
            {
                if (selectedRatioExceptions != null)
                {
                    foreach (var ratioException in selectedRatioExceptions)
                    {
                        col = 0;
                        cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                        cells[row++, col].Value = ratioException;
                    }
                }
            }

            col = 0;
            row++;

            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 45;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 9;
            cells[row, col++].Value = "FHA Number";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 12;
            cells[row, col++].Value = "UPB";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "Period Ending Date";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "Months In Period";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "DSCR";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "DCoH";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "APP";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "WC";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 10;
            cells[row, col++].Value = "AR";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Workload Manager";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Account Executive";
            cells[row, col].ColumnWidth = 22;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Date Inserted";
            cells["A" + (row + 1) + ":" + colName + (row + 1)].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A" + (row + 1) + ":" + colName + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row + 1;
        }

        public static int SetDataValuestoExcelForProjectDetailReport(ProjectReportModel reportModel, int headerRows, IRange cells)
        {
            int row = headerRows;
            int col = 0;
            string colName = "N";
            if (reportModel.ProjectReportGridlList != null)
                foreach (var rec in reportModel.ProjectReportGridlList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.FhaNumber;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.ProjectName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.LenderName;
                    cells[row, col].NumberFormat = "#,###0.0";
                    cells[row, col++].Value = rec.UnpaidPrincipalBalance;
                    cells[row, col].NumberFormat = "mm/dd/yyyy";
                    cells[row, col++].Value = rec.PeriodEndingDate;
                    cells[row, col++].Value = rec.MonthsInPeriod;
                    cells[row, col++].Value = rec.DebtCoverageRatio;
                    cells[row, col++].Value = rec.DaysCashOnHand;
                    cells[row, col++].Value = rec.AvgPaymentPeriod;
                    cells[row, col++].Value = rec.WorkingCapital;
                    cells[row, col++].Value = rec.DaysInAcctReceivable;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.HudWorkloadManagerName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.HudProjectManagerName;
                    cells[row, col].NumberFormat = "mm/dd/yyyy h:mm:ss AM/PM";
                    cells[row, col++].Value = TimezoneManager.GetPreferredTimeFromUtc(rec.DataInserted);
                    row++;
                }
            cells["A" + headerRows + ":" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int GetHeaderValuesForProjectSummary(ProjectReportModel reportModel, string reportLevelHeader, List<String> selectedRatioExceptions, IRange cells)
        {
            var row = 0;
            var col = 0;
            string colName = "I";

            string userName = UserPrincipal.Current.UserName;
            cells[row, col++].ColumnWidth = 35;
            cells[row, col++].ColumnWidth = 10;
            cells[row, col++].ColumnWidth = 15;
            cells[row, col++].ColumnWidth = 13;
            cells[row, col++].ColumnWidth = 13;
            cells[row, col++].ColumnWidth = 13;
            cells[row, col++].ColumnWidth = 13;
            cells[row, col++].ColumnWidth = 13;
            cells[row, col++].ColumnWidth = 13;

            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Report Name:";
            cells[row++, col].Value = EnumType.GetEnumDescription(reportModel.ReportHeaderModel.ReportType);
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Report Generated as of:";
            cells[row++, col].Value = DateTime.Today.ToShortDateString();
            if (!String.IsNullOrEmpty(reportModel.ReportHeaderModel.HudWorkloadManagerName))
            {
                col = 0;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                cells[row, col++].Value = "Workload Manager Name:";
                cells[row++, col].Value = reportModel.ReportHeaderModel.HudWorkloadManagerName;
            }
            if (!String.IsNullOrEmpty(reportModel.ReportHeaderModel.HudProjectManagerName))
            {
                col = 0;
                cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                cells[row, col++].Value = "Account Executive Name:";
                cells[row++, col].Value = reportModel.ReportHeaderModel.HudProjectManagerName;
            }
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Minimum Upload Date:";
            cells[row++, col].Value = reportModel.MinUploadDate;
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col++].Value = "Maximum Upload Date:";
            cells[row++, col].Value = reportModel.MaxUploadDate;

            if (reportModel.ReportHeaderModel.ReportType.ToString("g").Equals("RatioExceptionsReport"))
            {
                if (selectedRatioExceptions != null)
                {
                    foreach (var ratioException in selectedRatioExceptions)
                    {
                        col = 0;
                        cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
                        cells[row++, col].Value = ratioException;
                    }
                }
            }

            row++;
            col = 0;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = reportLevelHeader;
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Total UPB";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of HL Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of DSCR Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of DCoH Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of APP Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of WC Projects";
            cells[row, col].WrapText = true;
            cells[row, col++].Value = "Number of AR Projects";
            cells["A" + (row + 1) + ":" + colName + (row + 1)].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A" + (row + 1) + ":" + colName + (row + 1)].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row + 1;
        }

        public static List<String> GetSelectedRatioExceptions(bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            List<String> ratioExceptionsSelectedList = null;
            if (isDebtCoverageRatio || isWorkingCapital || isDaysCashOnHand || isDaysInAcctReceivable || isAvgPaymentPeriod)
            {
                ratioExceptionsSelectedList = new List<String>();
                if (isDebtCoverageRatio)
                {
                    ratioExceptionsSelectedList.Add("Debt Coverage Ratio (< 1.2)");
                }
                if (isWorkingCapital)
                {
                    ratioExceptionsSelectedList.Add("Working Capital (< 1.2)");
                }
                if (isDaysCashOnHand)
                {
                    ratioExceptionsSelectedList.Add("Days Cash on Hand (< 14 days)");
                }
                if (isDaysInAcctReceivable)
                {
                    ratioExceptionsSelectedList.Add("Days in Accounts Receivable (> 70 days)");
                }
                if (isAvgPaymentPeriod)
                {
                    ratioExceptionsSelectedList.Add("Average Payment Period (> 70 days)");
                }
            }
            return ratioExceptionsSelectedList;

        }
        public static int SetDataValuestoExcelForProjectSummaryReport(ProjectReportModel reportModel, string reportLevelHeader, int headerRows, IRange cells)
        {
            int row = headerRows;
            int col = 0;
            string userName = UserPrincipal.Current.UserName;
            string colName = "I";
            if (reportModel.ProjectReportGridlList != null)
                foreach (var rec in reportModel.ProjectReportGridlList)
                {
                    col = 0;
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    if (reportLevelHeader.Equals("Workload Manager"))
                    {
                        cells[row, col++].Value = rec.WorkloadManager;
                    }
                    else if (reportLevelHeader.Equals("Account Executive"))
                    {
                        cells[row, col++].Value = rec.AccountExecutive;
                    }
                    else
                    {
                        cells[row, col++].Value = rec.LenderName;
                    }
                    cells[row, col].NumberFormat = "#";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.NumberOfProjects;
                    cells[row, col].WrapText = true;
                    cells[row, col].NumberFormat = "#,###0.00";
                    cells[row, col++].Value = rec.TotalUnpaidPrincipalBalance;
                    cells[row, col++].Value = rec.UnpaidPrincipalBalance;
                    cells[row, col++].Value = rec.DebtCoverageRatio;
                    cells[row, col++].Value = rec.DaysCashOnHand;
                    cells[row, col++].Value = rec.AvgPaymentPeriod;
                    cells[row, col++].Value = rec.WorkingCapital;
                    cells[row, col++].Value = rec.DaysInAcctReceivable;
                    row++;
                }
            cells["A" + headerRows + ":" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static int SetDataValuesForAdhocReport(IRange cells, IEnumerable<AdhocReportModel> adhocReportModel, Dictionary<string, bool> columnsChosen, string reportType)
        {
            var row = 0;
            var col = 0;
            string columnName;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            //System.Drawing.Color.Black;
            cells[row, col].EntireRow.Font.Bold = true;
            var columnInfoGetter = new AdhocReportExcelModel();
            List<AdhocReportColumnInfo> columnInfoList = columnInfoGetter.GetAdhocReportColumns(null);
            foreach (var columnInfo in columnInfoList)
            {
                columnName = columnInfo.ColumnName;
                if (columnsChosen[columnName])
                {
                    cells[row, col].ColumnWidth = columnInfo.ColumnWidth;
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = columnName;
                }
            }
            row++;
            if (adhocReportModel != null)
            {
                foreach (var rec in adhocReportModel)
                {
                    col = 0;
                    List<AdhocReportColumnInfo> rowData = columnInfoGetter.GetAdhocReportColumns(rec);

                    foreach (var columnInfo in rowData)
                    {
                        columnName = columnInfo.ColumnName;
                        if (columnsChosen[columnName])
                        {
                            if (columnInfo.HorizontalAlignment == null)
                            {
                                cells[row, col].HorizontalAlignment = HAlign.Left;
                            }
                            else
                            {
                                cells[row, col].HorizontalAlignment = columnInfo.HorizontalAlignment;
                            }
                            cells[row, col].NumberFormat = columnInfo.NumberFormat;
                            cells[row, col++].Value = columnInfo.Data;
                        }
                    }
                    row++;
                }
            }
            var myFirst = col / 26 - 1;
            var mySecond = col % 26 - 1;
            char firstColLetter = (char)((char)(col / 26 - 1) + 'A');
            char secondColLetter = (char)((char)(col % 26 - 1) + 'A');
            string colName = firstColLetter >= 'A' ? firstColLetter.ToString() + secondColLetter.ToString() : "" + secondColLetter.ToString();
            cells["A1:" + colName + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void GetHeaderValuesForUsersReport(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;

            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "User Name";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Role";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "First Name";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Middle Name/Initial";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Last Name";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Title";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Organization";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Lender Name";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Phone Number";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Address";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "City";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "State";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Zip Code";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Email";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "User Timezone";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Registration Not Complete";
            if (RoleManager.IsAdminRole(UserPrincipal.Current.UserName))
            {
                cells[row, col].HorizontalAlignment = HAlign.Left;
                cells[row, col].ColumnWidth = 20;
                cells[row, col++].Value = "Account Locked";
                cells[row, col].HorizontalAlignment = HAlign.Left;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Inactive";
            }

            if (RoleManager.IsAdminRole(UserPrincipal.Current.UserName))
            {
                cells["A1:R1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                cells["A1:R1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            }
            else
            {
                cells["A1:P1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
                cells["A1:P1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            }

        }

        public static int SetDataValuesForUsersReport(IList<UserViewModel> usersReportData, IRange cells)
        {
            int row = 1;
            int col = 0;
            if (usersReportData != null)
                foreach (var rec in usersReportData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.UserName != null ? rec.UserName.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.SelectedRoles != null ? rec.SelectedRoles.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.FirstName != null ? rec.FirstName.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.MiddleName != null ? rec.MiddleName.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LastName != null ? rec.LastName.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.Title != null ? rec.Title.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.Organization != null ? rec.Organization.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LenderName != null ? rec.LenderName.ToString() : string.Empty;

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.PhoneNumber != null ? rec.PhoneNumber.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.AddressLine1 != null ? rec.AddressLine1.ToString() : string.Empty;

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.City != null ? rec.City.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.SelectedStateCd != null ? rec.SelectedStateCd.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.ZipCode != null ? rec.ZipCode.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.EmailAddress != null ? rec.EmailAddress.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.SelectedTimezone != null ? rec.SelectedTimezone.ToString() : string.Empty;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.IsRegisterComplete.HasValue ? rec.IsRegisterComplete.Value == true ? "No" : "Yes" : "No";
                    cells[row, col].NumberFormat = "@";
                    if (RoleManager.IsAdminRole(UserPrincipal.Current.UserName))
                    {
                        cells[row, col++].Value = rec.IsAccountLocked.HasValue
                            ? rec.IsAccountLocked.Value == true ? "Yes" : "No"
                            : "No";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = rec.Deleted_Ind.HasValue
                            ? rec.Deleted_Ind.Value == true ? "Yes" : "No"
                            : "No";
                        row++;
                    }
                }
            cells["A1:R" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void GetHeaderValuesForPAMReport(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 15;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "FHA Number";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Property ID";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Property Name";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "HUD Project Manager";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Current AE";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "WLM";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Action Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Submit Date";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Completion Date";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Project Action Days Open";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Is Project Action Open";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Request Amount";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Accepted Amount";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Lender User Contact Information";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Lender";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Re-Assigned From";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Re-Assigned to";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Re-Assigned by";

            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Date Re-Assigned";

            cells[row, col].ColumnWidth = 80;
            cells[row, col++].Value = "Comment";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Processed Status";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Processed By";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Request Additional Info";

            cells["A1:U1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:U1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        /// <summary>
        /// display common details of the PAM report
        /// </summary>
        /// <param name="name"></param>
        /// <param name="aes"></param>
        /// <param name="status"></param>
        /// <param name="action"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="cells"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public static void PopuateCommonPAMDetails(string status, string action, string fromDate, string toDate, IRange cells, int row, int col)
        {

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Status";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = status;

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Project Action";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = action;

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "From Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = fromDate;
            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "To Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = toDate;
        }


        /// <summary>
        /// Set pam report search criteria details work sheet, with header and values
        /// </summary>
        /// <param name="pamReport"></param>
        /// <param name="cells"></param>
        public static void SetPAMSearchCriteriaDetails(PAMReportModel pamReport, IRange cells)
        {
            int row = 0;
            int col = 0;
            var delimiter = "\r\n";
            if (pamReport != null)
            {

                if (UserPrincipal.Current.UserRole != "InternalSpecialOptionUser")
                {
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row++, 4].Value = pamReport.ReportViewName;
                }
                else
                {
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row++, 4].Value = "HUD InternalSpecialOptionUser VIEW";

                }


                if (pamReport.ReportViewName == "HUD ADMINSTRATOR VIEW" && UserPrincipal.Current.UserRole != "InternalSpecialOptionUser")
                {
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "WLM(s)";

                    cells[row, col].NumberFormat = "@";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col].WrapText = true;
                    cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedWLMNameList.ToList());

                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "Account Executive";

                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 35;
                    cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedAENameList.ToList());


                    PopuateCommonPAMDetails(pamReport.StatusName, pamReport.ProjectActionName
                        , pamReport.FromDate, pamReport.ToDate, cells, row, col);
                }

                else if (pamReport.ReportViewName == "HUD WORKLOAD MANAGER VIEW" && UserPrincipal.Current.UserRole != "InternalSpecialOptionUser")
                {
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "Account Executive";

                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 35;
                    cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedAENameList.ToList());

                    PopuateCommonPAMDetails(pamReport.StatusName, pamReport.ProjectActionName
                      , pamReport.FromDate, pamReport.ToDate, cells, row, col);
                }
                else
                {
                    PopuateCommonPAMDetails(pamReport.StatusName, pamReport.ProjectActionName
                     , pamReport.FromDate, pamReport.ToDate, cells, row, col);

                }
            }

        }

        public static int SetDataValuesForPAMReport(IList<PAMReportViewModel> pamReportData,
            IRange cells)
        {
            int row = 1;
            int col = 0;
            if (pamReportData != null)
                foreach (var rec in pamReportData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.FHANumber;
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.PropertyId;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.PropertyName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.HUDPM;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.HudProjectManagerName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.HudWorkloadManagerName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.ProjectActionName.ToString();
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value =
                        String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionSubmitDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionSubmitDate));
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionCompletionDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionCompletionDate));
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.ProjectActionDaysToComplete;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.IsProjectActionOpen ? "Yes" : "No";
                    if (rec.ProjectActionName != null &&
                        (rec.ProjectActionName == "NCR" || rec.ProjectActionName == "R4R" ||
                         rec.ProjectActionName == "NCREXTENSION"))
                    {
                        cells[row, col].NumberFormat = "$#,###0.00";
                        cells[row, col++].Value = rec.RequestedAmount;
                        cells[row, col].NumberFormat = "$#,###0.00";
                        cells[row, col++].Value = rec.ApprovedAmount;
                    }
                    else
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = "NA";
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = "NA";
                    }
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LenderEmail;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LenderUserName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ReAssignedTo) ? "NA" : rec.ReAssignedFrom;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ReAssignedTo) ? "NA" : rec.ReAssignedTo;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ReAssignedBy) ? "NA" : rec.ReAssignedBy;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ReAssignedDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ReAssignedDate));
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.Comment;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.RequestStatus) ? "NA" : rec.RequestStatus;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ProcessedBy) ? "NA" : rec.ProcessedBy;
                    cells["S:T"].EntireColumn.HorizontalAlignment = HAlign.Right;
                    if (rec.ProjectAction == "NCR" || rec.ProjectAction == "R4R" || rec.ProjectAction == "NCREXTENSION")
                    {
                        cells[row, col++].Value = !rec.AdditionalInfoCount.HasValue ? "NA" : rec.AdditionalInfoCount.ToString();
                    }
                    else
                    {
                        cells[row, col++].Value = rec.RAI;
                    }
                    row++;
                }
            cells["A1:O" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }


        public static void SetPAMSearchCriteriaValues(IRange cells, string userName, string aeIds, DateTime fromDate, DateTime todate)
        {
            //UserPrincipal.Current.UserName, aeIds, fromDate, toDate, wlmId, aeId, requestStatus
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 15;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "FHA Number";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Property ID";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Property Name";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AE";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "WLM";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Action Name";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Submit Date";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Completion Date";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Project Action Calendar Days to Complete";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Action Open";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Requested Amount";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Accepted Amount";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Servicer";
            cells[row, col].ColumnWidth = 50;
            cells[row, col++].Value = "Comment";
            cells["A1:N1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:N1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
        }

        //1904
        public static void SetHeaderValuesForLenderPAMReport(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;

            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "FHA Number";

            //cells[row, col].ColumnWidth = 15;
            //cells[row, col++].Value = "Property ID";

            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Property Name";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Account Executive";

            //cells[row, col].ColumnWidth = 25;
            //cells[row, col++].Value = "WLM";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Action Name";

            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Submit Date";

            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Action Completion Date";

            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Project Action Days to Complete";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Is Project Action Open";

            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Requested Amount";

            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Accepted Amount";

            cells[row, col].ColumnWidth = 40;
            cells[row, col++].Value = "Lender User Name";

            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "User Role";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Re-Assigned to";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Re-Assigned by";

            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Date Re-Assigned";

            cells[row, col].ColumnWidth = 80;
            cells[row, col++].Value = "Comment";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Processed Status";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Processed By";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Request Additional Info";

            cells["A1:S1"].Interior.Color = Color.FromArgb(255, 176, 196, 222);
            cells["A1:S1"].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static int SetDataValuesForLenderPAMReport(IList<PAMReportViewModel> pamReportData, IRange cells)
        {
            int row = 1;
            int col = 0;
            if (pamReportData != null)
                foreach (var rec in pamReportData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.FHANumber;
                    //cells[row, col].NumberFormat = "#0";
                    //cells[row, col++].Value = rec.PropertyId;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.PropertyName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.HudProjectManagerName;
                    //cells[row, col].NumberFormat = "@";
                    //cells[row, col++].Value = rec.HudWorkloadManagerName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.ProjectActionName.ToString();
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value =
                        String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionSubmitDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionSubmitDate));
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionCompletionDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ProjectActionCompletionDate));
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.ProjectActionDaysToComplete;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.IsProjectActionOpen ? "Yes" : "No";
                    if (rec.ProjectActionName != null &&
                        (rec.ProjectActionName == "NCR" || rec.ProjectActionName == "R4R" ||
                         rec.ProjectActionName == "NCREXTENSION"))
                    {
                        cells[row, col].NumberFormat = "$#,###0.00";
                        cells["I:I"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells[row, col++].Value = rec.RequestedAmount;
                        cells[row, col].NumberFormat = "$#,###0.00";
                        cells["J:J"].EntireColumn.HorizontalAlignment = HAlign.Right;
                        cells[row, col++].Value = rec.ApprovedAmount;
                    }
                    else
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = "NA";
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = "NA";
                    }

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.LenderUserName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.UserRoleName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ReAssignedTo) ? "NA" : rec.ReAssignedTo;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ReAssignedBy) ? "NA" : rec.ReAssignedBy;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ReAssignedDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.ReAssignedDate));
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.Comment;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.RequestStatus) ? "NA" : rec.RequestStatus;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.ProcessedBy) ? "NA" : rec.ProcessedBy;
                    cells["S:S"].EntireColumn.HorizontalAlignment = HAlign.Right;
                    if (rec.ProjectAction == "NCR" || rec.ProjectAction == "R4R" || rec.ProjectAction == "NCREXTENSION")
                    {
                        cells[row, col++].Value = !rec.AdditionalInfoCount.HasValue ? "NA" : rec.AdditionalInfoCount.ToString();
                    }
                    else
                    {
                        cells[row, col++].Value = rec.RAI;
                    }

                    row++;
                }
            cells["A1:S" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void SetLenderPAMCommonSearchCriteria(string status, IList<string> action, string fromDate, string toDate, IRange cells, int row, int col)
        {
            var delimiter = "\r\n";
            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Status";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = status;

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Project Action";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = String.Join(delimiter, action.ToList()); ;

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "From Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = fromDate;
            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "To Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = toDate;
        }

        public static void SetLenderPAMSearchCriteriaLAMBAM(PAMReportModel pamReport, IRange cells)
        {
            int row = 0;
            int col = 0;
            var delimiter = "\r\n";
            if (pamReport != null)
            {
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row++, 4].Value = pamReport.ReportViewName;

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Report Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].ColumnWidth = 35;
                cells[row, col].WrapText = true;
                cells[row++, col].Value = "Project Action Management Report";

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Lender Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 35;
                cells[row++, col].Value = pamReport.LenderName;

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Lender User Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 35;
                cells[row++, col].Value = UserPrincipal.Current.FullName;


                if (UserPrincipal.Current.UserRole == HUDRole.LenderAccountManager.ToString("g") ||
                     UserPrincipal.Current.UserRole == HUDRole.BackupAccountManager.ToString("g"))
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "Role List";

                    cells[row, col].NumberFormat = "@";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col].WrapText = true;
                    cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedRolesList.ToList());

                    if (pamReport.SelectedRolesList.Contains("LAM"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "LAM";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedLAMNameList.ToList());
                    }
                    if (pamReport.SelectedRolesList.Contains("BAM"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "BAM";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedBAMNameList.ToList());
                    }

                    if (pamReport.SelectedRolesList.Contains("LAR"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "LAR";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedLARNameList.ToList());
                    }
                }

                SetLenderPAMCommonSearchCriteria(pamReport.StatusName, pamReport.SelectedProjectActionList
                    , pamReport.FromDate, pamReport.ToDate, cells, row, col);

            }

        }

        //Production

        public static void SetSearchForProdPAMReport(ProductionPAMReportModel pamReport, IRange cells, string strpageTypeIds, string strprogramType, string strprodUserIds, string strstatus, DateTime? fromDate, DateTime? toDate)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Left;
            cells[row, col].ColumnWidth = 15;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Left;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;

            cells[row++, col].Value = "Report Name";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;

            cells[row, col].ColumnWidth = 45;
            cells[row++, col].Value = "Production Type";

            cells[row, col].EntireColumn.Font.Bold = true;


            cells[row, col].ColumnWidth = 45;
            cells[row++, col].Value = "Project Loan Type";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;

            cells[row, col].ColumnWidth = 25;
            cells[row++, col].Value = "Production Users/WLM";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;

            cells[row, col].ColumnWidth = 25;
            cells[row++, col].Value = "Status";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;

            cells[row, col].ColumnWidth = 25;
            cells[row++, col].Value = "From Date";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;

            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = "To Date";

            cells[row, col].EntireColumn.Font.Bold = true;
            cells[row, col].HorizontalAlignment = HAlign.Left;
            cells[row, col].ColumnWidth = 35;

            cells["B1:B1"].Value = "Production Project Action management Report";
            cells["B1:B1"].ColumnWidth = 40;
            cells["B1:B1"].WrapText = true;
            cells["B2:B2"].Value = strpageTypeIds;
            cells["B2:B2"].ColumnWidth = 40;
            cells["B2:B2"].WrapText = true;

            cells["B3:B3"].Value = strprogramType;
            cells["B3:B3"].ColumnWidth = 40;
            cells["B3:B3"].WrapText = true;

            cells["B4:B4"].Value = strprodUserIds;
            cells["B4:B4"].ColumnWidth = 40;
            cells["B4:B4"].WrapText = true;

            cells["B5:B5"].Value = strstatus;
            cells["B5:B5"].ColumnWidth = 40;
            cells["B5:B5"].WrapText = true;
            cells["B6:B6"].Value = fromDate == null ? "NA" : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(fromDate));


            cells["B6:B6"].ColumnWidth = 40;
            cells["B6:B6"].WrapText = true;

            cells["B7:B7"].Value = toDate == null ? "NA" : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(toDate));
            cells["B7:B7"].ColumnWidth = 40;
            cells["B7:B7"].WrapText = true;


        }
        public static void GetHeaderValuesForProdPAMReport(IRange cells, string type)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 15;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "FHA Number";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "Project Name";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Loan Type";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "StartDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "EndDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "DaysActive";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "LoanAmount";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "LenderName";
            if (type == "LenderPAM")
            {
                cells[row, col].ColumnWidth = 45;
                cells[row, col++].Value = "LenderRole";
            }
            else
            {
                cells[row, col].ColumnWidth = 45;
                cells[row, col++].Value = "Lender UserName";
            }

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IsReassigned";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "status";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "ProcessedBy";
            cells[row, col].ColumnWidth = 15;
            cells[row, col++].Value = "viewname";

            cells["A1:M1"].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["A1:M1"].Borders.Color = Color.FromArgb(228, 242, 253);
            cells["A1:M1"].Font.Color = Color.FromArgb(46, 110, 161);

        }

        public static int SetDataValuesForProdPAMReport(IList<PAMReportProductionViewModel> pamReportData,
    IRange cells, string type)
        {
            int row = 1;
            int col = 0;
            if (pamReportData != null)
                foreach (var rec in pamReportData)
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col++].Value = rec.fhanumber;
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.ProjectName;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col++].Value = rec.LoanType;

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value =
                        String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.StartDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.StartDate));
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.EndDate)) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(rec.EndDate));
                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.DaysActive;

                    cells[row, col].NumberFormat = "#0";
                    cells[row, col++].Value = rec.LoanAmount;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = string.IsNullOrEmpty(rec.LenderName) ? "NA" : rec.LenderName;
                    if (type == "LenderPAM")
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = string.IsNullOrEmpty(rec.Lenderrole) ? "NA" : rec.Lenderrole;
                    }
                    else
                    {
                        cells[row, col].NumberFormat = "@";
                        cells[row, col++].Value = string.IsNullOrEmpty(rec.LenderUserName) ? "NA" : rec.LenderUserName;
                    }

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.IsReassigned == null ? false : rec.IsReassigned;
                    cells[row, col].NumberFormat = "@";
                    if (rec.StatusId == 17 || rec.StatusId == 18)
                    {
                        cells[row, col++].Value = "Open";
                    }
                    else if (rec.StatusId == 16)
                    {
                        cells[row, col++].Value = "RAI";
                    }
                    else if (rec.StatusId == 20)
                    {
                        cells[row, col++].Value = "FC Request";
                    }
                    else if (rec.StatusId == 21)
                    {
                        cells[row, col++].Value = "FC Response";
                    }
                    else
                    {
                        cells[row, col++].Value = "Closed";
                    }

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.ProcessedBy;

                    cells[row, col].NumberFormat = "@";
                    cells[row, col++].Value = rec.viewname;

                    row++;
                }
            cells["A1:O" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            return row;
        }

        public static void SetProdLenderPAMSearchCriteriaLAMBAM(ProductionPAMReportModel pamReport, IRange cells)
        {
            int row = 0;
            int col = 0;
            var delimiter = "\r\n";
            if (pamReport != null)
            {
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row++, 4].Value = pamReport.ReportViewName;

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Report Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].ColumnWidth = 35;
                cells[row, col].WrapText = true;
                cells[row++, col].Value = "Project Action Management Report";

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Lender Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 35;
                cells[row++, col].Value = pamReport.LenderName;

                col = 0;
                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 30;
                cells[row, col++].Value = "Lender User Name";

                cells[row, col].NumberFormat = "@";
                cells[row, col].WrapText = true;
                cells[row, col].ColumnWidth = 35;
                cells[row++, col].Value = UserPrincipal.Current.FullName;


                if (UserPrincipal.Current.UserRole == HUDRole.LenderAccountManager.ToString("g") ||
                     UserPrincipal.Current.UserRole == HUDRole.BackupAccountManager.ToString("g"))
                {
                    col = 0;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].WrapText = true;
                    cells[row, col].ColumnWidth = 30;
                    cells[row, col++].Value = "Role List";

                    cells[row, col].NumberFormat = "@";
                    cells[row, col].ColumnWidth = 35;
                    cells[row, col].WrapText = true;
                    cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedRolesList.ToList());

                    if (pamReport.SelectedRolesList.Contains("LAM"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "LAM";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedLAMNameList.ToList());
                    }
                    if (pamReport.SelectedRolesList.Contains("BAM"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "BAM";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedBAMNameList.ToList());
                    }

                    if (pamReport.SelectedRolesList.Contains("LAR"))
                    {
                        col = 0;
                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 30;
                        cells[row, col++].Value = "LAR";

                        cells[row, col].NumberFormat = "@";
                        cells[row, col].WrapText = true;
                        cells[row, col].ColumnWidth = 35;
                        cells[row++, col].Value = String.Join(delimiter, pamReport.SelectedLARNameList.ToList());
                    }
                }

                SetProdLenderPAMCommonSearchCriteria(pamReport.SelectedStatusList, pamReport.SelectedPageTypesList, pamReport.SelectedProgramTypesList
                    , pamReport.FromDate, pamReport.ToDate, cells, row, col);

            }

        }

        public static void SetProdLenderPAMCommonSearchCriteria(IList<string> status, IList<string> SelectedPageTypesList, IList<string> SelectedProgramTypesList, string fromDate, string toDate, IRange cells, int row, int col)
        {
            var delimiter = "\r\n";
            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Status";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = String.Join(delimiter, status.ToList()); ;

            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Page Type";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = String.Join(delimiter, SelectedPageTypesList == null ? null : SelectedPageTypesList.ToList());


            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Program Type";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            if (SelectedProgramTypesList == null)
            {
                cells[row++, col].Value = null;
            }
            else
            {
                cells[row++, col].Value = String.Join(delimiter, SelectedProgramTypesList == null ? null : SelectedProgramTypesList.ToList());
            }


            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "From Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = fromDate;
            col = 0;
            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "To Date";

            cells[row, col].NumberFormat = "@";
            cells[row, col].WrapText = true;
            cells[row, col].ColumnWidth = 35;
            cells[row++, col].Value = toDate;
        }

        public static void SetValuesForProdPAMStatusCount(IRange cells, ProdPAMReportStatusGridViewModel statusGridViewModel)
        {
            var row = 0;
            var col = 0;
            cells[row, col].ColumnWidth = 30;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].Value = "Project Status Report As of : " + DateTime.Now.ToString("MMMM dd,yyyy");
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;

            col -= 7;
            cells[++row, col].Value = "Loan Type Count";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells["A2:G2"].Interior.Color = Color.FromArgb(217, 237, 247);

            col -= 7;
            cells[++row, col].Value = "Purchase / Refinance 223(f)";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOf223f;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Refinance 223(a)(7)";
            cells[row, col].ColumnWidth = 30;
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOf223a7;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "OLL 223(d)";
            cells[row, col].ColumnWidth = 30;
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOf223d;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Fire Safety 223(i)";
            cells[row, col].ColumnWidth = 30;
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOf223i;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);

            col -= 7;
            cells[++row, col].Value = "Construction 241(a)";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOf241a;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Construction NC";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOfNC;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Construction SR";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOfSR;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "IRR";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOfIRR;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);

            col -= 7;
            cells[++row, col].Value = "Total FY Count";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOfFY;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Total Loans Opened";
            cells[row, ++col].Value = statusGridViewModel.LoanOpened;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Total Loans Completed";
            cells[row, ++col].Value = statusGridViewModel.TotalClosing;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Total Loan Count";
            cells[row, ++col].Value = statusGridViewModel.TotalNumberOfProjects;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells["A5:H5"].Borders.Weight = BorderWeight.Medium;

            col -= 7;
            cells[++row, col].Value = "FHA Number Process";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells["A6:G6"].Interior.Color = Color.FromArgb(217, 237, 247);

            col -= 7;
            cells[++row, col].Value = "FHA Number InQueue";
            cells[row, ++col].Value = statusGridViewModel.TotalFHAInqueue;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "FHA Number InProcess";
            cells[row, ++col].Value = statusGridViewModel.TotalFHAInprocess;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "FHA Number Complete";
            cells[row, ++col].Value = statusGridViewModel.TotalFHAComplete;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);

            col -= 5;
            cells[++row, col].Value = "Application Process";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells["A8:G8"].Interior.Color = Color.FromArgb(217, 237, 247);

            col -= 7;
            cells[++row, col].Value = "Awaiting Lender Application Submission";
            cells[row, ++col].Value = statusGridViewModel.TotalWaitingApplication;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Application InQueue";
            cells[row, ++col].Value = statusGridViewModel.TotalApplicationInqueue;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Application InProcess";
            cells[row, ++col].Value = statusGridViewModel.TotalApplicationInprocess;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Application Firm Issued";
            cells[row, ++col].Value = statusGridViewModel.TotalApplicationComplete;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);

            col -= 7;
            cells[++row, col].Value = "Technical Reviews";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;


            col -= 7;
            cells[++row, col].Value = "Appraiser Assigned";
            cells[row, ++col].Value = statusGridViewModel.TotalAppraiserAssigned;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Environmentalist Assigned";
            cells[row, ++col].Value = statusGridViewModel.TotalEnvironmentalistAssigned;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Title and Survey Assigned";
            cells[row, ++col].Value = statusGridViewModel.TotalTitleSurveyAssigned;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Appraiser Site Visit";
            cells[row, ++col].Value = 0;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);

            col -= 7;
            cells[++row, col].Value = "Initial Closing Process";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells["A12:G12"].Interior.Color = Color.FromArgb(217, 237, 247);

            col -= 7;
            cells[++row, col].Value = "Awaiting Lender Closing Submission";
            cells[row, ++col].Value = statusGridViewModel.TotalWaitingClosing;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Closing InQueue";
            cells[row, ++col].Value = statusGridViewModel.TotalClosingInqueue;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Closing InProcess";
            cells[row, ++col].Value = statusGridViewModel.TotalClosingInprocess;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Initial Closing - Complete";
            cells[row, ++col].Value = statusGridViewModel.TotalClosingComplete;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);


            col -= 7;
            cells[++row, col].Value = "Final Closing Process";
            cells[row, col, row, col += 7].Merge();
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 20;
            cells["A14:G14"].Interior.Color = Color.FromArgb(217, 237, 247);

            col -= 7;
            cells[++row, col].Value = "290 Assigned";
            cells[row, ++col].Value = statusGridViewModel.Total290Assigned;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "290 Complete";
            cells[row, ++col].Value = statusGridViewModel.Total290Complete;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);
            cells[row, ++col].Value = "Closing - Executed Documents Uploaded";
            cells[row, ++col].Value = statusGridViewModel.TotalExecutedDocUploaded;
            cells[row, col].Interior.Color = Color.FromArgb(66, 139, 202);



            //cells["A1:M1"].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["A1:H15"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            cells["A1:H15"].EntireRow.Font.Bold = true;

        }

        public static void GetHeaderForProdPAMReportV3(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].ColumnWidth = 15;
            cells[row, col].WrapText = true;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col++].Value = "FHA#";
            cells[row, col].ColumnWidth = 30;
            cells[row, col++].Value = "Project Name";
            cells[row, col].ColumnWidth = 45;
            cells[row, col++].Value = "Loan Type";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project StartDate";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Stage";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Project Status";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Underwriter Assigned";
            cells[row, col].ColumnWidth = 35;
            cells[row, col++].Value = "Appraiser";

            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Environmentalist";
            cells[row, col].ColumnWidth = 20;
            cells[row, col++].Value = "Title & Survey";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Total Days";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Status";

            cells["A1:L1"].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["A1:L1"].Borders.Color = Color.FromArgb(228, 242, 253);
            cells["A1:L1"].Font.Color = Color.FromArgb(46, 110, 161);

        }

        public static void SetDataValuesForProdPAMReportV3(IList<PAMReportV3ViewModel> reportModel, IRange cells)
        {
            int col = 0;
            int row = 1;
            if (reportModel != null)
                foreach (var rec in reportModel)
                {


                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.FhaNumber;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].HorizontalAlignment = HAlign.Left;
                    cells[row, col].Value = rec.ProjectName;
                    if (rec.LoanAmount >= 25000000)
                    {
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    cells[row, col].WrapText = true;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LoanType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value =
                        String.Format("{0:MM/dd/yyyy}", rec.ProjectStartDate) ==
                        "01/01/0001"
                            ? "NA"
                            : String.Format("{0:MM/dd/yyyy}", rec.ProjectStartDate);


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectStage;

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ProjectStatus == "Complete" || rec.ProjectStatus == "N/A")
                    {
                        cells[row, col].Value = rec.ProjectStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    else if (rec.ProjectStatus == "Unassigned")
                    {
                        cells[row, col].Value = rec.ProjectStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    else if (rec.ProjectStatus == "In-Process")
                    {
                        cells[row, col].Value = rec.ProjectStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 255);
                    }

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.IsUnderWriterAssigned ? rec.UnderWriterAssigned : "No";

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppraiserStatus == "Complete")
                    {
                        cells[row, col].Value = rec.ReviewerAppraiserStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    if (rec.AppraiserStatus == "N/A")
                    {
                        cells[row, col].Value = rec.AppraiserStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    else if (rec.AppraiserStatus == "Unassigned")
                    {
                        cells[row, col].Value = rec.AppraiserStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    else if (rec.AppraiserStatus == "In-Process")
                    {
                        //cells[row, col].Value = rec.AppraiserStatus;
                        cells[row, col].Value = rec.ReviewerAppraiserStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 255);
                    }

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.EnvironmentalistStatus == "Complete")
                    {
                        cells[row, col].Value = rec.ReviewerEnvironmentalistStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    if (rec.EnvironmentalistStatus == "N/A")
                    {
                        cells[row, col].Value = rec.EnvironmentalistStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    else if (rec.EnvironmentalistStatus == "Unassigned")
                    {
                        cells[row, col].Value = rec.EnvironmentalistStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    else if (rec.EnvironmentalistStatus == "In-Process")
                    {
                        cells[row, col].Value = rec.ReviewerEnvironmentalistStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 255);
                    }

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.TitleSurveyStatus == "Complete")
                    {
                        cells[row, col].Value = rec.ReviewerTitleSurveyStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    if (rec.TitleSurveyStatus == "N/A")
                    {
                        cells[row, col].Value = rec.TitleSurveyStatus;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }
                    else if (rec.TitleSurveyStatus == "Unassigned")
                    {
                        cells[row, col].Value = rec.TitleSurveyStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    else if (rec.TitleSurveyStatus == "In-Process")
                    {
                        cells[row, col].Value = rec.ReviewerTitleSurveyStatus;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 255);
                    }


                    cells[row, ++col].NumberFormat = "#0";
                    cells[row, col].Value = rec.TotalDays;

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.Status == "Open")
                    {
                        cells[row, col].Value = rec.Status;
                        cells[row, col].Font.Color = Color.FromArgb(255, 0, 0);
                    }
                    else if (rec.Status == "Complete")
                    {
                        cells[row, col].Value = rec.Status;
                        cells[row, col].Font.Color = Color.FromArgb(0, 255, 0);
                    }

                    row++;
                    col = 0;
                }
            cells["A1:L" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
        }



        public static void SetDataValuesForProdPAMReportV3Filters(HUDPamReportFiltersModel filters, IRange cells)
        {
            if (filters != null)
            {
                var row = 0;
                var col = 0;
                cells[row, col].ColumnWidth = 30;
                cells[row, col].EntireRow.RowHeight = 30;
                cells[row, col].EntireColumn.Font.Bold = true;

                cells[row, col].Value = "Date From";
                cells[row, ++col].ColumnWidth = 15;
                cells[row, col].Value = filters.FromDate;

                cells[row, ++col].ColumnWidth = 15;
                cells[row, ++col].ColumnWidth = 30;

                cells[row, col].EntireColumn.Font.Bold = true;
                cells[row, col].Value = "Date To";
                cells[row, ++col].ColumnWidth = 15;
                cells[row, col].Value = filters.ToDate;

                col -= 4;
                cells[++row, col].Value = "Project Name";
                cells[row, ++col].Value = filters.ProjectName;
                ++col;
                cells[row, ++col].Value = "FHA Number";
                cells[row, ++col].Value = filters.FHANumber;

                col -= 4;
                cells[++row, col].Value = "Loan Type";
                cells[row, ++col].Value = filters.LoanType;
                ++col;
                cells[row, ++col].Value = "Status";
                cells[row, ++col].Value = filters.Status;

                col -= 4;
                cells[++row, col].Value = "FHA Number Request";
                cells[row, ++col].Value = filters.FHANumberRequest;
                ++col;
                cells[row, ++col].Value = "Corporate Credit Review";
                cells[row, ++col].Value = filters.CorporateCreditReview;

                col -= 4;
                cells[++row, col].Value = "Portfolio Number Assignment";
                cells[row, ++col].Value = filters.PortfolioNumberAssignment;
                ++col;
                cells[row, ++col].Value = "Executed Documents";
                cells[row, ++col].Value = filters.ExecutedDocuments;

                col -= 4;
                cells[++row, col].Value = "Underwriter";
                cells[row, ++col].Value = filters.Underwriter;
                ++col;
                cells[row, ++col].Value = "Appraiser";
                cells[row, ++col].Value = filters.Appraiser;

                col -= 4;
                cells[++row, col].Value = "Environmentalist";
                cells[row, ++col].Value = filters.Environmentalist;
                ++col;
                cells[row, ++col].Value = "Title & Survey";
                cells[row, ++col].Value = filters.Title_Survey;



                cells["A1:B7"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                cells["D1:E7"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                cells["B1:B7"].HorizontalAlignment = HAlign.Right;
                cells["E1:E7"].HorizontalAlignment = HAlign.Right;
            }

        }


        public static void GetHeaderForProdAdhocDataDownload(IRange cells, string reportDate)
        {
            var row = 0;
            var col = 0;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].Value = "Report Date:" + reportDate;
            row = 1; col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].WrapText = true;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHANumber";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PropertyId";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ConstructionStageType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CurrentLoanType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ActivityType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA MortgageInsuranceType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA BorrowerType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA IsAddressCorrect";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectStreetaddress";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectCity";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectState";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProjectZipcode";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CongressionalDistrict";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LenderId";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LenderName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LenderContactName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LenderContactEmail";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LenderContactPhonenumber";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProposedLoanAmount";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA ProposedInterestRate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA SkilledNursingBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA SkilledNursingUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA AssistedLivingBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA AssistedLivingUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA BoardCareBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA BoardCareUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA MemoryCareBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA MemoryCareUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA IndependentBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA IndependentUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA OtherBeds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA OtherUnits";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CMSStarRating";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA MasterLeaseProposed";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA MidLargePortfolio";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA SmallPortfolio";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Portfolio_Name";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Portfolio_Number";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Comments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAAssignedTo";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHACompletedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA InsertFHAReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioAssignedTo";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioCompletedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA PortfolioReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewAssignedTo";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewCompletedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditreviewComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CreditReviewReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA RequestStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CurrentFHANumber";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA RequestSubmitDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA DenyComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CancelComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA One80WarningSent";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Two70WarningSent";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA IsReadyforApplication";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA LIHTC";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA TaxExemptBonds";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA HOME";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA CDBG";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Other";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHARequestCompletionDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHARequestNotificationType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ProductionType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DateofLenderSubmission";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LenderComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LongTermHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DateOfHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ReasonForHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LoanCommitteApproved";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LoanCommitteApprovedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS FirmCommitmentIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS FirmCommitmentIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EarlyCommencementRequested";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EarlyCommencementRequestedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EarlyCommencementIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LDLIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LDLIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS LDLCompleteResponseReceived";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS MiscellaneousComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OtherQueueWorkComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AccountExecutiveMiscInfo";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS IremsCompleted";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppsCompleted";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS IremsPrintoutSaved";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS MirandaCompletedCMSListCheck";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractorContractPrice";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractorSecondaryAmtPaid";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractorAmountPaid";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvironmentalComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractorComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraisalComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOgcAddress";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OgcAddress";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DateFirmReviewBegan";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DateEnteredQueue";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ProductionWLMName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedUWDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS UWReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedEnvName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS EnvReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedSurveyName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS SurveyReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedAppraiserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AppraiserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS OGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedBackupOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS BackupOGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedContractUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS ContractUWReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS AssignedDECName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS DECReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ProductionType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DateofLenderSubmission";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LenderComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LongTermHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DateOfHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ReasonForHold";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LoanCommitteApproved";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LoanCommitteApprovedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS FirmCommitmentIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS FirmCommitmentIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EarlyCommencementRequested";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EarlyCommencementRequestedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EarlyCommencementIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LDLIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LDLIssuedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS LDLCompleteResponseReceived";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS MiscellaneousComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OtherQueueWorkComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AccountExecutiveMiscInfo";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS IremsCompleted";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppsCompleted";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS IremsPrintoutSaved";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS MirandaCompletedCMSListCheck";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractorContractPrice";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractorSecondaryAmtPaid";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractorAmountPaid";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvironmentalComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractorComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraisalComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOgcAddress";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OgcAddress";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DateFirmReviewBegan";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DateEnteredQueue";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ProductionWLMName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedUWDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS UWReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedEnvName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS EnvReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedSurveyName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS SurveyReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedAppraiserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AppraiserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS OGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedBackupOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS BackupOGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedContractUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS ContractUWReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS AssignedDECName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "AS Con-FS DECReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ProductionType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DateofLenderSubmission";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC LenderComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ClosingComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserAssigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractorContactName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ClosingProgramSpecialistId";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CostCertReceived";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CostCertRecievedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CostCertCompletedMIMIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CostCertIssueDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC Is290Completed";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC 290CompletedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ActiveNCRE";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC NCREDueDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC NCREInitalBalance";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ClosingPackageRecievedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ProgramSpecialist";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AccountExecutiveClosing";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC Closer";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC InitialClosingDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedCloserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC CloserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedEnvName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC EnvReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedSurveyName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC SurveyReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedAppraiserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AppraiserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC OGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedBackupOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC BackupOGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedContractUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC ContractCloserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC AssignedDECName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "IC DECReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ProductionType";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DateofLenderSubmission";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC LenderComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ClosingComment";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserAssigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractorContactName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ClosingProgramSpecialistId";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CostCertReceived";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CostCertRecievedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CostCertCompletedMIMIssued";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CostCertIssueDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC Is290Completed";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC 290CompletedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ActiveNCRE";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC NCREDueDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC NCREInitalBalance";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ClosingPackageRecievedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ProgramSpecialist";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AccountExecutiveClosing";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC Closer";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC InitialClosingDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedCloserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC CloserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedEnvName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC EnvReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedSurveyName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC SurveyReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedAppraiserName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AppraiserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC OGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedBackupOGCName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC BackupOGCReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedContractUWName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC ContractCloserReassignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC AssignedDECName";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECAssignedDate";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECComments";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECStatus";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECCompletedOn";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECIsReassigned";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECReassignedFrom";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECReassignedBy";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FC DECReassignedDate";




            //cells["A1:JK1"].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["A1:RR1"].Borders.Color = Color.FromArgb(228, 242, 253);
            cells["A1:RR1"].Font.Color = Color.FromArgb(46, 110, 161);

        }

        public static void SetDataValuesForProdAdhocDataDownload(IList<Prod_AdhocDataModel> reportModel, IRange cells)
        {
            int col = 0;
            int row = 2;
            if (reportModel != null)
                foreach (var rec in reportModel)
                {


                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.FHANumber;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PropertyId;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ConstructionStageType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CurrentLoanType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ActivityType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.MortgageInsuranceType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.BorrowerType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.IsAddressCorrect;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectStreetaddress;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectCity;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectState;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectZipcode;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CongressionalDistrict;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LenderId;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LenderName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LenderContactName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LenderContactEmail;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LenderContactPhonenumber;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.ProposedLoanAmount;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProposedInterestRate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.SkilledNursingBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.SkilledNursingUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AssistedLivingBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AssistedLivingUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.BoardCareBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.BoardCareUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.MemoryCareBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.MemoryCareUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.IndependentBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.IndependentUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.OtherBeds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.OtherUnits;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CMSStarRating;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.MasterLeaseProposed;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioStatus;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.MidLargePortfolio;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.SmallPortfolio;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.Portfolio_Name;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.Portfolio_Number;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.Comments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.InsertFHAAssignedTo;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.InsertFHAAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.InsertFHAAssignedDate);
                    else
                        cells[row, col].Value = rec.InsertFHAAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.InsertFHACompletedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.InsertFHACompletedDate);
                    else
                        cells[row, col].Value = rec.InsertFHACompletedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.InsertFHAComments;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.InsertFHAIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.InsertFHAReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.InsertFHAReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.InsertFHAReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.InsertFHAReassignedDate);
                    else
                        cells[row, col].Value = rec.InsertFHAReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioAssignedTo;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.PortfolioAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.PortfolioAssignedDate);
                    else
                        cells[row, col].Value = rec.PortfolioAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.PortfolioCompletedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.PortfolioCompletedDate);
                    else
                        cells[row, col].Value = rec.PortfolioCompletedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioComments;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.PortfolioReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.PortfolioReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.PortfolioReassignedDate);
                    else
                        cells[row, col].Value = rec.PortfolioReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CreditReviewAssignedTo;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.CreditReviewAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.CreditReviewAssignedDate);
                    else
                        cells[row, col].Value = rec.CreditReviewAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.CreditReviewCompletedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.CreditReviewCompletedDate);
                    else
                        cells[row, col].Value = rec.CreditReviewCompletedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CreditreviewComments;

                    cells[row, ++col].NumberFormat = "@";
                    if (rec.CreditReviewDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.CreditReviewDate);
                    else
                        cells[row, col].Value = rec.CreditReviewDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CreditReviewIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CreditReviewReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CreditReviewReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.CreditReviewReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.CreditReviewReassignedDate);
                    else
                        cells[row, col].Value = rec.CreditReviewReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.RequestStatus;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CurrentFHANumber;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.RequestSubmitDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.RequestSubmitDate);
                    else
                        cells[row, col].Value = rec.RequestSubmitDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DenyComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CancelComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.One80WarningSent;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.Two70WarningSent;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.IsReadyforApplication;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.LIHTC;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.TaxExemptBonds;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.HOME;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.CDBG;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.Other;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.FHARequestCompletionDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.FHARequestCompletionDate);
                    else
                        cells[row, col].Value = rec.FHARequestCompletionDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.FHARequestNotificationType;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppProductionType;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDateofLenderSubmission != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDateofLenderSubmission);
                    else
                        cells[row, col].Value = rec.AppDateofLenderSubmission;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppLenderComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppLongTermHold;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDateOfHold != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDateOfHold);
                    else
                        cells[row, col].Value = rec.AppDateOfHold;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppReasonForHold;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppLoanCommitteApproved;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppLoanCommitteApprovedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppLoanCommitteApprovedDate);
                    else
                        cells[row, col].Value = rec.AppLoanCommitteApprovedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFirmCommitmentIssued;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFirmCommitmentIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFirmCommitmentIssuedDate);
                    else
                        cells[row, col].Value = rec.AppFirmCommitmentIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEarlyCommencementRequested;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppEarlyCommencementRequestedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppEarlyCommencementRequestedDate);
                    else
                        cells[row, col].Value = rec.AppEarlyCommencementRequestedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppEarlyCommencementIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppEarlyCommencementIssuedDate);
                    else
                        cells[row, col].Value = rec.AppEarlyCommencementIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppLDLIssued;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppLDLIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppLDLIssuedDate);
                    else
                        cells[row, col].Value = rec.AppLDLIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppLDLCompleteResponseReceived != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppLDLCompleteResponseReceived);
                    else
                        cells[row, col].Value = rec.AppLDLCompleteResponseReceived;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppMiscellaneousComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOtherQueueWorkComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAccountExecutiveMiscInfo;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppIremsCompleted;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppsCompleted;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppIremsPrintoutSaved;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppMirandaCompletedCMSListCheck;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppContractorContractPrice;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppContractorSecondaryAmtPaid;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppContractorAmountPaid;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvironmentalComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractorComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraisalComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOgcAddress;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOgcAddress;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDateFirmReviewBegan != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDateFirmReviewBegan);
                    else
                        cells[row, col].Value = rec.AppDateFirmReviewBegan;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDateEnteredQueue != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDateEnteredQueue);
                    else
                        cells[row, col].Value = rec.AppDateEnteredQueue;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppProductionWLMName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppAssignedUWDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppAssignedUWDate);
                    else
                        cells[row, col].Value = rec.AppAssignedUWDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppUWCompletedOn);
                    else
                        cells[row, col].Value = rec.AppUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppUWIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppUWReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppUWReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppUWReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppUWReassignedDate);
                    else
                        cells[row, col].Value = rec.AppUWReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedEnvName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppEnvAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppEnvAssignedDate);
                    else
                        cells[row, col].Value = rec.AppEnvAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppEnvCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppEnvCompletedOn);
                    else
                        cells[row, col].Value = rec.AppEnvCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppEnvReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppEnvReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppEnvReassignedDate);
                    else
                        cells[row, col].Value = rec.AppEnvReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedSurveyName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppSurveyAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppSurveyAssignedDate);
                    else
                        cells[row, col].Value = rec.AppSurveyAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppSurveyCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppSurveyCompletedOn);
                    else
                        cells[row, col].Value = rec.AppSurveyCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppSurveyReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppSurveyReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppSurveyReassignedDate);
                    else
                        cells[row, col].Value = rec.AppSurveyReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedAppraiserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppAppraiserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppAppraiserAssignedDate);
                    else
                        cells[row, col].Value = rec.AppAppraiserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraiserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraiserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppAppraiserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppAppraiserCompletedOn);
                    else
                        cells[row, col].Value = rec.AppAppraiserCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraiserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraiserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAppraiserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppAppraiserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppAppraiserReassignedDate);
                    else
                        cells[row, col].Value = rec.AppAppraiserReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.AppOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.AppOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.AppOGCReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedBackupOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppBackupOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppBackupOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.AppBackupOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppBackupOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppBackupOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.AppBackupOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppBackupOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppBackupOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppBackupOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.AppBackupOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedContractUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppContractUWAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppContractUWAssignedDate);
                    else
                        cells[row, col].Value = rec.AppContractUWAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppContractUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppContractUWCompletedOn);
                    else
                        cells[row, col].Value = rec.AppContractUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractUWIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractUWReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppContractUWReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppContractUWReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppContractUWReassignedDate);
                    else
                        cells[row, col].Value = rec.AppContractUWReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppAssignedDECName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDECAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDECAssignedDate);
                    else
                        cells[row, col].Value = rec.AppDECAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppDECComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppDECStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDECCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDECCompletedOn);
                    else
                        cells[row, col].Value = rec.AppDECCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppDECIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppDECReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppDECReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppDECReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppDECReassignedDate);
                    else
                        cells[row, col].Value = rec.AppDECReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSProductionType;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDateofLenderSubmission != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDateofLenderSubmission);
                    else
                        cells[row, col].Value = rec.AppFSDateofLenderSubmission;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSLenderComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSLongTermHold;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDateOfHold != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDateOfHold);
                    else
                        cells[row, col].Value = rec.AppFSDateOfHold;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSReasonForHold;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSLoanCommitteApproved;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSLoanCommitteApprovedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSLoanCommitteApprovedDate);
                    else
                        cells[row, col].Value = rec.AppFSLoanCommitteApprovedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSFirmCommitmentIssued;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSFirmCommitmentIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSFirmCommitmentIssuedDate);
                    else
                        cells[row, col].Value = rec.AppFSFirmCommitmentIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEarlyCommencementRequested;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSEarlyCommencementRequestedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSEarlyCommencementRequestedDate);
                    else
                        cells[row, col].Value = rec.AppFSEarlyCommencementRequestedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSEarlyCommencementIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSEarlyCommencementIssuedDate);
                    else
                        cells[row, col].Value = rec.AppFSEarlyCommencementIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSLDLIssued;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSLDLIssuedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSLDLIssuedDate);
                    else
                        cells[row, col].Value = rec.AppFSLDLIssuedDate;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSLDLCompleteResponseReceived != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSLDLCompleteResponseReceived);
                    else
                        cells[row, col].Value = rec.AppFSLDLCompleteResponseReceived;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSMiscellaneousComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOtherQueueWorkComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAccountExecutiveMiscInfo;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSIremsCompleted;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppsCompleted;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSIremsPrintoutSaved;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSMirandaCompletedCMSListCheck;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppFSContractorContractPrice;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppFSContractorSecondaryAmtPaid;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.AppFSContractorAmountPaid;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvironmentalComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractorComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraisalComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOgcAddress;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOgcAddress;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDateFirmReviewBegan != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDateFirmReviewBegan);
                    else
                        cells[row, col].Value = rec.AppFSDateFirmReviewBegan;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDateEnteredQueue != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDateEnteredQueue);
                    else
                        cells[row, col].Value = rec.AppFSDateEnteredQueue;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSProductionWLMName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSAssignedUWDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSAssignedUWDate);
                    else
                        cells[row, col].Value = rec.AppFSAssignedUWDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSUWCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSUWIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSUWReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSUWReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSUWReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSUWReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSUWReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedEnvName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSEnvAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSEnvAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSEnvAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSEnvCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSEnvCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSEnvCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSEnvReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSEnvReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSEnvReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSEnvReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedSurveyName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSSurveyAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSSurveyAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSSurveyAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSSurveyCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSSurveyCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSSurveyCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSSurveyReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSSurveyReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSSurveyReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSSurveyReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedAppraiserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSAppraiserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSAppraiserAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSAppraiserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraiserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraiserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSAppraiserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSAppraiserCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSAppraiserCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraiserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraiserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAppraiserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSAppraiserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSAppraiserReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSAppraiserReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSOGCReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedBackupOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSBackupOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSBackupOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSBackupOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSBackupOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSBackupOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSBackupOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSBackupOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSBackupOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSBackupOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSBackupOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedContractUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSContractUWAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSContractUWAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSContractUWAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSContractUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSContractUWCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSContractUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractUWIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractUWReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSContractUWReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSContractUWReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSContractUWReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSContractUWReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSAssignedDECName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDECAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDECAssignedDate);
                    else
                        cells[row, col].Value = rec.AppFSDECAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSDECComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSDECStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDECCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDECCompletedOn);
                    else
                        cells[row, col].Value = rec.AppFSDECCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSDECIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSDECReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.AppFSDECReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.AppFSDECReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.AppFSDECReassignedDate);
                    else
                        cells[row, col].Value = rec.AppFSDECReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCProductionType;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCDateofLenderSubmission != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCDateofLenderSubmission);
                    else
                        cells[row, col].Value = rec.DCDateofLenderSubmission;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCLenderComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCClosingComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractCloserAssigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractorContactName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCClosingProgramSpecialistId;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCostCertReceived;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCostCertRecievedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCostCertCompletedMIMIssued;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCostCertIssueDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCTwo290Completed;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCTwo90CompletedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCActiveNCRE;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCNCREDueDate;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.DCNCREInitalBalance;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCClosingPackageRecievedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCProgramSpecialist;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAccountExecutiveClosing;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloser;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCInitialClosingDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedCloserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCCloserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCCloserAssignedDate);
                    else
                        cells[row, col].Value = rec.DCCloserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCCloserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCCloserCompletedOn);
                    else
                        cells[row, col].Value = rec.DCCloserCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCCloserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCCloserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCCloserReassignedDate);
                    else
                        cells[row, col].Value = rec.DCCloserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedEnvName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCEnvAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCEnvAssignedDate);
                    else
                        cells[row, col].Value = rec.DCEnvAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCEnvComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCEnvStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCEnvCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCEnvCompletedOn);
                    else
                        cells[row, col].Value = rec.DCEnvCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCEnvIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCEnvReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCEnvReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCEnvReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCEnvReassignedDate);
                    else
                        cells[row, col].Value = rec.DCEnvReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedSurveyName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCSurveyAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCSurveyAssignedDate);
                    else
                        cells[row, col].Value = rec.DCSurveyAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCSurveyComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCSurveyStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCSurveyCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCSurveyCompletedOn);
                    else
                        cells[row, col].Value = rec.DCSurveyCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCSurveyIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCSurveyReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCSurveyReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCSurveyReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCSurveyReassignedDate);
                    else
                        cells[row, col].Value = rec.DCSurveyReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedAppraiserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCAppraiserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCAppraiserAssignedDate);
                    else
                        cells[row, col].Value = rec.DCAppraiserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAppraiserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAppraiserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCAppraiserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCAppraiserCompletedOn);
                    else
                        cells[row, col].Value = rec.DCAppraiserCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAppraiserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAppraiserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAppraiserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCAppraiserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCAppraiserReassignedDate);
                    else
                        cells[row, col].Value = rec.DCAppraiserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.DCOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.DCOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.DCOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedBackupOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCBackupOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCBackupOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.DCBackupOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCBackupOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCBackupOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCBackupOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCBackupOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.DCBackupOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCBackupOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCBackupOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCBackupOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCBackupOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCBackupOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.DCBackupOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedContractUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCContractUWAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCContractUWAssignedDate);
                    else
                        cells[row, col].Value = rec.DCContractUWAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCContractUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCContractUWCompletedOn);
                    else
                        cells[row, col].Value = rec.DCContractUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractCloserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractCloserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCContractCloserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCContractCloserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCContractCloserReassignedDate);
                    else
                        cells[row, col].Value = rec.DCContractCloserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCAssignedDECName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCDECAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCDECAssignedDate);
                    else
                        cells[row, col].Value = rec.DCDECAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCDECComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCDECStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCDECCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCDECCompletedOn);
                    else
                        cells[row, col].Value = rec.DCDECCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCDECIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCDECReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.DCDECReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.DCDECReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.DCDECReassignedDate);
                    else
                        cells[row, col].Value = rec.DCDECReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECProductionType;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECDateofLenderSubmission != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECDateofLenderSubmission);
                    else
                        cells[row, col].Value = rec.ECDateofLenderSubmission;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECLenderComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECClosingComment;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractCloserAssigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractorContactName;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECClosingProgramSpecialistId;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCostCertReceived;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCostCertRecievedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCostCertCompletedMIMIssued;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCostCertIssueDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECTwo290Completed;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECTwo90CompletedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECActiveNCRE;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECNCREDueDate;
                    cells[row, ++col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.ECNCREInitalBalance;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECClosingPackageRecievedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECProgramSpecialist;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAccountExecutiveClosing;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloser;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECInitialClosingDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedCloserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECCloserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECCloserAssignedDate);
                    else
                        cells[row, col].Value = rec.ECCloserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECCloserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECCloserCompletedOn);
                    else
                        cells[row, col].Value = rec.ECCloserCompletedOn;
                    cells[row, ++col].NumberFormat = "@";

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECCloserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECCloserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECCloserReassignedDate);
                    else
                        cells[row, col].Value = rec.ECCloserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedEnvName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECEnvAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECEnvAssignedDate);
                    else
                        cells[row, col].Value = rec.ECEnvAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECEnvComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECEnvStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECEnvCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECEnvCompletedOn);
                    else
                        cells[row, col].Value = rec.ECEnvCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECEnvIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECEnvReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECEnvReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECEnvReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECEnvReassignedDate);
                    else
                        cells[row, col].Value = rec.ECEnvReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedSurveyName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECSurveyAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECSurveyAssignedDate);
                    else
                        cells[row, col].Value = rec.ECSurveyAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECSurveyComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECSurveyStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECSurveyCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECSurveyCompletedOn);
                    else
                        cells[row, col].Value = rec.ECSurveyCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECSurveyIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECSurveyReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECSurveyReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECSurveyReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECSurveyReassignedDate);
                    else
                        cells[row, col].Value = rec.ECSurveyReassignedDate;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedAppraiserName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECAppraiserAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECAppraiserAssignedDate);
                    else
                        cells[row, col].Value = rec.ECAppraiserAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAppraiserComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAppraiserStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECAppraiserCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECAppraiserCompletedOn);
                    else
                        cells[row, col].Value = rec.ECAppraiserCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAppraiserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAppraiserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAppraiserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECAppraiserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECAppraiserReassignedDate);
                    else
                        cells[row, col].Value = rec.ECAppraiserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.ECOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.ECOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.ECOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedBackupOGCName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECBackupOGCAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECBackupOGCAssignedDate);
                    else
                        cells[row, col].Value = rec.ECBackupOGCAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECBackupOGCComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECBackupOGCStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECBackupOGCCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECBackupOGCCompletedOn);
                    else
                        cells[row, col].Value = rec.ECBackupOGCCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECBackupOGCIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECBackupOGCReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECBackupOGCReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECBackupOGCReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECBackupOGCReassignedDate);
                    else
                        cells[row, col].Value = rec.ECBackupOGCReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedContractUWName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECContractUWAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECContractUWAssignedDate);
                    else
                        cells[row, col].Value = rec.ECContractUWAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractUWComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractUWStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECContractUWCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECContractUWCompletedOn);
                    else
                        cells[row, col].Value = rec.ECContractUWCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractCloserIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractCloserReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECContractCloserReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECContractCloserReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECContractCloserReassignedDate);
                    else
                        cells[row, col].Value = rec.ECContractCloserReassignedDate;


                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECAssignedDECName;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECDECAssignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECDECAssignedDate);
                    else
                        cells[row, col].Value = rec.ECDECAssignedDate;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECDECComments;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECDECStatus;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECDECCompletedOn != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECDECCompletedOn);
                    else
                        cells[row, col].Value = rec.ECDECCompletedOn;

                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECDECIsReassigned;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECDECReassignedFrom;
                    cells[row, ++col].NumberFormat = "@";
                    cells[row, col].Value = rec.ECDECReassignedBy;
                    cells[row, ++col].NumberFormat = "@";
                    if (rec.ECDECReassignedDate != null)
                        cells[row, col].Value = String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", rec.ECDECReassignedDate);
                    else
                        cells[row, col].Value = rec.ECDECReassignedDate;



                    row++;
                    col = 0;
                }
            cells["A1:RR" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);
            cells["A1:CD" + row].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["CE1:GI" + row].Interior.Color = Color.FromArgb(253, 227, 227);
            cells["GJ1:KN" + row].Interior.Color = Color.FromArgb(227, 253, 242);
            cells["KO1:OC" + row].Interior.Color = Color.FromArgb(235, 227, 253);
            cells["OD1:RR" + row].Interior.Color = Color.FromArgb(242, 140, 140);
        }

        public static void GetHeaderForLoanCommitteeScheduler(IRange cells)
        {
            var row = 0;
            var col = 0;
            cells[row, col].HorizontalAlignment = HAlign.Center;
            cells[row, col].WrapText = true;
            cells[row, col].EntireRow.HorizontalAlignment = HAlign.Center;
            cells[row, col].EntireRow.RowHeight = 30;
            cells[row, col].EntireRow.Font.Bold = true;
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Sequence Id ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Day ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Loan Committe Date ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "LC Decision ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Type ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Project Name ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "FHA Number ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Mortgage Amount ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "UW ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "WLM ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "OGC Attorney ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "OHP Appraisal Reviewer ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Lender ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "A7 Presentation? ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "2 Stage Submittal ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Non-profit project ";
            cells[row, col].ColumnWidth = 25;
            cells[row, col++].Value = "Comments";
            cells["A1:Q1"].Interior.Color = Color.FromArgb(228, 242, 253);
            cells["A1:Q1"].Borders.Color = Color.FromArgb(228, 242, 253);
            cells["A1:Q1"].Font.Color = Color.FromArgb(46, 110, 161);

        }

        public static void SetDataValuesForLoanCommitteeScheduler(IList<Prod_LoanCommitteeViewModel> reportModel, IRange cells)
        {
            int col = 0;
            int row = 1;
            if (reportModel != null)
                foreach (var rec in reportModel)
                {
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.SequenceId;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.LoanCommitteDate.Value.ToString("dddd");
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.LoanCommitteDate;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    if (rec.LCDecision != null)
                    {
                        cells[row, col].Value = EnumType.GetEnumDescription(EnumType.Parse<LoanCommitteeDecision>(rec.LCDecision.ToString()));
                        //cells[row, col].Value = rec.LCRecommendation == 1 ? "Approve" : "Reject";
                        //switch (rec.LCDecision)
                        //{
                        //    case (int)LoanCommitteeDecision.Approve:
                        //        
                        //        break;
                        //    case (int)LoanCommitteeDecision.Reject:
                        //        cells[row, col].Value = EnumType.GetEnumDescription(EnumType.Parse<LoanCommitteeDecision>(rec.LCDecision.ToString())); 
                        //        break;
                        //    case (int)LoanCommitteeDecision.Withdrawn:
                        //        cells[row, col].Value = EnumType.GetEnumDescription(EnumType.Parse<LoanCommitteeDecision>(rec.LCDecision.ToString()));
                        //        break;
                        //    case (int)LoanCommitteeDecision.RequestAdditionalInformation:
                        //        cells[row, col].Value = EnumType.GetEnumDescription(EnumType.Parse<LoanCommitteeDecision>(LoanCommitteeDecision.RequestAdditionalInformation));
                        //        break;
                        //}
                    }
                    else
                    {
                        cells[row, col].Value = "N/A";
                    }
                    
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectTypeName;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.ProjectName;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.FHANumber;
                    ++col;
                    cells[row, col].NumberFormat = "#,###0.00";
                    cells[row, col].Value = rec.LoanAmount;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = !string.IsNullOrEmpty(rec.Underwriter)?rec.Underwriter:"N/A";
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = !string.IsNullOrEmpty(rec.WLM)?rec.WLM:"N/A";
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = !string.IsNullOrEmpty(rec.Ogc)?rec.Ogc:"N/A";
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = !string.IsNullOrEmpty(rec.Appraiser)?rec.Appraiser:"N/A";
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.Lender;
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    if (rec.A7Presentation != null)
                    {
                        cells[row, col].Value = rec.A7Presentation == 1 ? "Yes" : "No";
                    }
                    else
                    {
                        cells[row, col].Value = "N/A";
                    }
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    if (rec.TwoStageSubmittal != null)
                    {
                        cells[row, col].Value = rec.TwoStageSubmittal == 1 ? "Yes" : "No";
                    }
                    else
                    {
                        cells[row, col].Value = "N/A";
                    }
                    
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = rec.NonProfit == "Non-Profit"?"Yes":"No";
                    ++col;
                    cells[row, col].NumberFormat = "@";
                    cells[row, col].Value = !string.IsNullOrEmpty(rec.LCComments)?rec.LCComments:"N/A";


                    ++row;
                    col = 0;
                }
            cells["A1:Q" + row].Borders.Color = Color.FromArgb(255, 0, 0, 0);

        }

        public static void SetValuesForProdLCFilters(LoanCommitteeFiltersModel filters, IRange cells)
        {
            if (filters != null)
            {
                var row = 0;
                var col = 0;
                cells[row, col].ColumnWidth = 30;
                cells[row, col].EntireRow.RowHeight = 30;
                cells[row, col].EntireColumn.Font.Bold = true;

                cells[row, col].Value = "Loan Committee Date";
                cells[row, ++col].ColumnWidth = 15;
                cells[row, col].Value = filters.LoanCommitteeDate;

                cells[row, ++col].ColumnWidth = 15;
                cells[row, ++col].ColumnWidth = 30;

                cells[row, col].EntireColumn.Font.Bold = true;
                cells[row, col].Value = "Underwriter";
                cells[row, ++col].ColumnWidth = 15;
                cells[row, col].Value = filters.Underwriter;

                col -= 4;
                cells[++row, col].Value = "Workload Manager";
                cells[row, ++col].Value = filters.WorkloadManager;
                ++col;
                cells[row, ++col].Value = "Loan Committee Decision";
                cells[row, ++col].Value = filters.SequenceId;

                col -= 4;
                cells[++row, col].Value = "Day";
                cells[row, ++col].Value = !string.IsNullOrEmpty(filters.Day)?DateTime.Parse(filters.Day).ToString("dddd"):"";
                ++col;
                cells[row, ++col].Value = "Project Type";
                cells[row, ++col].Value = filters.ProjectTypeName;

                col -= 4;
                cells[++row, col].Value = "OGC Attorney";
                cells[row, ++col].Value = filters.OGCAttorney;
                ++col;
                cells[row, ++col].Value = "OHP Appraisal Reviewer";
                cells[row, ++col].Value = filters.OHPAppraisalReviewer;

                col -= 4;
                cells[++row, col].Value = "Lender";
                cells[row, ++col].Value = filters.Lender;
              



                cells["A1:B5"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                cells["D1:E4"].Borders.Color = Color.FromArgb(255, 0, 0, 0);
                cells["B1:B5"].HorizontalAlignment = HAlign.Right;
                cells["E1:E4"].HorizontalAlignment = HAlign.Right;
            }

        }
    }

}
