﻿using System;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    // The model does not maintain HttpPostedFileBase data when the form is re-presented.
    // To handle this, the data is stored in tempData, so that when the form is submitted
    // finally, the file information is preserved.
    // This may be an initial submission (nothing cached, but new data)
    // May be re-submit (use cached, no data)
    // May be re-submit with differing data (replace cache and use new data)
    // Using a passed reference for TempData due to scoping.
    public static class TaskHelper
    {
        /// <summary>
        /// Saves file data into temporary variable.
        /// </summary>
        /// <param name="contextParam">The context parameter.</param>
        public static void CacheFileData(TempDataDictionary TempData, HttpPostedFileBase UploadFile)
        {
            if (TempData["UploadFile"] != null)
            {
                // Ensure Streams do not get left to garbage collection, the data can persist.
                ((HttpPostedFileBase)TempData["UploadFile"]).InputStream.Flush();
                ((HttpPostedFileBase)TempData["UploadFile"]).InputStream.Close();
                ((HttpPostedFileBase)TempData["UploadFile"]).InputStream.Dispose();
            }
            TempData["UploadFile"] = UploadFile;
        }

        public static void CacheFileData(TempDataDictionary TempData, HttpPostedFileBase file, string FileName)
        {
            if (TempData[FileName] != null)
            {
                // Ensure Streams do not get left to garbage collection, the data can persist.
                ((HttpPostedFileBase)TempData[FileName]).InputStream.Flush();
                ((HttpPostedFileBase)TempData[FileName]).InputStream.Close();
                ((HttpPostedFileBase)TempData[FileName]).InputStream.Dispose();
            }
            TempData[FileName] = file;
        }
        public static string GetUntilOrEmpty(string text, string stopAt)
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                if (text.ToLower().Contains(stopAt))
                {
                    int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                    if (charLocation > 0)
                    {
                        return text.Substring(0, charLocation);
                    }
                }

            }

            return String.Empty;
        }
    }
}
