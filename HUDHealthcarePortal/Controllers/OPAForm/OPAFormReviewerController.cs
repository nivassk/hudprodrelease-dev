﻿using BusinessService;
using BusinessService.Interfaces;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using Newtonsoft.Json;

namespace HUDHealthcarePortal.Controllers.OPAForm
{
    public class OPAFormReviewerController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private ITaskManager taskManager;
        private IReviewFileCommentManager reviewFileCommentManager;
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFileManager;
        private IBackgroundJobMgr backgroundJobManager;

        public OPAFormReviewerController()
            : this(new ProjectActionFormManager(), new ReviewFileStatusManager(), new TaskManager(), new ReviewFileCommentManager(),
            new ParentChildTaskManager(), new RequestAdditionalInfoFilesManager(), new BackgroundJobMgr())
        {

        }

        private OPAFormReviewerController(IProjectActionFormManager _projectActionFormManager, IReviewFileStatusManager _reviewFileStatusManager, 
            ITaskManager _taskManager, IReviewFileCommentManager _reviewFileCommentManager, IParentChildTaskManager _parentChildTaskManager,
            IRequestAdditionalInfoFileManager _requestAdditionalInfoFileManager, IBackgroundJobMgr _backgroundJobManager)
        {
            projectActionFormManager = _projectActionFormManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            taskManager = _taskManager;
            reviewFileCommentManager = _reviewFileCommentManager;
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFileManager = _requestAdditionalInfoFileManager;
            backgroundJobManager = _backgroundJobManager;
        }



        public ActionResult Index()
        {
            return View();
        }

        public class colModel
        {
            public string name { get; set; }
            public string index { get; set; }
            public string label { get; set; }
            public string editable { get; set; }
            public string frozen { get; set; }
            public string width { get; set; }
            public string type { get; set; }
            public string cellattr { get; set; }
            public string formatter { get; set; }
            public string formatoptions { get; set; }
        }
        public JsonResult GetGridColumn()
        {
            //Get Guid taskInstanceId from view 

            List<colModel> colModel1 = new List<colModel>();

            colModel model = new colModel();
            model.name = "actionTaken";
            model.index = "actionTaken";
            model.label = "Action Type";
            model.editable = "false";
            model.width = "80";
            model.frozen = "true";
            colModel1.Add(model);

            model = new colModel();
            model.name = "name";
            model.index = "name";
            model.label = "User Name";
            model.editable = "false";
            colModel1.Add(model);

            model = new colModel();
            model.name = "userRole";
            model.index = "userRole";
            model.label = "User Role";
            model.editable = "false";
            colModel1.Add(model);

            model = new colModel();
            model.name = "comment";
            model.index = "comment";
            model.label = "User comment";
            model.editable = "false";
            colModel1.Add(model);

            model = new colModel();
            model.name = "fileId";
            model.index = "fileId";
            model.label = "File Name";
            model.editable = "false";
            model.formatter = "FileDownloadFormatter";
            model.cellattr = "FileLinkAttribute";
            colModel1.Add(model);

            model = new colModel();
            model.name = "fileId";
            model.index = "fileId";
            model.label = "File Name";
            model.editable = "false";
            model.formatter = "FileDownloadFormatter";
            model.cellattr = "FileLinkAttribute";
            colModel1.Add(model);

            model = new colModel();
            model.name = "fileSize";
            model.index = "fileSize";
            model.label = "File Size";
            model.editable = "false";
            colModel1.Add(model);

            model = new colModel();
            model.name = "uploadDate";
            model.index = "uploadDate";
            model.label = "File uploadDate";
            model.editable = "false";
            model.formatter = "date";
            colModel1.Add(model);

            model = new colModel();
            model.name = "submitDate";
            model.index = "submitDate";
            model.label = "File submitDate";
            model.editable = "false";
            model.formatter = "date";
            colModel1.Add(model);


            //Bind Dynamic column.

            //Get List of reviewer list for the Task instance ID 

            // For each user in list 
           // { 

            //model = new colModel();
            //model.name = reviewerUserId+"_isReq;
            //model.index =  reviewerUserId+"_isReq
            //model.label = reviewer user name;
            //colModel1.Add(model);

            //model = new colModel();
            //model.name = reviewerUserId+"_isReq;
            //model.index =  reviewerUserId+"_isReq
            //model.label = reviewer user name Req INfo;
            //colModel1.Add(model);

            //model = new colModel();
            //model.name = reviewerUserId+"_cmt;
            //model.index =  reviewerUserId+"_cmt
            //model.label = reviewer user name Comment;
            //colModel1.Add(model);

            //model = new colModel();
            //model.name = reviewerUserId+"_apv";
            //model.index =  reviewerUserId+"_apv"
            //model.label = reviewer user name approve;
            //colModel1.Add(model);

             // }

            var jsonData = new
            {
                    colModels = colModel1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetReviewerGridContent(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
 
            List<OPAHistoryViewModel> reviewerTask = new List<OPAHistoryViewModel>();

            reviewerTask = GetOpaHistory(taskInstanceId);

            foreach (OPAHistoryViewModel obj in reviewerTask)
            {
                obj.isReqAddInfo = false;
                obj.Approve = 0;
				obj.isApprove = false;
            }

            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = reviewerTask,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
     
        public string UpdateFileTask(string editData)
        {
            JObject json = JObject.Parse(editData);
            var taskInstanceId = json["taskInstanceId"];
            var fileUpdates = json["fileUpdates"];
            var nfrComment = json["nfrComment"];
            var IsNewfile = json["IsNewfile"];
            var reviewStatusList = new List<ReviewFileStatusModel>();
            var reviewCommentList = new List<ReviewFileCommentModel>();
            var fileRenameList = new List<Tuple<Guid, string,string>>();
            var newFileRequestModel = new NewFileRequestModel();
            var IsRAI = false;
            var isApproved = false;
            //retrieve status and comments list and save in respective tables
            foreach (JObject content in fileUpdates.Children<JObject>())
            {
                var fileStatusModel = new ReviewFileStatusModel();
                var fileCommentModel = new ReviewFileCommentModel();
                fileCommentModel.ReviewFileCommentId = Guid.NewGuid();
                fileCommentModel.CreatedOn = DateTime.UtcNow;
                
                var isFileRenamed = false;
                Tuple<Guid,string,string> fileRenameTuple;
                var taskFileId = new Guid();
                var fileName = "";
                var fileExt = "";
                foreach (JProperty prop in content.Properties())
                {
                    
                    if (prop.Name == "fileId")
                    {
                        taskFileId = taskManager.GetTaskFileById(int.Parse(prop.Value.ToString())).TaskFileId;
                        fileStatusModel = reviewFileStatusManager.GetReviewFileStatusByTaskFileId(taskFileId);
                        fileCommentModel.FileTaskId = taskFileId;
                    } else if (prop.Name == "ReqAdditionalInfo" && prop.Value.ToString() == "True" && fileStatusModel.Status != 4)
                    {
                        //create child req
                        fileStatusModel.Status = 4;
                        IsRAI = true;
                    }
                    else if (prop.Name == "Comments")
                    {
                        fileCommentModel.Comment = prop.Value.ToString();
                    }
                    else if (prop.Name == "Approve" && prop.Value.ToString() == "True" && fileStatusModel.Status != 5)
                    {
                        fileStatusModel.Status = 5;
                        isApproved = true;
                    }
                    else if (prop.Name == "isFileRenamed" && prop.Value.ToString() == "True")
                    {
                        isFileRenamed = true;
                    }
                    if (prop.Name == "fileRename")
                    {
                        fileName = prop.Value.ToString();
                    }
                    if (prop.Name == "fileExt")
                    {
                        fileExt = prop.Value.ToString();
                    }
                }
                if (isFileRenamed)
                {
                    fileRenameTuple = new Tuple<Guid, string, string>(taskFileId,fileName,fileExt);
                    fileRenameList.Add(fileRenameTuple);
                }
                fileStatusModel.ModifiedOn = DateTime.UtcNow;
                fileStatusModel.ModifiedBy = fileStatusModel.ReviewerUserId;
                fileCommentModel.CreatedBy = fileStatusModel.ReviewerUserId;
                reviewStatusList.Add(fileStatusModel);
                if (IsRAI == true || isApproved == true)
                {
                    reviewCommentList.Add(fileCommentModel);
                }
            }
            

            //Creating child task for Request Additional info files
            var parentTask = taskManager.GetTasksByTaskInstanceId((Guid) taskInstanceId).ToList();
            var taskList = new List<TaskModel>();
            var childTask = new TaskModel();
            if (IsRAI || (bool)(IsNewfile)==true)
            {
                childTask.TaskInstanceId = Guid.NewGuid();
                childTask.SequenceId = 0;
                childTask.AssignedBy = UserPrincipal.Current.UserName;
                childTask.AssignedTo = parentTask[0].AssignedBy;
                childTask.StartTime = DateTime.UtcNow;
                childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                childTask.IsReAssigned = false;
                childTask.Concurrency = parentTask[0].Concurrency;
                childTask.DataStore1 = parentTask[0].DataStore1;
                //adding fhanumber and pagetype      
                childTask.FHANumber = parentTask[0].FHANumber;
                childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var childTaskOpenStatusModel = new TaskOpenStatusModel();
                childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                    typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(childTask);
            }
            
            var requestAdditionalChildTask = new ParentChildTaskModel();
            requestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
            requestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
            requestAdditionalChildTask.ParentTaskInstanceId = parentTask[0].TaskInstanceId;
            requestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
            requestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();

            var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();
            foreach (var item in reviewStatusList)
            {
                if (item.Status == 4)
                {
                    var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                    requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                    requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                    requestAdditionalInfoFiles.TaskFileId = item.TaskFileId;
                    requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                }
            }
            
            if (!string.IsNullOrEmpty((string) nfrComment))
            {
                newFileRequestModel.ChildTaskInstanceId = childTask.TaskInstanceId;
                newFileRequestModel.Comments = (string) nfrComment;
                newFileRequestModel.RequestedBy = UserPrincipal.Current.UserId;
                newFileRequestModel.RequestedOn = DateTime.UtcNow;
            }

            try
            {
                
                
                if (!string.IsNullOrEmpty((string)nfrComment)) projectActionFormManager.SaveNewFileRequest(newFileRequestModel);
                if (IsRAI || (bool)(IsNewfile) == true)
                {
                    taskManager.SaveTask(taskList, new List<TaskFileModel>());
                    parentChildTaskManager.AddParentChildTask(requestAdditionalChildTask);
                    requestAdditionalInfoFileManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                    
                }
                if(fileRenameList.Any()) taskManager.RenameFiles(fileRenameList);
                
                reviewFileStatusManager.UpdateReviewFileStatus(reviewStatusList);           
                reviewFileCommentManager.SaveReviewFileComment(reviewCommentList);
                var model = projectActionFormManager.GetOPAByTaskInstanceId(parentTask[0].TaskInstanceId);
                if (childTask.AssignedTo != null && childTask.AssignedBy!=null)
                {
                    if (childTask.TaskStepId==16)
                    {
                        model.RequestStatus = 7;
                    }
                    if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(new EmailManager(), childTask.AssignedTo,
                        childTask.AssignedBy, model))
                    {
                    }
                }

                if (IsRAI) return "Request";
                else return "Success";

            }
            catch (Exception)
            {
                
                throw;
            }
            return "Failure";
        }


        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            return projectActionFormManager.GetOpaHistory(taskInstanceId);

        }


    }
}
