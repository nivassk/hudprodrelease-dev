﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Services.Description;
using BusinessService.Interfaces;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using System.Web.Mvc;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Model;
using Model;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using MvcSiteMapProvider.Collections.Specialized;

namespace HUDHealthcarePortal.Controllers
{
    public class PortfolioController : Controller
    {

        IPortfolioManager portfolioMgr;
        
        public PortfolioController() : this(new PortfolioManager())
        {

        }

        public PortfolioController(IPortfolioManager portfolioManager)
        {
            portfolioMgr = portfolioManager;
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult Index(int? page, string sort, string sortDir, string quarterToken)
        {
            // display 4 years of data
            var model = new PortfolioSummaryModel { YearQrtModel = new YearQuarterModel(4, quarterToken) };
            string yearQrtToken = string.IsNullOrEmpty(quarterToken)
                ? model.YearQrtModel.SelectedQrtToken
                : quarterToken;
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolios = portfolioMgr.GetPortfolios(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "Portfolio_Name" : sort,
                 sortOrder, yearQrtToken);
            model.PortfolioModel = portfolios;
            ViewBag.YearQuarterToken = yearQrtToken;
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "page", page }, { "sort", sort }, { "sortdir", sortDir }, { "quarterToken", quarterToken } };
            Session[SessionHelper.SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT] = routeValuesDict;
            ViewBag.ExcelExportAction = "ExportPortfolioSummary";
            ViewBag.ExcelActionParam = new {page = page, sort = sort, sortDir = sortDir, quarter = quarterToken};
            ViewBag.PrintAction = "PrintPortfolioSummary";
            return View("PortfolioSummary2", model);
        }

        [HttpGet]
        public JsonResult GetPortfolios(string quarterToken)
        {
            var portfolios = portfolioMgr.GetPortfolios(quarterToken);
            return Json(portfolios, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPortfoliosPageSort(int? page, string sort, string sortDir, string quarterToken)
        {

            var yearQrtModel = new YearQuarterModel(4, quarterToken);
            string yearQrtToken = string.IsNullOrEmpty(quarterToken)
                ? yearQrtModel.SelectedQrtToken
                : quarterToken;
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolios = portfolioMgr.GetPortfolios(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "Portfolio_Name" : sort,
                sortOrder, yearQrtToken);
            ViewBag.YearQuarterToken = yearQrtToken;

            // stores route values, will be retrieved when detail link is clicked and attach route values to parent breadcrumb
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "page", page }, { "sort", sort }, { "sortdir", sortDir }, { "quarterToken", quarterToken } };
            Session[SessionHelper.SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT] = routeValuesDict;
            ViewBag.ExcelExportAction = "ExportPortfolioSummary";
            ViewBag.ExcelActionParam = new { page = page, sort = sort, sortDir = sortDir, quarter = quarterToken };
            ViewBag.PrintAction = "PrintPortfolioSummary";
            return PartialView("_PortfolioSummary", portfolios);
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Portfolio Detail", ParentKey = "Portfolio_Number")]
        //[SiteMapTitle("Portfolio_Name", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetPortfolioDetail(int? page, string sort, string sortDir, string portfolioNumber, string quarterToken)
        {
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolioDetail = portfolioMgr.GetPortfolioDetail(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, portfolioNumber, quarterToken);
            ISiteMapNode parentNode = null;
            if (SiteMaps.Current.CurrentNode != null) parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT);
            if (parentNode != null) {
                if (!parentNode.RouteValues.Keys.Contains("page"))
                    parentNode.RouteValues["page"] = routeValues["page"];
                if (!parentNode.RouteValues.Keys.Contains("sort"))
                    parentNode.RouteValues["sort"] = routeValues["sort"];
                if (!parentNode.RouteValues.Keys.Contains("sortdir"))
                    parentNode.RouteValues["sortdir"] = routeValues["sortdir"];
                if (!parentNode.RouteValues.Keys.Contains("quarterToken"))
                    parentNode.RouteValues["quarterToken"] = quarterToken;
            }
            if (portfolioDetail.Entities.Any())
                SiteMaps.Current.CurrentNode.Title = portfolioDetail.Entities.ToList()[0].Portfolio_Name;
            ViewBag.ExcelExportAction = "ExportPortfolioDetail";
            ViewBag.ExcelActionParam = new { page = page, sort = sort, sortDir = sortDir, portfolioNum = portfolioNumber, quarter = quarterToken };
            ViewBag.PrintAction = "PrintPortfolioDetail";
            return View("PortfolioSummaryDetail", portfolioDetail);
        }
    }
}
