﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using BusinessService.Interfaces;
using Core.Utilities;
using Elmah;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.BusinessService;
using MvcSiteMapProvider;
using HUDHealthcarePortal.Core.Utilities;
using Microsoft.Ajax.Utilities;
using MvcSiteMapProvider.Web.Mvc.Filters;

namespace HUDHealthCarePortal.Controllers
{
    [Authorize]
    public class ExcelUploadView_Controller : Controller
    {
        private readonly IUploadDataManager uploadDataMgr;
        private readonly IAccountManager accountMgr;

        public ExcelUploadView_Controller() : this(new UploadDataManager(), new AccountManager())
        {

        }

        public ExcelUploadView_Controller(IUploadDataManager uploadDataManager, IAccountManager accoutManager)
        {
            uploadDataMgr = uploadDataManager;
            accountMgr = accoutManager;
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Project Detail", ParentKey = "category", PreservedRouteParameters = "id")]
        [SiteMapTitle("ProjectName", Target = AttributeTarget.CurrentNode)]
        public ActionResult ProjectDetailCategory(int id, DBSource source, string contextParam, string quarter)
        {
            try
            {
                var model = uploadDataMgr.GetProjectDetailByID(id, source);
                var commentMgr = new CommentManager();
                //var comments = commentMgr.GetCommentsByProject(model.FHANumber); 
                var comments = commentMgr.GetCommentsByLdiID(model.LDI_ID);
                model.Comments = comments.ToList();
                ControllerHelper.PopulateParentNodeRouteValues(contextParam);
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT);

                model.ReportingPeriod = DateHelper.GetSelectedQuarterText(quarter);
                TempData["ReportingPeriod"] = quarter;
                Session["ReportingPeriod"] = quarter;

                return View("ProjectDetail", model);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error load project detail with category: {0}", e.InnerException), e.InnerException);
            }
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Project Detail", ParentKey = "OneUploadId", PreservedRouteParameters = "id")]
        [SiteMapTitle("ProjectName", Target = AttributeTarget.CurrentNode)]
        public ActionResult ProjectDetailTime(int id, DBSource source, string contextParam)
        {
            try
            {
                var model = uploadDataMgr.GetProjectDetailByID(id, source);
                var commentMgr = new CommentManager();
                //var comments = commentMgr.GetCommentsByProject(model.FHANumber);
                var comments = commentMgr.GetCommentsByLdiID(model.LDI_ID);
                model.Comments = comments.ToList();

                ControllerHelper.PopulateParentNodeRouteValues(contextParam);
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT);

                return View("ProjectDetail", model);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error load project detail with time: {0}", e.InnerException), e.InnerException);
            }
        }



        [HttpGet]
        [MvcSiteMapNode(Title = "Project Detail", ParentKey = "ProjectsId", PreservedRouteParameters = "id")]
        [SiteMapTitle("ProjectName", Target = AttributeTarget.CurrentNode)]
        public ActionResult ProjectDetailProject(int id, DBSource source, string contextParam)
        {
            try
            {
                var model = uploadDataMgr.GetProjectDetailByID(id, source);
                var commentMgr = new CommentManager();
                //var comments = commentMgr.GetCommentsByProject(model.FHANumber);
                var comments = commentMgr.GetCommentsByLdiID(model.LDI_ID);
                model.Comments = comments.ToList();

                ControllerHelper.PopulateParentNodeRouteValues(contextParam);
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT);

                return View("ProjectDetail", model);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error load project detail with project: {0}", e.InnerException), e.InnerException);
            }
        }

        [HttpGet]
        public ActionResult ViewExcelUpload(int? page, string sort, string sortDir)
        {
            ViewBag.ExcelExportAction = "ExportViewUploadAll";
            ViewBag.PrintAction = "PrintUploadAll";
            ViewBag.ResetAction = "ViewExcelUpload";
            ViewBag.ExcelActionParam = null;
            var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
            var uploadedData = uploadDataMgr
                .GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).ToList();

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT] = routeValuesDict;
            return View("ViewExcelUploadDetail", uploadedData);
        }

        [HttpGet]
        public ActionResult ViewExcelUploadPageSort(int? page, string sort = "MonthsInPeriod_Disp,PeriodEnding_Disp", string sortDir = "DESC")
        {
            ViewBag.ExcelExportAction = "ExportViewUploadAll";
            ViewBag.PrintAction = "PrintUploadAll";
            ViewBag.ResetAction = "ViewExcelUploadPageSort";
            ViewBag.ExcelActionParam = null;

              

            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            int pageSize = 10;
            // page size hardcode here, later put in web.config
            //var userRoles = Roles.Provider.GetRolesForUser(UserPrincipal.Current.UserName);
            var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);

            // TA- Temporary
            //var data =  uploadDataMgr.GetProjectInfoReport(UserPrincipal.Current.UserName);

            var uploadedData = uploadDataMgr
                .GetUploadDataByRoleWithPageSort(UserPrincipal.Current.UserId, userRoles,
                    string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, pageSize, page ?? 1);

            // stores route values, will be retrieved when detail link is clicked and attach route values to parent breadcrumb
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT] = routeValuesDict;
            return View("ViewExcelUploadDetailPageSort", uploadedData);
        }

        [MvcSiteMapNode(Title = "Upload Detail", ParentKey = "DetailId", Key = "OneUploadId",
            PreservedRouteParameters = "OneUploadId")]
        public ActionResult ViewExcelUploadDetail(DateTime timestamp, int? page, int userId, string sort = "MonthsInPeriod_Disp,PeriodEnding_Disp", string sortDir = "DESC")
        {
            ViewBag.ExcelExportAction = "ExportViewUploadByTime";
            ViewBag.PrintAction = "PrintUploadByTime";
            ViewBag.ResetAction = "ViewExcelUploadDetail";
            ViewBag.ExcelActionParam = new {timestamp = timestamp, userId = userId};
            ViewBag.Context = "timestamp";
            int pageSize = 10;
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            var uploadedData = uploadDataMgr.GetUploadedDataByTimePageSort(timestamp, userId,
                string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, pageSize, page ?? 1);

            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT] = routeValuesDict;

            return View("ViewExcelUploadDetailPageSort", uploadedData);
        }

        public ActionResult ViewExcelUploadsByCategoryBridge(string category, string quarter)
        {
            return RedirectToAction("ViewExcelUploadsByCategoryPageSort", new {category = category, quarter = quarter});
        }


        [MvcSiteMapNode(Title = "Project Report", PreservedRouteParameters = "category,quarter", ParentKey = "RiskSum",
            Key = "category")]
        public ActionResult ViewExcelUploadsByCategoryPageSort(string category, int? page, string sort, string sortDir,
            string quarter)
        {
            ViewBag.ExcelExportAction = "ExportViewUploadByCategory";
            ViewBag.PrintAction = "PrintUploadByCategory";
            ViewBag.ResetAction = "ViewExcelUploadsByCategoryBridge";
            ViewBag.Context = "category";

            if (TempData["ReportingPeriod"] != null)
            {
                quarter = TempData["ReportingPeriod"].ToString();
            }
            else if (TempData["ReportingPeriod"] == null && !string.IsNullOrEmpty(quarter))
            {
                TempData["ReportingPeriod"] = quarter;
                Session["ReportingPeriod"] = quarter;
            }
            else if (TempData["ReportingPeriod"] == null && string.IsNullOrEmpty(quarter))
            {
                TempData["ReportingPeriod"] = Session["ReportingPeriod"];
                quarter = Session["ReportingPeriod"].ToString();
                Session["ReportingPeriod"] = null;
            }

            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;

            if (string.IsNullOrEmpty(quarter) || string.Equals(quarter, "0") || string.Equals(quarter, "-1"))
            {
                quarter = DateHelper.GetCurrentQuarter();
            }
            if (!string.IsNullOrEmpty(category) && category.Contains("quarter"))
            {
                category = category.Split(',')[0];
            }
            ViewBag.ExcelActionParam = new {category = category, quarter = quarter};

            var result = DateHelper.GetValuesFromSelectedQuarter(quarter);
            var uploadedData = uploadDataMgr.GetUploadLiveDataByCategoryPageSort(category,
                string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, pageSize, page ?? 1, result.Item1,
                result.Item2);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT] = routeValuesDict;

            return View("ViewExcelUploadDetailPageSort", uploadedData);
        }


        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "AccountExecutive,Administrator,Attorney,BackupAccountManager,HUDAdmin,HUDDirector,LenderAccountRepresentative,LenderAccountManager,LenderAdmin,Servicer,SuperUser,WorkflowManager"
            )]
        public ActionResult ReportExcelUpload(int? page, string sort, string sortDir)
        {
            ViewBag.ExcelExportAction = "ExportUserUploadReport";
            ViewBag.PrintAction = "PrintUserUploadReport";
            ViewBag.ExcelActionParam = null;
            ViewBag.EmptyMessage = "You have not uploaded any file yet.";
            var model = uploadDataMgr.GetDataUploadSummaryByUserId(UserPrincipal.Current.UserId).ToList();

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_REPORT_LINK_DICT] = routeValuesDict;
            return View(model);
        }

        [HttpGet]
        public JsonResult SearchByFhaNumber(string searchText, int maxResults, string callingContext,
            string callingParam)
        {
            if (string.IsNullOrEmpty(callingContext))
                return null;
            var dataSet = new List<DerivedUploadData>();
            Dictionary<string, string> dictionary = null;
            if (!string.IsNullOrEmpty(callingParam))
            {
                dictionary = ControllerHelper.CreateDictionaryFromActionParameters(callingParam);
            }
            var quarter = string.Empty;
            switch (callingContext)
            {
                case "ExportViewUploadAll":
                    var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
                    dataSet = uploadDataMgr.GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).ToList();
                    break;
                case "ExportViewUploadByTime":
                    var dateInserted = DateTime.Parse(dictionary["timestamp"]);
                    dataSet = uploadDataMgr.GetUploadDataByTimeForSearch(dateInserted).ToList();
                    ViewBag.Context = "timestamp";
                    TempData["timestamp"] = dateInserted;
                    break;
                case "ExportViewUploadByCategory":
                    if (callingParam.Contains("quarter"))
                    {
                        quarter = dictionary["quarter"];
                        dataSet =
                            uploadDataMgr.GetUploadDataByCategoryAndQuarterBySearch(dictionary["category"], quarter)
                                .ToList();
                    }
                    ViewBag.Context = "category";
                    break;
            }
            TempData["ReportingPeriod"] = quarter;

            var results = dataSet.OrderBy(p => p.FHANumber).Select(p => new { p.FHANumber, p.ProjectName }).Distinct();

            if (!searchText.IsNullOrWhiteSpace())
            {
                results = results.Where(p => p.FHANumber.StartsWith(searchText));
            }

            if (maxResults != -1)
            {
                results = results.Take(maxResults);
            }

            var json = Json(results, JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpGet]
        public ActionResult GetUploadDataByFHANumber(string fhaNumber, string callingContext, string callingParam)
        {
            if (string.IsNullOrEmpty(callingContext))
                return null;
            var dataSet = new List<DerivedUploadData>();
            Dictionary<string, string> dictionary = null;
            if (!string.IsNullOrEmpty(callingParam))
            {
                dictionary = ControllerHelper.CreateDictionaryFromActionParameters(callingParam);
            }
            var quarter = string.Empty;
            switch (callingContext)
            {
                case "ExportViewUploadAll":
                    var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
                    dataSet = uploadDataMgr.GetUploadDataBySearch(UserPrincipal.Current.UserId,
                        userRoles).ToList();
                    ViewBag.ExcelActionParam = new {fhaNumber};
                    ViewBag.Context = "all";
                    break;
                case "ExportViewUploadByTime":
                    var dateInserted = DateTime.Parse(dictionary["timestamp"]);
                    dataSet = uploadDataMgr.GetUploadDataByTimeForSearch(dateInserted).ToList();
                    ViewBag.Context = "timestamp";
                    ViewBag.ExcelActionParam = new {timestamp = dateInserted, fhaNumber};
                    break;
                case "ExportViewUploadByCategory":

                    var category = dictionary["category"];
                    if (callingParam.Contains("quarter"))
                    {
                        quarter = dictionary["quarter"];
                        dataSet = uploadDataMgr.GetUploadDataByCategoryAndQuarterBySearch(category, quarter).ToList();
                        ViewBag.ExcelActionParam = new {fhaNumber, category, quarter};
                    }
                    else
                    {
                        ViewBag.ExcelActionParam = new {fhaNumber, category};
                    }
                    ViewBag.Context = "category";
                    break;
            }
            ViewBag.ExcelExportAction = "ExportViewUploadBySearch";
            ViewBag.PrintAction = "PrintUploadByCategoryBySearch";
            TempData["ReportingPeriod"] = quarter;

            dataSet = dataSet.Where(p => p.FHANumber == fhaNumber).OrderBy(p => p.ProjectName).ToList();
            dataSet.ForEach(i => i.MonthsInPeriod_Disp = Convert.ToInt32(i.MonthsInPeriod));
            dataSet.ForEach(i => i.PeriodEnding_Disp = Convert.ToDateTime(i.PeriodEnding));


       

            var list=dataSet.OrderByDescending(x =>  x.MonthsInPeriod_Disp).ThenByDescending(x => x.PeriodEnding_Disp);

            
            var results = new PaginateSortModel<DerivedUploadData>
            {
                Entities = list,
                PageSize = 10,
                TotalRows = dataSet.Count
            };

            return PartialView("_ViewExcelUploadPageSort", results);
        }


        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "AccountExecutive,Administrator,Attorney,BackupAccountManager,HUDAdmin,HUDDirector,LenderAccountRepresentative,LenderAccountManager,InternalSpecialOptionUser,LenderAdmin,Servicer,SuperUser,WorkflowManager,ProductionUser"
            )]
        public ActionResult UploadStatusReport(int? page, string sort, string sortDir, string strLenderId,
            string strSelectedQtr)
        {                     
            if (TempData["PasswordExpires"] != null && (bool)TempData["PasswordExpires"])
                PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true, "../Account/ChangePassword");
            // get lender id, if is lender, restrict view to lender's company
            int lenderId = -1;
            if (RoleManager.IsCurrentUserLenderRoles() && UserPrincipal.Current.LenderId.HasValue)
                lenderId = UserPrincipal.Current.LenderId.Value;
            ViewBag.ExcelExportAction = "ExportUploadStatusByLenderId";
            ViewBag.PrintAction = "PrintUploadStatusByLender";
            ViewBag.MissingProjectParam = "";
            var model = new UploadStatusModelUI();

            var quarters = uploadDataMgr.GetQuartersList();

            var currentQtr = uploadDataMgr.GetLatestQuarter("value");
            if (TempData["SelectedQuarter"] != null)
            {
                currentQtr = TempData["SelectedQuarter"].ToString();
                Session["SelectedQuarter"] = currentQtr;
            }
            else if (Session["SelectedQuarter"] != null)
            {
                currentQtr = (string) Session["SelectedQuarter"];
            }

            if (TempData["LenderIdFilter"] != null)
            {
                lenderId = int.Parse(TempData["LenderIdFilter"].ToString());
            }
            if (Session["LenderIdFilter"] != null)
            {
                TempData["LenderIdFilter"] = Session["LenderIdFilter"];
                lenderId = int.Parse(TempData["LenderIdFilter"].ToString());
            }

            if (string.IsNullOrEmpty(currentQtr))
            {
                currentQtr = uploadDataMgr.GetLatestQuarter("value");
                TempData["SelectedQuarter"] = currentQtr;
                Session["SelectedQuarter"] = currentQtr;
            }
            var allLenders = uploadDataMgr.GetUploadStatusDetailByQuarter(currentQtr, -1).ToList();
            var statuses =lenderId==-1?allLenders : allLenders.Where(m=>m.LenderID==lenderId).ToList();
               // uploadDataMgr.GetUploadStatusDetailByQuarter(currentQtr, lenderId).ToList();

            FilterUploadStatusResultsBasedOnRole(currentQtr, model, statuses);
            model.SelectedQuarter = currentQtr;
            model.ReportingPeriod = DateHelper.GetSelectedQuarterText(currentQtr);
            ViewBag.ExcelActionParam = new { strLenderId = lenderId.ToString(), selectedQtr = currentQtr };

           
            model.AllLenders.Add(new SelectListItem() { Text = "Select Lender", Value = "-1", Selected = true });
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                foreach (var item in allLenders)
                {
                    model.AllLenders.Add(new SelectListItem()
                    {
                        Text = item.LenderName,
                        Value = item.LenderID.ToString(),
                        Selected = false
                    });
                }
            } 
            else if (RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                IEnumerable<int> lendersForAeOrWlm =
                    accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName, currentQtr);

                foreach (var item in allLenders)
                {
                    if ((lendersForAeOrWlm != null && lendersForAeOrWlm.ToList().Contains(item.LenderID)))
                        model.AllLenders.Add(new SelectListItem()
                        {
                            Text = item.LenderName,
                            Value = item.LenderID.ToString(),
                            Selected = false
                        });
                }
            }
            else if (RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
            {
                IEnumerable<int> lendersForAeOrWlm =
                    accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName, currentQtr);

                foreach (var item in allLenders)
                {
                    if ((lendersForAeOrWlm != null && lendersForAeOrWlm.ToList().Contains(item.LenderID)))
                        model.AllLenders.Add(new SelectListItem()
                        {
                            Text = item.LenderName,
                            Value = item.LenderID.ToString(),
                            Selected = false
                        });
                }
            }

            else if (RoleManager.IsCurrentSpecialOptionUser())
            {
                if (UserPrincipal.Current.LenderId != null)
                {
                    var specialOptionUserLenders = accountMgr.GetLenderDetail((int)UserPrincipal.Current.LenderId).ToList();
                    foreach (var item in specialOptionUserLenders)
                    {
                        model.AllLenders.Add(new SelectListItem()
                        {
                            Text = item.Value,
                            Value = item.Key.ToString(CultureInfo.InvariantCulture),
                            Selected = false
                        });
                    }
                }
            }else if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                if (UserPrincipal.Current.LenderId != null)
                {
                    foreach (var uploadStatusViewModel in allLenders)
                    {
                        if (uploadStatusViewModel.LenderID == UserPrincipal.Current.LenderId)
                        {
                            model.AllLenders.Add(new SelectListItem()
                            {
                                Text = uploadStatusViewModel.LenderName,
                                Value = uploadStatusViewModel.LenderID.ToString(CultureInfo.InvariantCulture),
                                Selected = false
                            });
                        }
                    }
                }
            }

            model.AllQuarters.Add(new SelectListItem()
            {
                Text = uploadDataMgr.GetLatestQuarter("key"),
                Value = uploadDataMgr.GetLatestQuarter("value"),
                Selected = true
            });
            var qList = quarters.Select(kvp => kvp.Key).ToList();
            var minYear = Convert.ToInt32(qList.OrderBy(k => Convert.ToInt32(k.Substring(3))).First().Substring(3));
            for (int i = 2016; i <= minYear; i++)
            {
                if (!qList.Contains("Q4 " + i)) qList.Add("Q4 " + i);
                if (!qList.Contains("Q3 " + i)) qList.Add("Q3 " + i);
                if (!qList.Contains("Q2 " + i)) qList.Add("Q2 " + i);
                if (!qList.Contains("Q1 " + i)) qList.Add("Q1 " + i);
            }
            var all = qList.OrderByDescending(x => Convert.ToInt32(x.Substring(3))).ThenByDescending(x => x.Substring(0, 2));
            Dictionary<string, string> quarters2 = new Dictionary<string, string>();
            foreach (string k in all)
            {
                if (k.StartsWith("Q1")) quarters2.Add(k, "3/" + k.Substring(3));
                if (k.StartsWith("Q2")) quarters2.Add(k, "6/" + k.Substring(3));
                if (k.StartsWith("Q3")) quarters2.Add(k, "9/" + k.Substring(3));
                if (k.StartsWith("Q4")) quarters2.Add(k, "12/" + k.Substring(3));
            }
            foreach (var quarter in quarters2)
            {
                model.AllQuarters.Add(new SelectListItem() {Text = quarter.Key, Value = quarter.Value});
            }

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir},
                {"strLenderId", lenderId.ToString()},
                {"strSelectedQtr", currentQtr}
            };
            Session[SessionHelper.SESSION_KEY_UPLOAD_STATUS_LINK_DICT] = routeValuesDict;
            
            return View(model);
        }

        [HttpGet]
        public ActionResult UploadStatusReportByQuarter(string strSelectedQtr, string strLenderId, int? page,
            string sort, string sortDir)
        {
            int lenderId = -1;
            if (RoleManager.IsCurrentUserLenderRoles() && UserPrincipal.Current.LenderId.HasValue)
                lenderId = UserPrincipal.Current.LenderId.Value;

            ViewBag.ExcelExportAction = "ExportUploadStatusByLenderId";
            ViewBag.PrintAction = "PrintUploadStatusByLender";
            var model = new UploadStatusModelUI();
            if (!string.IsNullOrEmpty(strLenderId))
            {
                lenderId = int.Parse(strLenderId);
            }
            List<UploadStatusViewModel> statuses = new List<UploadStatusViewModel>();
            if (string.IsNullOrEmpty(strSelectedQtr) || string.Equals(strSelectedQtr, "0") ||
                string.Equals(strSelectedQtr, "-1"))
            {
                strSelectedQtr = DateHelper.GetCurrentQuarter();
            }
            statuses =
                    uploadDataMgr.GetUploadStatusDetailByQuarter(strSelectedQtr, lenderId).ToList();
            FilterUploadStatusResultsBasedOnRole(strSelectedQtr, model, statuses);
            ViewBag.ExcelActionParam = new { strLenderId = lenderId.ToString(), selectedQtr = strSelectedQtr };
            TempData["SelectedQuarter"] = strSelectedQtr;
            TempData["LenderId"] = lenderId;
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir},
                {"strLenderId", lenderId.ToString()},
                {"strSelectedQtr", strSelectedQtr}
            };
            Session[SessionHelper.SESSION_KEY_UPLOAD_STATUS_LINK_DICT] = routeValuesDict;
            return PartialView("_UploadStatusReport", model);
        }  

        /// <summary>
        /// Filters the upload status results based on role.
        /// </summary>
        /// <param name="strSelectedQtr">The string selected QTR.</param>
        /// <param name="model">The model.</param>
        /// <param name="statuses">The statuses.</param>
        private void FilterUploadStatusResultsBasedOnRole(string strSelectedQtr, UploadStatusModelUI model, List<UploadStatusViewModel> statuses)
        {
            // if user is AE or WLM, filter out inaccessible lenders from all lenders
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            // if user is Internal Special Option Userfilter out inaccessible lenders from all lenders
            bool isUserISU = RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName);
            IEnumerable<int> lendersForAeOrWlm = null;
            List<UploadStatusViewModel> statusesFiltered = new List<UploadStatusViewModel>();
            // get all lenders belong to the user
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName)
                || RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                model.UploadStatuses = statuses;
            }
            else
            {
                if (isUserAeOrWlm || isUserISU)
                    lendersForAeOrWlm = accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName, strSelectedQtr);
                foreach (var item in statuses)
                {
                    if (!isUserAeOrWlm || (lendersForAeOrWlm != null && lendersForAeOrWlm.ToList().Contains(item.LenderID)))
                        statusesFiltered.Add(item);
                }
                model.UploadStatuses = statusesFiltered;
            }
        }

        [HttpGet]
        public ActionResult UploadStatusReportFilter(string strLenderId, string strSelectedQtr)
        {
            int lenderId = int.Parse(strLenderId);
            ViewBag.ExcelExportAction = "ExportUploadStatusByLenderId";
            ViewBag.PrintAction = "PrintUploadStatusByLender";

            List<UploadStatusViewModel> statuses = new List<UploadStatusViewModel>();
            if (string.IsNullOrEmpty(strSelectedQtr) || string.Equals(strSelectedQtr, "0") ||
                string.Equals(strSelectedQtr, "-1"))
            {
                strSelectedQtr = DateHelper.GetCurrentQuarter();
            }
            var model = new UploadStatusModelUI();
            statuses =
                    uploadDataMgr.GetUploadStatusDetailByQuarter(strSelectedQtr, lenderId).ToList();
            FilterUploadStatusResultsBasedOnRole(strSelectedQtr, model, statuses);
            ViewBag.ExcelActionParam = new { strLenderId = strLenderId, selectedQtr = strSelectedQtr };
            TempData["SelectedQuarter"] = strSelectedQtr;
            TempData["LenderIdFilter"] = strLenderId;
            Session["LenderIdFilter"] = strLenderId;
            if (int.Parse(strLenderId) == -1)
            {
                Session["LenderIdFilter"] = null;
            }
            return PartialView("_UploadStatusReport", model);
        }

        [HttpGet]
       // [MvcSiteMapNode(Title = "Upload Status Detail", PreservedRouteParameters = "id", ParentKey = "Status",  Key = "StatusDetailId")]
         public ActionResult UploadStatusDetail(string id, string strSelectedQtr)
        {
            int lenderId = int.Parse(id);
            ViewBag.ExcelExportAction = "ExportUploadStatusDetailByLenderId";
            ViewBag.PrintAction = "PrintUploadStatusDetailByLender";
            ViewBag.ExcelActionParam = new { strlenderId = id, selectedQtr = strSelectedQtr };
            var model = uploadDataMgr.GetUploadStatusDetailByLenderId(lenderId, strSelectedQtr).ToList();
            if (TempData["SelectedQuarter"] == null)
            {
                TempData["SelectedQuarter"] = strSelectedQtr;
            }
            Session["SelectedQuarter"] = strSelectedQtr;
            if (TempData["LenderId"] == null)
            {
                TempData["LenderId"] = int.Parse(id);
            }
            Session["LenderId"] = int.Parse(id);
            TempData["Missing"] = model.Count(p => p.Received == false);
            TempData["Received"] = model.Count(p => p.Received == true);
            
            ControllerHelper.PopulateParentNodeRouteValuesForUploadStatus();
            return View(model);
        }

        [HttpGet]
        //[MvcSiteMapNode(Title = "Missing Project Detail", PreservedRouteParameters = "id", ParentKey = "Status",
        //    Key = "MissingProjectDetailId")]
        public ActionResult MissingProjectDetail(string id, string selectedQtr)
        {
            var lenderid = int.Parse(id);
            ViewBag.ExcelExportAction = "ExportMissingProjectDetailByLenderId";
            ViewBag.PrintAction = "PrintMissingProjectDetailByLender";
            ViewBag.ExcelActionParam = new { lenderId = id, quarter = selectedQtr };
            var model =
                uploadDataMgr.GetMissingProjectsForLender(lenderid, selectedQtr).OrderBy(p => p.ProjectName).ToList();
            if (model.Count > 0)
            {
                model[0].ReportingPeriod = DateHelper.GetSelectedQuarterText(selectedQtr);
            }

            ViewBag.MissingProjectParam = "MissingProjectDetailForLender";
            if (TempData["SelectedQuarter"] == null)
            {
                TempData["SelectedQuarter"] = selectedQtr;
            }
            Session["SelectedQuarter"] = selectedQtr;

            if (TempData["LenderId"] == null)
            {
                TempData["LenderId"] = int.Parse(id);
            }
            Session["LenderId"] = int.Parse(id);
            ControllerHelper.PopulateParentNodeRouteValuesForUploadStatus();
            return View("_MissingProjectGrid", model);
        }
        public JsonResult GetCurrentQuarterUploadStatus()
        {
            var quarters = uploadDataMgr.GetQuartersList();

            var currentQtr = uploadDataMgr.GetLatestQuarter("value");

            var currentUploadStatus = uploadDataMgr.GetUploadStatusDetailByQuarter(currentQtr, -1).ToList().Take(7);

            return Json(currentUploadStatus, JsonRequestBehavior.AllowGet);

        }

    }
}
