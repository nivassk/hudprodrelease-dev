﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Model;
using System;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.BusinessService;

namespace HUDHealthCarePortal.Controllers
{
    public class HomeController : Controller
    {
        private ILookupManager lookupMgr;

        public HomeController() : 
            this(new LookupManager())
        {
        }

        public HomeController(ILookupManager lookupManager)
        {
            lookupMgr = lookupManager;      
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Offline()
        {
            return View();
        }

        public ActionResult Glossary()
        {
            return View("Glossary");
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            ViewBag.Message = "This page is under construction";

            return View("_UnderConstruction");
        }

        public ActionResult Error(string id)
        {
            ViewData["ErrorId"] = id;
            return View("Error");
        }

        public ActionResult AccessDenied()
        {
            return View("AccessDenied");
        }

        public ActionResult Landing()
        {
            ViewBag.Message = "Landing Page";
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SendRequestEmail(RequestModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.Message = string.Empty;
                    MailMessage mail = new MailMessage();
                    
                    var emailMgr = new EmailManager();
                    SmtpClient smtpServer = new SmtpClient();
                    mail.From = new MailAddress(model.ClientEmail);
                    //mail.To.Add(emailMgr.C3SupportEmail);
                    mail.To.Add(emailMgr.HCPContactUSEmail); 
                    mail.Subject = model.Subject;
                    //mail.Body = model.ClientMessage;
                    //427
                    mail.Body = model.ClientMessage + "\n"+"Contact Phone Number:" +model.ClientPhone;
                    mail.IsBodyHtml = true;

                    var emailToSave = new EmailModel();
                    emailToSave.EmailTo = TextUtils.TokenDelimitedText(mail.To.ToList().Select(p => p.Address), ";");
                    emailToSave.EmailFrom = mail.From.Address;
                    emailToSave.Subject = mail.Subject;
                    emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                    //427
                    emailToSave.EmailPhone = model.ClientPhone;

                    emailToSave.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd=="RQST").EmailTypeId;
                    emailMgr.SaveEmail(emailToSave);

                    smtpServer.Send(mail);

                    model = new RequestModel();
                    ViewBag.Message = "Your request is sent.";
                    //return RedirectToAction("Contact", "Home");
                }
                catch (Exception e)
                {
                    ViewBag.Message = "Error occurred while sending request.";
                    return View("Contact");
                }
            }
            return View("Contact", model);
        }

        public ActionResult RedirectToTraining()
        {  // Old URL
           // return Redirect("http://portal.hud.gov/hudportal/HUD?src=/federal_housing_administration/healthcare_facilities/residential_care/Operator_Financial_Stmt/232_HCPTraining");

            return Redirect("http://portal.hud.gov/hudportal/HUD?src=/federal_housing_administration/healthcare_facilities/residential_care/232_healthcare_portal/232HCP_Training");
        }

        public ActionResult RedirectToDocuments()
        {
            return Redirect("https://www.hud.gov/federal_housing_administration/healthcare_facilities/residential_care");
        }

		/// <summary>
		/// session expire detection
		/// </summary>
		/// <param name="imageData"></param>
		/// <returns></returns>
		[HttpPost]
		public JsonResult SessionExpiredStatus(string imageData)
		{
			var getSes = Session["SessionId"];
			bool sesStatus = true;
			if (getSes == null)
				sesStatus = false;
			var jsonData = new
			{
				sessionStatus = sesStatus,
			};
			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

	}
}
