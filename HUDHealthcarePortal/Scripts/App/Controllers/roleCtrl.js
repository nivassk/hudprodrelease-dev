﻿multiSelectModule.controller('roleCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.user = {
            roles: []
        };

        $scope.userTypeChanged = function (e) {
            var userTypeId = $(e).find('option:selected').val();
            var multiSelect = $("#dvMultiSelect");
            if (userTypeId == "1" || userTypeId == "2") {
                $scope.userTypeId = userTypeId;
                multiSelectSvc.getRolesByCategory(userTypeId).success(function (data) {
                    $scope.roles = data;
                    $scope.available = data;
                    // register new user, clear selected roles when user type changes
                    $scope.user.roles = [];
                });
                multiSelect.show();
            }
            else {
                multiSelect.hide();
            }
        };

        $scope.saveSelectedRoles = function () {
            multiSelectSvc.saveSelectedRoles({ RolesToUpdate: $scope.user.roles, RoleSourceType: $scope.userTypeId })
                .success(function (data, status, headers, config) {
                    if(data.Success)
                        $(location).attr('href', '/Account/Register');
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.createUserRoles = function () {
            multiSelectSvc.createUserRoles({ RolesToUpdate: $scope.user.roles, RoleSourceType: $scope.userTypeId })
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        debugger;
                        if (data.IsOperator) $(location).attr('href', '/Account/RegisterOperator1');
                        else if (data.IsServicer) $(location).attr('href', '/Account/RegisterServicer');
                        else if (data.IsWorkloadManager) $(location).attr('href', '/Account/RegisterWorkloadManager');
                        else if( data.IsAccountExecutive) $(location).attr('href', '/Account/RegisterAccountExecutive') ;
                        else if(data.IsAdmin) $(location).attr('href', '/Account/Register') ;
                        else if(data.IsProdUser ) $(location).attr('href', '/Reviewer/RegisterUserWithTitle?Role=ProductionUser');
                        else if(data.IsReviewer) $(location).attr('href', '/Reviewer/RegisterUserWithTitle?Role=Reviewer');
                        else if (data.IsInternalSpecialOptionUser) $(location).attr('href', '/Account/RegisterServicerIsou');
                        else if (data.IsInternalAccountExecutive) $(location).attr('href', '/Account/RegisterServicer');
                        else if  ( data.IsContractInspector) $(location).attr('href', '/Account/RegisterInspectionContractor') ;                
                        else  $(location).attr('href', '/Account/Register');
                } else {
                    alert(data.Message);
                }
            }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);


multiSelectModule.controller('assignLendersCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.selectedLenders = {
            lenders: []
        };

        load();
        function load() {
            multiSelectSvc.getAllLenders()
                .success(function (data, status, headers, config) {
                    $scope.Lenders = data;
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
        
        $scope.createLenderServicerLinks = function () { 
            multiSelectSvc.saveSelectedLenders($scope.selectedLenders.lenders)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        //data.IsServicer ? $(location).attr('href', '/Account/RegisterServicer') : $(location).attr('href', '/Account/Register');
                        $(location).attr('href', '/Account/Register');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.saveSelectedLendersForLAR = function () {
            multiSelectSvc.saveSelectedLenders($scope.selectedLenders.lenders)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        $(location).attr('href', '/Account/RegisterOperator2');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);

multiSelectModule.controller('assignFHAsCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.selectedFHAs = {
            fhas: []
        };

        load();
        function load() {
            multiSelectSvc.getFHAsFromLenders()
                .success(function (data, status, headers, config) {
                    $scope.FHAs = data.Fhas;
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.createUserLARLinks = function () {
            multiSelectSvc.createUserLARLinks($scope.selectedFHAs.fhas)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        $(location).attr('href', '/Account/Register');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);

multiSelectModule.controller('mngOarFHAsCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.selectedFHAs = {
            fhas: []
        };

        load();
        function load() {
            multiSelectSvc.GetSelectedAndAvailableFhasForOar()
                .success(function (data, status, headers, config) {
                    $scope.FHAs = data.Fhas;
                    $scope.selectedFHAs.fhas = data.existingFHAs;
            }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.saveUpdatedOarFhas = function () {
            multiSelectSvc.saveUpdatedOarFhas($scope.selectedFHAs.fhas)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        $(location).attr('href', '/Account/ForwardToEditUserProfileView');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);

multiSelectModule.controller('assignFHAsSpecialOptionCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.selectedFHAs = {
            fhas: []
        };

        $scope.WLMChanged = function (e) {
            //alert("Here");
            debugger;
            var wlmId = $(e).find('option:selected').val();
            $scope.wlmId = wlmId;
            var multiSelect = $("#dvMultiSelect");
            if (wlmId == "-999") {
                alert("select wlm")
                //load();
            }
            else {
                multiSelectSvc.getFHAsForWLM(wlmId)
              .success(function (data, status, headers, config) {
                  //debugger;
                  $scope.FHAs = data.Fhas;
              }).error(function (data, status, headers, config) {
                  alert(data.Message);
              });

            }

        };

        load();
        function load() {
            multiSelectSvc.getFHAsFromLamOrBamOrAdmin()
                .success(function (data, status, headers, config) {
                    $scope.FHAs = data.Fhas;
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.createUserLARLinks = function (param) {
            debugger;
            var temp = $scope.wlmId;
            if (temp != null) {
                multiSelectSvc.createUserLARLinksForWLM($scope.selectedFHAs.fhas, temp)
                    .success(function (data, status, headers, config) {
                        if (data.Success) {
                            //$(location).attr('href', '/Account/Register?wlmid=' + $scope.wlmId);
                            $(location).attr('href', '/Account/Register', { wlmid: temp });
                        }
                    }).error(function (data, status, headers, config) {
                        alert(data.Message);
                    });
            }
            else {
                multiSelectSvc.createUserLARLinks($scope.selectedFHAs.fhas)
                    .success(function (data, status, headers, config) {
                        if (data.Success) {
                            $(location).attr('href', '/Account/Register');
                        }
                    }).error(function (data, status, headers, config) {
                        alert(data.Message);
                    });
            }
        };

    }
]);

multiSelectModule.controller('assignFHAsInspectionContractorCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {       
        $scope.selectedFHAs = {
            fhas: []
        };    
        if (typeof multiSelectSvc.getFhasReadyForApplication() == "undefined") {
            alert("Please Clear your browser's cache and try again!");
        }
        else{
            loadAppInfo();
        }
       
        function loadAppInfo() {        
            multiSelectSvc.getFhasReadyForApplication()
                .success(function (data, status, headers, config) {
                    $scope.FHAs = data.Fhas;
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });

        };

        $scope.saveSelectedFhasForIC = function () {
            multiSelectSvc.saveSelectedFhasForIC($scope.selectedFHAs.fhas)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        $(location).attr('href', '/Account/Register');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);

multiSelectModule.controller('mngInspectorFHAsCtrl', ['$scope', '$http', 'multiSelectSvc',
    function ($scope, $http, multiSelectSvc) {
        $scope.selectedFHAs = {
            fhas: []
        };
        if (typeof multiSelectSvc.getSelectedAndAvailableFhasForIC() == "undefined") {
            alert("Please Clear your browser's cache and try again!");
        }
        else {
            load();
    }
       
        function load() {
            multiSelectSvc.getSelectedAndAvailableFhasForIC()
                .success(function (data, status, headers, config) {
                    $scope.FHAs = data.Fhas;
                    $scope.selectedFHAs.fhas = data.existingFHAs;
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };

        $scope.saveUpdatedICFhas = function () {           
            multiSelectSvc.saveUpdatedICFhas($scope.selectedFHAs.fhas)
                .success(function (data, status, headers, config) {
                    if (data.Success) {
                        $(location).attr('href', '/Account/ForwardToEditUserProfileView');
                    }
                }).error(function (data, status, headers, config) {
                    alert(data.Message);
                });
        };
    }
]);
