﻿portfolioSummaryModule.factory('portfolioSummaryModuleSvc', ['$http', function ($http) {
    return {
        getPortfolioSummaries: function (qrtToken) {
            return $http.get('/Portfolio/GetPortfolios', { params: { quarterToken: qrtToken } });
        },

        getPortfolioDetail: function (portfolioID, qrtToken) {
            return $http.get('/Portfolio/GetPortfolioDetail', { params: { portfolioNumber: portfolioID, quarterToken: qrtToken } });
        }
    }
}]);